#!/bin/bash

LIST=(FB15N256 FB15N256_DM FB15N512 FB15N512_DM FB15N1024 FB15N1024_DM)
for NAME in ${LIST[@]}
do

	if [ $NAME == FB15N1024 ]
	then
		SNAP=(176 240 344 554)
	else
		SNAP=(176 240 344 554 1200)
	fi

	for i in ${SNAP[@]}
	do

		INDIR=/bulk1/feldmann/data/FIREbox/analysis/$NAME/snapdir_$i
		OUTDIR=/bulk1/feldmann/data/FIREbox/analysis/AHF_run1/$NAME/snapdir_$i
		cp $INDIR/*input $INDIR/*out $INDIR/*log $INDIR/*parameter $INDIR/*halos $INDIR/*particles $INDIR/*profiles $INDIR/*substructure $OUTDIR	
	done

done
