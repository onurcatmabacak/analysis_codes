import numpy as np
import glob

def selected_ids(halo_path):

	halo_files = sorted(glob.glob(halo_path + '/*/*.AHF_halos'))
	ids, mhalo, fMhires = np.genfromtxt(halo_files[-1], delimiter='', usecols=(0,3,37), skip_header=1, unpack=True)

	sel = (mhalo >= 0.7e9) & (fMhires >= 0.98) & (ids <= 500)

	id_list = ids[sel].astype(int)
	return id_list

sim_name = ['B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

ahf_filedir = 'AHF_com'
ahf_center = 'COM'

for x in range(len(sim_name)):

	if sim_suite[x] == 'MassiveFIRE':
		halo_path = '/net/cephfs/shares/feldmann/data/' + sim_suite[x] + '/analysis/' + ahf_filedir + '/HR/' + sim_name[x] + '/halo'
	else:
		halo_path = '/bulk1/feldmann/data/' + sim_suite[x] + '/analysis/' + ahf_filedir + '/HR/' + sim_name[x] + '/halo'

	ID_list = selected_ids(halo_path)

	f = open(ahf_center + '/' + sim_suite[x] + '/' + sim_name[x] + '/ID_list.txt', 'w+')

	for i in range(len(ID_list)):
		f.write('%5i \n' %ID_list[i])

	f.close()
