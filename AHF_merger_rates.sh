#!/bin/bash

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

#MassiveFIRE
#0  B100_N512_M3e13_TL00001_baryon_toz2_HR_9915
#1  B100_N512_M3e13_TL00003_baryon_toz2_HR_9915
#2  B100_N512_M3e13_TL00004_baryon_toz2_HR_9915
#3  B100_N512_TL00002_baryon_toz2
#4  B100_N512_TL00009_baryon_toz2_HR
#5  B100_N512_TL00013_baryon_toz2_HR
#6  B100_N512_TL00018_baryon_toz2_HR
#7  B100_N512_TL00029_baryon_toz2_HR
#8  B100_N512_TL00037_baryon_toz0_HR_9915
#9  B100_N512_TL00113_baryon_toz0_HR_9915
#10 B100_N512_TL00206_baryon_toz0_HR_9915
#11 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915
#12 B400_N512_z6_TL00000_baryon_toz6_HR_9915
#13 B400_N512_z6_TL00001_baryon_toz6_HR_9915
#14 B400_N512_z6_TL00002_baryon_toz6_HR_9915
#15 B400_N512_z6_TL00005_baryon_toz6_HR_9915
#16 B400_N512_z6_TL00006_baryon_toz6_HR_9915
#17 B400_N512_z6_TL00013_baryon_toz6_HR_9915
#18 B400_N512_z6_TL00017_baryon_toz6_HR_9915
#19 B400_N512_z6_TL00021_baryon_toz6_HR_9915
#20 B762_N1024_z6_TL00000_baryon_toz6_HR
#21 B762_N1024_z6_TL00001_baryon_toz6_HR
#22 B762_N1024_z6_TL00002_baryon_toz6_HR

#MassiveFIRE2
#23 B762_N1024_z6_TL00000_baryon_toz6_HR 
#24 B762_N1024_z6_TL00001_baryon_toz6_HR 
#25 B762_N1024_z6_TL00002_baryon_toz6_HR
#26 h113_HR_sn1dy300ro100ss
#27 h206_HR_sn1dy300ro100ss
#28 h29_HR_sn1dy300ro100ss
#29 h2_HR_sn1dy300ro100ss

SIM_NAME=(B100_N512_M3e13_TL00001_baryon_toz2_HR_9915 B100_N512_M3e13_TL00003_baryon_toz2_HR_9915 B100_N512_M3e13_TL00004_baryon_toz2_HR_9915 B100_N512_TL00002_baryon_toz2 B100_N512_TL00009_baryon_toz2_HR B100_N512_TL00013_baryon_toz2_HR B100_N512_TL00018_baryon_toz2_HR B100_N512_TL00029_baryon_toz2_HR B100_N512_TL00037_baryon_toz0_HR_9915 B100_N512_TL00113_baryon_toz0_HR_9915 B100_N512_TL00206_baryon_toz0_HR_9915 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915 B400_N512_z6_TL00000_baryon_toz6_HR_9915 B400_N512_z6_TL00001_baryon_toz6_HR_9915 B400_N512_z6_TL00002_baryon_toz6_HR_9915 B400_N512_z6_TL00005_baryon_toz6_HR_9915 B400_N512_z6_TL00006_baryon_toz6_HR_9915 B400_N512_z6_TL00013_baryon_toz6_HR_9915 B400_N512_z6_TL00017_baryon_toz6_HR_9915 B400_N512_z6_TL00021_baryon_toz6_HR_9915 B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR h113_HR_sn1dy300ro100ss h206_HR_sn1dy300ro100ss h29_HR_sn1dy300ro100ss h2_HR_sn1dy300ro100ss)
SIM_SUITE=(MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2)
DUMMY=(16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 178 178 178 11 11 11 11)
AHF=/bulk1/feldmann/data/MassiveFIRE2/analysis/ahf
AHF_VERSION=AHF-v1.0-094

LIST=(28)

for i in ${LIST[@]}
do

	haloid_file=/bulk1/feldmann/Onur/${SIM_SUITE[$i]}/${SIM_NAME[$i]}/haloid_list.txt
	prefix_file=/bulk1/feldmann/Onur/${SIM_SUITE[$i]}/${SIM_NAME[$i]}/mergertree_list.txt	
	zred_pos_file=/bulk1/feldmann/Onur/${SIM_SUITE[$i]}/${SIM_NAME[$i]}/zred_pos_list
	zred_file=/bulk1/feldmann/Onur/${SIM_SUITE[$i]}/${SIM_NAME[$i]}/zred_list_rates
	merger_rates_file=/bulk1/feldmann/Onur/${SIM_SUITE[$i]}/${SIM_NAME[$i]}/merger_rates.sh 

	rm $haloid_file $prefix_file $merger_rates_file $zred_file

	tac $zred_pos_file > $zred_file

	halo_dir=/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}/halo	
	prefix_list=(/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}/halo/*/*backward_mtree)
	numoflines=${#prefix_list[@]}
	let "start=$numoflines-1"

	omega0=0.2821
	lambda0=0.7179
	hubble=0.697

	for ((k=0; k<=5; k++))	
	do
		echo $k >> $haloid_file
	done

	for ((k=$start; k>=0; k--))	
	do
		prefix=${prefix_list[$k]}
		echo $prefix >> $prefix_file
	done

	printf "$AHF/$AHF_VERSION/bin/MergerRates $haloid_file $numoflines $prefix_file $zred_file $omega0 $lambda0 $hubble  \n" > $merger_rates_file 
done
