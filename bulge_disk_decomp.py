import os
import glob
import sys
import numpy as np
import onur_tools as ot
import h5py
from joblib import Parallel, delayed
import multiprocessing

def calculate_bh_vel(data_gas, data_star):
	radius = 0.2
	cold_gas_temp = 1e5
	radius_gas = np.sqrt(np.sum(np.square(data_gas["Coordinates"]), axis=1))
	radius_star = np.sqrt(np.sum(np.square(data_star["Coordinates"]), axis=1))
	sel_gas1 = (radius_gas < radius)
	sel_star = (radius_star < radius)

	data_gas = ot.select_particles(data_gas,sel_gas1)
	
	# extra filtering for polluted data, i.e. sometimes helium mass fraction is above 1.0 and that points are needed to be removed
	if len(data_gas["Masses"]) != 0:
		ne = data_gas["ElectronAbundance"]
		u = data_gas["InternalEnergy"]
		temperature = np.array([ot.calculate_temperature(u1,ne1) for u1,ne1 in zip(u,ne)])
		sel_gas = (temperature < cold_gas_temp) & (temperature > 0.0)
		data_gas = ot.select_particles(data_gas,sel_gas)

	data_star = ot.select_particles(data_star,sel_star)

	# youngest 100  star particles 
	################################
	index = data_star["StellarFormationTime"].argsort()

	star_mass = data_star["Masses"][index][0:100]
	star_vel = data_star["Velocities"][index][0:100]
	################################

	if len(data_gas["Masses"]) == 0.0:
		# According to average velocity of star particles within 
		mass = star_mass
		velocity = star_vel
	else:
		mass = np.array(list(data_gas["Masses"]) + list(star_mass))
		velocity = np.array(list(data_gas["Velocities"]) + list(star_vel))

	if len(mass) == 0:
		bh_vel = np.array([0.0, 0.0, 0.0])
	else:
		bh_vel = np.sum((mass * velocity.T).T,axis=0) / np.sum(mass)

	return bh_vel

def calculate_sfr(data_star, z_1, position, rvir_hsm, k, redshift, dummy):
	radius_star = np.sqrt(np.sum(np.square(data_star['Coordinates']), axis=1))
	sel_star = (radius_star < rvir_hsm)
	data_star = ot.select_particles(data_star, sel_star)

	if z_1[k] >= 12.0:
		z_dummy = 12.4
		time_dummy = ot.time(z_1[k]) - ot.time(12.4)
	else:
		z_dummy = redshift[position[k]-1-dummy]
		time_dummy = ot.time(z_1[k]) - ot.time(z_dummy)

	sft = 1.0 / (1.0 + z_dummy)
	new_stars = np.array([dummy1 for dummy1,dummy2 in zip(data_star["Masses"],data_star["StellarFormationTime"]) if dummy2 > sft])
	num_new_stars = len(new_stars)
	sfr = np.sum(new_stars) / time_dummy

	mstar = np.sum(data_star["Masses"])

	if mstar == 0.0:
		ssfr = 0.0
	else:
		ssfr = sfr / mstar * 1e9

	return num_new_stars, sfr, ssfr # Gyr^-1

def calculate_half_mass_radius(data_gas, data_star, rvir, z_1, position, k, redshift, dummy):
	upper_limit = 100
	mstar_total = len(data_star["Masses"])
	if mstar_total <= 1:
		radius_star_final = 0.0
	else:
		mass = 0
		limit = mstar_total / 2
		radius_star = np.sqrt(np.sum(np.square(data_star['Coordinates']), axis=1))
		radius_min = 0.0
		radius_max = rvir
		flag = 1

		while mass != limit:
			if flag <= upper_limit:
				r = (radius_min + radius_max) / 2.0
				data_star_sel = data_star["Masses"][(radius_star <= r)]
				mass = len(data_star_sel)
				if mass > limit:
					radius_min = radius_min
					radius_max = r
				elif mass < limit:
					radius_min = r  
					radius_max = radius_max
				else:
					radius_star_final = r

				flag += 1
			else:
				radius_star_final = r
				break

	mgas_total = len(data_gas["Masses"])
	if mgas_total <= 1:
		radius_gas_final = 0.0
	else:
		mass = 0
		limit = mgas_total / 2
		radius_gas = np.sqrt(np.sum(np.square(data_gas["Coordinates"]), axis=1))
		radius_min = 0.0
		radius_max = rvir

		flag = 1
		while mass != limit:
			if flag <= upper_limit:
				r = (radius_min + radius_max) / 2.0
				data_gas_sel = data_gas["Masses"][(radius_gas <= r)]
				mass = len(data_gas_sel)
				if mass > limit:
					radius_min = radius_min
					radius_max = r
				elif mass < limit:
					radius_min = r  
					radius_max = radius_max
				else:
					radius_gas_final = r
			
				flag += 1
			else:
				radius_gas_final = r
				break

	sf_total, xx1, xx2 = calculate_sfr(data_star, z_1, position, rvir, k, redshift, dummy)
	if sf_total <= 1:
		radius_sf_final = 0.0
	else:
		sf = 0
		limit = sf_total / 2
		#print "limit: ", limit
		radius_sf = np.sqrt(np.sum(np.square(data_star["Coordinates"]), axis=1))
		radius_min = 0.0
		radius_max = rvir
		flag = 1
		while sf != limit:
			#print "flag: ", flag, "sf: ", sf
			if flag <= upper_limit:
				r = (radius_min + radius_max) / 2.0
				#print "radius: ", r
				sel_sf = (radius_sf <= r)
				data_sf_sel = ot.select_particles(data_star, sel_sf)
				sf, xx1, xx2 = calculate_sfr(data_sf_sel, z_1, position, r, k, redshift, dummy)
				#print "final sf: ", sf
				if sf > limit:
					radius_min = radius_min
					radius_max = r
				elif sf < limit:
					radius_min = r  
					radius_max = radius_max
				else:
					radius_sf_final = r

				flag += 1

			else:
				radius_sf_final = r
				break

	return radius_gas_final, radius_star_final, radius_sf_final

def calculate_velocity_phi_gas(data_gas, mgas, rvir, total_angular_momentum):
	# According to Angles-Alcazar et al. 2013
	if(mgas == 0.0):

		velocity_phi_gas = 0.0

	else:
		
		# unit total angular momentum vector 
		#tam = total_angular_momentum / np.linalg.norm(total_angular_momentum)

		#e_phi unit vector for all star particles within 1 kpc
		l_cross_r_gas = np.cross(total_angular_momentum, data_gas["Coordinates"])
		
		if np.sum(np.linalg.norm(l_cross_r_gas)) == 0.0:
			l_cross_r_gas = data_gas["Coordinates"] * 0.0
		else:
			l_cross_r_gas /= np.linalg.norm(l_cross_r_gas)

		# phi component of velocity of < 1kpc star particles
		velocity_phi = np.array([np.abs(np.dot(dummy1,dummy2)) for dummy1,dummy2 in zip(data_gas["Velocities"],l_cross_r_gas)])
		velocity_phi_gas = ot.weighted_average(velocity_phi,data_gas["Masses"])

	return velocity_phi_gas

def calculate_bulge_mass(data_star, mgas, mstar, bh_vel, rvir):
	# According to Angles-Alcazar et al. 2013
	if(mstar == 0.0):

		if(mgas == 0.0):
			m_bulge = 0.0
			f_bulge = 0.0
			m_disk = 0.0
			f_disk = 0.0
			vel_disp = 0.0
		else:
			m_bulge = 0.0
			f_bulge = m_bulge / (mgas + mstar)
			m_disk = mgas + mstar - m_bulge
			f_disk = 1.0 - f_bulge
			vel_disp = 0.0

		total_angular_momentum = np.array([0.0, 0.0, 0.0])

	else:
		
		new_mass = data_star["Masses"]
		new_pos = data_star["Coordinates"]
		new_vel = data_star["Velocities"]

		m = new_mass
		x = new_pos.T[0]
		y = new_pos.T[1]
		z = new_pos.T[2]
		vx = new_vel.T[0]
		vy = new_vel.T[1]
		vz = new_vel.T[2]

		# calculated total angular momentum using gas particles within 1 kpc
		total_angular_momentum, npart = ot.get_angular_momentum(x, y, z, vx, vy, vz, m, radius=rvir, normalize=False)
		# unit total angular momentum vector 
		#tam = total_angular_momentum / np.linalg.norm(total_angular_momentum)

		#e_phi unit vector for all star particles within 1 kpc
		l_cross_r_star = np.cross(total_angular_momentum, data_star["Coordinates"])
	
		if np.sum(np.linalg.norm(l_cross_r_star)) == 0.0:
			l_cross_r_star = data_star["Coordinates"] * 0.0
		else:
			l_cross_r_star /= np.linalg.norm(l_cross_r_star)

		# phi component of velocity of < 1kpc star particles
		velocity_phi_star = np.array([np.dot(dummy1,dummy2) for dummy1,dummy2 in zip(data_star["Velocities"],l_cross_r_star)])
		negative_part = np.array([b3 for a3,b3 in zip(velocity_phi_star,data_star["Velocities"]) if a3<0.0])

		if len(negative_part) > 0:
			positive_part = negative_part * -1.0 
			pos_mag = [ot.magnitude(a) for a in positive_part]
			neg_mag = [ot.magnitude(a) for a in negative_part] 
			vel_disp = np.std(np.array(neg_mag + pos_mag), ddof=1)
			mass_star_bulge = np.sum(np.array([b3 for a3,b3 in zip(velocity_phi_star,data_star["Masses"]) if a3<0.0])) * 2.0
		else:
			vel_disp = 0.0
			mass_star_bulge = 0.0

		if mass_star_bulge > mstar:
			mass_star_bulge = mstar
			f_star_bulge = 1.0
		else:
			f_star_bulge = mass_star_bulge / mstar
		
		m_bulge = f_star_bulge * mstar
		f_bulge = m_bulge / (mgas + mstar)
		m_disk = mgas + mstar - m_bulge
		f_disk = 1.0 - f_bulge

	return m_bulge,m_disk,f_bulge,f_disk,vel_disp, total_angular_momentum

def onur(snapshot_path, halo_path, output, z_1, ID, halo_ID, position, k, redshift, dummy):

	filename = snapshot_path + '/snapshot_' + str('%03d' %position[k]) + '_ID_' + str(ID[k]) + '.hdf5'
	halo_dir = glob.glob(halo_path + '/' + str('%03d' %position[k]) + '/*.AHF_halos')[0]

	h = h5py.File(filename, 'r')
	gas = ot.dict_converter(h["PartType0"])
	star = ot.dict_converter(h["PartType4"])					 
	halo = ot.dict_converter(h["PartType1"])					 
	data_header = h["Header"].attrs

	hubble = data_header["HubbleParam"]
	z_snap = data_header["Redshift"]
	hinv = 1.0 / hubble
	ascale = 1.0 / (1.0 + z_snap)
	constant = hinv * ascale
	center = np.array(ot.get_haloCenter(halo_dir, ID[k])) * constant

	# velocities are according to BH velocity
	bh_vel = calculate_bh_vel(gas, star)
	gas["Velocities"] -= bh_vel
	star["Velocities"] -= bh_vel
	halo["Velocities"] -= bh_vel
	gas["Masses"] *= 1e10 
	star["Masses"] *= 1e10
	halo["Masses"] *= 1e10

	rvir = np.array(ot.get_haloRvir(halo_dir, ID[k])) * constant
	rvir_10per = rvir * 0.1

	regime_list = ['cold', 'total']
	radius_list = [0.1, 0.2, 0.5, 1.0, rvir_10per]

	for regime in regime_list:

		if regime == 'cold':
			temp_limit = 1e5
		else:
			temp_limit = 1e20

		# extra filtering for polluted data, i.e. sometimes helium mass fraction is above 1.0 and that points are needed to be removed
		if len(gas["Masses"]) != 0:
			ne = gas["ElectronAbundance"]
			u = gas["InternalEnergy"]
			temperature = np.array([ot.calculate_temperature(u1,ne1) for u1,ne1 in zip(u,ne)])
			sel_gas = (temperature < temp_limit) & (temperature > 0.0)
			data_gas_tot = ot.select_particles(gas,sel_gas)
		else:
			data_gas_tot = gas

		#print data_gas_tot
		data_star_tot = star
		data_halo_tot = halo

		radius_gas = np.sqrt(np.sum(np.square(data_gas_tot["Coordinates"]), axis=1))
		radius_star = np.sqrt(np.sum(np.square(data_star_tot["Coordinates"]), axis=1))
		radius_halo = np.sqrt(np.sum(np.square(data_halo_tot["Coordinates"]), axis=1))
		
		for radius in radius_list:
	
			if len(data_gas_tot["Masses"]) != 0:
				sel_gas1 = (radius_gas < radius)
				data_gas = ot.select_particles(data_gas_tot,sel_gas1)

				if len(data_gas["Masses"]) != 0:
					mgas = np.sum(data_gas["Masses"])
					ne = data_gas["ElectronAbundance"]
					u = data_gas["InternalEnergy"]
					temperature = np.array([ot.calculate_temperature(u1,ne1) for u1,ne1 in zip(u,ne)])
					sound_speed = np.array([ot.calculate_sound_speed(u1,ne1) for u1,ne1 in zip(u,ne)])

					mean_cs = ot.weighted_average(sound_speed,data_gas["Masses"])
					#mean_nh = ot.weighted_average(data_gas["NeutralHydrogenAbundance"],data_gas["Masses"])
					#mean_vel = ot.weighted_average(np.linalg.norm(data_gas["Velocities"], axis=1),data_gas["Masses"])
					mean_vel = ot.magnitude(np.sum((data_gas["Masses"] * data_gas["Velocities"].T).T,axis=0) / np.sum(data_gas["Masses"]))
					mean_rho = ot.weighted_average(data_gas["Density"],data_gas["Masses"])
					mean_temp = ot.weighted_average(temperature,data_gas["Masses"])
					mean_gas_surf_density = mgas / (np.pi * (radius**2.0))

				else:
					mgas = 0.0
					mean_cs = 0.0
					#mean_nh = 0.0
					mean_vel = 0.0
					mean_temp = 0.0
					mean_rho = 0.0
					mean_gas_surf_density = 0.0

			else:
				data_gas = data_gas_tot
				mgas = 0.0
				mean_cs = 0.0
				#mean_nh = 0.0
				mean_vel = 0.0
				mean_temp = 0.0
				mean_rho = 0.0
				mean_gas_surf_density = 0.0

			sel_star1 = (radius_star < radius)
			data_star = ot.select_particles(data_star_tot,sel_star1)
			mstar = np.sum(data_star["Masses"])  
			sel_halo1 = (radius_halo < radius)
			data_halo = ot.select_particles(data_halo_tot,sel_halo1)
			mhalo = np.sum(data_halo["Masses"])

			mean_star_surf_density = mstar / (np.pi * (radius**2.0))

			if radius == radius_list[-1]:
				rvir_hgm, rvir_hsm, rvir_hsf = calculate_half_mass_radius(data_gas, data_star, radius, z_1, position, k, redshift, dummy)

			inflow, outflow = ot.calculate_outflow(data_gas_tot["Coordinates"], data_gas_tot["Masses"], data_gas_tot["Velocities"], radius, shell_thickness=0.1)
			num_new_stars, sfr, ssfr = calculate_sfr(data_star, z_1, position, radius, k, redshift, dummy)
			mbulge, mdisk, fbulge, fdisk, vel_disp, total_angular_momentum = calculate_bulge_mass(data_star, mgas, mstar, bh_vel, radius)
			velocity_phi_gas = calculate_velocity_phi_gas(data_gas, mgas, radius, total_angular_momentum)

			if radius == radius_list[-1]:
				write_path = output + '_rvir_' + regime + '_halo_' + str(halo_ID) + '.txt' 
			else:
				write_path = output + '_' + str(radius) + '_kpc_' + regime + '_halo_' + str(halo_ID) + '.txt' 
	
			write_data = open(write_path,'a+')
			if radius == radius_list[-1]:
				write_data.write('%-5i %-6.3f %-5i %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e \n' %(halo_ID, z_1[k], ID[k], radius, rvir_hgm, rvir_hsm, rvir_hsf, mgas, mstar, mhalo, mbulge, mdisk, fbulge, fdisk, inflow, outflow, ot.magnitude(bh_vel), vel_disp, num_new_stars, sfr, ssfr, mean_temp, mean_rho, mean_gas_surf_density, mean_star_surf_density, mean_cs, mean_vel, velocity_phi_gas))
			else:
				write_data.write('%-5i %-6.3f %-5i %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e \n' %(halo_ID, z_1[k], ID[k], mgas, mstar, mhalo, mbulge, mdisk, fbulge, fdisk, inflow, outflow, ot.magnitude(bh_vel), vel_disp, num_new_stars, sfr, ssfr, mean_temp, mean_rho, mean_gas_surf_density, mean_star_surf_density, mean_cs, mean_vel, velocity_phi_gas))

			write_data.close()
			
##########################################################
###################### MAIN BODY ########################
##########################################################
ot.global_variables()

halo_path = str(sys.argv[1])
snapshot_path = str(sys.argv[2])
read_path = str(sys.argv[3])
output_path = str(sys.argv[4])
dummy = int(sys.argv[5]) 
ncpu = int(sys.argv[6])

remove = len(read_path) + 6
filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

halo_ID_list = []
for f in filelist:
	halo_ID_list.append(f[remove:-4])

halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))[1:]

for halo_ID in halo_ID_list:

	redshift, place = ot.z(read_path)
	filename = read_path + '/halo_' + str(halo_ID) + '.txt'
	z_1, ID = np.genfromtxt(filename, delimiter='', usecols=(0,1), unpack=True)
	ID = ID.astype(int) 
	position = np.array([redshift.tolist().index(a)+dummy for a in z_1])
	k_list = range(0, len(ID))
	Parallel(n_jobs=ncpu)(delayed(onur)(snapshot_path, halo_path, output_path, z_1, ID, halo_ID, position, k, redshift, dummy) for k in k_list)

