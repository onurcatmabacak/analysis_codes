import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from astropy.cosmology import FlatLambdaCDM
import itertools
import seaborn as sns

def time(i):
	return cosmo.age(i).value * 1e9 # yr

def sfr(t,a,b):
	return a * ((t*1e-9)**b)

global cosmo
cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

z = np.arange(0,12,0.01)
t = np.array([time(xxx) for xxx in z])

a = np.array([0.01, 0.1, 0.5, 1.0, 5.0, 10.0])
alpha = np.array([-1.0, -0.1, 0.0, 0.1, 1.0, 10.0])
b = np.array([0.01, 0.1, 0.5, 1.0, 5.0, 10.0])
beta = np.array([-1.0, -0.1, 0.0, 0.1, 1.0, 10.0])

paramlist = list(itertools.product(alpha, beta))
a_b_list = list(itertools.product(a,b))

color = sns.color_palette(None, 16)

for yyy in range(len(a_b_list)):

	for xxx in range(len(paramlist)):

		sfr_10per = sfr(t,a_b_list[yyy][0],paramlist[xxx][0])
		sfr_1kpc = sfr(t,a_b_list[yyy][1],paramlist[xxx][1])
		mstar_10per = integrate.cumtrapz(sfr_10per[::-1], t[::-1], initial=1e6)
		mstar_1kpc = integrate.cumtrapz(sfr_1kpc[::-1], t[::-1], initial=1e6)
		
		mstar_10per[0] = mstar_10per[1]
		mstar_1kpc[0] = mstar_1kpc[1]

		mstar_bh = mstar_1kpc * 1e-10
		mbh = integrate.cumtrapz(mstar_bh, t[::-1], initial=1e4)
		mbh[0] = mbh[1]

		fig = plt.figure(1, figsize=(6,9))
		fig.subplots_adjust(wspace=0, hspace=0.25)
		ax1 = fig.add_subplot(311)	
		ax1.set_xscale('linear')
		ax1.set_yscale('log')
		ax1.set_xlabel(r'$z$')
		ax1.set_ylabel(r'$SFR\;(M_{\odot})/yr$')
		ax1.plot(z, sfr_10per, color='k', ls='-', label=r'$\alpha\;=\;$' + str(paramlist[xxx][0]))
		ax1.plot(z, sfr_1kpc, color='k', ls='--', label=r'$\beta\;=\;$' + str(paramlist[xxx][1]))
		ax1.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

		ax2 = fig.add_subplot(312)	
		ax2.set_xscale('linear')
		ax2.set_yscale('log')
		ax2.set_xlabel(r'$z$')
		ax2.set_ylabel(r'$M_{*}\;(M_{\odot})$')
		ax2.plot(z, mstar_10per, color='k', ls='-', label=r'$\alpha\;=\;$' + str(paramlist[xxx][0]))
		ax2.plot(z, mstar_1kpc, color='k', ls='--', label=r'$\beta\;=\;$' + str(paramlist[xxx][1]))
		ax2.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

		ax3 = fig.add_subplot(313)	
		ax3.set_xscale('log')
		ax3.set_yscale('log')
		ax3.set_xlabel(r'$M_{*}$')
		ax3.set_ylabel(r'$M_{BH}$')
		ax3.plot(mstar_10per, mbh, color='k', ls='-')
		filename = 'a_' + str(a_b_list[yyy][0]) + '_alpha_' + str(paramlist[xxx][0]) + '_b_' + str(a_b_list[yyy][1]) + '_beta_' + str(paramlist[xxx][1]) + '.png'
		plt.savefig(filename, dpi=300, format='png')
		plt.close()

