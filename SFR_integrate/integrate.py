import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from astropy.cosmology import FlatLambdaCDM

def time(i):
	return cosmo.age(i).value * 1e9 # yr

global cosmo
cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

z = np.arange(0,12,0.1)
t = np.array([time(xxx) for xxx in z])

a = 1.0
b = 1.0
alpha = 1.0
beta = 0.8

sfr_10per = a * ((t*1e-9)**alpha)
sfr_1kpc = b * ((t*1e-9)**beta)

mstar_10per = integrate.cumtrapz(sfr_10per[::-1], t[::-1], initial=1e6) 
mstar_1kpc = integrate.cumtrapz(sfr_1kpc[::-1], t[::-1], initial=1e6) 
mstar_bh = mstar_1kpc * 1e-10
mbh = integrate.cumtrapz(mstar_bh[::-1], t[::-1], initial=1e4)

mstar_10per[0] = mstar_10per[1]
mbh[0] = mbh[1]

fig = plt.figure(1)
fig.subplots_adjust(wspace=0, hspace=0)
ax1 = fig.add_subplot(111)	
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.plot(mstar_10per,mbh,'k-')
ax1.set_xlabel(r'$x$')
ax1.set_ylabel(r'$func$')

plt.show()


