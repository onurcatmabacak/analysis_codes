import numpy as np
from scipy import integrate
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from astropy.cosmology import FlatLambdaCDM
import pdb 
from scipy.optimize import curve_fit

def time(i):
	return cosmo.age(i).value * 1e9 # yr

def sfr(t,a,b):
	return a * ((t*1e-9)**b)

def sfr_dt(t,a,b,tau):
	return (10.0**a) * ((t*1e-9)**b) * np.exp(-t/(1e9*tau))

def Reines2015(Mstar):

   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)


global cosmo
cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

def z_vs_sfr_theoretical():
	z, sfr1, sfr2 = np.genfromtxt('data.txt', delimiter='', usecols=(0,1,2), unpack=True)
	sfr3 = sfr2 - sfr1	
	t = np.array([time(xxx) for xxx in z])

	new_1 = 100.0 * np.power((t / 3e9),-1)
	new_2 = 100.0 * np.power((t / 3e9),-2) 

	mstar_1 = integrate.cumtrapz(sfr1[::-1], t[::-1], initial=1e7)
	mstar_2 = integrate.cumtrapz(sfr2[::-1], t[::-1], initial=1e7)
	mstar_3 = mstar_2 - mstar_1
	mstar_1[0] = mstar_1[1]
	mstar_2[0] = mstar_2[1]
	mstar_bh = mstar_1 * 1e-10
	mbh = integrate.cumtrapz(mstar_bh, t[::-1], initial=1e4)
	mbh[0] = mbh[1]
	#pdb.set_trace()

	a1 = np.array([4.383e+09, 6.563e+10])
	a2 = np.array([1.968e+07, 1.789e+08])
	a3 = np.array([6.092e+09])
	a4 = np.array([2.411e+07])

	b1 = np.array([3.5, 4.5])
	b2 = np.array([1e-4, 1e-4])
	b3 = np.array([1e4, 1e4])

	mass_list = np.array([1e1, 1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8, 1e9, 1e10, 1e11, 1e12, 1e13, 1e14])
	Reines = Reines2015(mass_list) * 1e2
	fig = plt.figure(1, figsize=(6,9))
	fig.subplots_adjust(wspace=0.0, hspace=0.3)

	ax0 = fig.add_subplot(411)	
	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(1,12)
	ax0.set_ylim(1e-2,1e3)
	ax0.plot(z,sfr1,'r-',lw=2,label=r'$1\;kpc$')
	ax0.plot(z,sfr2,'k-',lw=2,label=r'$\%10\;R_{vir}$')
	#ax0.plot(z,sfr3,'b-',lw=2,label=r'$diff$')
	ax0.fill_between(b1, b2, b3, color='k', zorder=0, alpha=0.3)
	ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax1 = fig.add_subplot(412)	
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax1.set_xlim(4e8,6e9)
	ax1.set_ylim(1e-2,1e3)
	ax1.plot(t,sfr1,'r-',lw=2,label=r'$1\;kpc$')
	ax1.plot(t, new_1,'b-', t, new_2, 'b--')
	ax1.plot(t,sfr2,'k-',lw=2,label=r'$\%10\;R_{vir}$')
	#ax1.plot(t,sfr3,'b-',lw=2,label=r'$diff$')
	#ax1.fill_between(b1, b2, b3, color='k', zorder=0, alpha=0.3)
	ax1.xaxis.set_major_locator(plt.FixedLocator([5e8, 7e8, 9e8, 1e9, 3e9, 5e9]))
	ax1.set_xlabel(r'$time$')
	ax1.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(413)	
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(1,12)
	ax2.set_ylim(1e6,1e12)
	ax2.plot(z,mstar_1[::-1],'r-',lw=2,label=r'$1\;kpc$')
	ax2.plot(z,mstar_2[::-1],'k-',lw=2,label=r'$\%10\;R_{vir}$')
	#ax2.plot(z,mstar_3[::-1],'b-',lw=2,label=r'$diff$')
	#ax2.fill_between(b1, b2, b3, color='k', zorder=0, alpha=0.3)
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.set_xlabel(r'$z$')
	ax2.set_ylabel(r'$M_{*}\;(M_{\odot})$')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax3 = fig.add_subplot(414)	
	ax3.set_xscale('log')
	ax3.set_yscale('log')
	ax3.set_xlim(1e6,1e12)
	ax3.set_ylim(1e4,1e11)
	ax3.plot(mstar_1, mbh, color='k', ls='-', lw=2, label=r'$\%100\;R_{vir}$')
	ax3.plot(mstar_2, mbh, color='r', ls='-', lw=2, label=r'$1\;kpc$')
	#ax3.plot(mstar_3, mbh, color='b', ls='-', lw=2, label=r'$diff$')
	ax3.plot(mass_list, Reines, color='k', ls='--', lw=1, label=r'$10xReines+15$')
	ax3.fill_between(mass_list, Reines * np.sqrt(1e1), Reines / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
	ax3.scatter(a1, a2, color='r', marker='o', s=8)
	ax3.scatter(a3, a4, color='b', marker='o', s=8)
	ax3.set_xlabel(r'$M_{*}\;(M_{\odot})$')
	ax3.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
	ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig('z_vs_sfr_gas_inflow_theoretical.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()

def z_vs_sfr_delayed_time():
	tau1 = 3e9
	tau2 = 3e9
	p1 = 0.0
	p2 = 0.0
	norm1 = 1.0
	norm2 = 1.0
	z = np.linspace(0,12,100)
	t = np.array([time(xxx) for xxx in z])

	sfr1 = norm1 * np.power((t * 1e-9),p1) #* np.exp(-t/tau1)
	sfr2 = norm2 * np.power((t * 1e-9),p2) #* np.exp(-t/tau2)
	mstar1 = integrate.cumtrapz(sfr1[::-1], t[::-1], initial=1e6)
	mstar2 = integrate.cumtrapz(sfr2[::-1], t[::-1], initial=1e6)
	mstar1[0] = mstar1[1]
	mstar2[0] = mstar2[1]
	mstar_bh = mstar2 * 1e-10
	mbh = integrate.cumtrapz(mstar_bh, t[::-1], initial=1e4)
	mbh[0] = mbh[1]

	b1 = np.array([3.5, 4.5])
	b2 = np.array([1e-4, 1e-4])
	b3 = np.array([1e4, 1e4])
	
	fig = plt.figure(1, figsize=(6,9))
	fig.subplots_adjust(wspace=0.0, hspace=0.3)

	ax0 = fig.add_subplot(311)
	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(0,12)
	ax0.set_ylim(1e-1,1e1)
	ax0.plot(z,sfr1,'r-',label=r'$\%10\;R_{vir}$')
	ax0.plot(z,sfr2,'k-',label=r'$1\;kpc$')
	#ax0.fill_between(b1, b2, b3, color='k', zorder=0, alpha=0.3)
	#ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax1 = fig.add_subplot(312)  
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	#ax1.set_xlim()
	#ax1.set_ylim(1e-2,1e3)
	ax1.plot(z,mstar1[::-1],'r-',label=r'$\%10\;R_{vir}$')
	#ax1.plot(time, new_1, 'b-', t1, new_2, 'b--')
	ax1.plot(z,mstar2[::-1],'k-',label=r'$1\;kpc$')
	#ax1.fill_between(b1, b2, b3, color='k', zorder=0, alpha=0.3)
	#ax1.xaxis.set_major_locator(plt.FixedLocator([5e8, 7e8, 9e8, 1e9, 3e9, 5e9]))
	ax1.set_xlabel(r'$z$')
	ax1.set_ylabel(r'$M_{*}\;(M_{\odot})$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(313)  
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_xlim(1e4,1e12)
	ax2.set_ylim(1e4,1e11)
	ax2.plot(mstar1, mbh, color='k', ls='-', lw=2, label=r'$1\;kpc$')
	#ax2.plot(m_star, m_bh_torque[3][3], color='r', ls='-', lw=2, label=r'$M_{*}$')
	#ax2.scatter(a1, a2, color='r', marker='o', s=8)
	#ax2.scatter(a3, a4, color='b', marker='o', s=8)
	ax2.set_xlabel(r'$M_{*}\;(M_{\odot})$')
	ax2.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig('z_vs_sfr_delayed_time.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()

def mstar_vs_mbh():
	z1, sfr1kpc, sfr10per = np.genfromtxt('data.txt', delimiter='', usecols=(0,1,2), unpack=True)
	t1 = time(z1)

	z = np.linspace(0,12,100)
	t = time(z) 

	popt, pcov = curve_fit(sfr_dt, t1, sfr1kpc)
	popt1, pcov1 = curve_fit(sfr_dt, t1, sfr10per)

	sfr1 = sfr_dt(t,0.1,0.5,0.8)
	sfr2 = sfr_dt(t,0.1,0.2,3.0) 
	sfr3 = sfr_dt(t,0.1,0.4,3.0) 
	sfr4 = sfr_dt(t,0.1,0.8,3.0) 

	#mstar1 = integrate.cumtrapz(sfr1[::-1], t[::-1], initial=1e6)
	#mstar2 = integrate.cumtrapz(sfr2[::-1], t[::-1], initial=1e6)
	#mstar1[0] = mstar1[1]
	#mstar2[0] = mstar2[1]
	#mstar_bh = mstar2 * 1e-10
	#mbh = integrate.cumtrapz(mstar_bh, t[::-1], initial=1e4)
	#mbh[0] = mbh[1]

	#b1 = np.array([3.5, 4.5])
	#b2 = np.array([1e-4, 1e-4])
	#b3 = np.array([1e4, 1e4])

	fig = plt.figure(1) #, figsize=(6,9))
	fig.subplots_adjust(wspace=0.0, hspace=0.3)

	ax0 = fig.add_subplot(111)
	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(0,12)
	#ax0.set_ylim(1e-3,1e3)
	ax0.plot(z1, sfr1kpc, 'r--', label=r'$1kpc$')
	ax0.plot(z1, sfr10per, 'k--', label=r'$10per$')
	ax0.plot(z1, sfr_dt(t1, *popt), 'g--', label=r'1kpc\;fit')
	ax0.plot(z1, sfr_dt(t1, *popt1), 'b--', label=r'$10per\;fit$')
	ax0.plot(z,sfr1,'k-',label=r'$sfr1$')
	ax0.plot(z,sfr2,'r-',label=r'$sfr2$')
	ax0.plot(z,sfr3,'g-',label=r'$sfr3$')
	ax0.plot(z,sfr4,'b-',label=r'$sfr4$')
	#ax0.fill_between(b1, b2, b3, color='k', zorder=0, alpha=0.3)
	#ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	'''
	ax1 = fig.add_subplot(312)  
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	#ax1.set_xlim()
	#ax1.set_ylim(1e-2,1e3)
	ax1.plot(z,mstar1[::-1],'r-',label=r'$\%10\;R_{vir}$')
	#ax1.plot(time, new_1, 'b-', t1, new_2, 'b--')
	ax1.plot(z,mstar2[::-1],'k-',label=r'$1\;kpc$')
	#ax1.fill_between(b1, b2, b3, color='k', zorder=0, alpha=0.3)
	#ax1.xaxis.set_major_locator(plt.FixedLocator([5e8, 7e8, 9e8, 1e9, 3e9, 5e9]))
	ax1.set_xlabel(r'$z$')
	ax1.set_ylabel(r'$M_{*}\;(M_{\odot})$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(313)  
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_xlim(1e4,1e12)
	ax2.set_ylim(1e4,1e11)
	ax2.plot(mstar1, mbh, color='k', ls='-', lw=2, label=r'$1\;kpc$')
	#ax2.plot(m_star, m_bh_torque[3][3], color='r', ls='-', lw=2, label=r'$M_{*}$')
	#ax2.scatter(a1, a2, color='r', marker='o', s=8)
	#ax2.scatter(a3, a4, color='b', marker='o', s=8)
	ax2.set_xlabel(r'$M_{*}\;(M_{\odot})$')
	ax2.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	'''
	plt.savefig('mstar_vs_mbh.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()

def sfr_min_max_bhmass():

	z = np.linspace(0,12,25)
	t = time(z) 

	#sfr = sfr_dt(t, 3.51079281, 15.87257082, 0.14499396)
	sfr1 = sfr_dt(t, 1.0, 12.0, 0.3)
	sfr2 = sfr_dt(t, 3.0, 5.0, 0.5)
	mstar1 = integrate.cumtrapz(sfr1[::-1], t[::-1], initial=1e6)
	mstar2 = integrate.cumtrapz(sfr2[::-1], t[::-1], initial=1e6)
	mstar1[0] = mstar1[1]
	mstar2[0] = mstar2[1]
	mstar_bh1 = mstar1 * 1e-10
	mstar_bh2 = mstar2 * 1e-10
	mbh1 = integrate.cumtrapz(mstar_bh1, t[::-1], initial=1e4)
	mbh2 = integrate.cumtrapz(mstar_bh2, t[::-1], initial=1e4)
	mbh1[0] = mbh1[1]
	mbh2[0] = mbh2[1]

	fig = plt.figure(1) #, figsize=(6,9))
	fig.subplots_adjust(wspace=0.0, hspace=0.3)

	ax0 = fig.add_subplot(311)
	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(0,12)
	#ax0.set_ylim(1e-3,1e3)
	ax0.plot(z, sfr1, 'r-', label=r'$SFR1$')
	ax0.plot(z, sfr2, 'g-', label=r'$SFR2$')
	#ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax1 = fig.add_subplot(312)
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(0,15)
	#ax1.set_ylim(1e-3,1e3)
	ax1.plot(t*1e-9, sfr1, 'r-', label=r'$SFR1$')
	ax1.plot(t*1e-9, sfr2, 'g-', label=r'$SFR2$')
	#ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.set_xlabel(r'$time\;(Gyr)$')
	ax1.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax1 = fig.add_subplot(313)
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	#ax1.set_xlim(0,15)
	#ax1.set_ylim(1e-3,1e3)
	ax1.plot(mstar1, mbh1, 'r-', label=r'$SFR1$')
	ax1.plot(mstar2, mbh2, 'g-', label=r'$SFR2$')
	#ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.set_xlabel(r'$M_{*}\;(M_{\odot})$')
	ax1.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig('sfr_min_max_mbh.png', dpi=300, format='png')
	plt.close()
############################
######## MAIN BODY #########
############################
global z_ticks
z_ticks = [2,3,4,5,6,7,8,9,10,11]

#z_vs_sfr_delayed_time()
z_vs_sfr_theoretical()
#mstar_vs_mbh()
#sfr_min_max_bhmass()
