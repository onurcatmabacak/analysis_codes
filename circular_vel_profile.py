import matplotlib
matplotlib.use('Agg')
import os
import glob
import numpy as np
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
from astropy import units as u
from matplotlib import pyplot as plt
from matplotlib import animation
import multiprocessing
from joblib import Parallel, delayed
import onur_tools as ot

def plot(fname, output, scale, hinv, halo_ID, x):

	radius, m_tot, mgas, mstar = np.genfromtxt(fname, delimiter='', usecols=(0,1,2,19), skip_header=1, unpack=True)
	radius = np.array([np.abs(a) for a in radius])

	flag = 0
	init = [0]
	for xxx in range(10000):
		if radius[xxx+1] < radius[xxx]:
			init.append(xxx)
			if flag == halo_ID*2+1:
				break
			else:
				flag += 1

	start = init[2 * halo_ID] + 1
	finish = init[2 * halo_ID + 1] + 1

	constant = scale * hinv
	redshift = (1.0 / scale) - 1.0
	print fname 

	radius = radius[start:finish] * constant
	m_tot = m_tot[start:finish] * hinv
	mgas = mgas[start:finish] * hinv
	mstar = mstar[start:finish] * hinv
	m_baryon = mgas + mstar
	m_dm = m_tot - m_baryon
	m_dm = np.array([a if a>=0.0 else 0.0 for a in m_dm])

	compactness_tot = m_tot / radius
	compactness_dm = m_dm / radius
	compactness_baryon = m_baryon / radius
	compactness_star = mstar / radius

	circ_vel_tot = np.sqrt(2.0 * grav_constant * m_tot * m_sun / (radius * kpc_cm_conversion)) * 1e-5
	circ_vel_baryon = np.sqrt(2.0 * grav_constant * m_baryon * m_sun / (radius * kpc_cm_conversion)) * 1e-5
	circ_vel_dm = np.sqrt(2.0 * grav_constant * m_dm * m_sun / (radius * kpc_cm_conversion)) * 1e-5

	###############################################
	#################### FIG 1 ####################
	###############################################
	fig = plt.figure(x)
	#fig.suptitle(title)
	fig.subplots_adjust(wspace=0, hspace=0)
	
	onur = np.array([1e-1,2e3])

	ax1 = fig.add_subplot(311)	
	ax1.plot(radius, circ_vel_tot, color='k', ls='-', lw=1, label=r'$v_{circ\;tot}$')
	ax1.plot(radius, circ_vel_dm, color='r', ls='-', lw=1, label=r'$v_{circ\;dm}$')
	ax1.plot(radius, circ_vel_baryon, color='g', ls='-', lw=1, label=r'$v_{circ\;b}$')
	ax1.plot(np.ones(len(onur))*radius[-1]*0.1, onur, color='b', ls='-', lw=4, alpha=0.25)
	ax1.plot(np.ones(len(onur))*radius[-1]*0.05, onur, color='r', ls='-', lw=4, alpha=0.25)
	ax1.plot(np.ones(len(onur))*1.0, onur, color='k', ls='-', lw=4, alpha=0.25)
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax1.set_xlim(1e-2,1e3)
	ax1.set_ylim(1e-1,2e3)
	ax1.xaxis.set_major_locator(plt.FixedLocator([1e-1,1e0,1e1,1e2]))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1e0,1e1,1e2,1e3]))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.text(8e1,8e2,r'$z:\;$' + str(redshift))
	#ax1.set_xlabel(r'$radius\;(kpc)$')
	ax1.set_ylabel(r'$velocity\;(km/s)$')
	ax1.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
	
	onder = np.array([1e4,1e13])
	merve = np.array([1e-3,1e4])

	ax2 = fig.add_subplot(312)
	ax2.plot(radius, m_tot, color='k', ls='-', lw=1, label=r'$M_{tot}$')
	ax2.plot(radius, m_dm, color='r', ls='-', lw=1, label=r'$M_{dm}$')
	ax2.plot(radius, m_baryon, color='g', ls='-', lw=1, label=r'$M_{b}$')
	ax2.plot(radius, mstar, color='b', ls='-', lw=1, label=r'$M_{star}$')
	ax2.plot(radius, mgas, color='m', ls='-', lw=1, label=r'$M_{gas}$')
	ax2.plot(np.ones(len(onder))*radius[-1]*0.1, onder, color='b', ls='-', lw=4, alpha=0.25)
	ax2.plot(np.ones(len(onder))*radius[-1]*0.05, onder, color='r', ls='-', lw=4, alpha=0.25)
	ax2.plot(np.ones(len(onder))*1.0, onder, color='k', ls='-', lw=4, alpha=0.25)
	ax2.plot(merve, np.ones(len(merve))*1e9, color='k', ls='--', lw=0.5)
	ax2.plot(merve, np.ones(len(merve))*1e10, color='k', ls='--', lw=0.5)
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_xlim(1e-2,1e3)
	ax2.set_ylim(1e4,1e13)
	ax2.xaxis.set_major_locator(plt.FixedLocator([1e-1,1e0,1e1,1e2]))
	ax2.yaxis.set_major_locator(plt.FixedLocator([1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12]))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_xlabel(r'$radius\;(kpc)$')
	ax2.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax2.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)

	onder = np.array([1e4,1e13])
	merve = np.array([1e-3,1e4])

	ax3 = fig.add_subplot(313)
	ax3.plot(radius, compactness_tot, color='k', ls='-', lw=1, label=r'$tot$')
	ax3.plot(radius, compactness_dm, color='r', ls='-', lw=1, label=r'$dm$')
	ax3.plot(radius, compactness_baryon, color='g', ls='-', lw=1, label=r'$b$')
	ax3.plot(radius, compactness_star, color='b', ls='-', lw=1, label=r'$star$')
	ax3.plot(np.ones(len(onder))*radius[-1]*0.1, onder, color='b', ls='-', lw=4, alpha=0.25)
	ax3.plot(np.ones(len(onder))*radius[-1]*0.05, onder, color='r', ls='-', lw=4, alpha=0.25)
	ax3.plot(np.ones(len(onder))*1.0, onder, color='k', ls='-', lw=4, alpha=0.25)
	ax3.plot(merve, np.ones(len(merve))*1e8, color='k', ls='--', lw=0.5)
	ax3.plot(merve, np.ones(len(merve))*1e9, color='k', ls='--', lw=0.5)
	ax3.plot(merve, np.ones(len(merve))*1e10, color='k', ls='--', lw=0.5)
	ax3.set_xscale('log')
	ax3.set_yscale('log')
	ax3.set_xlim(1e-2,1e3)
	ax3.set_ylim(1e4,1e12)
	ax3.xaxis.set_major_locator(plt.FixedLocator([1e-1,1e0,1e1,1e2]))
	ax3.yaxis.set_major_locator(plt.FixedLocator([1e5,1e6,1e7,1e8,1e9,1e10,1e11]))
	ax3.tick_params(axis='y', labelsize=8)
	ax3.set_xlabel(r'$radius\;(kpc)$')
	ax3.set_ylabel(r'$Compactness\;(M_{\odot}/kpc)$')
	ax3.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output + 'image_' + str('%03d' %(x)) + '.png', dpi=300, format='png')
	plt.close()

#####################
##### MAIN BODY #####
#####################
global cosmo
global grav_constant
global kpc_cm_conversion
global m_sun
global yr_s_conversion

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)
grav_constant = const.G.cgs.value
kpc_cm_conversion = 3.086e21
m_sun = const.M_sun.cgs.value
yr_s_conversion = 3.15576e7

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2'] 

i_list = np.arange(37)

for i in i_list:

	output = './' + sim_suite[i] + '/' + sim_name[i] + '/circular_vel_profile/'
	redshift_path = './' + sim_suite[i] + '/' + sim_name[i] + '/zred_pos_list'
	timeline_path = './' + sim_suite[i] + '/' + sim_name[i] + '/timeline_0.txt'

	# from redshift 12 to 0
	redshift, pos = np.genfromtxt(redshift_path, delimiter='', usecols=(0,1), skip_header=0, unpack=True) 
	pos = pos.astype(int)
	# from redshift 0 to 12
	redshift_list, halo_ID = np.genfromtxt(timeline_path, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	redshift_list = redshift_list[::-1]
	
	halo_ID = halo_ID.astype(int)[::-1]
	scale = 1.0 / (redshift_list + 1.0)

	pos_list = np.array([x1 for x1,x2 in zip(pos, redshift) if x2 in redshift_list])

	snapshot_dir = '/bulk1/feldmann/data/' + sim_suite[i] + '/production_runs/HR/' + sim_name[i] + '/snapdir_' + str('%03d' %pos[-1])
	data_header = ot.readsnap(snapshot_dir, snum=pos[-1], ptype=0, header_only=1)
	hinv = 1.0 / data_header['hubble']

	filename = []
	for xxx in pos_list:
		filename.append(glob.glob('/bulk1/feldmann/data/' + sim_suite[i] + '/analysis/AHF/HR/' + sim_name[i] + '/halo/' + str('%03d' %(xxx)) + '/*disks')[0])

	scale = scale[::-1]
	halo_ID = halo_ID[::-1]

	if not os.path.exists(output):
		os.makedirs(output)

	Parallel(n_jobs=32)(delayed(plot)(filename[x],output,scale[x],hinv,int(halo_ID[x]),x) for x in range(len(filename)))
