import numpy as np
import glob
import onur_tools as ot
import argparse
import sys
import os
from collections import OrderedDict

def tree_builder(ID, halo_data, idx_data, all_data):

	z_list = []
	ID_list = []
	mass_list = []

	for i in range(len(position)):

		if i == 0:

			halo_ID = halo_data[str('%03d' %(position[i])) + '/halo_ID']  
			halo_mass = halo_data[str('%03d' %(position[i])) + '/halo_mass']

			data = [[a,b] for a,b in zip(halo_ID, halo_mass) if a == ID]
			ID = data[0][0]
			mass = data[0][1]
			z_list.append(redshift[i])
			ID_list.append(ID)
			mass_list.append(mass)

		else:

			ID_0 = idx_data[str('%03d' %(position[i])) + '/id0']  
			ID_1 = idx_data[str('%03d' %(position[i])) + '/id1']

			new = [a for a,b in zip(ID_0, ID_1) if b == ID]
			if new:

				new_ID = new[0]
				halo_ID = halo_data[str('%03d' %(position[i])) + '/halo_ID']  
				halo_mass = halo_data[str('%03d' %(position[i])) + '/halo_mass']

				data = [[a,b] for a,b in zip(halo_ID, halo_mass) if a == new_ID]

				ID = data[0][0]
				mass = data[0][1]
			
				z_list.append(redshift[i])
				ID_list.append(ID)
				mass_list.append(mass)

			else:
				break

		#f.write('id' + str(ID_list[0]) + '\n')



def main(halo_path, sim_dir, z): #, start, finish):

	#filename = 'trace_z_' + str('%.4f' %z) + '_' + str(start) + '_' + str(finish) + '.txt'
	filename = sim_dir + '/trace_z_' + str('%.1f' %z) + '.txt'
	f = open(filename,'w+')

	#ID_list = range(start,finish+1)

	redshift_all, position_all = np.genfromtxt(sim_dir + '/zred_pos_list', delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	position_all = position_all.astype(int) 

	sel = (redshift_all >= z)
	redshift = redshift_all[sel][::-1]
	position = position_all[sel][::-1]

	######################################
	####### READ ALL NECESSARY DATA ######
	######################################

	if z == 0.0:
		initial_idxfile = glob.glob(halo_path + '/' + str('%03d' %position[1]) + '/*idx')[0]
		ID_list = np.genfromtxt(initial_idxfile, delimiter='', usecols=(1), skip_header=1, unpack=True, dtype=int)
		ID_list = list(OrderedDict.fromkeys(ID_list))
	else:
		initial_idxfile = glob.glob(halo_path + '/' + str('%03d' %position[0]) + '/*idx')[0]
		ID_list = np.genfromtxt(initial_idxfile, delimiter='', usecols=(0), skip_header=1, unpack=True, dtype=int)

	idx_data = {}
	halo_data = {}

	for xxx in range(len(position)):

		idxfile = glob.glob(halo_path + '/' + str('%03d' %position[xxx]) + '/*idx')[0]
		ID_0, ID_1 = np.genfromtxt(idxfile, delimiter='', usecols=(0,1), skip_header=1, unpack=True, dtype=int)
		idx_data[str('%03d' %(position[xxx])) + '/id0'] = ID_0
		idx_data[str('%03d' %(position[xxx])) + '/id1'] = ID_1

		halofile = glob.glob(halo_path + '/' + str('%03d' %position[xxx]) + '/*halos')[0]
		halo_ID, halo_mass = np.genfromtxt(halofile, delimiter='', usecols=(0,3), skip_header=1, unpack=True)
		halo_ID = halo_ID.astype(int)
		halo_data[str('%03d' %(position[xxx])) + '/halo_ID'] = halo_ID
		halo_data[str('%03d' %(position[xxx])) + '/halo_mass'] = halo_mass

	#######################################

	z_all = {}
	ID_all = {}
	mass_all = {}

	for ID in ID_list:

		ID_column = np.ones(len(ID_list)) * ID_list[0]
		ID_column = ID_column.astype(int)
		data = np.array([ID_column, ID_list, z_list, mass_list])
		data = data.T
		np.savetxt(f, data, fmt=['%-5i', '%-5i', '%-6.4f', '%-10.3e'])

	f.close()

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Tracer for B100_1024_DM simulation' '(c) O. Catmabacak 2018')
	parser.add_argument('-H', '--halopath',help='path to the halos files of simulation, default "."', default='.')
	parser.add_argument('-P', '--simpath',help='path to the simulation, default "."', default='.')
	parser.add_argument('-z', '--redshift',help='starting from redshift <?>, default "0.0"', default=0.0)
	#parser.add_argument('-s', '--start',help='starting halo id, default "0"', default=0)
	#parser.add_argument('-f', '--finish', help='finishing halo id, default "."', default='.')
	
	args = parser.parse_args()

	if len(sys.argv[1:]) == 0:
		print('Error: {} requires more parameters'.format(__file__))
		parser.print_help()
		parser.exit()

	main(args.halopath, args.simpath, float(args.redshift)) #, int(args.start), int(args.finish))
