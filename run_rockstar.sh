#!/bin/bash
module load hydra
export OMP_NUM_THREADS=32

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

rockstar=/home/cluster/ocatma/rockstar/rockstar
FILE_FORMAT=GADGET
MASS_DEF=mvir
NUM_BLOCKS=8
NUM_SNAPS=101
STARTING_SNAP=100
NUM_READERS=8
NUM_WRITERS=8
CPU_PER_MACHINE=8
FORCE_RES=0.001

SIM_NAME=FB15N512

NTASK=2
NCPU=32
MEM_PER_CPU=4
let "MEMORY=NTASK*NCPU*MEM_PER_CPU"

#INBASE="/bulk1/feldmann/data/FIREbox/production_runs/$SIM_NAME/snapdir_$STARTING_SNAP"
INBASE=/bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/$SIM_NAME/halo/$STARTING_SNAP
OUTBASE=/bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/$SIM_NAME/halo/$STARTING_SNAP
FILENAME="snapshot_converted_<snap>.<block>"

create_dir /bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/$SIM_NAME
create_dir /bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/$SIM_NAME/halo
create_dir /bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/$SIM_NAME/halo/$STARTING_SNAP

config_file=/bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/$SIM_NAME/halo/$STARTING_SNAP/rockstar.cfg

printf "FILE_FORMAT = "$FILE_FORMAT" \n" > $config_file
printf "MASS_DEFINITION = "$MASS_DEF" \n" >> $config_file
printf "GADGET_MASS_CONVERSION = 1e+10 \n" >> $config_file
printf "GADGET_LENGTH_CONVERSION = 0.001 \n" >> $config_file
printf "PARALLEL_IO = 1 \n" >> $config_file
printf "NUM_READERS = "$NUM_READERS" \n" >> $config_file
printf "NUM_BLOCKS = "$NUM_BLOCKS" \n" >> $config_file
printf "INBASE = "$INBASE" \n" >> $config_file
printf "OUTBASE = $OUTBASE \n" >> $config_file
printf "FILENAME = "$FILENAME" \n" >> $config_file
printf "NUM_SNAPS = $NUM_SNAPS \n" >> $config_file
printf "STARTING_SNAP = $STARTING_SNAP \n" >> $config_file							 #defaults to 0
printf "NUM_WRITERS = $NUM_WRITERS \n" >> $config_file
printf "FORK_READERS_FROM_WRITERS = 1 \n" >> $config_file
printf "FORK_PROCESSORS_PER_MACHINE = $CPU_PER_MACHINE \n" >> $config_file
printf "FORCE_RES = $FORCE_RES \n" >> $config_file

JOBSUB=rockstar_batch_file.sh

printf "#!/bin/bash -l \n" > $JOBSUB
printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
printf "#SBATCH --time=24:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
printf "#SBATCH --ntasks=$NTASK --cpus-per-task=$NCPU \n" >> $JOBSUB
printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
printf "#======START===== \n" >> $JOBSUB
printf "$rockstar -c $config_file >& $OUTBASE/server.dat & \n" >> $JOBSUB
printf "sleep 1 \n" >> $JOBSUB
printf "mpirun -n 8 $rockstar -c $OUTBASE/auto-rockstar.cfg \n " >> $JOBSUB
printf "#=====END==== \n" >> $JOBSUB
sbatch $JOBSUB 
