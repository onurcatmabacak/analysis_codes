import numpy as np
import h5py
import sys
import yt
sys.path.append('/bulk1/feldmann/Onur')
import onur_tools as ot


snapdir = '/bulk1/feldmann/data/FIREbox/production_runs/FB15N2048'
#snum = 176
#comoving = 1
#data_gas = ot.readsnap_compact(snapdir, snum=snum, ptype=0, cosmological=comoving)
#data_halo = ot.readsnap_compact(snapdir, snum=snum, ptype=1, cosmological=comoving)
#data_star = ot.readsnap_compact(snapdir, snum=snum, ptype=4, cosmological=comoving)

ds = yt.load(snapdir + '/snapdir_176/snapshot_176.9.hdf5')
px = yt.ProjectionPlot(ds, 'x', ('gas', 'density'))
px.show()
