import numpy as np
import os
import sys
import onur_tools as ot
import glob
import multiprocessing
from joblib import Parallel, delayed

def selected_ids(halo_path):

	halo_files = sorted(glob.glob(halo_path + '/*/*.AHF_halos'))
	ids, mhalo, fMhires = np.genfromtxt(halo_files[-1], delimiter='', usecols=(0,3,37), skip_header=1, unpack=True)

	sel = (mhalo >= 0.7e8) & (fMhires >= 0.98)

	id_list = ids[sel].astype(int)
	return id_list

def constraints(pos, m, rvir, mhalo, fMhires, mstar):
	
	if (fMhires >= 0.98) and (mstar >= 1e7):

		radius = rvir * 0.1
		radius_star = np.sqrt(np.sum(np.square(pos), axis=1))
		sel = (radius_star < radius)
		mass = np.sum(m[sel]) * 1e10

		if mass >= 1e7:
			flag = 1
			mhalo1 = mhalo
			mstar1 = mass
		else:
			flag = 0
			mhalo1 = -1
			mstar1 = -1

	else:

		flag = 0
		mhalo1 = -1
		mstar1 = -1

	return flag, mhalo1, mstar1

def tree_list(snapshot_path, input_path, halo_path, halo_ID, dummy):
	
	redshift, position = ot.z(input_path)

	redshift = redshift[::-1]
	position = position.astype(int)[::-1]	

	halo_file = input_path + '/halo_' + str(halo_ID) + '.txt'
	write_data = open(halo_file, 'w')

	for j in range(len(redshift)):	

		if j == 0:

			halofile = glob.glob(halo_path + '/' + str('%03d' %position[j]) + '/*AHF_halos')[0]
			snapshot_dir = snapshot_path + '/snapdir_' + str('%03d' %position[j])

			if dummy == 178:
				star = ot.readsnap_compact(snapshot_dir, snum=position[j], ptype=4, cosmological=1)
				header = ot.readsnap_compact(snapshot_dir, snum=position[j], ptype=0, header_only=1)		
			else:
				star = ot.readsnap(snapshot_dir, snum=position[j], ptype=4, cosmological=1)
				header = ot.readsnap(snapshot_dir, snum=position[j], ptype=0, header_only=1)

			hinv = 1.0 / header["hubble"]
			a_scale = 1.0 / (1.0 + header["redshift"])
			constant = a_scale * hinv

			ID, mhalo, rvir, fMhires, mstar = np.genfromtxt(halofile, delimiter='', usecols=(0,3,11,37,64), skip_header=1, unpack=True)
			data = [[a,b,c,d,e] for a,b,c,d,e in zip(ID, mhalo, rvir, fMhires, mstar) if a == halo_ID]
			center = np.array(ot.get_haloCenter(halofile, halo_ID)) * constant

			star["p"] -= center

			ID_new = int(data[0][0])
			mhalo_new = data[0][1] * hinv
			rvir_new = data[0][2] * constant
			fMhires_new = data[0][3]
			mstar_new = data[0][4] * hinv

			flag, m_halo, m_star = constraints(star["p"], star["m"], rvir_new, mhalo_new, fMhires_new, mstar_new)
			new_ID_list = [halo_ID]
			new_mhalo_list = [m_halo]
			new_mstar_list = [m_star]

		else:

			filename = glob.glob(halo_path + '/' + str('%03d' %position[j]) + '/*AHF_mtree_idx')[0]
			halofile = glob.glob(halo_path + '/' + str('%03d' %position[j]) + '/*AHF_halos')[0]
			snapshot_dir = snapshot_path + '/snapdir_' + str('%03d' %position[j])

			# STAR AND HEADER DATA READ
			if dummy == 178:
				star = ot.readsnap_compact(snapshot_dir, snum=position[j], ptype=4, cosmological=1)
				header = ot.readsnap_compact(snapshot_dir, snum=position[j], ptype=0, header_only=1)		
			else:
				star = ot.readsnap(snapshot_dir, snum=position[j], ptype=4, cosmological=1)
				header = ot.readsnap(snapshot_dir, snum=position[j], ptype=0, header_only=1)

			hinv = 1.0 / header["hubble"]
			a_scale = 1.0 / (1.0 + header["redshift"])
			constant = a_scale * hinv

			# READ IDX DATA AND FILTER 
			ID_0, ID_1 = np.genfromtxt(filename, delimiter='', usecols=(0,1), skip_header=1, unpack=True, dtype=int)

			# READ HALO INFO
			ID_halo, mhalo_halo, rvir_halo, fMhires_halo, mstar_halo = np.genfromtxt(halofile, delimiter='', usecols=(0,3,11,37,64), skip_header=1, unpack=True)
			zip_halo = zip(ID_halo, mhalo_halo, rvir_halo, fMhires_halo, mstar_halo) 

			iHalo_list = []
			mhalo_list = []
			mstar_list = []
			flag_lastline = 0

			for iHalo, mhalo, mstar in zip(new_ID_list, new_mhalo_list, new_mstar_list):
	
				temp_iHalo_list = [a for a,b in zip(ID_0,ID_1) if b == iHalo]
				temp_flag_list = []
				temp_mhalo_list = []
				temp_mstar_list = []

				for halo_ID in temp_iHalo_list:

					center = np.array(ot.get_haloCenter(halofile, halo_ID)) * constant
					pos = star["p"] - center
					data = [[a,b,c,d,e] for a,b,c,d,e in zip_halo if a == halo_ID]

					ID_new = int(data[0][0])
					mhalo_new = data[0][1] * hinv
					rvir_new = data[0][2] * constant
					fMhires_new = data[0][3]
					mstar_new = data[0][4] * hinv

					flag, m_halo, m_star = constraints(pos, star["m"], rvir_new, mhalo_new, fMhires_new, mstar_new)
					temp_flag_list.append(flag)
					temp_mhalo_list.append(m_halo)
					temp_mstar_list.append(m_star)

				# filter candidates with their flag values
				temp_iHalo_list = [a for a,b in zip(temp_iHalo_list, temp_flag_list) if b == 1]
				temp_mhalo_list = [a for a,b in zip(temp_mhalo_list, temp_flag_list) if b == 1]
				temp_mstar_list = [a for a,b in zip(temp_mstar_list, temp_flag_list) if b == 1]

				if temp_iHalo_list:
					write_data.write('%-6.3f %-5i %-10.3e %-10.3e %-6.3f %-30s \n' %(redshift[j-1], iHalo, mstar, mhalo, redshift[j], ' '.join(map(str, temp_iHalo_list))))
				else:	
					write_data.write('%-6.3f %-5i %-10.3e %-10.3e %-6.3f %-30s \n' %(redshift[j-1], iHalo, mstar, mhalo, redshift[j], '-1'))

				iHalo_list += temp_iHalo_list
				mhalo_list += temp_mhalo_list	
				mstar_list += temp_mstar_list
	
				if j == len(redshift)-1:
					if temp_iHalo_list:
						flag_lastline = 1
				#	for iHalo, mhalo, mstar in zip(temp_iHalo_list, temp_mhalo_list, temp_mstar_list):
				#		print '%-6.3f %-5i %-10.3e %-10.3e %-6.3f %-30s \n' %(redshift[j], iHalo, mstar, mhalo, redshift[j]+0.4, '-1')

			new_ID_list = iHalo_list
			new_mhalo_list = mhalo_list
			new_mstar_list = mstar_list
			
			if flag_lastline == 1:
				for iHalo, mhalo, mstar in zip(new_ID_list, new_mhalo_list, new_mstar_list):
					write_data.write('%-6.3f %-5i %-10.3e %-10.3e %-6.3f %-30s \n' %(redshift[-1], iHalo, mstar, mhalo, redshift[-1]+0.4, '-1'))

	write_data.close()

def timeline(input_path, halo_ID):

	redshift, place = ot.z(input_path)
	redshift = redshift[::-1]		

	z_1 = []
	position = []
	ID = []
	mstar = []
	mhalo = []
	z_2 = []
	descendant = []	

	filename = input_path + '/halo_' + str(halo_ID) + '.txt'
	file_halo = open(filename, 'r')
	for line in file_halo.readlines():
		data = map(float, np.array(line.split()))
		z_1.append(data[0])
		position.append(redshift.tolist().index(data[0]))
		ID.append(int(data[1]))
		mstar.append(data[2])
		mhalo.append(data[3])
		z_2.append(data[4])
		descendant.append(int(data[5]))
	file_halo.close()

	timeline_file = input_path + '/timeline_' + str(halo_ID) + '.txt'
	write_data = open(timeline_file, 'w')	

	ID_filter = halo_ID
	for i in range(len(redshift)):

		data = [[a,b,c,d,e] for a,b,c,d,e in zip(z_1,ID,mstar,mhalo,descendant) if a == redshift[i] if b == ID_filter]

		if data:
			if data[0] != -1:
				write_data.write('%-6.3f %-5i %-10.3e %-10.3e \n' %(data[0][0],data[0][1],data[0][2],data[0][3]))
				#print '%-6.3f %-5i %-10.3e %-10.3e' %(data[0][0],data[0][1],data[0][2],data[0][3])
				ID_filter = data[0][4]
			else:
				break

	write_data.close()

##############################
######### MAIN BODY ##########
##############################
halo_path = str(sys.argv[1])
snapshot_path = str(sys.argv[2])
input_path = str(sys.argv[3])
dummy = int(sys.argv[4])
halo_ID = int(sys.argv[5])

tree_list(snapshot_path, input_path, halo_path, halo_ID, dummy)
timeline(input_path, halo_ID)
