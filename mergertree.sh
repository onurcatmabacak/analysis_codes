#!/bin/bash
module load hydra
export OMP_NUM_THREADS=32

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

#MassiveFIRE
#0  B100_N512_M3e13_TL00001_baryon_toz2_HR_9915
#1  B100_N512_M3e13_TL00003_baryon_toz2_HR_9915
#2  B100_N512_M3e13_TL00004_baryon_toz2_HR_9915
#3  B100_N512_TL00002_baryon_toz2
#4  B100_N512_TL00007_baryon_toz2_HR
#5  B100_N512_TL00009_baryon_toz2_HR
#6  B100_N512_TL00011_baryon_toz2_HR
#7  B100_N512_TL00013_baryon_toz2_HR
#8  B100_N512_TL00018_baryon_toz2_HR
#9  B100_N512_TL00029_baryon_toz2_HR
#10 B100_N512_TL00031_baryon_toz2_HR
#11 B100_N512_TL00037_baryon_toz0_HR_9915
#12 B100_N512_TL00113_baryon_toz0_HR_9915
#13 B100_N512_TL00206_baryon_toz0_HR_9915
#14 B100_N512_TL00217_baryon_toz2
#15 B100_N512_TL00223_baryon_toz2_HR
#16 B100_N512_TL00228_baryon_toz2
#17 B100_N512_TL00236_baryon_toz2
#18 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915
#19 B400_N512_z6_TL00000_baryon_toz6_HR_9915
#20 B400_N512_z6_TL00001_baryon_toz6_HR_9915
#21 B400_N512_z6_TL00002_baryon_toz6_HR_9915
#22 B400_N512_z6_TL00005_baryon_toz6_HR_9915
#23 B400_N512_z6_TL00006_baryon_toz6_HR_9915
#24 B400_N512_z6_TL00013_baryon_toz6_HR_9915
#25 B400_N512_z6_TL00017_baryon_toz6_HR_9915
#26 B400_N512_z6_TL00021_baryon_toz6_HR_9915
#27 B762_N1024_z6_TL00000_baryon_toz6_HR
#28 B762_N1024_z6_TL00001_baryon_toz6_HR
#29 B762_N1024_z6_TL00002_baryon_toz6_HR

#MassiveFIRE2
#30 B762_N1024_z6_TL00000_baryon_toz6_HR 
#31 B762_N1024_z6_TL00001_baryon_toz6_HR 
#32 B762_N1024_z6_TL00002_baryon_toz6_HR
#33 h113_HR_sn1dy300ro100ss
#34 h206_HR_sn1dy300ro100ss
#35 h29_HR_sn1dy300ro100ss
#36 h2_HR_sn1dy300ro100ss

SIM_NAME=(B100_N512_M3e13_TL00001_baryon_toz2_HR_9915 B100_N512_M3e13_TL00003_baryon_toz2_HR_9915 B100_N512_M3e13_TL00004_baryon_toz2_HR_9915 B100_N512_TL00002_baryon_toz2 B100_N512_TL00007_baryon_toz2_HR B100_N512_TL00009_baryon_toz2_HR B100_N512_TL00011_baryon_toz2_HR B100_N512_TL00013_baryon_toz2_HR B100_N512_TL00018_baryon_toz2_HR B100_N512_TL00029_baryon_toz2_HR B100_N512_TL00031_baryon_toz2_HR B100_N512_TL00037_baryon_toz0_HR_9915 B100_N512_TL00113_baryon_toz0_HR_9915 B100_N512_TL00206_baryon_toz0_HR_9915 B100_N512_TL00217_baryon_toz2 B100_N512_TL00223_baryon_toz2_HR B100_N512_TL00228_baryon_toz2 B100_N512_TL00236_baryon_toz2 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915 B400_N512_z6_TL00000_baryon_toz6_HR_9915 B400_N512_z6_TL00001_baryon_toz6_HR_9915 B400_N512_z6_TL00002_baryon_toz6_HR_9915 B400_N512_z6_TL00005_baryon_toz6_HR_9915 B400_N512_z6_TL00006_baryon_toz6_HR_9915 B400_N512_z6_TL00013_baryon_toz6_HR_9915 B400_N512_z6_TL00017_baryon_toz6_HR_9915 B400_N512_z6_TL00021_baryon_toz6_HR_9915 B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR h113_HR_sn1dy300ro100ss h206_HR_sn1dy300ro100ss h29_HR_sn1dy300ro100ss h2_HR_sn1dy300ro100ss)
SIM_SUITE=(MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2)

# MDC settings (default)
#AHF_VERSION=AHF-v1.0-094
#AHF_FILEDIR=AHF
# COM settings
#AHF_VERSION=AHF-v1.0-094_com
#AHF_FILEDIR=AHF_com

AHF_VERSION=AHF-v1.0-094_com
AHF_FILEDIR=AHF_com
HALO=halo
MEMORY_PER_JOB=50
LIST=(27 28 29)

for i in ${LIST[@]}
do
	AHF=/bulk1/feldmann/Onur/ahf
	FILE_LIST=(/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/$AHF_FILEDIR/HR/${SIM_NAME[$i]}/halo/*/*particles)
	NUMOFLINES=${#FILE_LIST[@]}	
	NJOBS=20
	NCPU=2
	let "MEMORY=$NCPU*$MEMORY_PER_JOB"

	for ((j=0; j<$NUMOFLINES; j=j+NJOBS))
	do
		let "k=$j+$NJOBS"

		if [ "$k" -ge "$NUMOFLINES" ];
		then
			let "k=$NUMOFLINES"
			let "NJOBS=$k-$j+1"
		fi

		START=$j
		let "FINISH=$k"
		let "DIFF=$NJOBS+1"
		
		if [ "$k" -eq "$NUMOFLINES" ];
		then
			let "DIFF=$NJOBS-1"
		fi

		###### PREPARING MERGERTREE FILE
		MERGERTREE=`printf "mergertree_%s_%03d_%03d" ${SIM_NAME[$i]} $START $FINISH`
		rm $MERGERTREE
		echo $DIFF > $MERGERTREE
		echo " " >> $MERGERTREE 

		for ((k=$START; k<=$FINISH; k++))	
		do
			echo ${FILE_LIST[$k]} >> $MERGERTREE
		done
		
		echo " " >> $MERGERTREE 

		for ((k=$START; k<=$FINISH; k++))	
		do
			PREFIX=${FILE_LIST[$k]::(-10)}
			echo $PREFIX >> $MERGERTREE
		done
		################################

		JOBSUB=`printf "jobsub_mergertree_%s_%03d_%03d.sh" ${SIM_NAME[$i]} $START $FINISH`

		printf "#!/bin/bash -l \n" > $JOBSUB
		printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
		printf "#SBATCH --time=24:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
		printf "#SBATCH --ntasks=1 --cpus-per-task=$NCPU \n" >> $JOBSUB
		printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
		printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
		printf "#======START===== \n" >> $JOBSUB
		printf "srun $AHF/$AHF_VERSION/bin/MergerTree < $MERGERTREE \n" >> $JOBSUB 
		printf "#=====END==== \n" >> $JOBSUB
		sbatch $JOBSUB 
	done
	
done

