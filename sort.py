import numpy as np
import h5py
import glob 

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

i_list = range(37)
extra_list = [12, 13, 33, 34, 35, 36]

regime_list = ['cold', 'total']
radius_list = ['0.1', '0.2', '0.5', '1.0', 'rvir']

ahf_centering = 'COM' # MDC or COM

data = h5py.File(ahf_centering + '/BDD_data.hdf5','w')

for i in i_list:
	print sim_name[i]

	if i in extra_list:
		read_path = '/bulk1/feldmann/Onur/' + ahf_centering + '/' + sim_suite[i] + '/' + sim_name[i]
		remove = len(read_path) + 6
		filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

		halo_ID_list = []
		for f in filelist:
			halo_ID_list.append(f[remove:-4])

		halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
	else:
		halo_ID_list = [0]

	print halo_ID_list

	for halo_id in halo_ID_list:
		for regime in regime_list:

			fname = '/bulk1/feldmann/Onur/' + ahf_centering + '/' + sim_suite[i] + '/' + sim_name[i] + '/halo_' + str(halo_id) + '.txt'
			sort_z, sort_ID = np.genfromtxt(fname, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
			sort_ID = sort_ID.astype(int)
			sort_all = zip(sort_z, sort_ID)

			for radius_name in radius_list:

				if radius_name == radius_list[-1]:
					filename = '/bulk1/feldmann/Onur/' + ahf_centering + '/bdd_data/' + sim_suite[i] + '/' + sim_name[i] + '_rvir_' + regime + '_halo_' + str(halo_id) + '.txt'
					halo_ID, z_1, ID, radius, rvir_hgm, rvir_hsm, rvir_hsf, mgas, mstar, mhalo, mbulge, mdisk, fbulge, fdisk, inflow, outflow, bh_vel, vel_disp, num_new_stars, sfr, ssfr, mean_temp, mean_rho, mean_gas_surf_density, mean_star_surf_density, mean_cs, mean_vel, mean_vel_phi = np.genfromtxt(filename, delimiter='', usecols=range(28), skip_header=0, unpack=True)
				else:
					filename = '/bulk1/feldmann/Onur/' + ahf_centering + '/bdd_data/' + sim_suite[i] + '/' + sim_name[i] + '_' + radius_name + '_kpc_' + regime + '_halo_' + str(halo_id) + '.txt'
					halo_ID, z_1, ID, mgas, mstar, mhalo, mbulge, mdisk, fbulge, fdisk, inflow, outflow, bh_vel, vel_disp, num_new_stars, sfr, ssfr, mean_temp, mean_rho, mean_gas_surf_density, mean_star_surf_density, mean_cs, mean_vel, mean_vel_phi = np.genfromtxt(filename, delimiter='', usecols=range(24), skip_header=0, unpack=True)

				halo_ID = halo_ID.astype(int)
				ID = ID.astype(int)
				num_new_stars = num_new_stars.astype(int)

				inds = []
				sort_me = zip(z_1, ID)
				for line in sort_all:
					for xxx in sort_me:
						if xxx == line:
							inds.append(sort_me.index(xxx))

				halo_ID = halo_ID[inds][::-1]
				z_1 = z_1[inds][::-1]
				ID = ID[inds][::-1]

				if radius_name == radius_list[-1]:
					radius = radius[inds][::-1]
					rvir_hgm = rvir_hgm[inds][::-1]
					rvir_hsm = rvir_hsm[inds][::-1]
					rvir_hsf = rvir_hsf[inds][::-1]
				
				mgas = mgas[inds][::-1]
				mstar = mstar[inds][::-1]
				mhalo = mhalo[inds][::-1]
				mbulge = mbulge[inds][::-1]
				mdisk = mdisk[inds][::-1]
				fbulge = fbulge[inds][::-1]
				fdisk = fdisk[inds][::-1]
				inflow = inflow[inds][::-1]
				outflow = outflow[inds][::-1]
				bh_vel = bh_vel[inds][::-1]
				vel_disp = vel_disp[inds][::-1]
				num_new_stars = num_new_stars[inds][::-1]
				sfr = sfr[inds][::-1]
				ssfr = ssfr[inds][::-1]
				mean_temp = mean_temp[inds][::-1]
				mean_rho = mean_rho[inds][::-1]
				mean_gas_surf_density = mean_gas_surf_density[inds][::-1]
				mean_star_surf_density = mean_star_surf_density[inds][::-1]
				mean_cs = mean_cs[inds][::-1]
				#mean_nh = mean_nh[inds][::-1]
				mean_vel = mean_vel[inds][::-1]
				mean_vel_phi = mean_vel_phi[inds][::-1]

				prefix = 'halo_' + str(halo_id) + '/' + regime + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + radius_name

				if radius_name == radius_list[-1]:
					data[prefix + '/radius'] = radius
					data[prefix + '/rvir_hgm'] = rvir_hgm
					data[prefix + '/rvir_hsm'] = rvir_hsm
					data[prefix + '/rvir_hsf'] = rvir_hsf

				data[prefix + '/z_1'] = z_1
				data[prefix + '/ID'] = ID
				data[prefix + '/mgas'] = mgas
				data[prefix + '/mstar'] = mstar
				data[prefix + '/mhalo'] = mhalo
				data[prefix + '/mbulge'] = mbulge
				data[prefix + '/mdisk'] = mdisk
				data[prefix + '/fbulge'] = fbulge
				data[prefix + '/fdisk'] = fdisk
				data[prefix + '/inflow'] = inflow
				data[prefix + '/outflow'] = outflow
				data[prefix + '/bh_vel'] = bh_vel
				data[prefix + '/vel_disp'] = vel_disp
				data[prefix + '/num_new_stars'] = num_new_stars
				data[prefix + '/sfr'] = sfr
				data[prefix + '/ssfr'] = ssfr
				data[prefix + '/mean_temp'] = mean_temp
				data[prefix + '/mean_rho'] = mean_rho 
				data[prefix + '/mean_gas_surf_density'] = mean_gas_surf_density
				data[prefix + '/mean_star_surf_density'] = mean_star_surf_density
				data[prefix + '/mean_cs'] = mean_cs
				#data[prefix + '/mean_nh'] = mean_nh
				data[prefix + '/mean_vel'] = mean_vel
				data[prefix + '/mean_vel_phi'] = mean_vel_phi

data.close()	
