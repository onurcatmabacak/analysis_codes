#!/bin/bash
CODE_PATH=/bulk1/feldmann/Onur
cp -r $CODE_PATH/*.{py,sh} $CODE_PATH/Paper_1 $CODE_PATH/analysis_codes
rm jobsub*.sh
git add --all
git commit -m "`date +'%d.%m.%Y %H:%M:%S'`"
git push
