import os
import re
import glob
import h5py
import numpy as np
from astropy import constants as const
from astropy import units as u
from astropy.cosmology import FlatLambdaCDM
from scipy.optimize import curve_fit, minimize_scalar, fmin

def global_variables():
	global gamma
	global mp
	global k_b
	global grav_constant
	global light_speed
	global sigma	
	global m_sun
	global yr_s_conversion
	global kpc_cm_conversion
	global cosmo
	global cold_gas_temp
	global z_he
	global alpha_t
	global alpha
	global eta
	global comoving   

	gamma = 5.0/3.0
	mp = const.m_p.cgs.value
	k_b = const.k_B.cgs.value
	grav_constant = const.G.cgs.value
	light_speed = const.c.cgs.value
	sigma = 6.652458734e-25
	m_sun = const.M_sun.cgs.value
	yr_s_conversion = 3.15576e7
	kpc_cm_conversion = 3.086e21
	cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284) 
	cold_gas_temp = 1e5
	z_he = 0.76
	alpha_t = 5.0
	eta = 0.1
	comoving = 1

def time(i):
	return cosmo.age(i).value * 1e9 # yr

def calculate_temperature(u, ne):
	u = u * 1e10 # cm/s
	y_he = z_he / (4.0 * (1.0 - z_he)) # z_he helium metallicity
	mu = (1.0 + 4.0 * y_he) / (1.0 + y_he + ne) # ne is electron abundance
	t = mu * mp * (gamma - 1.0) * u / k_b # u is internal energy 
	return t # K

def calculate_sound_speed(u, ne):
	c_s = np.sqrt(gamma * calculate_temperature(u, ne) * k_b / mp) * 1e-5
	return c_s # km/s

def calculate_mdot_tremmel(m_bh, mean_cs, mean_vel, mean_vel_phi, mgas, radius):
	# Tremmel+17 modified Bondi Model

	if mgas == 0.0:

		mdot = 0.0

	else:
		G = 6.67e-8
		beta = 2.0
		rho_crit = 2.45e-4 #2.44e6 solarmass kpc-3 or 2.45e-4 g cm-3

		km_to_cm = 1e5
		kpc_to_cm = 3.086e21
		solar_mass = 2e33
		yr_to_s = 3.15e7

		mgas *= solar_mass
		m_bh *= solar_mass
		radius *= kpc_to_cm
		mean_cs *= km_to_cm
		mean_vel *= km_to_cm
		mean_vel_phi *= km_to_cm

		mean_rho = mgas / (4.0 * np.pi / 3.0 * (radius**3.0)) # density is in cgs
		if mean_rho > rho_crit:
			#alpha = (mean_rho / rho_crit)**beta
			alpha = 1.0
		else:
			alpha = 1.0
	
		if mean_vel_phi < mean_vel:
			v = ((mean_vel**2.0) + (mean_cs**2.0))**1.5
			mdot = alpha * 4.0 * np.pi * ((G * m_bh)**2.0) * mean_rho / v
		else:
			v = ((mean_vel_phi**2.0) + (mean_cs**2.0))**2.0
			mdot = alpha * 4.0 * np.pi * ((G * m_bh)**2.0) * mean_rho * mean_cs / v

		mdot *= yr_to_s / solar_mass
		
	return mdot # m_sun/yr

def calculate_mdot_bondi(m_bh, mean_cs, mean_vel, mgas, radius):

	if mgas == 0.0:			# if there is no gas 
		mdot = 0.0
	else:					  	# if there is gas
		G = 6.67e-8 
		beta = 2.0
		rho_crit = 2.45e-4 #2.44e6 solarmass kpc-3 or 2.45e-4 g cm-3

		km_to_cm = 1e5
		kpc_to_cm = 3.086e21
		solar_mass = 2e33
		yr_to_s = 3.15e7

		mgas *= solar_mass
		m_bh *= solar_mass
		radius *= kpc_to_cm
		mean_cs *= km_to_cm
		mean_vel *= km_to_cm

		mean_rho = mgas / (4.0 * np.pi / 3.0 * (radius**3.0))	
		if mean_rho > rho_crit:
			#alpha = (mean_rho / rho_crit)**beta
			alpha = 1.0
		else:
			alpha = 1.0
		
		v = ((mean_cs**2.0) + (mean_vel**2.0))**1.5
		mdot = alpha * 4.0 * np.pi * ((G * m_bh)**2.0) * mean_rho / v
		mdot *= yr_to_s / solar_mass

	return mdot 

def calculate_mdot_hobbs(m_bh, mtot, mean_cs, mean_vel, mgas, radius):
	if mgas == 0.0:			# if there is no gas 
		mdot = 0.0
	else:					  	# if there is gas
		G = 6.67e-8 
		gama = 1.12

		km_to_cm = 1e5
		kpc_to_cm = 3.086e21
		solar_mass = 2e33
		yr_to_s = 3.15e7

		mgas *= solar_mass
		m_bh *= solar_mass
		mtot *= solar_mass
		radius *= kpc_to_cm
		mean_cs *= km_to_cm
		mean_vel *= km_to_cm

		mean_rho = mgas / (4.0 * np.pi / 3.0 * (radius**3.0))	
		sigma_vel = np.sqrt(G * (mtot+m_bh) / radius)
		v = ((mean_cs**2.0) + (sigma_vel**2.0))**1.5
		mdot = 4.0 * np.pi * gama * (G**2.0) * ((mtot+m_bh)**2.0) * mean_rho / v
		mdot *= yr_to_s / solar_mass

	return mdot 

def calculate_mdot_sfr(sfr):
	mdot = sfr / 500.0
	return mdot 

def calculate_mdot_dyn(mtot, mgas, radius, alpha):

	if mgas == 0.0:
		mdot = 0.0
	else:
		G = grav_constant * (yr_s_conversion**2.0) * m_sun / (kpc_cm_conversion**3.0)
		t_dyn = np.sqrt((radius**3.0) / (2.0 * G * mtot)) * np.pi / 2.0
		mdot = alpha * mgas / t_dyn

	return mdot

def calculate_mdot_torque(f_disk, m_star, m_gas, m_bh, rvir, mass_retention): # r in kpc
	if (m_gas == 0.0 or f_disk == 0.0):
		mdot = 0.0
	else:
		m_tot = m_gas + m_star
		f_gas = m_gas / m_tot
		f_0 = 0.31 * (f_disk ** 2.0) * ((m_tot / 1e09) ** (-1.0/3.0))
		mdot = (1.0 - eta) * mass_retention * alpha_t * (f_disk ** (5.0/2.0)) * ((m_bh * 1e-8) ** (1.0/6.0)) * (m_tot * 1e-9) * ((rvir / 0.1) ** (-3.0/2.0)) * ((1.0 + f_0/f_gas) ** (-1.0))
		#print '%-10.3f %-10.3f %-10.3f %-10.3f %-10.3e %-10.3e %-10.3f %-10.3f %-10.3f %-10.3e' %(1.0-eta, mass_retention, alpha_t, f_disk, m_bh, m_tot, rvir, f_0, f_gas, mdot)
	return mdot # m_sun/yr

def calculate_mdot_eddington(m_bh, eta):
	mdot = 4.0 * np.pi * grav_constant * m_bh * m_sun * mp * yr_s_conversion / (eta * sigma * light_speed * m_sun) # cgs to M_sun / yr conversion
	return mdot # m_sun/yr

def z(input_path):
    fname = input_path + '/zred_pos_list'
    z, position = np.genfromtxt(fname, delimiter='', usecols=(0,1), unpack=True)
    position = position.astype(int)

	#sel = (position >= dummy)
	#z = z[sel]
	#position = position[sel]

    return z, position

def cross(x,y):
	#return np.cross(x,y,axis=1)
	c=0.*x
	c[:,0] = x[:,1]*y[:,2] - x[:,2]*y[:,1]
	c[:,1] =-x[:,0]*y[:,2] + x[:,2]*y[:,0]
	c[:,2] = x[:,0]*y[:,1] - x[:,1]*y[:,0]
	return c

def get_angular_momentum(x, y, z, vx, vy, vz, m, radius, normalize=False):
	"""Return angular momentum vector

	Keyword arguments:
	x,y,z,vx,vy,vz,m -- lists of position, velocity, mass of particles
	radius		   -- only include particles in this radius around [0,0,0]
	normalize		-- if True, normalize length of ang. mom. vector to 1
	"""
	# copy references
	for q in [x, y, z, vx, vy, vz]: 
		q = np.array(q, dtype='d')+1.0e-10
	# copy list because modify below
	m = np.array(m, dtype='d', copy=True)+1.0e-10
	
	r = np.sqrt(x*x + y*y + z*z) 
	nstars = sum(r <= radius)
	if nstars == 0:
		return [0, 0, 1], 0

	m[r > radius] = 0
	pos = np.zeros([x.size, 3], dtype='d')
	pos[:, 0] = x
	pos[:, 1] = y
	pos[:, 2] = z
	vel = np.zeros([vx.size, 3], dtype='d')
	vel[:, 0] = vx 
	vel[:, 1] = vy 
	vel[:, 2] = vz 
	j = cross(pos, vel) 
	j[:, 0] *= m 
	j[:, 1] *= m 
	j[:, 2] *= m
	 
	j_tot = np.sum(j, axis=0)
   
	if normalize:
		j_tot /= np.linalg.norm(j_tot)
		
	return j_tot, nstars

def select_particles(ppp, selection):
	"""Copy selected subset of particles"""
	sel_ppp = {}
	for property in ppp:
		if property == 'k':
			sel_ppp[property] = ppp[property]
		else:
			sel_ppp[property] = np.copy(ppp[property][selection])
	return sel_ppp

def exclude_particles(ppp, inds):
	"""Exclude and remove selected subset of particles"""
	sel_ppp = {}
	for property in ppp:
		if property == 'k':
			sel_ppp[property] = ppp[property]
		else:
			print property, "started", len(ppp[property])
			sel_ppp[property] = np.delete(ppp[property], inds, axis=0)
			print property, "finished", len(sel_ppp[property])
	return sel_ppp

def dict_filter(a,c,key):
	final = []
	for i in c[key]:
		sel = (a[key] == i)
		a_sel = select_particles(a,sel)
		final.append(a_sel)

	onur = {}
	onur_new = {}
	for d in final:
		for k, v in d.iteritems():  
			onur.setdefault(k, []).append(v)
	onur['k'] = 1

	for property in onur:
		if property == 'k':
			onur_new[property] = onur[property]
		else:
			dummy_list = []
			for j in onur[property]:
				dummy_list.append(j.tolist()[0])
			onur_new[property] = dummy_list
	return onur_new

def dict_converter(dictionary):
	dicty = {}
	for keys in dictionary:
		dicty[keys] = dictionary[keys].value
	return dicty

def filter_inner(data_gas,data_star,radius):

	radius_gas = np.sqrt(np.sum(np.square(data_gas["Coordinates"]), axis=1))
	#radius_dm = np.sqrt(np.sum(np.square(data_dm["Coordinates"]), axis=1))
	radius_star = np.sqrt(np.sum(np.square(data_star["Coordinates"]), axis=1))
	sel_gas1 = (radius_gas < radius)
	#sel_dm = (radius_dm < radius)
	sel_star = (radius_star < radius)

	data_gas = select_particles(data_gas,sel_gas1)
	
	# extra filtering for polluted data, i.e. sometimes helium mass fraction is above 1.0 and that points are needed to be removed
	if len(data_gas["Masses"]) != 0:
		ne = data_gas["ElectronAbundance"]
		u = data_gas["InternalEnergy"]
		temperature = np.array([calculate_temperature(u1,ne1) for u1,ne1 in zip(u,ne)])
		sel_gas = (temperature < cold_gas_temp) & (temperature > 0.0)
		data_gas = select_particles(data_gas,sel_gas)

	#data_dm = select_particles(data_dm,sel_dm)
	data_star = select_particles(data_star,sel_star)
	return data_gas,data_star

def calculate_half_mass_radius(data_gas, data_star, mgas, mstar, center, rvir):
	mtotal = mgas + mstar
	mass = 0.0
	ratio = mass / mtotal

	radius_gas = np.sqrt(np.sum(np.square(data_gas['Coordinates']), axis=1))
	radius_star = np.sqrt(np.sum(np.square(data_star['Coordinates']), axis=1))
	
	radius_min = 0.0
	radius_max = rvir
	while np.abs(ratio - 0.5) >= 0.01:

		r = (radius_min + radius_max) / 2.0
		data_gas_sel = data_gas["Masses"][(radius_gas <= r)]
		data_star_sel = data_star["Masses"][(radius_star <= r)]
		mass = (np.sum(data_gas_sel) + np.sum(data_star_sel)) 
		ratio = mass / mtotal
		if ratio > 0.51:
			radius_min = radius_min
			radius_max = r
		elif ratio < 0.49:
			radius_min = r  
			radius_max = radius_max
		else:
			radius = r

	return radius

def weighted_average(ghost,vice_ghost):
	if len(ghost) == 0:
		new_ghost = 0
	else:
		if type(ghost[0]) == np.ndarray:
			new_ghost =np.sum((vice_ghost * ghost.T).T, axis=0) / np.sum(vice_ghost)
		else:# type(ghost[0]) == 'numpy.float64':
			new_ghost =np.sum((vice_ghost * ghost.T).T) / np.sum(vice_ghost)
	return new_ghost

def magnitude(vector):
	return np.sqrt(np.sum(i*i for i in vector))

def calculate_velocities(data_gas, bh_vel):

	if len(data_gas["Masses"]) == 0:
		velocity = 0.0
		soundspeed = 0.0
	else:
		u = data_gas["InternalEnergy"]
		ne = data_gas["ElectronAbundance"]
		velocity = data_gas["Velocities"] - bhvel
		sound_speed = np.array([calculate_sound_speed(u1,ne1) for u1,ne1 in zip(u,ne)])
		soundspeed = weighted_average(sound_speed,data_gas["Masses"]) * 1e-5 # km/s
		velocity = magnitude(weighted_average(velocity, data_gas["Masses"]))

	return soundspeed, velocity

def MERGER(z_1, ID, z_2, descendant, halo_path, redshift, place, z_1_timeline, ID_timeline, fdisk, mhalo, mstar, mgas, radius, mean_cs, mean_vel, mean_vel_phi, mean_rho, sfr, mass_retention, initial_seed_mass, merger, supp=0, filter_for_timeline=0):

	alpha_dyn = np.array([1e0, 5e-1, 2.5e-1, 1e-1, 5e-2, 1e-2, 1e-3, 2e-4, 1e-4])
	radius_dummy = radius
	mtot = mstar + mgas + mhalo

	f_torque_edd = []
	f_bondi_edd = []
	f_tremmel_edd = []
	f_hobbs_edd = []
	f_dyn_edd = [[] for xxx in range(len(alpha_dyn))]
	f_sfr_edd = []

	mdot_torque = []
	mdot_bondi = []
	mdot_tremmel = []
	mdot_hobbs = []
	mdot_dyn = [[] for xxx in range(len(alpha_dyn))]
	mdot_sfr = []

	m_bh_torque = []
	m_bh_bondi = []
	m_bh_tremmel = []
	m_bh_hobbs = []
	m_bh_dyn = [[] for xxx in range(len(alpha_dyn))]
	m_bh_sfr = []

	mass_seed = []

	#soundspeed, velocity = calculate_velocities(data_gas)

	# Follow the tree 
	for i in range(len(z_1)):

		if radius_dummy != 10.0:
			radius = radius
		else:
			position = place[redshift.tolist().index(z_1[i])]
			halodir = glob.glob(halo_path + '/' + str('%03d' %position) + '/*AHF_halos')[0]
			rvir = get_haloRvir(halodir, ID[i])[0]
			radius = 0.1 * rvir

		if z_1[i] == redshift[-1]:
			time_between_snapshots = 2e7 #time(1.98) - time(2.0)
		else:
			inds = list(redshift).index(z_1[i])
			time_between_snapshots = time(redshift[inds+1]) - time(redshift[inds])
		# read values from bulge file
		#bh_vel = calculate_bh_vel(data_gas[i], data_star[i])
		#m_b,m_d,f_b,f_d,vel_bulge = calculate_bulge_mass(data_star[i], m_g[i], m_s[i], bh_vel, radius)
		################

		# if structure is formed at that specific redshift
		if descendant[i][0] == -1:							  

			m_seed_torque_total = initial_seed_mass
			m_seed_bondi_total = initial_seed_mass
			m_seed_tremmel_total = initial_seed_mass
			m_seed_hobbs_total = initial_seed_mass

			m_seed_dyn_total = []
			for xxx in range(len(alpha_dyn)):
				m_seed_dyn_total.append(initial_seed_mass)

			m_seed_sfr_total = initial_seed_mass
			mass_seed_total = initial_seed_mass

			mdot_t = calculate_mdot_torque(fdisk[i], mstar[i], mgas[i], m_seed_torque_total, radius, mass_retention) 
			mdot_b = calculate_mdot_bondi(m_seed_bondi_total, mean_cs[i], mean_vel[i], mgas[i], radius)
			mdot_bt = calculate_mdot_tremmel(m_seed_tremmel_total, mean_cs[i], mean_vel[i], mean_vel_phi[i], mgas[i], radius)
			mdot_h = calculate_mdot_hobbs(m_seed_hobbs_total, mtot[i], mean_cs[i], mean_vel[i], mgas[i], radius)

			mdot_d = []
			for xxx in range(len(alpha_dyn)):
				mdot_d.append(calculate_mdot_dyn(mtot[i], mgas[i], radius, alpha_dyn[xxx]))

			mdot_s = calculate_mdot_sfr(sfr[i])

			mdot_edd_t = calculate_mdot_eddington(m_seed_torque_total, eta)
			mdot_edd_b = calculate_mdot_eddington(m_seed_bondi_total, eta)
			mdot_edd_bt = calculate_mdot_eddington(m_seed_tremmel_total, eta)
			mdot_edd_h = calculate_mdot_eddington(m_seed_hobbs_total, eta)
		
			mdot_edd_d = []
			for xxx in range(len(alpha_dyn)):
				mdot_edd_d.append(calculate_mdot_eddington(m_seed_dyn_total[xxx], eta))

			mdot_edd_s = calculate_mdot_eddington(m_seed_sfr_total, eta)

			f_t_edd = mdot_t / mdot_edd_t
			f_b_edd = mdot_b / mdot_edd_b
			f_bt_edd = mdot_bt / mdot_edd_bt
			f_h_edd = mdot_h / mdot_edd_h

			f_d_edd = []
			for xxx in range(len(alpha_dyn)):
				f_d_edd.append(mdot_d[xxx] / mdot_edd_d[xxx])

			f_s_edd = mdot_s / mdot_edd_s

			if f_t_edd > 10.0:
				mdot_t = 10.0 * mdot_edd_t
				f_t_edd = 10.0
			else:
				mdot_t = mdot_t

			if f_b_edd > 10.0:
				mdot_b = 10.0 * mdot_edd_b
				f_b_edd = 10.0
			else:
				mdot_b = mdot_b

			if f_bt_edd > 10.0:
				mdot_bt = 10.0 * mdot_edd_bt
				f_bt_edd = 10.0
			else:
				mdot_bt = mdot_bt

			if f_h_edd > 10.0:
				mdot_h = 10.0 * mdot_edd_h
				f_h_edd = 10.0
			else:
				mdot_h = mdot_h

			for xxx in range(len(alpha_dyn)):
				if f_d_edd[xxx] > 10.0:
					mdot_d[xxx] = 10.0 * mdot_edd_d[xxx]
					f_d_edd[xxx] = 10.0
				else:
					mdot_d[xxx] = mdot_d[xxx]

			if f_s_edd > 10.0:
				mdot_s = 10.0 * mdot_edd_s
				f_s_edd = 10.0
			else:
				mdot_s = mdot_s

			if supp == 0:

				m_seed_torque_total += mdot_t * time_between_snapshots
				m_seed_bondi_total += mdot_b * time_between_snapshots
				m_seed_tremmel_total += mdot_bt * time_between_snapshots
				m_seed_hobbs_total += mdot_h * time_between_snapshots

				for xxx in range(len(alpha_dyn)):
					m_seed_dyn_total[xxx] += mdot_d[xxx] * time_between_snapshots

				m_seed_sfr_total += mdot_s * time_between_snapshots

			else: # suppressed early bh growth, edd acc %10 of time

				if f_t_edd > 1.0:
					mdot_t = 1.0 * mdot_edd_t
					f_t_edd = 1.0
				else:
					mdot_t = mdot_t

				if f_b_edd > 1.0:
					mdot_b = 1.0 * mdot_edd_b
					f_b_edd = 1.0
				else:
					mdot_b = mdot_b

				if f_bt_edd > 1.0:
					mdot_bt = 1.0 * mdot_edd_bt
					f_bt_edd = 1.0
				else:
					mdot_bt = mdot_bt

				if f_h_edd > 1.0:
					mdot_h = 1.0 * mdot_edd_h
					f_h_edd = 1.0
				else:
					mdot_h = mdot_h

				for xxx in range(len(alpha_dyn)):
					if f_d_edd[xxx] > 1.0:
						mdot_d[xxx] = 1.0 * mdot_edd_d[xxx]
						f_d_edd[xxx] = 1.0
					else:
						mdot_d[xxx] = mdot_d[xxx]

				if f_s_edd > 1.0:
					mdot_s = 1.0 * mdot_edd_s
					f_s_edd = 1.0
				else:
					mdot_s = mdot_s

				m_seed_sfr_total += mdot_s * time_between_snapshots
				m_seed_torque_total += mdot_t * time_between_snapshots
				m_seed_bondi_total += mdot_b * time_between_snapshots
				m_seed_tremmel_total += mdot_bt * time_between_snapshots
				m_seed_hobbs_total += mdot_h * time_between_snapshots

				for xxx in range(len(alpha_dyn)):
					m_seed_dyn_total[xxx] += mdot_d[xxx] * time_between_snapshots

				m_seed_sfr_total += mdot_s * time_between_snapshots

		else:

			if merger == 1:

				m_seed_torque_total = 0.0
				m_seed_bondi_total = 0.0
				m_seed_tremmel_total = 0.0
				m_seed_hobbs_total = 0.0

				m_seed_dyn_total = []	
				for xxx in range(len(alpha_dyn)):
					m_seed_dyn_total.append(0.0)

				m_seed_sfr_total = 0.0
				mass_seed_total = 0.0

				# Last BH mass plus sum all the BH masses in previous merger
				for x in descendant[i]:							 

					m_seed_torque = [bb2 for aa2,bb2,cc2 in zip(z_1[0:i],m_bh_torque, ID[0:i]) if aa2 == z_2[i] if cc2 == x][0]
					m_seed_bondi = [bb2 for aa2,bb2,cc2 in zip(z_1[0:i],m_bh_bondi, ID[0:i]) if aa2 == z_2[i] if cc2 == x][0]
					m_seed_tremmel = [bb2 for aa2,bb2,cc2 in zip(z_1[0:i],m_bh_tremmel, ID[0:i]) if aa2 == z_2[i] if cc2 == x][0]
					m_seed_hobbs = [bb2 for aa2,bb2,cc2 in zip(z_1[0:i],m_bh_hobbs, ID[0:i]) if aa2 == z_2[i] if cc2 == x][0]

					m_seed_dyn = []
					for xxx in range(len(alpha_dyn)):
						m_seed_dyn.append([bb2 for aa2,bb2,cc2 in zip(z_1[0:i], m_bh_dyn[xxx], ID[0:i]) if aa2 == z_2[i] if cc2 == x][0])

					m_seed_sfr = [bb2 for aa2,bb2,cc2 in zip(z_1[0:i], m_bh_sfr, ID[0:i]) if aa2 == z_2[i] if cc2 == x][0]
					mass_seed_soldier = [bb4 for aa4,bb4,cc4 in zip(z_1[0:i],mass_seed, ID[0:i]) if aa4 == z_2[i] if cc4 == x][0]
					
					m_seed_torque_total += m_seed_torque
					m_seed_bondi_total += m_seed_bondi
					m_seed_tremmel_total += m_seed_tremmel
					m_seed_hobbs_total += m_seed_hobbs
	
					for xxx in range(len(alpha_dyn)):
						m_seed_dyn_total[xxx] += m_seed_dyn[xxx]
	
					m_seed_sfr_total += m_seed_sfr
					mass_seed_total += mass_seed_soldier
			
			mdot_t = calculate_mdot_torque(fdisk[i], mstar[i], mgas[i], m_seed_torque_total, radius, mass_retention) 
			mdot_b = calculate_mdot_bondi(m_seed_bondi_total, mean_cs[i], mean_vel[i], mgas[i], radius)
			mdot_bt = calculate_mdot_tremmel(m_seed_tremmel_total, mean_cs[i], mean_vel[i], mean_vel_phi[i], mgas[i], radius)
			mdot_h = calculate_mdot_hobbs(m_seed_hobbs_total, mtot[i], mean_cs[i], mean_vel[i], mgas[i], radius)

			mdot_d = []
			for xxx in range(len(alpha_dyn)):
				mdot_d.append(calculate_mdot_dyn(mtot[i], mgas[i], radius, alpha_dyn[xxx]))

			mdot_s = calculate_mdot_sfr(sfr[i])

			mdot_edd_t = calculate_mdot_eddington(m_seed_torque_total, eta)
			mdot_edd_b = calculate_mdot_eddington(m_seed_bondi_total, eta)
			mdot_edd_bt = calculate_mdot_eddington(m_seed_tremmel_total, eta)
			mdot_edd_h = calculate_mdot_eddington(m_seed_hobbs_total, eta)

			mdot_edd_d = []
			for xxx in range(len(alpha_dyn)):
				mdot_edd_d.append(calculate_mdot_eddington(m_seed_dyn_total[xxx], eta))

			mdot_edd_s = calculate_mdot_eddington(m_seed_sfr_total, eta)

			f_t_edd = mdot_t / mdot_edd_t
			f_b_edd = mdot_b / mdot_edd_b
			f_bt_edd = mdot_bt / mdot_edd_bt
			f_h_edd = mdot_h / mdot_edd_h

			f_d_edd = []
			for xxx in range(len(alpha_dyn)):
				f_d_edd.append(mdot_d[xxx] / mdot_edd_d[xxx])

			f_s_edd = mdot_s / mdot_edd_s

			if f_t_edd > 10.0:
				mdot_t = 10.0 * mdot_edd_t
				f_t_edd = 10.0
			else:
				mdot_t = mdot_t

			if f_b_edd > 10.0:
				mdot_b = 10.0 * mdot_edd_b
				f_b_edd = 10.0
			else:
				mdot_b = mdot_b

			if f_bt_edd > 10.0:
				mdot_bt = 10.0 * mdot_edd_bt
				f_bt_edd = 10.0
			else:
				mdot_bt = mdot_bt

			if f_h_edd > 10.0:
				mdot_h = 10.0 * mdot_edd_h
				f_h_edd = 10.0
			else:
				mdot_h = mdot_h

			for xxx in range(len(alpha_dyn)):
				if f_d_edd[xxx] > 10.0:
					mdot_d[xxx] = 10.0 * mdot_edd_d[xxx]
					f_d_edd[xxx] = 10.0
				else:
					mdot_d[xxx] = mdot_d[xxx]

			if f_s_edd > 10.0:
				mdot_s = 10.0 * mdot_edd_s
				f_s_edd = 10.0
			else:
				mdot_s = mdot_s

			if supp == 0:

				m_seed_torque_total += mdot_t * time_between_snapshots
				m_seed_bondi_total += mdot_b * time_between_snapshots
				m_seed_tremmel_total += mdot_bt * time_between_snapshots
				m_seed_hobbs_total += mdot_h * time_between_snapshots

				for xxx in range(len(alpha_dyn)):
					m_seed_dyn_total[xxx] += mdot_d[xxx] * time_between_snapshots

				m_seed_sfr_total += mdot_s * time_between_snapshots

			else: # suppressed early bh growth, edd acc %10 of time

				if f_t_edd > 1.0:
					mdot_t = 1.0 * mdot_edd_t
					f_t_edd = 1.0
				else:
					mdot_t = mdot_t

				if f_b_edd > 1.0:
					mdot_b = 1.0 * mdot_edd_b
					f_b_edd = 1.0
				else:
					mdot_b = mdot_b

				if f_bt_edd > 1.0:
					mdot_bt = 1.0 * mdot_edd_bt
					f_bt_edd = 1.0
				else:
					mdot_bt = mdot_bt

				if f_h_edd > 1.0:
					mdot_h = 1.0 * mdot_edd_h
					f_h_edd = 1.0
				else:
					mdot_h = mdot_h

				for xxx in range(len(alpha_dyn)):
					if f_d_edd[xxx] > 1.0:
						mdot_d[xxx] = 1.0 * mdot_edd_d[xxx]
						f_d_edd[xxx] = 1.0
					else:
						mdot_d[xxx] = mdot_d[xxx]

				if f_s_edd > 1.0:
					mdot_s = 1.0 * mdot_edd_s
					f_s_edd = 1.0
				else:
					mdot_s = mdot_s

				m_seed_torque_total += mdot_t * time_between_snapshots
				m_seed_bondi_total += mdot_b * time_between_snapshots
				m_seed_tremmel_total += mdot_bt * time_between_snapshots
				m_seed_hobbs_total += mdot_h * time_between_snapshots

				for xxx in range(len(alpha_dyn)):
					m_seed_dyn_total[xxx] += mdot_d[xxx] * time_between_snapshots

				m_seed_sfr_total += mdot_s * time_between_snapshots

		mdot_torque.append(mdot_t)
		mdot_bondi.append(mdot_b)
		mdot_tremmel.append(mdot_bt)
		mdot_hobbs.append(mdot_h)

		for xxx in range(len(alpha_dyn)):
			mdot_dyn[xxx].append(mdot_d[xxx])

		mdot_sfr.append(mdot_s)

		f_torque_edd.append(f_t_edd)
		f_bondi_edd.append(f_b_edd)
		f_tremmel_edd.append(f_bt_edd)
		f_hobbs_edd.append(f_h_edd)

		for xxx in range(len(alpha_dyn)):
			f_dyn_edd[xxx].append(f_d_edd[xxx])
	
		f_sfr_edd.append(f_s_edd)

		m_bh_torque.append(m_seed_torque_total)
		m_bh_bondi.append(m_seed_bondi_total)
		m_bh_tremmel.append(m_seed_tremmel_total)
		m_bh_hobbs.append(m_seed_hobbs_total)

		for xxx in range(len(alpha_dyn)):
			m_bh_dyn[xxx].append(m_seed_dyn_total[xxx])

		m_bh_sfr.append(m_seed_sfr_total)
		mass_seed.append(mass_seed_total)
		
	if merger == 1:

		if filter_for_timeline == 1:
			# Filter timeline from halo data

			Z_ID = zip(z_1, ID)
			Z_ID_timeline = zip(z_1_timeline, ID_timeline)
			zeytin_store = zip(Z_ID, mdot_torque, mdot_bondi, mdot_tremmel, mdot_hobbs, map(list, zip(*mdot_dyn)), mdot_sfr, f_torque_edd, f_bondi_edd, f_tremmel_edd, f_hobbs_edd, map(list, zip(*f_dyn_edd)), f_sfr_edd, m_bh_torque, m_bh_bondi, m_bh_tremmel, m_bh_hobbs, map(list, zip(*m_bh_dyn)), m_bh_sfr, mass_seed)

			zeytin = [[a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19] for a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19 in zeytin_store if a0 in Z_ID_timeline]
	
			mdot_torque = np.array(zip(*zeytin)[0])
			mdot_bondi = np.array(zip(*zeytin)[1])
			mdot_tremmel = np.array(zip(*zeytin)[2])
			mdot_hobbs = np.array(zip(*zeytin)[3])
			mdot_dyn = np.array(map(list, zip(*np.array(zip(*zeytin)[4]))))
			mdot_sfr = np.array(zip(*zeytin)[5])
			f_torque_edd = np.array(zip(*zeytin)[6])
			f_bondi_edd = np.array(zip(*zeytin)[7])
			f_tremmel_edd = np.array(zip(*zeytin)[8])
			f_hobbs_edd = np.array(zip(*zeytin)[9])
			f_dyn_edd = np.array(map(list, zip(*np.array(zip(*zeytin)[10]))))
			f_sfr_edd = np.array(zip(*zeytin)[11])
			m_bh_torque = np.array(zip(*zeytin)[12])
			m_bh_bondi = np.array(zip(*zeytin)[13])
			m_bh_tremmel = np.array(zip(*zeytin)[14])
			m_bh_hobbs = np.array(zip(*zeytin)[15])
			m_bh_dyn = np.array(map(list, zip(*np.array(zip(*zeytin)[16]))))
			m_bh_sfr = np.array(zip(*zeytin)[17])
			mass_seed = np.array(zip(*zeytin)[18])

	return mdot_torque, mdot_bondi, mdot_tremmel, mdot_hobbs, mdot_dyn, mdot_sfr, f_torque_edd, f_bondi_edd, f_tremmel_edd, f_hobbs_edd, f_dyn_edd, f_sfr_edd, m_bh_torque, m_bh_bondi, m_bh_tremmel, m_bh_hobbs, m_bh_dyn, m_bh_sfr, mass_seed

def trace_star_particles(halodir, snapshot_dir, i_prev, i_next, iHalo_prev, iHalo_next, comoving, dummy):
	i_new_prev = i_prev + dummy
	i_new_next = i_next + dummy

	halodir_prev = halodir + str(i_new_prev) + '/'
	halodir_prev = glob.glob(halodir_prev + '*_halos')[0]
	halodir_next = halodir + str(i_new_next) + '/'
	halodir_next = glob.glob(halodir_next + '*_halos')[0]

	halo_center_prev = np.array(get_haloCenter(halodir_prev, iHalo_prev))
	rvir_prev = np.array(get_haloRvir(halodir_prev, iHalo_prev))
	halo_center_next = np.array(get_haloCenter(halodir_next, iHalo_next))
	rvir_next = np.array(get_haloRvir(halodir_next, iHalo_next))

	snapshot_dir_prev = snapshot_dir + str(i_new_prev)
	snapshot_dir_next = snapshot_dir + str(i_new_next)

	data_header_prev = []
	data_header_next = []

	data_header_prev = readsnap(snapshot_dir_prev, snum=i_new_prev, ptype=0, header_only=1)
	hubble_prev = data_header_prev["hubble"]
	redshift_prev = data_header_prev["redshift"]
	data_header_next = readsnap(snapshot_dir_next, snum=i_new_next, ptype=0, header_only=1)
	hubble_next = data_header_next["hubble"]
	redshift_next = data_header_next["redshift"]

	data_prev = []
	data_next = []

	if comoving == 0:
		halo_center_prev = halo_center_prev
		halo_center_next = halo_center_next
		rvir_prev = rvir_prev
		rvir_next = rvir_next
		data_prev = readsnap(snapshot_dir_prev, snum=i_new_prev, ptype=4, cosmological=0)
		data_next = readsnap(snapshot_dir_next, snum=i_new_next, ptype=4, cosmological=0)

	else:
		halo_center_prev = halo_center_prev / (1.0 + redshift_prev) / hubble_prev
		halo_center_next = halo_center_next / (1.0 + redshift_next) / hubble_next
		rvir_prev = rvir_prev / (1.0 + redshift_prev) / hubble_prev
		rvir_next = rvir_next / (1.0 + redshift_next) / hubble_next
		data_prev = readsnap(snapshot_dir_prev, snum=i_new_prev, ptype=4, cosmological=1)
		data_next = readsnap(snapshot_dir_next, snum=i_new_next, ptype=4, cosmological=1)

	radius_prev = np.sqrt(np.sum(np.square(data_prev['p'] - halo_center_prev), axis=1))

	#PREV STAR PARTICLES
	sel_prev = (radius_prev <= rvir_prev)
	data_prev_sel = select_particles(data_prev, sel_prev)
	data_prev_sel['p'] -= halo_center_prev

	data_next_sel = dict_filter(data_next,data_prev_sel,'id')
	data_next_sel['p'] -= halo_center_prev
	center_prev = [list(halo_center_prev - halo_center_prev)]
	center_next = [list(halo_center_next - halo_center_prev)]

	npart_gas = 0
	npart_halo = len(data_prev_sel['m'])
	npart_disk = 0 
	npart_bulge = 1  
	npart_star = len(data_next_sel['m'])
	npart_bndry = 1  

	data_filename = './snapshot_' + str(i_new_prev) + '_' + str(i_new_next) + '_' + str('%.2f' %rvir_prev) + 'kpc.hdf5'

	f = h5py.File(data_filename,'w')
	f.create_group("Header")
	f["Header"].attrs["NumPart_ThisFile"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
	f["Header"].attrs["NumPart_Total"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
	f["Header"].attrs["NumPart_Total_HighWord"] = data_header_prev["npartTotal_high"]
	f["Header"].attrs["MassTable"] = data_header_prev["massarr"]
	f["Header"].attrs["Time"] = data_header_prev["time"]
	f["Header"].attrs["Redshift"] = data_header_prev["redshift"]
	f["Header"].attrs["BoxSize"] = data_header_prev["boxsize"]
	f["Header"].attrs["NumFilesPerSnapshot"] = 1
	f["Header"].attrs["Omega0"] = data_header_prev["omega_matter"]
	f["Header"].attrs["OmegaLambda"] = data_header_prev["omega_lambda"]
	f["Header"].attrs["HubbleParam"] = data_header_prev["hubble"]
	f["Header"].attrs["Flag_Sfr"] = data_header_prev["flag_sfr"]
	f["Header"].attrs["Flag_Cooling"] = data_header_prev["flag_cooling"]
	f["Header"].attrs["Flag_StellarAge"] = data_header_prev["flag_stellarage"]
	f["Header"].attrs["Flag_Metals"] = data_header_prev["flag_metals"]
	f["Header"].attrs["Flag_Feedback"] = data_header_prev["flag_feedbacktp"]
	f["Header"].attrs["Flag_DoublePrecision"] = data_header_prev["flag_doubleprecision"]
	f["Header"].attrs["Flag_IC_Info"] = data_header_prev["flag_icinfo"]
	#f["PartType0/Coordinates"] = data_gas_sel['p']
	#f["PartType0/Density"] = data_gas_sel['rho']
	#f["PartType0/ElectronAbundance"] = data_gas_sel['ne']
	#f["PartType0/InternalEnergy"] = data_gas_sel['u']
	#f["PartType0/Masses"] = data_gas_sel['m']
	#f["PartType0/Metallicity"] = data_gas_sel['z']
	#f["PartType0/NeutralHydrogenAbundance"] = data_gas_sel['nh']
	#f["PartType0/StarFormationRate"] = data_gas_sel['sfr']
	#f["PartType0/Velocities"] = data_gas_sel['v']
	f["PartType1/Coordinates"] = data_prev_sel['p']
	f["PartType1/Velocities"] = data_prev_sel['v']
	f["PartType1/Masses"] = data_prev_sel['m']
	f["PartType1/Metallicity"] = data_prev_sel['z']
	f["PartType1/StellarFormationTime"] = data_prev_sel['age']
	f["PartType3/Coordinates"] = center_prev
	f["PartType4/Coordinates"] = data_next_sel['p']
	f["PartType4/Velocities"] = data_next_sel['v']
	f["PartType4/Masses"] = data_next_sel['m']
	f["PartType4/Metallicity"] = data_next_sel['z']
	f["PartType4/StellarFormationTime"] = data_next_sel['age']
	f["PartType5/Coordinates"] = center_next
	f.close()

def readsnap(sdir,snum,ptype,
	snapshot_name='snapshot',
	extension='.hdf5',
	h0=0,cosmological=0,skip_bh=0,four_char=0,
	header_only=0,loud=0):
	
	if (ptype<0): return {'k':-1};
	if (ptype>5): return {'k':-1};

	fname,fname_base,fname_ext = check_if_filename_exists(sdir,snum,\
		snapshot_name=snapshot_name,extension=extension,four_char=four_char)
	#print fname
	if(fname=='NULL'): return {'k':-1}
	if(loud==1): print 'loading file : '+fname

	## open file and parse its header information
	nL = 0 # initial particle point to start at 
	if(fname_ext=='.hdf5'):
		file = h5py.File(fname,'r') # Open hdf5 snapshot file
		header_master = file["Header"] # Load header dictionary (to parse below)
		header_toparse = header_master.attrs
	else:
		file = open(fname) # Open binary snapshot file
		header_toparse = load_gadget_binary_header(file)

	npart = header_toparse["NumPart_ThisFile"]
	massarr = header_toparse["MassTable"]
	time = header_toparse["Time"]
	redshift = header_toparse["Redshift"]
	flag_sfr = header_toparse["Flag_Sfr"]
	flag_feedbacktp = header_toparse["Flag_Feedback"]
	npartTotal = header_toparse["NumPart_Total"]
	npartTotal_high = header_toparse["NumPart_Total_HighWord"]
	flag_cooling = header_toparse["Flag_Cooling"]
	numfiles = header_toparse["NumFilesPerSnapshot"]
	boxsize = header_toparse["BoxSize"]
	omega_matter = header_toparse["Omega0"]
	omega_lambda = header_toparse["OmegaLambda"]
	hubble = header_toparse["HubbleParam"]
	flag_stellarage = header_toparse["Flag_StellarAge"]
	flag_metals = header_toparse["Flag_Metals"]
	flag_doubleprecision = header_toparse["Flag_DoublePrecision"]
	flag_icinfo = header_toparse["Flag_IC_Info"]
	#print "npart_file: ",npart
	#print "npart_total:",npartTotal
	
	hinv=1.
	if (h0==1):
		hinv=1./hubble
	ascale=1.
	if (cosmological==1):
		ascale=time
		hinv=1./hubble
	if (cosmological==0): 
		time*=hinv
	
	boxsize*=hinv*ascale
	if (npartTotal[ptype]<=0): file.close(); return {'k':-1};
	if (header_only==1): file.close(); return {'k':0,'npart':npart,'massarr':massarr,'time':time,'redshift':redshift,'flag_sfr':flag_sfr,'flag_feedbacktp':flag_feedbacktp,'npartTotal':npartTotal,'flag_cooling':flag_cooling,'numfiles':numfiles,'boxsize':boxsize,'omega_matter':omega_matter,'omega_lambda':omega_lambda,'hubble':hubble,'flag_stellarage':flag_stellarage,'flag_metals':flag_metals,'npartTotal_high':npartTotal_high,'flag_doubleprecision':flag_doubleprecision,'flag_icinfo':flag_icinfo};

	# initialize variables to be read
	pos=np.zeros([npartTotal[ptype],3],dtype=np.float64)
	vel=np.copy(pos)
	ids=np.zeros([npartTotal[ptype]],dtype=long)
	mass=np.zeros([npartTotal[ptype]],dtype=np.float64)
	if (ptype==0):
		ugas=np.copy(mass)
		rho=np.copy(mass)
		hsml=np.copy(mass) 
		#if (flag_cooling>0): 
		nume=np.copy(mass)
		numh=np.copy(mass)
		#if (flag_sfr>0): 
		sfr=np.copy(mass)
		metal=np.copy(mass)
		alpha=np.copy(mass)
		vorticity=np.copy(pos)
		bfield=np.copy(pos)
		divb=np.copy(mass)
		phib=np.copy(mass)
		cregy=np.copy(mass)
	if (ptype==0 or ptype==4) and (flag_metals > 0):
		metal=np.zeros([npartTotal[ptype],flag_metals],dtype=np.float64)
	if (ptype==4) and (flag_sfr>0) and (flag_stellarage>0):
		stellage=np.copy(mass)
		mimf=np.copy(mass)
	if (ptype==3):
		grainsize=np.copy(mass)
	if (ptype==5) and (skip_bh==0):
		bhmass=np.copy(mass)
		bhmdot=np.copy(mass)
		bhmass_alpha=np.copy(mass)

	# loop over the snapshot parts to get the different data pieces
	for i_file in range(numfiles):
		if (numfiles>1):
			file.close()
			fname = fname_base+'.'+str(i_file)+fname_ext
			if(fname_ext=='.hdf5'):
				file = h5py.File(fname,'r') # Open hdf5 snapshot file
			else:
				file = open(fname) # Open binary snapshot file
				header_toparse = load_gadget_binary_header(file)
				
		if (fname_ext=='.hdf5'):
			input_struct = file
			npart = file["Header"].attrs["NumPart_ThisFile"]
			bname = "PartType"+str(ptype)+"/"
		else:
			npart = header_toparse['NumPart_ThisFile']
			input_struct = load_gadget_binary_particledat(file, header_toparse, ptype, skip_bh=skip_bh)
			bname = ''
			
		# now do the actual reading
		if(npart[ptype]>0):
			nR=nL + npart[ptype]
			pos[nL:nR,:]=input_struct[bname+"Coordinates"]
			vel[nL:nR,:]=input_struct[bname+"Velocities"]
			ids[nL:nR]=input_struct[bname+"ParticleIDs"]
			mass[nL:nR]=massarr[ptype]
			
			if (massarr[ptype] <= 0.):
				mass[nL:nR]=input_struct[bname+"Masses"]
				
			if (ptype==0):
				ugas[nL:nR]=input_struct[bname+"InternalEnergy"]
				rho[nL:nR]=input_struct[bname+"Density"]
				hsml[nL:nR]=input_struct[bname+"SmoothingLength"]
				if (flag_cooling > 0):
					nume[nL:nR]=input_struct[bname+"ElectronAbundance"]
					numh[nL:nR]=input_struct[bname+"NeutralHydrogenAbundance"]
				if (flag_sfr > 0):
					sfr[nL:nR]=input_struct[bname+"StarFormationRate"]
				# these and other optional flags are detected IF the file is HDF5
				if(fname_ext=='.hdf5'):
					if("ArtificialViscosity" in input_struct[bname].keys()):
						alpha[nL:nR]=input_struct[bname+"ArtificialViscosity"]
					if("Vorticity" in input_struct[bname].keys()):
						vorticity[nL:nR,:]=input_struct[bname+"Vorticity"]
					if("DivergenceOfMagneticField" in input_struct[bname].keys()):
						divb[nL:nR]=input_struct[bname+"DivergenceOfMagneticField"]
					if("MagneticField" in input_struct[bname].keys()):
						bfield[nL:nR,:]=input_struct[bname+"MagneticField"]
					if("DivBcleaningFunctionPhi" in input_struct[bname].keys()):
						phib[nL:nR]=input_struct[bname+"DivBcleaningFunctionPhi"]
					if("CosmicRayEnergy" in input_struct[bname].keys()):
						cregy[nL:nR]=input_struct[bname+"CosmicRayEnergy"]
					
			if (ptype==0 or ptype==4) and (flag_metals > 0):
				metal_t=input_struct[bname+"Metallicity"]
				if (flag_metals > 1):
					if (metal_t.shape[0] != npart[ptype]): 
						metal_t=np.transpose(metal_t)
				else:
					metal_t=np.reshape(np.array(metal_t),(np.array(metal_t).size,1))
				metal[nL:nR,:]=metal_t
				
			if (ptype==4) and (flag_sfr>0) and (flag_stellarage>0):
				stellage[nL:nR]=input_struct[bname+"StellarFormationTime"]
				
			if (ptype==4) and (flag_sfr>0):
				if(fname_ext=='.hdf5'):				
					if("IMFTurnOverMass" in input_struct[bname].keys()):
						mimf[nL:nR]=input_struct[bname+"IMFTurnOverMass"]

			if (ptype==3):
				if(fname_ext=='.hdf5'):				
					if("GrainSize" in input_struct[bname].keys()):
						grainsize[nL:nR]=input_struct[bname+"GrainSize"]
				
			if (ptype==5) and (skip_bh==0):
				if(fname_ext=='.hdf5'):
					if("BH_Mass" in input_struct[bname].keys()):
						bhmass[nL:nR]=input_struct[bname+"BH_Mass"]
					if("BH_Mass_AlphaDisk" in input_struct[bname].keys()):
						bhmass_alpha[nL:nR]=input_struct[bname+"BH_Mass_AlphaDisk"]
					if("BH_Mdot" in input_struct[bname].keys()):
						bhmdot[nL:nR]=input_struct[bname+"BH_Mdot"]
			nL = nR # sets it for the next iteration	

	## correct to same ID as original gas particle for new stars, if bit-flip applied
	if ((np.min(ids)<0) | (np.max(ids)>1.e9)):
		bad = (ids < 0) | (ids > 1.e9)
		ids[bad] += (1L << 31)

	# do the cosmological conversions on final vectors as needed
	pos *= hinv*ascale # snapshot units are comoving
	mass *= hinv
	vel *= np.sqrt(ascale) # remember gadget's weird velocity units!
	if (ptype==0):
		rho *= (hinv/((ascale*hinv)**3))
		hsml *= hinv*ascale
	if (ptype==4) and (flag_sfr>0) and (flag_stellarage>0) and (cosmological==0):
		stellage *= hinv
	if (ptype==5) and (skip_bh==0):
		bhmass *= hinv

	file.close();
	if (ptype==0):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,'u':ugas,'rho':rho,
		'h':hsml,'ne':nume,'nh':numh,'sfr':sfr,'z':metal,
		'ArtificialViscosity':alpha,'Velocity':vel,'Masses':mass,'Coordinates':pos,
		'Density':rho,'Hsml':hsml,'ParticleIDs':ids,'InternalEnergy':ugas,
		'Metallicity':metal,'SFR':sfr,'Vorticity':vorticity,
		'B':bfield,'divB':divb,'DivBcleaningFunctionPhi':phib,'CosmicRayEnergy':cregy};
	if (ptype==3):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,'GrainSize':grainsize}
	if (ptype==4):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,'z':metal,'age':stellage,'IMFTurnOverMass':mimf}
	if (ptype==5) and (skip_bh==0):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,
		'mbh':bhmass,'mdot':bhmdot,'BHMass_AlphaDisk':bhmass_alpha}
	return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids}

def readsnap_compact(sdir,snum,ptype,
	snapshot_name='snapshot',
	extension='.hdf5',
	h0=0,cosmological=0,skip_bh=0,four_char=0,
	header_only=0,loud=0):
	
	if (ptype<0): return {'k':-1};
	if (ptype>5): return {'k':-1};

	fname,fname_base,fname_ext = check_if_filename_exists(sdir,snum,\
		snapshot_name=snapshot_name,extension=extension,four_char=four_char)
	#print fname
	if(fname=='NULL'): return {'k':-1}
	if(loud==1): print 'loading file : '+fname

	## open file and parse its header information
	nL = 0 # initial particle point to start at 
	if(fname_ext=='.hdf5'):
		file = h5py.File(fname,'r') # Open hdf5 snapshot file
		header_master = file["Header"] # Load header dictionary (to parse below)
		header_toparse = header_master.attrs
	else:
		file = open(fname) # Open binary snapshot file
		header_toparse = load_gadget_binary_header(file)
	
	compactlevel = header_toparse["CompactLevel"]
	npart = header_toparse["NumPart_ThisFile"]
	massarr = header_toparse["MassTable"]
	time = header_toparse["Time"]
	redshift = header_toparse["Redshift"]
	flag_sfr = header_toparse["Flag_Sfr"]
	flag_feedbacktp = header_toparse["Flag_Feedback"]
	npartTotal = header_toparse["NumPart_Total"]
	npartTotal_high = header_toparse["NumPart_Total_HighWord"]
	flag_cooling = header_toparse["Flag_Cooling"]
	numfiles = header_toparse["NumFilesPerSnapshot"]
	boxsize = header_toparse["BoxSize"]
	omega_matter = header_toparse["Omega0"]
	omega_lambda = header_toparse["OmegaLambda"]
	hubble = header_toparse["HubbleParam"]
	flag_stellarage = header_toparse["Flag_StellarAge"]
	flag_metals = header_toparse["Flag_Metals"]
	flag_doubleprecision = header_toparse["Flag_DoublePrecision"]
	flag_icinfo = header_toparse["Flag_IC_Info"]
	#print "npart_file: ",npart
	#print "npart_total:",npartTotal
	
	hinv=1.
	if (h0==1):
		hinv=1./hubble
	ascale=1.
	if (cosmological==1):
		ascale=time
		hinv=1./hubble
	if (cosmological==0): 
		time*=hinv
	
	boxsize*=hinv*ascale
	if (npartTotal[ptype]<=0): file.close(); return {'k':-1};
	if (header_only==1): file.close(); return {'k':0,'compactlevel':compactlevel,'npart':npart,'massarr':massarr,'time':time,'redshift':redshift,'flag_sfr':flag_sfr,'flag_feedbacktp':flag_feedbacktp,'npartTotal':npartTotal,'flag_cooling':flag_cooling,'numfiles':numfiles,'boxsize':boxsize,'omega_matter':omega_matter,'omega_lambda':omega_lambda,'hubble':hubble,'flag_stellarage':flag_stellarage,'flag_metals':flag_metals,'npartTotal_high':npartTotal_high,'flag_doubleprecision':flag_doubleprecision,'flag_icinfo':flag_icinfo};

	# initialize variables to be read
	pos=np.zeros([npartTotal[ptype],3],dtype=np.float64)
	vel=np.copy(pos)
	ids=np.zeros([npartTotal[ptype]],dtype=long)
	mass=np.zeros([npartTotal[ptype]],dtype=np.float64)
	if (ptype==0):
		ugas=np.copy(mass)
		rho=np.copy(mass)
		hsml=np.copy(mass) 
		#if (flag_cooling>0): 
		nume=np.copy(mass)
		numh=np.copy(mass)
		#if (flag_sfr>0): 
		sfr=np.copy(mass)
		metal=np.copy(mass)
		alpha=np.copy(mass)
		vorticity=np.copy(pos)
		bfield=np.copy(pos)
		divb=np.copy(mass)
		phib=np.copy(mass)
		cregy=np.copy(mass)
	if (ptype==0 or ptype==4) and (flag_metals > 0):
		metal=np.zeros([npartTotal[ptype],flag_metals],dtype=np.float64)
	if (ptype==4) and (flag_sfr>0) and (flag_stellarage>0):
		stellage=np.copy(mass)
		mimf=np.copy(mass)
	if (ptype==3):
		grainsize=np.copy(mass)
	if (ptype==5) and (skip_bh==0):
		bhmass=np.copy(mass)
		bhmdot=np.copy(mass)
		bhmass_alpha=np.copy(mass)

	# loop over the snapshot parts to get the different data pieces
	for i_file in range(numfiles):
		if (numfiles>1):
			file.close()
			fname = fname_base+'.'+str(i_file)+fname_ext
			if(fname_ext=='.hdf5'):
				file = h5py.File(fname,'r') # Open hdf5 snapshot file
			else:
				file = open(fname) # Open binary snapshot file
				header_toparse = load_gadget_binary_header(file)
				
		if (fname_ext=='.hdf5'):
			input_struct = file
			npart = file["Header"].attrs["NumPart_ThisFile"]
			bname = "PartType"+str(ptype)+"/"
		else:
			npart = header_toparse['NumPart_ThisFile']
			input_struct = load_gadget_binary_particledat(file, header_toparse, ptype, skip_bh=skip_bh)
			bname = ''
			
		# now do the actual reading
		if(npart[ptype]>0):
			nR=nL + npart[ptype]
			pos[nL:nR,:]=input_struct[bname+"Coordinates"]
			vel[nL:nR,:]=input_struct[bname+"Velocities"]
			ids[nL:nR]=input_struct[bname+"ParticleIDs"]
			mass[nL:nR]=massarr[ptype]
			
			if (massarr[ptype] <= 0.):
				mass[nL:nR]=input_struct[bname+"Masses"]
				
			if (ptype==0):
				ugas[nL:nR]=input_struct[bname+"InternalEnergy"]
				rho[nL:nR]=input_struct[bname+"Density"]

				if (compactlevel == 0) or (compactlevel == -1): # added by onur
					hsml[nL:nR]=input_struct[bname+"SmoothingLength"]

				if (flag_cooling > 0):
					nume[nL:nR]=input_struct[bname+"ElectronAbundance"]
					if (compactlevel == 0) or (compactlevel == -1): # added by onur
						numh[nL:nR]=input_struct[bname+"NeutralHydrogenAbundance"]
				if (flag_sfr > 0):
					sfr[nL:nR]=input_struct[bname+"StarFormationRate"]
				# these and other optional flags are detected IF the file is HDF5
				if(fname_ext=='.hdf5'):
					if("ArtificialViscosity" in input_struct[bname].keys()):
						alpha[nL:nR]=input_struct[bname+"ArtificialViscosity"]
					if("Vorticity" in input_struct[bname].keys()):
						vorticity[nL:nR,:]=input_struct[bname+"Vorticity"]
					if("DivergenceOfMagneticField" in input_struct[bname].keys()):
						divb[nL:nR]=input_struct[bname+"DivergenceOfMagneticField"]
					if("MagneticField" in input_struct[bname].keys()):
						bfield[nL:nR,:]=input_struct[bname+"MagneticField"]
					if("DivBcleaningFunctionPhi" in input_struct[bname].keys()):
						phib[nL:nR]=input_struct[bname+"DivBcleaningFunctionPhi"]
					if("CosmicRayEnergy" in input_struct[bname].keys()):
						cregy[nL:nR]=input_struct[bname+"CosmicRayEnergy"]

			# changed by onur
			#################	
			if (ptype==0) and (flag_metals > 0):
				metal_t=input_struct[bname+"Metallicity"]
				#if (flag_metals > 1):
				if compactlevel == 0 or compactlevel == -1:
					if (metal_t.shape[0] != npart[ptype]): 
						metal_t=np.transpose(metal_t)
				else:
					metal_t=np.reshape(np.array(metal_t),(np.array(metal_t).size,1))
				metal[nL:nR,:]=metal_t

			if (ptype==4) and (flag_metals > 0):
				metal_t=input_struct[bname+"Metallicity"]
				if (flag_metals > 1):
					if (metal_t.shape[0] != npart[ptype]): 
						metal_t=np.transpose(metal_t)
				else:
					metal_t=np.reshape(np.array(metal_t),(np.array(metal_t).size,1))
				metal[nL:nR,:]=metal_t
			#################	

			if (ptype==4) and (flag_sfr>0) and (flag_stellarage>0):
				stellage[nL:nR]=input_struct[bname+"StellarFormationTime"]
				
			if (ptype==4) and (flag_sfr>0):
				if(fname_ext=='.hdf5'):				
					if("IMFTurnOverMass" in input_struct[bname].keys()):
						mimf[nL:nR]=input_struct[bname+"IMFTurnOverMass"]

			if (ptype==3):
				if(fname_ext=='.hdf5'):				
					if("GrainSize" in input_struct[bname].keys()):
						grainsize[nL:nR]=input_struct[bname+"GrainSize"]
				
			if (ptype==5) and (skip_bh==0):
				if(fname_ext=='.hdf5'):
					if("BH_Mass" in input_struct[bname].keys()):
						bhmass[nL:nR]=input_struct[bname+"BH_Mass"]
					if("BH_Mass_AlphaDisk" in input_struct[bname].keys()):
						bhmass_alpha[nL:nR]=input_struct[bname+"BH_Mass_AlphaDisk"]
					if("BH_Mdot" in input_struct[bname].keys()):
						bhmdot[nL:nR]=input_struct[bname+"BH_Mdot"]
			nL = nR # sets it for the next iteration	

	## correct to same ID as original gas particle for new stars, if bit-flip applied
	if ((np.min(ids)<0) | (np.max(ids)>1.e9)):
		bad = (ids < 0) | (ids > 1.e9)
		ids[bad] += (1L << 31)

	# do the cosmological conversions on final vectors as needed
	pos *= hinv*ascale # snapshot units are comoving
	mass *= hinv
	vel *= np.sqrt(ascale) # remember gadget's weird velocity units!
	if (ptype==0):
		rho *= (hinv/((ascale*hinv)**3))
		hsml *= hinv*ascale
	if (ptype==4) and (flag_sfr>0) and (flag_stellarage>0) and (cosmological==0):
		stellage *= hinv
	if (ptype==5) and (skip_bh==0):
		bhmass *= hinv

	file.close();
	if (ptype==0):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,'u':ugas,'rho':rho,
		'h':hsml,'ne':nume,'nh':numh,'sfr':sfr,'z':metal,
		'ArtificialViscosity':alpha,'Velocity':vel,'Masses':mass,'Coordinates':pos,
		'Density':rho,'Hsml':hsml,'ParticleIDs':ids,'InternalEnergy':ugas,
		'Metallicity':metal,'SFR':sfr,'Vorticity':vorticity,
		'B':bfield,'divB':divb,'DivBcleaningFunctionPhi':phib,'CosmicRayEnergy':cregy};
	if (ptype==3):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,'GrainSize':grainsize}
	if (ptype==4):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,'z':metal,'age':stellage,'IMFTurnOverMass':mimf}
	if (ptype==5) and (skip_bh==0):
		return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids,
		'mbh':bhmass,'mdot':bhmdot,'BHMass_AlphaDisk':bhmass_alpha}
	return {'k':1,'p':pos,'v':vel,'m':mass,'id':ids}

def check_if_filename_exists(sdir,snum,snapshot_name='snapshot',extension='.hdf5',four_char=0):
	for extension_touse in [extension,'.bin','']:
		fname=sdir+'/'+snapshot_name+'_'
		ext='00'+str(snum);
		if (snum>=10): ext='0'+str(snum)
		if (snum>=100): ext=str(snum)
		if (four_char==1): ext='0'+ext
		if (snum>=1000): ext=str(snum)
		fname+=ext
		fname_base=fname

		s0=sdir.split("/"); snapdir_specific=s0[len(s0)-1];
		if(len(snapdir_specific)<=1): snapdir_specific=s0[len(s0)-2];

		## try several common notations for the directory/filename structure
		fname=fname_base+extension_touse;
		if not os.path.exists(fname): 
			## is it a multi-part file?
			fname=fname_base+'.0'+extension_touse;
		if not os.path.exists(fname): 
			## is the filename 'snap' instead of 'snapshot'?
			fname_base=sdir+'/snap_'+ext; 
			fname=fname_base+extension_touse;
		if not os.path.exists(fname): 
			## is the filename 'snap' instead of 'snapshot', AND its a multi-part file?
			fname=fname_base+'.0'+extension_touse;
		if not os.path.exists(fname): 
			## is the filename 'snap(snapdir)' instead of 'snapshot'?
			fname_base=sdir+'/snap_'+snapdir_specific+'_'+ext; 
			fname=fname_base+extension_touse;
		if not os.path.exists(fname): 
			## is the filename 'snap' instead of 'snapshot', AND its a multi-part file?
			fname=fname_base+'.0'+extension_touse;
		if not os.path.exists(fname): 
			## is it in a snapshot sub-directory? (we assume this means multi-part files)
			fname_base=sdir+'/snapdir_'+ext+'/'+snapshot_name+'_'+ext; 
			fname=fname_base+'.0'+extension_touse;
		if not os.path.exists(fname): 
			## is it in a snapshot sub-directory AND named 'snap' instead of 'snapshot'?
			fname_base=sdir+'/snapdir_'+ext+'/'+'snap_'+ext; 
			fname=fname_base+'.0'+extension_touse;
		if not os.path.exists(fname): 
			## wow, still couldn't find it... ok, i'm going to give up!
			fname_found = 'NULL'
			fname_base_found = 'NULL'
			fname_ext = 'NULL'
			continue;
		fname_found = fname;
		fname_base_found = fname_base;
		fname_ext = extension_touse
		break; # filename does exist! 
	return fname_found, fname_base_found, fname_ext;

def load_gadget_binary_header(f):
	### Read header.
	import array
	# Skip 4-byte integer at beginning of header block.
	f.read(4)
	# Number of particles of each type. 6*unsigned integer.
	Npart = array.array('I')
	Npart.fromfile(f, 6)
	# Mass of each particle type. If set to 0 for a type which is present, 
	# individual particle masses from the 'mass' block are used instead.
	# 6*double.
	Massarr = array.array('d')
	Massarr.fromfile(f, 6)
	# Expansion factor (or time, if non-cosmological sims) of output. 1*double. 
	a = array.array('d')
	a.fromfile(f, 1)
	a = a[0]
	# Redshift of output. Should satisfy z=1/a-1. 1*double.
	z = array.array('d')
	z.fromfile(f, 1)
	z = float(z[0])
	# Flag for star formation. 1*int.
	FlagSfr = array.array('i')
	FlagSfr.fromfile(f, 1)
	# Flag for feedback. 1*int.
	FlagFeedback = array.array('i')
	FlagFeedback.fromfile(f, 1)
	# Total number of particles of each type in the simulation. 6*int.
	Nall = array.array('i')
	Nall.fromfile(f, 6)
	# Flag for cooling. 1*int.
	FlagCooling = array.array('i')
	FlagCooling.fromfile(f, 1)
	# Number of files in each snapshot. 1*int.
	NumFiles = array.array('i')
	NumFiles.fromfile(f, 1)
	# Box size (comoving kpc/h). 1*double.
	BoxSize = array.array('d')
	BoxSize.fromfile(f, 1)
	# Matter density at z=0 in units of the critical density. 1*double.
	Omega0 = array.array('d')
	Omega0.fromfile(f, 1)
	# Vacuum energy density at z=0 in units of the critical density. 1*double.
	OmegaLambda = array.array('d')
	OmegaLambda.fromfile(f, 1)
	# Hubble parameter h in units of 100 km s^-1 Mpc^-1. 1*double.
	h = array.array('d')
	h.fromfile(f, 1)
	h = float(h[0])
	# Creation times of stars. 1*int.
	FlagAge = array.array('i')
	FlagAge.fromfile(f, 1)
	# Flag for metallicity values. 1*int.
	FlagMetals = array.array('i')
	FlagMetals.fromfile(f, 1)

	# For simulations that use more than 2^32 particles, most significant word 
	# of 64-bit total particle numbers. Otherwise 0. 6*int.
	NallHW = array.array('i')
	NallHW.fromfile(f, 6)

	# Flag that initial conditions contain entropy instead of thermal energy
	# in the u block. 1*int.
	flag_entr_ics = array.array('i')
	flag_entr_ics.fromfile(f, 1)

	# Unused header space. Skip to particle positions.
	f.seek(4+256+4+4)

	return {'NumPart_ThisFile':Npart, 'MassTable':Massarr, 'Time':a, 'Redshift':z, \
	'Flag_Sfr':FlagSfr[0], 'Flag_Feedback':FlagFeedback[0], 'NumPart_Total':Nall, \
	'Flag_Cooling':FlagCooling[0], 'NumFilesPerSnapshot':NumFiles[0], 'BoxSize':BoxSize[0], \
	'Omega0':Omega0[0], 'OmegaLambda':OmegaLambda[0], 'HubbleParam':h, \
	'Flag_StellarAge':FlagAge[0], 'Flag_Metals':FlagMetals[0], 'Nall_HW':NallHW, \
	'Flag_EntrICs':flag_entr_ics[0]}

def load_gadget_binary_particledat(f, header, ptype, skip_bh=0):
	## load old format=1 style gadget binary snapshot files (unformatted fortran binary)
	import array
	gas_u=0.; gas_rho=0.; gas_ne=0.; gas_nhi=0.; gas_hsml=0.; gas_SFR=0.; star_age=0.; 
	zmet=0.; bh_mass=0.; bh_mdot=0.; mm=0.;
	Npart = header['NumPart_ThisFile']
	Massarr = header['MassTable']
	NpartTot = np.sum(Npart)
	NpartCum = np.cumsum(Npart)
	n0 = NpartCum[ptype] - Npart[ptype]
	n1 = NpartCum[ptype]
	
	### particles positions. 3*Npart*float.
	pos = array.array('f')
	pos.fromfile(f, 3*NpartTot)
	pos = np.reshape(pos, (NpartTot,3))
	f.read(4+4) # Read block size fields.

	### particles velocities. 3*Npart*float.
	vel = array.array('f')
	vel.fromfile(f, 3*NpartTot)
	vel = np.reshape(vel, (NpartTot,3))
	f.read(4+4) # Read block size fields.

	### Particle IDs. # (Npart[0]+...+Npart[5])*int
	id = array.array('i')
	id.fromfile(f, NpartTot)
	id = np.array(id)
	f.read(4+4) # Read block size fields.
		
	### Variable particle masses. 
	Npart_MassCode = np.copy(np.array(Npart))
	Npart=np.array(Npart)
	Npart_MassCode[(Npart <= 0) | (np.array(Massarr,dtype='d') > 0.0)] = 0L
	NwithMass = np.sum(Npart_MassCode)
	mass = array.array('f')
	mass.fromfile(f, NwithMass)
	f.read(4+4) # Read block size fields.
	if (Massarr[ptype]==0.0):
		Npart_MassCode_Tot = np.cumsum(Npart_MassCode)
		mm = mass[Npart_MassCode_Tot[ptype]-Npart_MassCode[ptype]:Npart_MassCode_Tot[ptype]]

	if ((ptype==0) | (ptype==4) | (ptype==5)):
		if (Npart[0]>0):
			### Internal energy of gas particles ((km/s)^2).
			gas_u = array.array('f')
			gas_u.fromfile(f, Npart[0])
			f.read(4+4) # Read block size fields.
			### Density for the gas paraticles (units?).
			gas_rho = array.array('f')
			gas_rho.fromfile(f, Npart[0])
			f.read(4+4) # Read block size fields.

			if (header['Flag_Cooling'] > 0):
				### Electron number density for gas particles (fraction of n_H; can be >1).
				gas_ne = array.array('f')
				gas_ne.fromfile(f, Npart[0])
				f.read(4+4) # Read block size fields.
				### Neutral hydrogen number density for gas particles (fraction of n_H).
				gas_nhi = array.array('f')
				gas_nhi.fromfile(f, Npart[0])
				f.read(4+4) # Read block size fields.

			### Smoothing length (kpc/h). ###
			gas_hsml = array.array('f')
			gas_hsml.fromfile(f, Npart[0])
			f.read(4+4) # Read block size fields.

			if (header['Flag_Sfr'] > 0):
				### Star formation rate (Msun/yr). ###
				gas_SFR = array.array('f')
				gas_SFR.fromfile(f, Npart[0])
				f.read(4+4) # Read block size fields.

		if (Npart[4]>0):
			if (header['Flag_Sfr'] > 0):
				if (header['Flag_StellarAge'] > 0):
					### Star formation time (in code units) or scale factor ###
					star_age = array.array('f')
					star_age.fromfile(f, Npart[4])
					f.read(4+4) # Read block size fields.
		
		if (Npart[0]+Npart[4]>0):
			if (header['Flag_Metals'] > 0):
				## Metallicity block (species tracked = Flag_Metals)
				if (Npart[0]>0):
					gas_z = array.array('f')
					gas_z.fromfile(f, header['Flag_Metals']*Npart[0])
				if (Npart[4]>0):
					star_z = array.array('f')
					star_z.fromfile(f, header['Flag_Metals']*Npart[4])
				f.read(4+4) # Read block size fields.
				if (ptype==0): zmet=np.reshape(gas_z,(-1,header['Flag_Metals']))
				if (ptype==4): zmet=np.reshape(star_z,(-1,header['Flag_Metals']))
		
		if (Npart[5]>0):
			if (skip_bh > 0):
				## BH mass (same as code units, but this is the separately-tracked BH mass from particle mass)
				bh_mass = array.array('f')
				bh_mass.fromfile(f, Npart[5])
				f.read(4+4) # Read block size fields.
				## BH accretion rate in snapshot
				bh_mdot = array.array('f')
				bh_mdot.fromfile(f, Npart[5])
				f.read(4+4) # Read block size fields.
	
	return {'Coordinates':pos[n0:n1,:], 'Velocities':vel[n0:n1,:], 'ParticleIDs':id[n0:n1], \
		'Masses':mm, 'Metallicity':zmet, 'StellarFormationTime':star_age, 'BH_Mass':bh_mass, \
		'BH_Mdot':bh_mdot, 'InternalEnergy':gas_u, 'Density':gas_rho, 'SmoothingLength':gas_hsml, \
		'ElectronAbundance':gas_ne, 'NeutralHydrogenAbundance':gas_nhi, 'StarFormationRate':gas_SFR}

def get_haloColumns(filename, iHalo, cols):
	"""Return values of column(s) in halo file

	Keyword arguments:
	filename -- of the halo file
	iHalo	-- haloID(>=0) or read whole column(s) (<0)
	cols	 -- specific column (>=1) or list of columns
				note: column numbering in cols starts with 1
				if single column==0 given => read whole line
	"""
	if (isinstance(cols, list) and 0 in cols):
		print 'ERROR: column indices start from 1'
		return -1

	p = re.compile('#')
	return_data = []
	with open(filename, 'r') as f:
		lineIndex = 0
		for line in f:
			lines = p.split(line, maxsplit=1)
			line = lines[0]  # now all comments are removed
			line = line.strip()
			if not line:  # continue if empty line
				continue
			columns = line.split()
			if iHalo < 0 or lineIndex == iHalo:
				if isinstance(cols, list):
					data = [columns[i-1] for i in cols]
				elif cols == 0:
					data = line.strip()
				else:
					data = columns[cols-1]
				if iHalo >= 0:
					return_data = data 
					break
				else:
					return_data.append(data)
			lineIndex += 1 

	return return_data

def get_haloRvir(filename, iHalo):
	"""Return center of halo (from halo file)"""
	return map(float, get_haloColumns(filename, iHalo, [12]))

def get_haloParent(filename, iHalo):
	"""Return center of halo (from halo file)"""
	return map(float, get_haloColumns(filename, iHalo, [2]))

def get_haloCenter(filename, iHalo):
	"""Return center of halo (from halo file)"""
	return map(float, get_haloColumns(filename, iHalo, [6, 7, 8]))

def Rmax(halodir, snap_dir, i, comoving, iHalo, dummy):
	i_new = i + dummy

	halodir = halodir + str(i_new) + '/'
	halodir = glob.glob(halodir + '*_halos')[0]

	halo_center = np.array(get_haloCenter(halodir, iHalo))

	data_header = []
	snapshot_dir = snap_dir + str(i_new)

	data_header = readsnap(snapshot_dir, snum=i_new, ptype=0, header_only=1)
	redshift = data_header['redshift']
	hubble = data_header['hubble']

	if comoving==1:
		ascale = 1.0 / (1.0 + redshift)
		hinv = 1.0 / hubble
		constant = ascale * hinv
	else:
		ascale = 1.0 
		hinv = 1.0 
		constant = ascale * hinv

	halo_center *= constant

	data_gas = []
	data_halo = []
	data_star = []
	radius_gas = []
	radius_halo = []
	radius_star = []

	if comoving == 1:
		data_gas = readsnap(snapshot_dir, snum=i_new, ptype=0, cosmological=1)
		data_halo = readsnap(snapshot_dir, snum=i_new, ptype=1, cosmological=1)
		data_star = readsnap(snapshot_dir, snum=i_new, ptype=4, cosmological=1)
	else:
		data_gas = readsnap(snapshot_dir, snum=i_new, ptype=0, cosmological=0)
		data_halo = readsnap(snapshot_dir, snum=i_new, ptype=1, cosmological=0)
		data_star = readsnap(snapshot_dir, snum=i_new, ptype=4, cosmological=0)

	rmax = []
	vmax = []
	radius_list = np.arange(1.0,50.1,1.0)
	for rvir in radius_list:
		#GAS PARTICLES
		radius_gas = np.sqrt(np.sum(np.square(data_gas['p'] - halo_center), axis=1))
		sel_gas = (radius_gas <= rvir)
		data_gas_sel = select_particles(data_gas, sel_gas)
		data_gas_sel['p'] -= halo_center
		#HALO PARTICLES
		radius_halo = np.sqrt(np.sum(np.square(data_halo['p'] - halo_center), axis=1))
		sel_halo = (radius_halo <= rvir)
		data_halo_sel = select_particles(data_halo, sel_halo)
		data_halo_sel['p'] -= halo_center
		#STAR PARTICLES
		radius_star = np.sqrt(np.sum(np.square(data_star['p'] - halo_center), axis=1))
		sel_star = (radius_star <= rvir)
		data_star_sel = select_particles(data_star, sel_star)
		data_star_sel['p'] -= halo_center

		rmax.append(rvir)
		mass_dummy = (np.sum(data_gas_sel['m']) + np.sum(data_halo_sel['m']) + np.sum(data_star_sel['m'])) 
		velocity = np.sqrt(grav_constant * mass_dummy * m_sun / (rvir * kpc_cm_conversion)) *1e-5
		vmax.append(velocity)

	v_max = max(vmax)
	r_max = rmax[vmax.index(v_max)]

	#fig = plt.figure(1)
	#fig.subplots_adjust(wspace=0, hspace=0)
	#ax1 = fig.add_subplot(111)
	#ax1.scatter(r_max, v_max, color='b', marker='+', s=100)
	#ax1.scatter(rmax, vmax, color='r', marker='.', s=50, label=r'$halo#0$')
	#ax1.plot(rmax, vmax, color='r', ls='-', lw=2)
	#ax1.set_xscale('linear')
	#ax1.set_yscale('linear')
	#ax1.set_xlim(0,20)
	#ax1.set_ylim(0,200)
	#ax1.xaxis.set_major_locator(ticker.MaxNLocator(prune=None, nbins=10))
	#ax1.xaxis.set_minor_locator(ticker.MultipleLocator(0.5))
	#ax1.yaxis.set_major_locator(plt.FixedLocator([25,50,75,100,125,150,175]))
	#ax1.tick_params(axis='y', labelsize=10)
	#ax1.set_xlabel(r'$Radius\;(kpc)$', size=20)
	#ax1.set_ylabel(r'$Velocity\;(km/s)$')
	#ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	#plt.savefig('deneme.jpeg', format='jpeg')
	#GAS PARTICLES
	radius_gas = np.sqrt(np.sum(np.square(data_gas['p'] - halo_center), axis=1))
	sel_gas = (radius_gas <= r_max)
	data_gas_sel = select_particles(data_gas, sel_gas)
	#data_gas_sel['p'] -= halo_center
	#HALO PARTICLES
	radius_halo = np.sqrt(np.sum(np.square(data_halo['p'] - halo_center), axis=1))
	sel_halo = (radius_halo <= r_max)
	data_halo_sel = select_particles(data_halo, sel_halo)
	#data_halo_sel['p'] -= halo_center
	#STAR PARTICLES
	radius_star = np.sqrt(np.sum(np.square(data_star['p'] - halo_center), axis=1))
	sel_star = (radius_star <= r_max)
	data_star_sel = select_particles(data_star, sel_star)
	#data_star_sel['p'] -= halo_center

	mass_dummy = (np.sum(data_gas_sel['m']) + np.sum(data_halo_sel['m']) + np.sum(data_star_sel['m']))  

	return r_max, mass_dummy

def filter_Rvir(halodir, snap_dir, savepath, i, comoving, iHalo_list, rvir_per, dummy):

	i_new = i + dummy

	directory = savepath
	if not os.path.exists(directory):
		os.makedirs(directory)
		
	iHalo_list_new = []
	for iHalo in iHalo_list:
		data_filename = directory + 'snapshot_' + str('%03d' %i_new) + '_ID_' + str(iHalo) + '.hdf5'
		if os.path.isfile(data_filename):
			print data_filename
			print "does exist"
			continue
		else:
			iHalo_list_new.append(iHalo)

	if iHalo_list_new:
	
		halodir = halodir + str('%03d' %i_new) + '/'
		halodir = glob.glob(halodir + '*AHF_halos')[0]
		
		data_header = []
		snapshot_dir = snap_dir + str('%03d' %i_new)
		if dummy != 178:
			data_header = readsnap(snapshot_dir, snum=i_new, ptype=0, header_only=1)
		else:
			data_header = readsnap_compact(snapshot_dir, snum=i_new, ptype=0, header_only=1)
		
		time = data_header['time']
		redshift = data_header['redshift']
		hubble = data_header['hubble']

		if comoving==1:
			ascale = 1.0 / (1.0 + redshift)
			hinv = 1.0 / hubble
			constant = ascale * hinv
		else:
			ascale = 1.0 
			hinv = 1.0 
			constant = ascale * hinv

		data_gas = []
		data_halo = []
		data_star = []

		if dummy != 178:
			data_gas = readsnap(snapshot_dir, snum=i_new, ptype=0, cosmological=comoving)
			#data_halo = readsnap(snapshot_dir, snum=i_new, ptype=1, cosmological=comoving)
			data_star = readsnap(snapshot_dir, snum=i_new, ptype=4, cosmological=comoving)
		else:
			data_gas = readsnap_compact(snapshot_dir, snum=i_new, ptype=0, cosmological=comoving)
			#data_halo = readsnap_compact(snapshot_dir, snum=i_new, ptype=1, cosmological=comoving)
			data_star = readsnap_compact(snapshot_dir, snum=i_new, ptype=4, cosmological=comoving)

		halo_i = []
		halo_c = []
		halo_rvir = []

		halo_data = open(halodir, 'r')
		next(halo_data)
		for line in halo_data:
			halodata = map(float, np.array(line.split()))
			halo_i.append(int(halodata[0]))
			halo_c.append(halodata[5:8])
			halo_rvir.append(halodata[11])

		for iHalo in iHalo_list_new:

			data_filename = directory + 'snapshot_' + str('%03d' %i_new) + '_ID_' + str(iHalo) + '.hdf5'

			radius_gas = []
			#radius_halo = []
			radius_star = []

			halo_center = np.array([b1 for a1,b1 in zip(halo_i,halo_c) if a1==iHalo])[0]
			rvir = np.array([b1 for a1,b1 in zip(halo_i,halo_rvir) if a1==iHalo])[0]

			halo_center *= constant
			rvir *= constant
			rvir *= rvir_per # 
			#GAS PARTICLES
			radius_gas = np.sqrt(np.sum(np.square(data_gas['p'] - halo_center), axis=1))
			sel_gas = (radius_gas <= rvir)
			data_gas_sel = select_particles(data_gas, sel_gas)
			data_gas_sel['p'] -= halo_center

			#HALO PARTICLES
			#radius_halo = np.sqrt(np.sum(np.square(data_halo['p'] - halo_center), axis=1))
			#sel_halo = (radius_halo <= rvir)
			#data_halo_sel = select_particles(data_halo, sel_halo)
			#data_halo_sel['p'] -= halo_center

			#STAR PARTICLES
			radius_star = np.sqrt(np.sum(np.square(data_star['p'] - halo_center), axis=1))
			sel_star = (radius_star <= rvir)
			data_star_sel = select_particles(data_star, sel_star)
			data_star_sel['p'] -= halo_center

			center = [list(halo_center - halo_center)]

			npart_gas = len(data_gas_sel['m'])
			npart_halo = 0#len(data_halo_sel['m'])
			npart_disk = 0
			npart_bulge = 0
			npart_star = len(data_star_sel['m'])
			npart_bndry = 1

			f = h5py.File(data_filename,'w')
			f.create_group("Header")

			if dummy == 178:
				f["Header"].attrs["CompactLevel"] = data_header["compactlevel"]

			f["Header"].attrs["NumPart_ThisFile"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
			f["Header"].attrs["NumPart_Total"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
			f["Header"].attrs["NumPart_Total_HighWord"] = data_header["npartTotal_high"]
			f["Header"].attrs["MassTable"] = data_header["massarr"]
			f["Header"].attrs["Time"] = data_header["time"]
			f["Header"].attrs["Redshift"] = data_header["redshift"]
			f["Header"].attrs["BoxSize"] = data_header["boxsize"]
			f["Header"].attrs["NumFilesPerSnapshot"] = 1
			f["Header"].attrs["Omega0"] = data_header["omega_matter"]
			f["Header"].attrs["OmegaLambda"] = data_header["omega_lambda"]
			f["Header"].attrs["HubbleParam"] = data_header["hubble"]
			f["Header"].attrs["Flag_Sfr"] = data_header["flag_sfr"]
			f["Header"].attrs["Flag_Cooling"] = data_header["flag_cooling"]
			f["Header"].attrs["Flag_StellarAge"] = data_header["flag_stellarage"]
			f["Header"].attrs["Flag_Metals"] = data_header["flag_metals"]
			f["Header"].attrs["Flag_Feedback"] = data_header["flag_feedbacktp"]
			f["Header"].attrs["Flag_DoublePrecision"] = data_header["flag_doubleprecision"]
			f["Header"].attrs["Flag_IC_Info"] = data_header["flag_icinfo"]
			f.create_dataset('PartType0/Coordinates', data=data_gas_sel['p'], compression='gzip')
			f.create_dataset('PartType0/Velocities', data=data_gas_sel['v'], compression='gzip')
			f.create_dataset('PartType0/Density', data=data_gas_sel['rho'], compression='gzip')
			f.create_dataset('PartType0/Masses', data=data_gas_sel['m'], compression='gzip')
			f.create_dataset('PartType0/ElectronAbundance', data=data_gas_sel['ne'], compression='gzip')
			f.create_dataset('PartType0/InternalEnergy', data=data_gas_sel['u'], compression='gzip')
			f.create_dataset('PartType0/Metallicity', data=data_gas_sel['z'], compression='gzip')
			f.create_dataset('PartType0/NeutralHydrogenAbundance', data=data_gas_sel['nh'], compression='gzip')
			f.create_dataset('PartType0/StarFormationRate', data=data_gas_sel['sfr'], compression='gzip')
			#f.create_dataset('PartType1/Coordinates', data=data_halo_sel['p'], compression='gzip')
			#f.create_dataset('PartType1/Velocities', data=data_halo_sel['v'], compression='gzip')
			f.create_dataset('PartType4/Coordinates', data=data_star_sel['p'], compression='gzip')
			f.create_dataset('PartType4/Velocities', data=data_star_sel['v'], compression='gzip')
			f.create_dataset('PartType4/Masses', data=data_star_sel['m'], compression='gzip')
			f.create_dataset('PartType4/Metallicity', data=data_star_sel['z'], compression='gzip')
			f.create_dataset('PartType4/Density', data=data_star_sel['rho'], compression='gzip')
			f.create_dataset('PartType4/StellarFormationTime', data=data_star_sel['age'], compression='gzip')
			f.create_dataset("PartType5/Coordinates", data=center, compression='gzip') 
			f.close()

def trace_halo(halo_dir, snap_dir, i_prev, i_next, iHalo_prev, iHalo_next, comoving, dummy):
	i_new_prev = i_prev + dummy
	i_new_next = i_next + dummy

	halodir_prev = halo_dir + str(i_new_prev) + '/'
	halodir_prev = glob.glob(halodir_prev + '*_halos')[0]
	halodir_next = halo_dir + str(i_new_next) + '/'
	halodir_next = glob.glob(halodir_next + '*_halos')[0]   

	halo_center_prev = np.array(get_haloCenter(halodir_prev, iHalo_prev))
	halo_center_next = np.array(get_haloCenter(halodir_next, iHalo_next)) 
	rvir_prev = np.array(get_haloRvir(halodir_prev, iHalo_prev))
	rvir_next = np.array(get_haloRvir(halodir_next, iHalo_next))

	snapshot_dir_prev = snap_dir + str(i_new_prev)
	snapshot_dir_next = snap_dir + str(i_new_next)

	data_header_prev = []
	data_header_next = []

	data_header_prev = readsnap(snapshot_dir_prev, snum=i_new_prev, ptype=0, header_only=1)
	hubble_prev = data_header_prev["hubble"]
	redshift_prev = data_header_prev["redshift"]

	data_header_next = readsnap(snapshot_dir_next, snum=i_new_next, ptype=0, header_only=1)
	hubble_next = data_header_next["hubble"]
	redshift_next = data_header_next["redshift"]

	data_prev = []
	data_next = []

	if comoving == 0:
		halo_center_prev = halo_center_prev
		halo_center_next = halo_center_next
		rvir_prev = rvir_prev
		rvir_next = rvir_next

		data_prev = readsnap(snapshot_dir_prev, snum=i_new_prev, ptype=4, cosmological=0)
		data_next = readsnap(snapshot_dir_next, snum=i_new_next, ptype=4, cosmological=0)

	else:
		halo_center_prev = halo_center_prev / (1.0 + redshift_prev) / hubble_prev
		halo_center_next = halo_center_next / (1.0 + redshift_next) / hubble_next
		rvir_prev = rvir_prev / (1.0 + redshift_prev) / hubble_prev
		rvir_next = rvir_next / (1.0 + redshift_next) / hubble_next

		data_prev = readsnap(snapshot_dir_prev, snum=i_new_prev, ptype=4, cosmological=1)
		data_next = readsnap(snapshot_dir_next, snum=i_new_next, ptype=4, cosmological=1)

	radius_prev = np.sqrt(np.sum(np.square(data_prev['p'] - halo_center_prev), axis=1))

	#PREV STAR PARTICLES
	sel_prev = (radius_prev <= rvir_prev)
	data_prev_sel = select_particles(data_prev, sel_prev)
	data_prev_sel['p'] -= halo_center_prev
	#NEXT STAR PARTICLES FILTERED
	data_next_sel = dict_filter(data_next,data_prev_sel,'id')
	data_next_sel['p'] -= halo_center_prev
	center_prev = [list(halo_center_prev - halo_center_prev)]
	center_next = [list(halo_center_next - halo_center_prev)]

	npart_gas = 0
	npart_halo = len(data_prev_sel['m'])
	npart_disk = 0 
	npart_bulge = 1  
	npart_star = len(data_next_sel['m'])
	npart_bndry = 1  

	data_filename = './snapshot_' + str(i_new_prev) + '_' + str(i_new_next) + '_' + str('%.2f' %rvir_prev) + 'kpc.hdf5'

	f = h5py.File(data_filename,'w')
	f.create_group("Header")
	f["Header"].attrs["NumPart_ThisFile"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
	f["Header"].attrs["NumPart_Total"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
	f["Header"].attrs["NumPart_Total_HighWord"] = data_header["npartTotal_high"]
	f["Header"].attrs["MassTable"] = data_header["massarr"]
	f["Header"].attrs["Time"] = data_header["time"]
	f["Header"].attrs["Redshi#ft"] = data_header["redshi#ft"]
	f["Header"].attrs["BoxSize"] = data_header["boxsize"]
	f["Header"].attrs["NumFilesPerSnapshot"] = 1
	f["Header"].attrs["Omega0"] = data_header["omega_matter"]
	f["Header"].attrs["OmegaLambda"] = data_header["omega_lambda"]
	f["Header"].attrs["HubbleParam"] = data_header["hubble"]
	f["Header"].attrs["Flag_S#fr"] = data_header["#flag_s#fr"]
	f["Header"].attrs["Flag_Cooling"] = data_header["#flag_cooling"]
	f["Header"].attrs["Flag_StellarAge"] = data_header["#flag_stellarage"]
	f["Header"].attrs["Flag_Metals"] = data_header["#flag_metals"]
	f["Header"].attrs["Flag_Feedback"] = data_header["#flag_#feedbacktp"]
	f["Header"].attrs["Flag_DoublePrecision"] = data_header["#flag_doubleprecision"]
	f["Header"].attrs["Flag_IC_In#fo"] = data_header["#flag_icin#fo"]
	f["PartType0/Coordinates"] = data_gas_sel['p']
	f["PartType0/Density"] = data_gas_sel['rho']
	f["PartType0/ElectronAbundance"] = data_gas_sel['ne']
	f["PartType0/InternalEnergy"] = data_gas_sel['u']
	f["PartType0/Masses"] = data_gas_sel['m']
	f["PartType0/Metallicity"] = data_gas_sel['z']
	f["PartType0/NeutralHydrogenAbundance"] = data_gas_sel['nh']
	f["PartType0/StarFormationRate"] = data_gas_sel['s#fr']
	f["PartType0/Velocities"] = data_gas_sel['v']
	f["PartType1/Coordinates"] = data_prev_sel['p']
	f["PartType1/Velocities"] = data_prev_sel['v']
	f["PartType1/Masses"] = data_prev_sel['m']
	f["PartType1/Metallicity"] = data_prev_sel['z']
	f["PartType1/StellarFormationTime"] = data_prev_sel['age']
	f["PartType3/Coordinates"] = center_prev
	f["PartType4/Coordinates"] = data_next_sel['p']
	f["PartType4/Velocities"] = data_next_sel['v']
	f["PartType4/Masses"] = data_next_sel['m']
	f["PartType4/Metallicity"] = data_next_sel['z']
	f["PartType4/StellarFormationTime"] = data_next_sel['age']
	f["PartType5/Coordinates"] = center_next
	f.close()

	return data_prev_sel, data_next_sel

def quadratic_sum(x):
	return x[:,0]*x[:,0] + x[:,1]*x[:,1] + x[:,2]*x[:,2]

def calculate_outflow(gas_pos, gas_mass, gas_vel, r_shell, shell_thickness):

	r = quadratic_sum(gas_pos)
	radius = np.sqrt(r)
	yr_s_conversion = 3.1536e7
	kpc_to_km_conversion = 3.086e16

	sel = (radius > r_shell * (1.0 - shell_thickness)) & (radius < r_shell * (1.+shell_thickness))
	pos = gas_pos[sel]
	m_gas = gas_mass[sel]
	vel_gas = gas_vel[sel]
	r_gas = radius[sel]
	v_r = 0.0 * r_gas
	for j in [0,1,2]: v_r += pos[:,j] * vel_gas[:,j] / r_gas;
	delta_r = r_shell*(2.0 * shell_thickness)
	mdot = m_gas * v_r * yr_s_conversion/ (delta_r * kpc_to_km_conversion)
	outflow = np.sum(mdot[v_r > 0.0])
	inflow = -np.sum(mdot[v_r < 0.0])
	return inflow, outflow
