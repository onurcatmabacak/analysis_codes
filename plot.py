import os
import pdb
import numpy as np
import onur_tools as ot
import h5py
from scipy.optimize import curve_fit
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
from astropy import units as u
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy import integrate
from scipy.special import lambertw
from scipy import stats

def time(i):
	return cosmo.age(i).value * 1e9 # yr

def weighted_average(data, weight, step):
	radius = np.linspace(np.min(weight), np.max(weight), step+1)
	weighted_average = []
	radius_mean = []

	for i in range(len(radius) - 1):
		total_data = np.array([a for a,b in zip(data, weight) if b >= radius[i] if b <= radius[i+1]])
		total_weight = np.array([b for b in weight if b >= radius[i] if b <= radius[i+1]]) 
		weighted_average.append(np.sum(total_data * total_weight) / np.sum(total_weight))
		radius_mean.append((radius[i]+radius[i+1])/2.0)
	return weighted_average, radius_mean

def Muratov15(mstar,a,b):
	return np.log10(a) + b * np.log10(mstar*1e-10)
	#return a * (Mstar**b) # Mstar is normalized with 1e10 M_solar

def HH15(mstar,a,b,c):
	return np.log10(a) + b * np.log10(mstar*1e-10) + np.log10(np.exp(c*mstar*1e-10))

def HH15_eq44(mstar,mgas,a,b,c):
	return np.log10(a) + b* np.log10(mgas*mstar/(mgas+mstar)*1e-10) + np.log10(np.exp(c/mgas*(mgas+mstar)))
	#return np.log10(a) +b* np.log10((mgas*mstar/(mgas+mstar)*1e-10))  +np.log10( np.exp(c/mgas*(mgas+mstar)))

def Ferrerase02(Mass):	
	#return 10.**( 8.0 ) * 0.027 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	return 10.**( 8.0 ) * 0.100 * ( Mass/1e12 )**(1.65) # Ferrerase 2002
	#return 10.**( 8.0 ) * 0.670 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	#return 10.**( 8.18 ) * ( Mass/1e13 )**(1.55) # Bandara et al 2009
	#return 10.**( 7.85 ) * ( Mass/1e12 )**(1.33) # Di Matteo et al 2003

def bin_average(x,y, nbins):
	
	x = np.log10(x[(y != 0.0)])
	y = np.log10(y[(y != 0.0)])
	bins = np.linspace(np.min(x), np.max(x), nbins+1)

	xresult = []
	yresult = []
	for i in range(len(bins)-1):
		sel = (x <= bins[i+1] ) & (x >= bins[i])	
		xdata = np.array(x[sel])
		ydata = np.array(y[sel])
		yresult.append(np.sum(xdata * ydata) / np.sum(xdata))
		xresult.append(np.sum(xdata) / len(xdata))

	xx = 10.0 ** (np.array(xresult))
	yy = 10.0 ** (np.array(yresult))

	return xx, yy

def Haring04(Mbulge):

   return 10.**( 8.20 ) * ( Mbulge/1e11 )**(1.12)


def Kormendy2013(Mbulge):

   return 0.49 * 1e9 * ( Mbulge/1e11 )**(1.16)


def McConnell2013(Mbulge):

   return 10.**( 8.46 ) * ( Mbulge/1e11 )**(1.05)

def Reines2015(Mstar):

   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

def Savorgnan2016( Mstar, sample='all'):
	Mbh_early = 10.**( 8.56 + 1.04*np.log10(Mstar/10.**10.81) )
	Mbh_late = 10.**( 7.24 + 2.28*np.log10(Mstar/10.**10.05) )

	ind = np.where( Mbh_late - Mbh_early >= 0 )[0] 
	Mbh_all =  np.append( Mbh_late[:ind[0]], Mbh_early[ind[0]:] )

	if Mbh_all.size != Mstar.size:
	 print 'ahhhh.... check Savorgnan2016 !!!'

	if sample == 'early':
	 return Mbh_early
	elif sample == 'late':
	 return Mbh_late
	elif sample == 'all':
	 return Mbh_all

def McConnell_Ma2013(vel_disp):
	return 10.**( 8.32 ) * ( vel_disp/200.0 )**(5.64)

#####################
###### COLD GAS #####
#####################

def scaling_relations(output_path, i):

	data = data_merger

	for regime in regime_list:

		for radius_name in radius_name_list:

			m_star = data[regime + '/' + radius_name_list[-1] + '/mstar'].value
			m_bulge = data[regime + '/' + radius_name_list[-1] + '/mbulge'].value
			m_halo = data[regime + '/' + radius_name_list[-1] + '/mhalo'].value
	
			seed_mass_list = [1, 2, 3, 4, 5]	
			for seed_mass_name,seed_mass in zip(seed_mass_name_list, seed_mass_list):

				fig, axes = plt.subplots(2,3)
				fig.subplots_adjust(hspace = 0, wspace=0)
				fig.suptitle(sim_name[i])
				ax = axes.ravel()

				for k in range(len(mass_retention)):

					m_bh_torque = data[regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention[k]) + '/seed_' + seed_mass_name + '/m_bh_torque'].value

					ax[k].plot(m_star, m_bh_torque, color='r', ls='-', lw=2, label=r'$M_{*}$')
					ax[k].plot(m_bulge, m_bh_torque, color='b', ls='-', lw=2, label=r'$M_{bulge}$')
					ax[k].plot(m_halo, m_bh_torque, color='k', ls='-', lw=2, label=r'$M_H$')
					ax[k].plot(mass_list, Ferrerase02_data, color='k', ls='--', lw=1, label='Ferrerase02')
					ax[k].plot(mass_list, Haring04_data, color='b', ls='--', lw=1, label='Haring04')
					ax[k].plot(mass_list, Reines2015_data, color='r', ls='--', lw=1, label='Reines15')

					ax[k].set_xscale('log')
					ax[k].set_yscale('log')

					ax[k].set_xlim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]))
					ax[k].set_ylim(np.power(10.0,seed_mass),np.power(10.0,bh_upper[i]))

					ax[k].yaxis.set_major_locator(plt.FixedLocator(np.logspace(seed_mass,bh_upper[i]-1,num=bh_upper[i]-seed_mass)))
					ax[k].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))

					ax[k].tick_params(axis='x', labelsize=8)
					ax[k].tick_params(axis='y', labelsize=8)
					ax[k].text(0.9*(10**mass_upper[i]), 1.5*(10.0**seed_mass), '%' + str(int(100.0 * mass_retention[k])), verticalalignment='bottom', horizontalalignment='right')
					ax[k].fill_between(mass_list, Ferrerase02_data * np.sqrt(1e1), Ferrerase02_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
					ax[k].fill_between(mass_list, Haring04_data * np.sqrt(1e1), Haring04_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.1 )
					ax[k].fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='red', zorder=0, alpha=0.1 )
					#ax[k].fill_between(mass_list, Savorgnan2016_data * np.sqrt(1e1), Savorgnan2016_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.3 )
					ax[k].legend(bbox_to_anchor=(2.05,-0.1), loc="upper left", prop={'size':6},frameon=False,scatterpoints=1)

					if k == 0:
						ax[k].get_xaxis().set_ticks([])
						#ax[k].get_yaxis().set_ticks([])
						ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
					if k == 1:
						ax[k].get_xaxis().set_ticks([])
						ax[k].get_yaxis().set_ticks([])
						#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
					if k == 2:
						#ax[k].get_xaxis().set_ticks([])
						ax[k].get_yaxis().set_ticks([])
						#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
					if k == 3:
						#ax[k].get_xaxis().set_ticks([])
						#ax[k].get_yaxis().set_ticks([])
						ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
					if k == 4:
						#ax[k].get_xaxis().set_ticks([])
						ax[k].get_yaxis().set_ticks([])
						#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
				
					#ax[5].get_xaxis().set_ticks([])
					#ax[5].get_yaxis().set_ticks([])
					#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
					#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')

					if (k != 0):
						ax[k].legend().set_visible(False)

				fig.delaxes(ax.flatten()[-1])
				#plt.tight_layout()
				plt.savefig(output_path + 'mbh_vs_halo_star_bulge_' + regime + '_' + radius_name + '_kpc_seed_' + seed_mass_name + '.png', dpi=300, format='png')
				plt.close()

def BH_scaling_relations(output_path, i):

	data = data_merger

	for regime in regime_list:

		for radius_name in radius_name_list:

			m_star = data[regime + '/' + radius_name_list[-1] + '/mstar'].value
			m_bulge = data[regime + '/' + radius_name_list[-1] + '/mbulge'].value
			m_halo = data[regime + '/' + radius_name_list[-1] + '/mhalo'].value
			vel_disp = data[regime + '/' + radius_name_list[-1] + '/vel_disp'].value 
			#vel_disp = data[regime + '/' + radius_name + '/vel_disp'].value 
	
			seed_mass_list = [1, 2, 3, 4, 5]	
			for seed_mass_name,seed_mass in zip(seed_mass_name_list, seed_mass_list):

				fig, axes = plt.subplots(2,3)
				fig.subplots_adjust(hspace = 0, wspace=0)
				fig.suptitle(sim_name[i])
				ax = axes.ravel()

				for k in range(len(mass_retention)):

					m_bh_torque = data[regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention[k]) + '/seed_' + seed_mass_name + '/m_bh_torque'].value

					ax1 = ax[k].twiny()
					ax[k].plot(m_star, m_bh_torque, color='r', ls='-', lw=2, label=r'$M_{*}$')
					ax[k].plot(m_bulge, m_bh_torque, color='b', ls='-', lw=2, label=r'$M_{bulge}$')
					ax[k].plot(m_halo, m_bh_torque, color='k', ls='-', lw=2, label=r'$M_H$')
					ax[k].plot(mass_list, Ferrerase02_data, color='k', ls='--', lw=1, label='Ferrerase02')
					ax[k].plot(mass_list, Haring04_data, color='b', ls='--', lw=1, label='Haring04')
					ax[k].plot(mass_list, Reines2015_data, color='r', ls='--', lw=1, label='Reines15')
					ax1.plot(vel_disp, m_bh_torque, color='g', ls='-', lw=2, label=r'$\sigma$')
					ax1.plot(vel_list, McConnell_Ma2013_data, color='g', ls='--', lw=1, label='McConnell13')

					ax[k].set_xscale('log')
					ax[k].set_yscale('log')
					ax1.set_xscale('log')

					ax[k].set_xlim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]))
					ax[k].set_ylim(np.power(10.0,seed_mass),np.power(10.0,bh_upper[i]))
					#ax1.set_xlim(np.power(10.0,vel_lower[i]),np.power(10.0,vel_upper[i]))
					#ax1.set_xlim(0.5*np.min(vel_disp),1.5*np.max(vel_disp))

					ax[k].yaxis.set_major_locator(plt.FixedLocator(np.logspace(seed_mass,bh_upper[i]-1,num=bh_upper[i]-seed_mass)))
					ax[k].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
					#ax1.xaxis.set_major_locator(plt.FixedLocator(vel_ticks))

					ax[k].tick_params(axis='x', labelsize=8)
					ax[k].tick_params(axis='y', labelsize=8)
					ax[k].text(0.9*(10**mass_upper[i]), 1.5*(10.0**seed_mass), '%' + str(int(100.0 * mass_retention[k])), verticalalignment='bottom', horizontalalignment='right')
					ax[k].fill_between(mass_list, Ferrerase02_data * np.sqrt(1e1), Ferrerase02_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
					ax[k].fill_between(mass_list, Haring04_data * np.sqrt(1e1), Haring04_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.1 )
					ax[k].fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='red', zorder=0, alpha=0.1 )
					#ax[k].fill_between(mass_list, Savorgnan2016_data * np.sqrt(1e1), Savorgnan2016_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.3 )
					ax[k].legend(bbox_to_anchor=(2.05,-0.1), loc="upper left", prop={'size':6},frameon=False,scatterpoints=1)
					ax1.fill_between(vel_list, McConnell_Ma2013_data * np.sqrt(1e1), McConnell_Ma2013_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.1 )
					ax1.legend(bbox_to_anchor=(2.05,-0.5), loc="upper left", prop={'size':6},frameon=False,scatterpoints=1)

					if k == 0:
						ax[k].get_xaxis().set_ticks([])
						#ax[k].get_yaxis().set_ticks([])
						#ax1.get_xaxis().set_ticks([])
						ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
						ax1.set_xlabel(r'$\sigma\;(km/s)$')
					if k == 1:
						ax[k].get_xaxis().set_ticks([])
						ax[k].get_yaxis().set_ticks([])
						#ax1.get_xaxis().set_ticks([])
						#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
						#ax1.set_xlabel(r'$\sigma\;(km/s)$')
					if k == 2:
						#ax[k].get_xaxis().set_ticks([])
						ax[k].get_yaxis().set_ticks([])
						#ax1.get_xaxis().set_ticks([])
						#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
						ax1.set_xlabel(r'$\sigma\;(km/s)$')
					if k == 3:
						#ax[k].get_xaxis().set_ticks([])
						#ax[k].get_yaxis().set_ticks([])
						ax1.get_xaxis().set_ticks([])
						ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
						#ax1.set_xlabel(r'$\sigma\;(km/s)$')
					if k == 4:
						#ax[k].get_xaxis().set_ticks([])
						ax[k].get_yaxis().set_ticks([])
						ax1.get_xaxis().set_ticks([])
						#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
						ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
						#ax1.set_xlabel(r'$\sigma\;(km/s)$')
				
					#ax[5].get_xaxis().set_ticks([])
					#ax[5].get_yaxis().set_ticks([])
					#ax1.get_xaxis().set_ticks([])
					#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
					#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
					#ax1.set_xlabel(r'$\sigma\;(km/s)$')  

					if (k != 0):
						ax[k].legend().set_visible(False)
						ax1.legend().set_visible(False)

				fig.delaxes(ax.flatten()[-1])
				#plt.tight_layout()
				plt.savefig(output_path + 'mbh_vs_halo_star_bulge_sigma_' + regime + '_' + radius_name + '_kpc_seed_' + seed_mass_name + '.png', dpi=300, format='png')
				plt.close()

def BH_scaling_relations_supp(output_path, radius, mass_retention, i):
	for j in range(len(radius)):
		fig, axes = plt.subplots(2,3)
		fig.subplots_adjust(hspace = 0, wspace=0)
		fig.suptitle(r'$R_0:\;$' + str(radius[j]) + r'$\;kpc$')
		ax = axes.ravel()

		for k in range(len(mass_retention)):
			ax1 = ax[k].twiny()
			ax[k].plot(m_star, m_bh_torque_supp[j][k], color='r', ls='-', lw=2, label=r'$M_{*}$')
			ax[k].plot(m_bulge, m_bh_torque_supp[j][k], color='b', ls='-', lw=2, label=r'$M_{bulge}$')
			ax[k].plot(m_halo, m_bh_torque_supp[j][k], color='k', ls='-', lw=2, label=r'$M_H$')
			ax[k].plot(mass_list, Ferrerase02_data, color='k', ls='--', lw=1, label='HaloBH')
			ax[k].plot(mass_list, Haring04_data, color='b', ls='--', lw=1, label='Haring04')
			ax[k].plot(mass_list, Reines2015_data, color='r', ls='--', lw=1, label='Reines15')
			ax1.plot(vel_disp, m_bh_torque_supp[j][k], color='g', ls='-', lw=1, label=r'$\sigma$')
			ax1.plot(vel_list, McConnell_Ma2013_data, color='g', ls='--', lw=1, label='McConnell13')

			ax[k].set_xscale('log')
			ax[k].set_yscale('log')
			ax1.set_xscale('log')

			ax[k].set_xlim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]))
			ax[k].set_ylim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
			ax1.set_xlim(np.power(10.0,vel_lower[i]),np.power(10.0,vel_upper[i]))

			ax[k].yaxis.set_major_locator(plt.FixedLocator(bh_ticks))
			ax[k].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
			ax1.xaxis.set_major_locator(plt.FixedLocator(vel_ticks))

			ax[k].tick_params(axis='x', labelsize=8)
			ax[k].tick_params(axis='y', labelsize=8)
			ax[k].text(np.power(10,label[i]),2e4,'%' + str(int(100.0 * mass_retention[k])))
			ax[k].fill_between(mass_list, Ferrerase02_data * np.sqrt(1e1), Ferrerase02_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
			ax[k].fill_between(mass_list, Haring04_data * np.sqrt(1e1), Haring04_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.1 )
			ax[k].fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='red', zorder=0, alpha=0.1 )
			#ax[k].fill_between(mass_list, Savorgnan2016_data * np.sqrt(1e1), Savorgnan2016_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.3 )
			ax[k].legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)
			ax1.fill_between(vel_list, McConnell_Ma2013_data * np.sqrt(1e1), McConnell_Ma2013_data / np.sqrt(1e1), color='blue', zorder=0, alpha=0.1 )
			ax1.legend(loc=6,prop={'size':6},frameon=False,scatterpoints=1)

			if k == 0:
				ax[k].get_xaxis().set_ticks([])
				#ax[k].get_yaxis().set_ticks([])
				#ax1.get_xaxis().set_ticks([])
				ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
				#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
				ax1.set_xlabel(r'$\sigma\;(km/s)$')
			if k == 1:
				ax[k].get_xaxis().set_ticks([])
				ax[k].get_yaxis().set_ticks([])
				#ax1.get_xaxis().set_ticks([])
				#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
				#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
				#ax1.set_xlabel(r'$\sigma\;(km/s)$')
			if k == 2:
				ax[k].get_xaxis().set_ticks([])
				ax[k].get_yaxis().set_ticks([])
				#ax1.get_xaxis().set_ticks([])
				#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
				#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
				ax1.set_xlabel(r'$\sigma\;(km/s)$')
			if k == 3:
				#ax[k].get_xaxis().set_ticks([])
				#ax[k].get_yaxis().set_ticks([])
				ax1.get_xaxis().set_ticks([])
				ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
				ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
				#ax1.set_xlabel(r'$\sigma\;(km/s)$')
			if k == 4:
				#ax[k].get_xaxis().set_ticks([])
				ax[k].get_yaxis().set_ticks([])
				ax1.get_xaxis().set_ticks([])
				#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
				ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
				#ax1.set_xlabel(r'$\sigma\;(km/s)$')
		
			ax[5].get_xaxis().set_ticks([])
			ax[5].get_yaxis().set_ticks([])
			#ax1.get_xaxis().set_ticks([])
			#ax[k].set_ylabel(r'$M_{bh}\;(M_{\odot})$')
			#ax[k].set_xlabel(r'$Mass\;(M_{\odot})$')
			#ax1.set_xlabel(r'$\sigma\;(km/s)$')  

			if (k != 0):
				ax[k].legend().set_visible(False)
				ax1.legend().set_visible(False)

		#plt.tight_layout()
		plt.savefig(output_path + 'mbh_vs_halo_star_bulge_sigma_supp_' + str(radius[j]) + '_kpc.png', dpi=300, format='png')
		plt.close()

def m_star_bulge_vs_r_hsm_vir(output_path, i):
	fig = plt.figure(1)
	#fig.suptitle(title)
	fig.subplots_adjust(wspace=0, hspace=0)

	ax1 = fig.add_subplot(211)	
	ax2 = ax1.twinx()
	#ax1.plot(m_bulge, r_hsm, color='k', ls='-', lw=1, label=r'$M_{bulge}$')
	#ax2.plot(m_bulge, r, color='r', ls='--', lw=1, label=r'$M_{bulge}/R_{VIR}$')
	ax1.scatter(m_bulge, r_hsm, color='k', marker='s', s=4, label=r'$M_{bulge}$')
	ax2.scatter(m_bulge, r_hsm_vir, color='r', marker='^', s=4, label=r'$M_{bulge}$')
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax2.set_yscale('log')
	ax1.set_xlim(np.power(10.0,bulge_lower[i]),np.power(10.0,bulge_upper[i]))
	ax1.set_ylim(np.power(10.0,rhm_lower[i]),np.power(10.0,rhm_upper[i]))
	ax2.set_ylim(np.power(10.0,rvir_lower[i]),np.power(10.0,rvir_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(bulge_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(rhm_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(rvir_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax2.tick_params(axis='y', labelsize=8)
	ax1.set_xlabel(r'$Mass\;(M_{\odot})$')
	ax1.set_ylabel(r'$R_{HSM}\;(kpc)$')
	ax2.set_ylabel(r'$R_{VIR}/R_{HGM}$',rotation=270, color='red')
	ax1.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
	ax2.legend(loc=1,prop={'size':8},frameon=False,scatterpoints=1)

	ax3 = fig.add_subplot(212)	
	ax4 = ax3.twinx()
	#ax3.plot(m_star, r_hsm, color='k', ls='-', lw=1, label=r'$M_{*}$')
	#ax4.plot(m_star, r, color='r', ls='--', lw=1, label=r'$M_{*}/R_{VIR}$')
	ax3.scatter(m_star, r_hsm, color='k', marker='s', s=4, label=r'$M_{*}$')
	ax4.scatter(m_star, r_hsm_vir, color='r', marker='^', s=4, label=r'$M_{*}$')
	ax3.set_xscale('log')
	ax3.set_yscale('log')
	ax4.set_yscale('log')
	ax3.set_xlim(np.power(10.0,bulge_lower[i]),np.power(10.0,bulge_upper[i]))
	ax3.set_ylim(np.power(10.0,rhm_lower[i]),np.power(10.0,rhm_upper[i]))
	ax4.set_ylim(np.power(10.0,rvir_lower[i]),np.power(10.0,rvir_upper[i]))
	ax3.xaxis.set_major_locator(plt.FixedLocator(bulge_ticks))
	ax3.yaxis.set_major_locator(plt.FixedLocator(rhm_ticks))
	ax4.yaxis.set_major_locator(plt.FixedLocator(rvir_ticks))
	ax3.tick_params(axis='y', labelsize=8)
	ax4.tick_params(axis='y', labelsize=8)
	ax3.set_xlabel(r'$Mass\;(M_{\odot})$')
	ax3.set_ylabel(r'$R_{HSM}\;(kpc)$')
	ax4.set_ylabel(r'$R_{VIR}/R_{HSM}$',rotation=270, color='red')
	ax3.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
	ax4.legend(loc=1,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'mstar_mbulge_vs_r.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()

def m_bh_vs_compactness_gas_star_fdisk(output_path, radius, mass_retention, i):
	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)
	#fig.suptitle(r'$R_0:$' + str(radius[j]) + ' kpc')

	if i==5:
		m_bh = m_bh_torque[0][3]
	else:
		m_bh = m_bh_torque[3][3]

	ax0 = fig.add_subplot(411)
	for j in range(len(radius)):
		ax0.plot(m_bh, m_gas_inner[j], color=color[j], ls='-', lw=1, label='gas<' + str(radius[j]) + ' kpc')
	ax0.plot(m_bh, m_gas, color='k', ls='-', lw=1, label=r'$gas<\%10\;R_{VIR}$')
	ax0.set_xscale('log')
	ax0.set_yscale('log')
	ax0.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
	ax0.set_ylim(np.power(10.0,m_star_comp_lower[i]),np.power(10.0,m_star_comp_upper[i]))
	ax0.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
	ax0.yaxis.set_major_locator(plt.FixedLocator(m_star_comp_ticks))
	ax0.tick_params(axis='y', labelsize=8)
	ax0.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax0.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

	ax1 = fig.add_subplot(412)
	for j in range(len(radius)):
		ax1.plot(m_bh, m_star_inner[j], color=color[j], ls='-', lw=1, label='star<' + str(radius[j]) + ' kpc')
	ax1.plot(m_bh, m_star, color='k', ls='-', lw=1, label=r'$star<\%10\;R_{VIR}$')
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax1.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
	ax1.set_ylim(np.power(10.0,m_star_comp_lower[i]),np.power(10.0,m_star_comp_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(m_star_comp_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax1.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(413)
	m_star_compactness = np.array([0.5*a/b for a,b in zip(m_star,r_hsm)])
	m_halo_compactness = np.array([1e-1*a/b for a,b in zip(m_halo,rvir_10per)])
	ax2.plot(m_bh, m_star_compactness, color=color[0], ls='-', lw=1, label=r'$M_{*}/R_{HSM}$')
	ax2.plot(m_bh, m_halo_compactness, color=color[1], ls='-', lw=1, label=r'$M_{Halo}/R_{VIR}$')
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
	ax2.set_ylim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(mass_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$compactness$', size=8)
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	 
	ax3 = fig.add_subplot(414)
	for j in range(len(radius)):
		ax3.plot(m_bh, f_disk_inner[j], color=color[j], ls='-', lw=1, label=str(radius[j])+' kpc')
	ax3.set_xscale('log')
	ax3.set_yscale('linear')
	ax3.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
	ax3.set_ylim(0,1)
	ax3.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
	ax3.yaxis.set_major_locator(plt.FixedLocator([0.2,0.4,0.6,0.8]))
	ax3.tick_params(axis='y', labelsize=8)
	ax3.set_xlabel(r'$M_{BH}\;(M_{\odot})$')
	ax3.set_ylabel(r'$f_{disk}$')
	ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	#ax3.legend().set_visible(False)

	plt.savefig(output_path + 'm_bh_vs_compactness_gas_star_fdisk.png', dpi=300, format='png')
	plt.close()
	 
def m_halo_vs_compactness_gas_star_fdisk(output_path, radius, mass_retention, i):
	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)
	#fig.suptitle(r'$R_0:$' + str(radius[j]) + ' kpc')

	ax0 = fig.add_subplot(411)
	for j in range(len(radius)):
		ax0.plot(m_halo, m_gas_inner[j], color=color[j], ls='-', lw=1, label='gas<' + str(radius[j]) + ' kpc')
	ax0.plot(m_halo, m_gas, color='k', ls='-', lw=1, label=r'$gas<\%10\;R_{VIR}$')
	ax0.set_xscale('log')
	ax0.set_yscale('log')
	ax0.set_xlim(np.power(10.0,mass_lower[i]+3),np.power(10.0,mass_upper[i]))
	ax0.set_ylim(np.power(10.0,m_star_comp_lower[i]),np.power(10.0,m_star_comp_upper[i]))
	ax0.xaxis.set_major_locator(plt.FixedLocator(mass_ticks[3:]))
	ax0.yaxis.set_major_locator(plt.FixedLocator(m_star_comp_ticks))
	ax0.tick_params(axis='y', labelsize=8)
	ax0.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax0.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)


	ax1 = fig.add_subplot(412)
	for j in range(len(radius)):
		ax1.plot(m_halo, m_star_inner[j], color=color[j], ls='-', lw=1, label='star<' + str(radius[j]) + ' kpc')
	ax1.plot(m_halo, m_star, color='k', ls='-', lw=1, label=r'$star<\%10\;R_{VIR}$')
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax1.set_xlim(np.power(10.0,mass_lower[i]+3),np.power(10.0,mass_upper[i]))
	ax1.set_ylim(np.power(10.0,m_star_comp_lower[i]),np.power(10.0,m_star_comp_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(mass_ticks[3:]))
	ax1.yaxis.set_major_locator(plt.FixedLocator(m_star_comp_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax1.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(413)
	m_star_compactness = np.array([0.5*a/b for a,b in zip(m_star,r_hsm)])
	m_halo_compactness = np.array([1e-1*a/b for a,b in zip(m_halo,rvir_10per)])
	ax2.plot(m_halo, m_star_compactness, color=color[0], ls='-', lw=1, label=r'$M_{*}/R_{HSM}$')
	ax2.plot(m_halo, m_halo_compactness, color=color[1], ls='-', lw=1, label=r'$M_{Halo}/R_{VIR}$')
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_xlim(np.power(10.0,mass_lower[i]+3),np.power(10.0,mass_upper[i]))
	ax2.set_ylim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(mass_ticks[3:]))
	ax2.yaxis.set_major_locator(plt.FixedLocator(mass_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$compactness$', size=8)
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	 
	ax3 = fig.add_subplot(414)
	for j in range(len(radius)):
		ax3.plot(m_halo, f_disk_inner[j], color=color[j], ls='-', lw=1, label=str(radius[j])+' kpc')
	ax3.set_xscale('log')
	ax3.set_yscale('linear')
	ax3.set_xlim(np.power(10.0,mass_lower[i]+3),np.power(10.0,mass_upper[i]))
	ax3.set_ylim(0,1)
	ax3.xaxis.set_major_locator(plt.FixedLocator(mass_ticks[3:]))
	ax3.yaxis.set_major_locator(plt.FixedLocator([0.2,0.4,0.6,0.8]))
	ax3.tick_params(axis='y', labelsize=8)
	ax3.set_xlabel(r'$M_{Halo}\;(M_{\odot})$')
	ax3.set_ylabel(r'$f_{disk}$')
	ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	#ax3.legend().set_visible(False)

	plt.savefig(output_path + 'm_halo_vs_compactness_gas_star_fdisk.png', dpi=300, format='png')
	plt.close()
	 
def outflow_vs_mstar_mdot_mbh(output_path, i):
	data_1 = [outflow, sfr, mdot_torque[4][3], m_gas, m_star, m_bh_torque[4][3]]
	data_2 = [outflow_inner[3], sfr_inner[3], mdot_torque[3][3], m_gas_inner[3], m_star_inner[3], m_bh_torque[3][3]]
	data = [data_1, data_2]
	title = ['%10 Rvir', '1 kpc']
	fit_co = [c1[i], c2[i]]
	pieces = [1000, 50]

	for xxx in range(2):

		xdata = np.array(data[xxx][0])
		ydata = np.array(data[xxx][1])
		mdot = np.array(data[xxx][2])
		mgas = np.array(data[xxx][3])
		mstar = np.array(data[xxx][4])
		mbh = np.array(data[xxx][5])

		fig = plt.figure(1, figsize=(6,9))
		fig.suptitle(title[xxx])
		fig.subplots_adjust(wspace=0, hspace=0.35)

		mass_loading = np.array([0.0 if b==0.0 else 0.0 if a==0.0 else a/b for a,b in zip(xdata, ydata)])
		sel = (mass_loading != 0.0)
		mass_loading = mass_loading[sel]
		mstar = mstar[sel]
		mgas = mgas[sel]
		mbh = mbh[sel]
		mdot = mdot[sel]
		xdata = xdata[sel]
		ydata = ydata[sel]

		x_axis = mstar
		y_axis = np.log10(mass_loading)

		guess1 = [1.0, -1.0, -1.0]
		guess2 = [1.0, -1.0]

		bound1 = ((0.0, -np.inf, -np.inf), (np.inf, 0.0, 0.0))
		bound2 = ((0.0, -np.inf), (np.inf,0.0))

		popt1, pcov1 = curve_fit(lambda mstar,a,b,c: HH15_eq44(mstar,mgas,a,b,c), x_axis, y_axis, guess1) #, bounds=bound1)
		popt2, pcov2 = curve_fit(HH15, x_axis, y_axis, guess1, bounds=bound1)
		popt3, pcov3 = curve_fit(Muratov15, x_axis, y_axis, guess2, bounds=bound2)	

		fit_cons = np.power(mass_loading/popt2[0],1.0/popt2[1]) * popt2[2] / popt2[1]
		m_loading = np.linspace(np.max(mass_loading), np.min(mass_loading[np.nonzero(mass_loading)]), pieces[xxx])
		t = np.linspace(time(np.max(redshift)), time(np.min(redshift)), pieces[xxx])
		fit_cons = np.power(m_loading/popt2[0],1.0/popt2[1]) * popt2[2] / popt2[1]
		mdot_lambertw = lambertw(fit_cons).real * popt2[1] / popt2[2]
		
		init_mbh = 1e4
		mbh_fit = integrate.cumtrapz(mdot_lambertw, t, initial=init_mbh)
		mbh_fit += init_mbh
		mbh_fit[0] = init_mbh		

		ax0 = fig.add_subplot(311)	
		ax0.scatter(x_axis, 10.0**y_axis, color='k', marker='.', s=4)
		ax0.plot(x_axis, 10.0**HH15_eq44(mstar,mgas,*popt1), 'r-',label='HHeq44 %s' %popt1)
		ax0.plot(x_axis, 10.0**HH15(x_axis, *popt2), 'g-', lw=4, label='HH15 %s' %popt2)
		ax0.plot(x_axis, 10.0**Muratov15(x_axis, *popt3), 'b-', label='M15 %s' %popt3)

		ax0.set_xlim(np.power(10.0,bulge_lower[i]),np.power(10.0,bulge_upper[i]))
		ax0.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
		#ax0.set_xlim(bulge_lower[i],bulge_upper[i])
		#ax0.set_ylim(outflow_lower[i],outflow_upper[i])
		ax0.set_xscale('log')
		ax0.set_yscale('log')
		ax0.xaxis.set_major_locator(plt.FixedLocator(bulge_ticks))
		ax0.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
		ax0.tick_params(axis='y', labelsize=8)
		ax0.set_xlabel(r'$M_{*}$')
		ax0.set_ylabel(r'$\eta$')
		ax0.text(10,1e-2,'GAL')
		ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
		
		ax1 = fig.add_subplot(312)	
		ax1.scatter(mdot, mass_loading, color='k', marker='.', s=4, label=r'$\eta$')
		ax1.plot(mdot_lambertw, m_loading, 'g-', lw=4, label='Lambert W')
		#ax1.plot(mdot_lambertw_real, mass_loading, 'g-', label='Lambert W sim')
		#ax1.plot(mdot, Muratov15_mdot(mdot, *popt_mdot), 'r-', label='fit: %5.3f x** %5.3f' % tuple(popt_mdot))
		ax1.set_xscale('log')
		ax1.set_yscale('log')
		ax1.set_xlim(np.power(10.0,mdot_lower[i]),np.power(10.0,mdot_upper[i]))
		ax1.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
		ax1.xaxis.set_major_locator(plt.FixedLocator(mdot_ticks))
		ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
		ax1.tick_params(axis='y', labelsize=8)
		ax1.set_xlabel(r'$\dot{M}$')
		ax1.set_ylabel(r'$\eta$')
		ax1.text(10,1e-2,'GAL')
		ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
		
		ax2 = fig.add_subplot(313)	
		ax2.scatter(mbh, mass_loading, color='k', marker='.', s=4, label=r'$\eta$')
		ax2.plot(mbh_fit, m_loading, 'g-', lw=4, label='Lambert W')
		#ax2.plot(mbh_fit_real, mass_loading, 'g-', label='Lambert W sim')
		ax2.set_xscale('log')
		ax2.set_yscale('log')
		ax2.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
		ax2.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
		ax2.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
		ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
		ax2.tick_params(axis='y', labelsize=8)
		ax2.set_xlabel(r'$M_{BH}$')
		ax2.set_ylabel(r'$\eta$')
		ax2.text(10,1e-2,'GAL')
		ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
		
		plt.savefig(output_path + 'outflow_vs_mstar_mdot_mbh_' + str(xxx) + '.png', dpi=300, format='png')
		#plt.tight_layout()
		#plt.subplots_adjust(top=0.88)
		plt.close()	

def outflow_twozone_mbh(output_path, i):
	data_1 = [outflow, sfr, mdot_torque[4][3], m_gas, m_star, m_bh_torque[4][3]]
	data_2 = [outflow_inner[3], sfr_inner[3], mdot_torque[3][3], m_gas_inner[3], m_star_inner[3], m_bh_torque[3][3]]
	data = [data_1, data_2]
	title = ['%10 Rvir', '1 kpc']
	fit_co = [c1[i], c2[i]]
	pieces = [1000, 1000]

	xdata = np.array(data[xxx][0])
	ydata = np.array(data[xxx][1])
	mdot = np.array(data[xxx][2])
	mgas = np.array(data[xxx][3])
	mstar = np.array(data[xxx][4])
	mbh = np.array(data[xxx][5])

	fig = plt.figure(1, figsize=(6,9))
	fig.suptitle(title[xxx])
	fig.subplots_adjust(wspace=0, hspace=0.35)

	mass_loading = np.array([0.0 if b==0.0 else 0.0 if a==0.0 else a/b for a,b in zip(xdata, ydata)])
	sel = (mass_loading != 0.0)
	mass_loading = mass_loading[sel]
	mstar = mstar[sel]
	mgas = mgas[sel]
	mbh = mbh[sel]
	mdot = mdot[sel]
	xdata = xdata[sel]
	ydata = ydata[sel]

	x_axis = mstar
	y_axis = np.log10(mass_loading)

	guess1 = [1.0, -1.0, -1.0]
	guess2 = [1.0, -1.0]

	bound1 = ((0.0, -np.inf, -np.inf), (np.inf, 0.0, 0.0))
	bound2 = ((0.0, -np.inf), (np.inf,0.0))

	popt1, pcov1 = curve_fit(lambda mstar,a,b,c: HH15_eq44(mstar,mgas,a,b,c), x_axis, y_axis, guess1) #, bounds=bound1)
	popt2, pcov2 = curve_fit(HH15, x_axis, y_axis, guess1, bounds=bound1)
	popt3, pcov3 = curve_fit(Muratov15, x_axis, y_axis, guess2, bounds=bound2)	

	fit_cons = np.power(mass_loading/popt2[0],1.0/popt2[1]) * popt2[2] / popt2[1]
	m_loading = np.linspace(np.max(mass_loading), np.min(mass_loading[np.nonzero(mass_loading)]), pieces[xxx])
	t = np.linspace(time(np.max(redshift)), time(np.min(redshift)), pieces[xxx])
	fit_cons = np.power(m_loading/popt2[0],1.0/popt2[1]) * popt2[2] / popt2[1]
	mdot_lambertw = lambertw(fit_cons).real * popt2[1] / popt2[2]
	
	init_mbh = 1e4
	mbh_fit = integrate.cumtrapz(mdot_lambertw, t, initial=init_mbh)
	mbh_fit += init_mbh
	mbh_fit[0] = init_mbh		

	ax0 = fig.add_subplot(311)	
	ax0.scatter(x_axis, 10.0**y_axis, color='k', marker='.', s=4)
	ax0.plot(x_axis, 10.0**HH15_eq44(mstar,mgas,*popt1), 'r-',label='HHeq44 %s' %popt1)
	ax0.plot(x_axis, 10.0**HH15(x_axis, *popt2), 'g-', lw=4, label='HH15 %s' %popt2)
	ax0.plot(x_axis, 10.0**Muratov15(x_axis, *popt3), 'b-', label='M15 %s' %popt3)

	ax0.set_xlim(np.power(10.0,bulge_lower[i]),np.power(10.0,bulge_upper[i]))
	ax0.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
	#ax0.set_xlim(bulge_lower[i],bulge_upper[i])
	#ax0.set_ylim(outflow_lower[i],outflow_upper[i])
	ax0.set_xscale('log')
	ax0.set_yscale('log')
	ax0.xaxis.set_major_locator(plt.FixedLocator(bulge_ticks))
	ax0.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax0.tick_params(axis='y', labelsize=8)
	ax0.set_xlabel(r'$M_{*}$')
	ax0.set_ylabel(r'$\eta$')
	ax0.text(10,1e-2,'GAL')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	
	ax1 = fig.add_subplot(312)	
	ax1.scatter(mdot, mass_loading, color='k', marker='.', s=4, label=r'$\eta$')
	ax1.plot(mdot_lambertw, m_loading, 'g-', lw=4, label='Lambert W')
	#ax1.plot(mdot_lambertw_real, mass_loading, 'g-', label='Lambert W sim')
	#ax1.plot(mdot, Muratov15_mdot(mdot, *popt_mdot), 'r-', label='fit: %5.3f x** %5.3f' % tuple(popt_mdot))
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax1.set_xlim(np.power(10.0,mdot_lower[i]),np.power(10.0,mdot_upper[i]))
	ax1.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(mdot_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_xlabel(r'$\dot{M}$')
	ax1.set_ylabel(r'$\eta$')
	ax1.text(10,1e-2,'GAL')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	
	ax2 = fig.add_subplot(313)	
	ax2.scatter(mbh, mass_loading, color='k', marker='.', s=4, label=r'$\eta$')
	ax2.plot(mbh_fit, m_loading, 'g-', lw=4, label='Lambert W')
	#ax2.plot(mbh_fit_real, mass_loading, 'g-', label='Lambert W sim')
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
	ax2.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
	ax2.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_xlabel(r'$M_{BH}$')
	ax2.set_ylabel(r'$\eta$')
	ax2.text(10,1e-2,'GAL')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	
	plt.savefig(output_path + 'outflow_vs_mstar_mdot_mbh_' + str(xxx) + '.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	

def outflow_vs_mbh(output_path, i):
	#data_1 = [outflow, sfr, mdot_torque[4][3], m_gas, m_star, m_bh_torque[4][3]]
	#data_2 = [outflow_inner[3], sfr_inner[3], mdot_torque[3][3], m_gas_inner[3], m_star_inner[3], m_bh_torque[3][3]]

	mass_loading_10per = np.array([0.0 if b==0.0 else 0.0 if a==0.0 else a/b for a,b in zip(outflow, sfr)])
	mass_loading_1kpc = np.array([0.0 if b==0.0 else 0.0 if a==0.0 else a/b for a,b in zip(outflow_inner[3], sfr_inner[3])])

	dummy = np.array([1e0,1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12,1e13,1e14])

	fig = plt.figure(1, figsize=(6,9))
	fig.suptitle(sim_name[i])
	fig.subplots_adjust(wspace=0, hspace=0.35)

	ax0 = fig.add_subplot(311)	
	ax0.scatter(m_bh_torque[3][3], mass_loading_10per, color='k', marker='s', s=8, label=r'$0.1R_{vir}$')
	ax0.scatter(m_bh_torque[3][3], mass_loading_1kpc, color='r', marker='o', s=4, label=r'$1kpc$')
	ax0.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
	ax0.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
	ax0.plot(dummy, np.ones(len(dummy)), 'k--', lw=0.5 )
	ax0.set_xscale('log')
	ax0.set_yscale('log')
	ax0.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
	ax0.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax0.tick_params(axis='y', labelsize=8)
	ax0.set_xlabel(r'$M_{BH}$')
	ax0.set_ylabel(r'$\eta$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	
	ax1 = fig.add_subplot(312)	
	x_1kpc, y_1kpc = bin_average(m_bh_torque[3][3], mass_loading_1kpc, nbins=8)
	x_10per, y_10per = bin_average(m_bh_torque[3][3], mass_loading_10per, nbins=8)
	#ax1.scatter(x_1kpc, y_1kpc, color='r', marker='o', s=4, label=r'$1kpc$')
	#ax1.scatter(x_10per, y_10per, color='k', marker='s', s=8, label=r'$0.1R_{vir}$')
	ax1.plot(x_10per, y_10per, 'k-', lw=3, label=r'$0.1R_{vir}$')
	ax1.plot(x_1kpc, y_1kpc,'r-', lw=3, label=r'$1kpc$')
	ax1.scatter(m_bh_torque[3][3], mass_loading_10per, color='b', marker='s', s=4, label=r'$0.1R_{vir}$')
	ax1.scatter(m_bh_torque[3][3], mass_loading_1kpc, color='g', marker='o', s=8, label=r'$1kpc$')

	ax1.plot(dummy, np.ones(len(dummy)), 'k--', lw=0.5 )
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax1.set_xlim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
	ax1.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(bh_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_xlabel(r'$M_{BH}$')
	ax1.set_ylabel(r'$\eta$')
	ax1.text(10,1e-2,'GAL')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	
	ax2 = fig.add_subplot(313)	
	x_1kpc, y_1kpc = bin_average(m_star_inner[3], mass_loading_1kpc, nbins=8)
	x_10per, y_10per = bin_average(m_star, mass_loading_10per, nbins=8)
	ax2.scatter(m_star, mass_loading_10per, color='b', marker='o', s=4, label=r'$0.1R_{vir}$')
	ax2.scatter(m_star_inner[3], mass_loading_1kpc, color='g', marker='s', s=8, label=r'$1kpc$')
	ax2.plot(x_10per, y_10per, 'k-', lw=3, label=r'$0.1R_{vir}$')
	ax2.plot(x_1kpc, y_1kpc,'r-', lw=3, label=r'$1kpc$')
	ax2.plot(dummy, np.ones(len(dummy)), 'k--', lw=0.5 )
	ax2.set_xscale('log')
	ax2.set_yscale('log')
	ax2.set_xlim(np.power(10.0,bulge_lower[i]),np.power(10.0,bulge_upper[i]))
	ax2.set_ylim(np.power(10.0,outflow_lower[i]),np.power(10.0,outflow_upper[i]))
	ax2.xaxis.set_major_locator(plt.FixedLocator(bulge_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_xlabel(r'$M_{*}$')
	ax2.set_ylabel(r'$\eta$')
	ax2.text(10,1e-2,'GAL')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	
	plt.savefig(output_path + 'outflow_vs_mbh.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	

def z_vs_mass_gsh_sfr(output_path, i):

	data = data_merger

	for regime in regime_list:
		redshift = data[regime + '/' + radius_name_list[-1] + '/z_1'].value
		m_star = data[regime + '/' + radius_name_list[-1] + '/mstar'].value
		m_gas = data[regime + '/' + radius_name_list[-1] + '/mgas'].value
		m_bulge = data[regime + '/' + radius_name_list[-1] + '/mbulge'].value
		m_halo = data[regime + '/' + radius_name_list[-1] + '/mhalo'].value
		m_baryon = m_star + m_gas #np.array([a+b for a,b in zip(m_gas,m_star)])
		sfr = data[regime + '/' + radius_name_list[-1] + '/sfr'].value 

		if i >= 30:
			sfr_inner = data[regime + '/' + radius_name_list[0] + '/sfr'].value 
		else:
			sfr_inner = data[regime + '/' + radius_name_list[3] + '/sfr'].value 

		fig = plt.figure(1)
		fig.suptitle(sim_name[i] + '   ' + regime)
		fig.subplots_adjust(wspace=0, hspace=0)

		ax1 = fig.add_subplot(211)	
		ax1.plot(redshift, m_gas, color='r', ls='--', lw=1, label=r'$gas$')
		ax1.plot(redshift, m_star, color='g', ls='--', lw=1, label=r'$star$')
		ax1.plot(redshift, m_halo, color='k', ls='-', lw=1, label=r'$halo$')   
		ax1.plot(redshift, m_baryon, color='k', ls='--', lw=1, label=r'$baryon$')   
		ax1.set_xscale('linear')
		ax1.set_yscale('log')
		ax1.set_xlim(lower_limit[i],upper_limit[i])
		ax1.set_ylim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]))
		ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax1.yaxis.set_major_locator(plt.FixedLocator(mass_ticks))
		ax1.tick_params(axis='y', labelsize=8)
		ax1.set_ylabel(r'$Mass\;(M_{\odot})$')
		ax12 = ax1.twiny()
		ax1Xs = ax1.get_xticks()
		ax12Xs = []
		for X in ax1Xs:
			dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
			ax12Xs.append("%.2f" %dummy_age)
		ax12.set_xticks(ax1Xs)
		ax12.set_xbound(ax1.get_xbound())
		ax12.set_xticklabels(ax12Xs)
		#ax12.set_xlabel(r'$time \quad (Gyr)$')
		ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

		#ax2 = ax1.twinx()
		ax2 = fig.add_subplot(212)
		ax2.plot(redshift, sfr, color='b', ls='-', lw=1, label=r'$SFR\;gal$')
		ax2.plot(redshift, sfr_inner, color='m', ls='-', lw=1, label=r'$SFR\;inner$')
		ax2.set_xscale('linear')
		ax2.set_yscale('log')
		ax2.set_xlim(lower_limit[i],upper_limit[i])
		ax2.set_ylim(np.power(10.0,sfr_lower[i]),np.power(10.0,sfr_upper[i]))
		ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax2.yaxis.set_major_locator(plt.FixedLocator(sfr_ticks))
		ax2.set_xlabel(r'$z$')
		ax2.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
		ax2.tick_params(axis='y', labelsize=8)
		ax2.legend(loc=4,prop={'size':8},frameon=False,scatterpoints=1)

		plt.savefig(output_path + 'z_vs_mass_gsbh_sfr_' + regime + '.png', dpi=300, format='png')
		#plt.tight_layout()
		#plt.subplots_adjust(top=0.88)
		plt.close()
	
def z_vs_mass_gshdb_f_sfr(output_path, i):

	data = data_merger

	for regime in regime_list:

		redshift = data[regime + '/' + radius_name_list[-1] + '/z_1'].value
		m_star = data[regime + '/' + radius_name_list[-1] + '/mstar'].value
		m_gas = data[regime + '/' + radius_name_list[-1] + '/mgas'].value
		m_bulge = data[regime + '/' + radius_name_list[-1] + '/mbulge'].value
		m_disk = data[regime + '/' + radius_name_list[-1] + '/mdisk'].value
		m_halo = data[regime + '/' + radius_name_list[-1] + '/mhalo'].value
		m_baryon = m_star + m_gas #np.array([a+b for a,b in zip(m_gas,m_star)])
		sfr = data[regime + '/' + radius_name_list[-1] + '/sfr'].value 
		ssfr = data[regime + '/' + radius_name_list[-1] + '/ssfr'].value 
		f_bulge = data[regime + '/' + radius_name_list[-1] + '/fbulge'].value 
		f_disk = data[regime + '/' + radius_name_list[-1] + '/fdisk'].value 

		fig = plt.figure(1)
		fig.suptitle(sim_name[i] + '   ' + regime)
		fig.subplots_adjust(wspace=0, hspace=0)

		ax1 = fig.add_subplot(311)	
		ax1.plot(redshift, m_gas, color='r', ls='-', lw=1, label=r'$gas$')
		ax1.plot(redshift, m_star, color='g', ls='-', lw=1, label=r'$star$')
		ax1.plot(redshift, m_halo, color='k', ls='-', lw=1, label=r'$halo$')   
		ax1.plot(redshift, m_disk, color='b', ls='-', lw=1, label=r'$disk$')   
		ax1.plot(redshift, m_bulge, color='m', ls='-', lw=1, label=r'$bulge$')
		ax1.set_xscale('linear')
		ax1.set_yscale('log')
		ax1.set_xlim(lower_limit[i],upper_limit[i])
		ax1.set_ylim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]))
		ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax1.yaxis.set_major_locator(plt.FixedLocator(mass_ticks))
		ax1.tick_params(axis='y', labelsize=8)
		ax1.set_ylabel(r'$Mass\;(M_{\odot})$')
		ax12 = ax1.twiny()
		ax1Xs = ax1.get_xticks()
		ax12Xs = []
		for X in ax1Xs:
			dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
			ax12Xs.append("%.2f" %dummy_age)
		ax12.set_xticks(ax1Xs)
		ax12.set_xbound(ax1.get_xbound())
		ax12.set_xticklabels(ax12Xs)
		#ax12.set_xlabel(r'$time \quad (Gyr)$')
		ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

		ax2 = fig.add_subplot(312)
		ax2.plot(redshift, f_bulge, color='r', ls='-', lw=1, label=r'$f_{bulge}$')
		ax2.plot(redshift, f_disk, color='g', ls='-', lw=1, label=r'$f_{disk}$')
		ax2.set_xscale('linear')
		ax2.set_yscale('linear')
		ax2.set_xlim(lower_limit[i],upper_limit[i])
		ax2.set_ylim(-0.1,1.1)
		ax2.xaxis.set_ticklabels([])
		ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax2.yaxis.set_major_locator(plt.FixedLocator([0.0,0.2,0.4,0.6,0.8,1.0]))
		ax2.tick_params(axis='y', labelsize=8)
		ax2.set_ylabel(r'$fractions$')
		ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

		ax3 = fig.add_subplot(313)
		ax3.plot(redshift, sfr, color='r', ls='-', lw=1, label=r'$SFR$')
		ax3.set_xscale('linear')
		ax3.set_yscale('log')
		ax3.set_xlim(lower_limit[i],upper_limit[i])
		ax3.set_ylim(np.power(10.0,sfr_lower[i]),np.power(10.0,sfr_upper[i]))
		ax3.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax3.yaxis.set_major_locator(plt.FixedLocator(sfr_ticks))
		ax3.tick_params(axis='y', labelsize=8)
		ax3.set_xlabel(r'$z$', size=10)
		ax3.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
		ax31 = ax3.twinx()
		ax31.plot(redshift, ssfr, color='g', ls='-', lw=1, label=r'$sSFR$')
		ax31.set_yscale('log')
		ax31.set_ylim(np.power(10.0,sfr_lower[i]),np.power(10.0,sfr_upper[i]))
		ax31.yaxis.set_major_locator(plt.FixedLocator(sfr_ticks))
		ax31.set_ylabel(r'$sSFR\;(Gyr^{-1})$')
		ax31.tick_params(axis='y', labelsize=8)
		ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
		ax31.legend(loc=4,prop={'size':8},frameon=False,scatterpoints=1)
	 
		plt.savefig(output_path + 'z_vs_mass_gsbh_' + regime + '.png', dpi=300, format='png')
		#plt.tight_layout()
		#plt.subplots_adjust(top=0.88)
		plt.close()
	
def z_vs_mbh_mdot(output_path, i):

	data = data_merger

	for regime in regime_list:

		redshift = data[regime + '/' + radius_name_list[-1] + '/z_1'].value
		t = time(redshift)
		t_diff = np.diff(t).tolist()
		t_diff.insert(0, 0.0)
		m_seed = data[regime + '/r_1.0/mass_ret_0.5/seed_1e4/mass_seed'].value

		for radius,radius_name in zip(radius_list, radius_name_list):

			fig = plt.figure(1)
			fig.subplots_adjust(wspace=0, hspace=0)
			#fig.suptitle(sim_name[i] + '   ' + regime + '   ' + radius_name + ' kpc')

			ax1 = fig.add_subplot(411)

			for k in range(len(mass_retention)):
				m_bh_torque = data[regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention[k]) + '/seed_1e4/m_bh_torque'].value
				ax1.plot(redshift, m_bh_torque, color=color[k], ls='-', lw=1, label='%' + str(int(100.0 * mass_retention[k])))

			ax1.plot(redshift, m_seed, color='k', ls='-', lw=1, label='Seed')
			ax1.set_xscale('linear')
			ax1.set_yscale('log')
			ax1.set_xlim(lower_limit[i],upper_limit[i])
			ax1.set_ylim(np.power(10.0,bh_lower[i]),np.power(10.0,bh_upper[i]))
			ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
			ax1.yaxis.set_major_locator(plt.FixedLocator(bh_ticks))
			ax1.tick_params(axis='y', labelsize=8)
			ax1.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
			ax12 = ax1.twiny()
			ax1Xs = ax1.get_xticks()
			ax12Xs = []
			for X in ax1Xs:
				dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
				ax12Xs.append("%.2f" %dummy_age)
			ax12.set_xticks(ax1Xs)
			ax12.set_xbound(ax1.get_xbound())
			ax12.set_xticklabels(ax12Xs)
			#ax12.set_xlabel(r'$time \quad (Gyr)$')
			ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

			ax2 = fig.add_subplot(412)
			for k in range(len(mass_retention)):
				mdot_torque = data[regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention[k]) + '/seed_1e4/mdot_torque'].value
				ax2.plot(redshift, mdot_torque, color=color[k], ls='-', lw=1, label='%' + str(int(100.0 * mass_retention[k])))

			ax2.set_xscale('linear')
			ax2.set_yscale('log')
			ax2.set_xlim(lower_limit[i],upper_limit[i])
			ax2.set_ylim(np.power(10.0,mdot_lower[i]),np.power(10.0,mdot_upper[i]))
			ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
			ax2.yaxis.set_major_locator(plt.FixedLocator(mdot_ticks))
			ax2.tick_params(axis='y', labelsize=8)
			ax2.set_ylabel(r'$\dot{M_t}\;(M_{\odot}/yr)$')
			ax12.set_xlabel('time (Gyr)')
			#ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
			 
			ax3 = fig.add_subplot(413)
			for k in range(len(mass_retention)):
				f_torque = data[regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention[k]) + '/seed_1e4/f_torque_edd'].value
				ax3.plot(redshift, f_torque, color=color[k], ls='-', lw=1, label='%' + str(int(100.0 * mass_retention[k])))

			ax3.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.3)
			ax3.plot(redshift, np.ones(len(redshift)), color='k', ls='--', lw=0.3)
			ax3.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.3)
			ax3.set_xscale('linear')
			ax3.set_yscale('log')
			ax3.set_xlim(lower_limit[i],upper_limit[i])
			ax3.set_ylim(np.power(10.0,f_lower[i]),np.power(10.0,f_upper[i]))
			ax3.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
			ax3.yaxis.set_major_locator(plt.FixedLocator(f_ticks))
			ax3.tick_params(axis='y', labelsize=8)
			ax3.set_xlabel(r'$z$',size=10)
			ax3.set_ylabel(r'$\dot{M}_{t}/\dot{M}_{edd}$')
			#ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
			#ax3.legend().set_visible(False)

			ax4 = fig.add_subplot(414)
			for k in range(len(mass_retention)):
				m_bh_torque = data[regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention[k]) + '/seed_1e4/m_bh_torque'].value
				m_gas = data[regime + '/' + radius_name + '/mgas'].value
				mdot_torque = data[regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention[k]) + '/seed_1e4/mdot_torque'].value
				time_ff = np.pi / 2.0 * np.power((radius * kpc_cm_conversion),1.5) / np.sqrt(2.0 * grav_constant * m_sun * m_bh_torque) / yr_s_conversion 
				growth_limit_ff = np.array([a/b for a,b in zip(m_gas,time_ff)])
				growth_limit_diff = np.array([a/b if b!=0.0 else 0.0 for a,b in zip(m_gas,t_diff)])
				growth_limit_ratio_ff = np.array([a/b if b!=0.0 else 0.0 for a,b in zip(mdot_torque,growth_limit_ff)])
				growth_limit_ratio_diff = np.array([a/b if b!=0.0 else 0.0 for a,b in zip(mdot_torque,growth_limit_diff)]) 
				ax4.plot(redshift, growth_limit_ratio_ff, color=color[k], ls='-', lw=1, label='%' + str(int(100.0 * mass_retention[k])))
				#ax4.plot(redshift, growth_limit_ratio_diff, color=color[k], ls='--', lw=1, label='%' + str(int(100.0 * mass_retention[k])))

			ax4.plot(redshift, np.ones(len(redshift)), color='k', ls='--', lw=0.5)
			ax4.set_xscale('linear')
			ax4.set_yscale('log')
			ax4.set_xlim(lower_limit[i],upper_limit[i])
			ax4.set_ylim(np.power(10.0,mdot_lower[i]),np.power(10.0,mdot_upper[i]))
			ax4.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
			ax4.yaxis.set_major_locator(plt.FixedLocator(mdot_ticks))
			ax4.tick_params(axis='y', labelsize=8)
			ax4.set_ylabel(r'$\frac{\dot{M}_{t}}{M_{gas}/t_{ff}}$')
			ax4.set_xlabel('z')
			#ax4.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
		
			plt.savefig(output_path + 'z_vs_mbh_mdot_f_' + regime + '_' + radius_name + '_kpc.png', dpi=300, format='png')
			plt.close()
	
def z_vs_hmr(output_path, i):
	for regime in regime_list:

		redshift = data[regime + '/' + radius_name_list[-1] + '/z_1'].value
		rvir = data[regime + '/' + radius_name_list[-1] + '/radius'].value * 10.0
		r_hgm = data[regime + '/' + radius_name_list[-1] + '/r_hgm'].value
		r_hsm = data[regime + '/' + radius_name_list[-1] + '/r_hsm'].value
		r_hsf = data[regime + '/' + radius_name_list[-1] + '/r_hsf'].value
		r_hgm_vir = r_hgm / rvir
		r_hsm_vir = r_hsm / rvir
		r_hsf_vir = r_hsf / rvir

		fig = plt.figure(1)
		#fig.suptitle(title)
		fig.subplots_adjust(wspace=0, hspace=0)

		ax1 = fig.add_subplot(311)	
		ax2 = ax1.twinx()
		ax1.plot(redshift, r_hgm, color='k', ls='-', lw=1, label=r'$R_{HGM}$')
		ax2.plot(redshift, r_hgm_vir, color='r', ls='-', lw=0.5, label=r'$R_{VIR}/R_{HGM}$')
		ax1.set_xscale('linear')
		ax1.set_yscale('log')
		ax2.set_yscale('log')
		ax1.set_xlim(lower_limit[i],upper_limit[i])
		ax1.set_ylim(np.power(10.0,rhm_lower[i]),np.power(10.0,rhm_upper[i]))
		ax2.set_ylim(np.power(10.0,rvir_lower[i]),np.power(10.0,rvir_upper[i]))
		ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax1.yaxis.set_major_locator(plt.FixedLocator(rhm_ticks))
		ax2.yaxis.set_major_locator(plt.FixedLocator(rvir_ticks))
		#ax2.spines['right'].set_color('red')
		ax1.tick_params(axis='y', labelsize=8)
		ax2.tick_params(axis='y', labelsize=8, colors='red')
		ax1.set_xlabel(r'$z$')
		ax1.set_ylabel(r'$R_{HGM}\;(kpc)$')
		#ax2.set_ylabel(r'$R_{VIR}/R_{HGM}$',rotation=270,size=8)
		ax12 = ax1.twiny()
		ax1Xs = ax1.get_xticks()
		ax12Xs = []
		for X in ax1Xs:
			dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
			ax12Xs.append("%.2f" %dummy_age)
		ax12.set_xticks(ax1Xs)
		ax12.set_xbound(ax1.get_xbound())
		ax12.set_xticklabels(ax12Xs)
		ax12.set_xlabel(r'$Time\;(Gyr)$')
		ax1.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
		ax2.legend(loc=1,prop={'size':8},frameon=False,scatterpoints=1)

		ax3 = fig.add_subplot(312)	
		ax4 = ax3.twinx()
		ax3.plot(redshift, r_hsm, color='k', ls='-', lw=1, label=r'$R_{HSM}$')
		ax4.plot(redshift, r_hsm_vir, color='r', ls='-', lw=0.5, label=r'$R_{VIR}/R_{HSM}$')
		ax3.set_xscale('linear')
		ax3.set_yscale('log')
		ax4.set_yscale('log')
		ax3.set_xlim(lower_limit[i],upper_limit[i])
		ax3.set_ylim(np.power(10.0,rhm_lower[i]),np.power(10.0,rhm_upper[i]))
		ax4.set_ylim(np.power(10.0,rvir_lower[i]),np.power(10.0,rvir_upper[i]))
		ax3.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax3.yaxis.set_major_locator(plt.FixedLocator(rhm_ticks))
		ax4.yaxis.set_major_locator(plt.FixedLocator(rvir_ticks))
		ax3.tick_params(axis='y', labelsize=8)
		ax4.tick_params(axis='y', labelsize=8, colors='red')
		ax3.set_xlabel(r'$z$')
		ax3.set_ylabel(r'$R_{HSM}\;(kpc)$')
		#ax4.set_ylabel(r'$R_{VIR}/R_{HSM}$',rotation=270)
		ax3.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
		ax4.legend(loc=1,prop={'size':8},frameon=False,scatterpoints=1)

		ax5 = fig.add_subplot(313)	
		ax6 = ax5.twinx()
		ax5.plot(redshift, r_hsf, color='k', ls='-', lw=1, label=r'$R_{HSF}$')
		ax6.plot(redshift, r_hsf_vir, color='r', ls='-', lw=0.5, label=r'$R_{VIR}/R_{HSF}$')
		ax5.set_xscale('linear')
		ax5.set_yscale('log')
		ax6.set_yscale('log')
		ax5.set_xlim(lower_limit[i],upper_limit[i])
		ax5.set_ylim(np.power(10.0,rhm_lower[i]),np.power(10.0,rhm_upper[i]))
		ax6.set_ylim(np.power(10.0,rvir_lower[i]),np.power(10.0,rvir_upper[i]))
		ax5.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax5.yaxis.set_major_locator(plt.FixedLocator(rhm_ticks))
		ax6.yaxis.set_major_locator(plt.FixedLocator(rvir_ticks))
		ax5.tick_params(axis='y', labelsize=8)
		ax6.tick_params(axis='y', labelsize=8, colors='red')
		ax5.set_xlabel(r'$z$')
		ax5.set_ylabel(r'$R_{HSF}\;(kpc)$')
		#ax6.set_ylabel(r'$R_{VIR}/R_{HSF}$',rotation=270)
		ax5.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
		ax6.legend(loc=1,prop={'size':8},frameon=False,scatterpoints=1)

		plt.savefig(output_path + 'z_vs_r.png', dpi=300, format='png')
		#plt.tight_layout()
		#plt.subplots_adjust(top=0.88)
		plt.close()

def z_vs_rvir(output_path, i):
	fig = plt.figure(1)
	#fig.suptitle(title)
	fig.subplots_adjust(wspace=0, hspace=0)

	rvir_halo = np.array([a*10.0 for a in rvir_10per])

	ax1 = fig.add_subplot(111)	
	ax1.plot(redshift, rvir_halo, color='k', ls='-', lw=1, label=r'$R_{VIR}$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,r_lower[i])+2,np.power(10.0,r_upper[i]+3))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(r_ticks*1e2))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_xlabel(r'$z$')
	ax1.set_ylabel(r'$Radius\;(kpc)$')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	ax12.set_xlabel(r'$Time\;(Gyr)$')
	ax1.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_rvir.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()

def z_vs_compactness_gas_star_fdisk(output_path, radius, mass_retention, i):
	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)
	#fig.suptitle(r'$R_0:$' + str(radius[j]) + ' kpc')

	ax0 = fig.add_subplot(411)
	for j in range(len(radius)):
		ax0.plot(redshift, m_gas_inner[j], color=color[j], ls='-', lw=1, label='gas<' + str(radius[j]) + ' kpc')
		#ax0.plot(redshift, m_star_inner[j], color=color[j], ls='--', lw=1, label='star<' + str(radius[j]) + ' kpc')
	ax0.plot(redshift, m_gas, color='k', ls='-', lw=1, label=r'$gas<\%10\;R_{VIR}$')
	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(lower_limit[i],upper_limit[i])
	ax0.set_ylim(np.power(10.0,m_star_comp_lower[i]),np.power(10.0,m_star_comp_upper[i]))
	ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax0.yaxis.set_major_locator(plt.FixedLocator(m_star_comp_ticks))
	ax0.tick_params(axis='y', labelsize=8)
	ax0.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax02 = ax0.twiny()
	ax0Xs = ax0.get_xticks()
	ax02Xs = []
	for X in ax0Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax02Xs.append("%.2f" %dummy_age)
	ax02.set_xticks(ax0Xs)
	ax02.set_xbound(ax0.get_xbound())
	ax02.set_xticklabels(ax02Xs)
	#ax02.set_xlabel(r'$time \quad (Gyr)$')
	ax0.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)


	ax1 = fig.add_subplot(412)
	for j in range(len(radius)):
		#ax1.plot(redshift, m_gas_inner[j], color=color[j], ls='-', lw=2, label='gas<' + str(radius[j]) + ' kpc')
		ax1.plot(redshift, m_star_inner[j], color=color[j], ls='--', lw=1, label='star<' + str(radius[j]) + ' kpc')
	ax1.plot(redshift, m_star, color='k', ls='-', lw=1, label=r'$star<\%10\;R_{VIR}$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,m_star_comp_lower[i]),np.power(10.0,m_star_comp_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(m_star_comp_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax1.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(413)
	m_star_compactness = np.array([0.5*a/b for a,b in zip(m_star,r_hsm)])
	m_halo_compactness = np.array([1e-1*a/b for a,b in zip(m_halo,rvir_10per)])
	ax2.plot(redshift, m_star_compactness, color=color[0], ls='-', lw=1, label=r'$M_{*}/R_{HSM}$')
	ax2.plot(redshift, m_halo_compactness, color=color[1], ls='-', lw=1, label=r'$M_{Halo}/R_{VIR}$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,mass_lower[i]),np.power(10.0,mass_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(mass_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$compactness$', size=8)
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	 
	ax3 = fig.add_subplot(414)
	for j in range(len(radius)):
		ax3.plot(redshift, f_disk_inner[j], color=color[j], ls='-', lw=1, label=str(radius[j])+' kpc')
	ax3.set_xscale('linear')
	ax3.set_yscale('linear')
	ax3.set_xlim(lower_limit[i],upper_limit[i])
	ax3.set_ylim(0,1)
	ax3.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax3.yaxis.set_major_locator(plt.FixedLocator([0.2,0.4,0.6,0.8]))
	ax3.tick_params(axis='y', labelsize=8)
	ax3.set_xlabel(r'$z$',size=10)
	ax3.set_ylabel(r'$f_{disk}$')
	ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	#ax3.legend().set_visible(False)

	plt.savefig(output_path + 'z_vs_compactness_gas_star_fdisk.png', dpi=300, format='png')
	plt.close()
	 
def z_vs_fgas_Mtot_tdep(output_path, radius, mass_retention, i):
	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)
	#fig.suptitle(r'$R_0:$' + str(radius[j]) + ' kpc')

	ax0 = fig.add_subplot(311)
	for j in range(len(radius)):
		fgas_inner = np.array([a/(a+b) for a,b in zip(m_gas_inner[j],m_star_inner[j])])
		ax0.plot(redshift, fgas_inner, color=color[j], ls='-', lw=1, label=r'$f_{gas}<$' + str(radius[j]) + r'$ kpc$')
	fgas = np.array([a/(a+b) for a,b in zip(m_gas, m_star)])
	ax0.plot(redshift, fgas, color='k', ls='-', lw=1, label=r'$f_{gas}<\%10\;R_{VIR}$')
	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(lower_limit[i],upper_limit[i])
	ax0.set_ylim(np.power(10.0,fgas_lower[i]),np.power(10.0,fgas_upper[i]))
	ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax0.yaxis.set_major_locator(plt.FixedLocator(fgas_ticks))
	ax0.tick_params(axis='y', labelsize=8)
	ax0.set_ylabel(r'$f_{gas}$')
	ax02 = ax0.twiny()
	ax0Xs = ax0.get_xticks()
	ax02Xs = []
	for X in ax0Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax02Xs.append("%.2f" %dummy_age)
	ax02.set_xticks(ax0Xs)
	ax02.set_xbound(ax0.get_xbound())
	ax02.set_xticklabels(ax02Xs)
	#ax02.set_xlabel(r'$time \quad (Gyr)$')
	ax0.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

	ax1 = fig.add_subplot(312)
	for j in range(len(radius)):
		m_tot_inner = np.array([a+b for a,b in zip(m_gas_inner[j],m_star_inner[j])])
		ax1.plot(redshift, m_tot_inner, color=color[j], ls='-', lw=1, label=r'$M_{tot}<$' + str(radius[j]) + r'$ kpc$')
	m_tot = np.array([a+b for a,b in zip(m_gas,m_star)])
	ax1.plot(redshift, m_tot, color='k', ls='-', lw=1, label=r'$M_{tot}<\%10\;R_{VIR}$')
	ax1.plot(redshift, np.ones(len(redshift))*1e9, color='k', ls='--', lw=0.5)
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,m_star_comp_lower[i]+2),np.power(10.0,m_star_comp_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(m_star_comp_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_xlabel(r'$z$')
	ax1.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax1.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(313)
	for j in range(len(radius)):
		m_tot_inner = np.array([(a+b)*1e-9 for a,b in zip(m_gas_inner[j],m_star_inner[j])])
		f_0_inner = np.array([0.31*np.power(a,2.0)*np.power(b,-1.0/3.0) for a,b in zip(f_disk_inner[j],m_tot_inner)])
		fgas_inner = np.array([a/(a+b) for a,b in zip(m_gas_inner[j],m_star_inner[j])])
		final_term_inner = np.array([a/(1+(b/c)) if c!=0.0 else 0.0 for a,b,c in zip(m_tot_inner, f_0_inner, fgas_inner)])
		ax2.plot(redshift, final_term_inner, color=color[j], ls='-', lw=1, label=str(radius[j]) + ' kpc')

	ax2.plot(redshift, np.ones(len(redshift)), color='k', ls='--', lw=0.5)
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,final_term_lower[i]),np.power(10.0,final_term_upper[i]))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(final_term_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$\frac{M_{tot}}{10^9\;M_{\odot}}\;(1+\frac{f_0}{f_{gas}})^{-1}$', size=8)
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	
	plt.savefig(output_path + 'z_vs_fgas_Mtot_tdep.png', dpi=300, format='png')
	plt.close()
	
def z_vs_sfr(output_path, radius, i):
	snapshot_time = []
	for xxx in range(len(redshift)-1):
		snapshot_time.append(time(redshift[xxx+1]) - time(redshift[xxx]))
	snapshot_time.append(0.0)

	gas_inflow_inner = []
	gas_inflow_inner = []
	for j in range(len(radius)):
		gas_inflow_in = []
		for xxx in range(len(redshift)-1):
			gas_inflow_in.append(m_gas_inner[j][xxx+1] - m_gas_inner[j][xxx])
		gas_inflow_in.append(0.0)
		gas_inflow_in = np.array(gas_inflow_in) / np.array(snapshot_time)
		gas_inflow_inner.append(gas_inflow_in)
		epsilon = 3.6 * np.power((np.array(m_star_inner[j]) * 1e-10),-0.35)
		total = gas_inflow_in - (1.0 - 0.4 + epsilon) * sfr_inner[j]
		gas_inflow_inner.append(total)

	gas_inflow_10per = []
	for xxx in range(len(redshift)-1):
		gas_inflow_10per.append(m_gas[xxx+1] - m_gas[xxx])
	gas_inflow_10per.append(0.0)

	gas_inflow_10per = np.array(gas_inflow_10per) / np.array(snapshot_time)

	fig = plt.figure(1)
	#fig.suptitle(r'$$')
	fig.subplots_adjust(wspace=0, hspace=0)

	ax1 = fig.add_subplot(211)	
	for j in range(len(radius)):
		ax1.plot(redshift, sfr_inner[j], color=color[j], ls='-', lw=1, label=str(radius[j]) + r'$\;kpc$')
	ax1.plot(redshift, sfr, color='k', ls='-', lw=1, label=r'$\%10\;R_{vir}$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,sfr_lower[i]), np.power(10.0,sfr_upper[i]))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(sfr_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_xlabel(r'$z$')
	ax1.set_ylabel(r'$SFR\;(M_{\odot}/yr)$')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(212)	
	for j in range(len(radius)):
		ax2.plot(redshift, gas_inflow_inner[j], color=color[j], ls='-', lw=1, label=str(radius[j]) + r'$\;kpc$')
	ax2.plot(redshift, gas_inflow_10per, color='k', ls='-', lw=1, label=r'$\%10\;R_{vir}$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,sfr_lower[i]), np.power(10.0,sfr_upper[i]))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(sfr_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_xlabel(r'$z$')
	ax2.set_ylabel(r'$\dot{M}_{g\;in}\;(M_{\odot}/yr)$')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_sfr_gas_inflow.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()
	
def z_vs_outflow(output_path, i):
	fig = plt.figure(1)
	fig.suptitle(sim_name[i] + ' cold gas')
	fig.subplots_adjust(wspace=0, hspace=0)

	mass_loading = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow,sfr)])
	mass_loading_inner = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow_inner[3],sfr_inner[3])])

	ax1 = fig.add_subplot(211)	
	ax1.plot(redshift, mass_loading, color='k', ls='--', lw=1, label=r'$\eta$')
	ax1.plot(redshift, outflow, color='r', ls='-', lw=1, label=r'$\dot{M}_{out}$')
	ax1.plot(redshift, inflow, color='g', ls='-', lw=1, label=r'$\dot{M}_{in}$')
	ax1.plot(redshift, sfr, color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax1.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, m_gas*1e-9, color='k', ls='-', lw=2, label=r'$M_{g}/1e9$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,outflow_lower[i]+1),np.power(10.0,outflow_upper[i]-1))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$M_{\odot}/yr$')
	ax1.text(10,1e-2,'GAL')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	#ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(212)	
	ax2.plot(redshift, mass_loading_inner, color='k', ls='--', lw=1, label=r'$\eta$')
	ax2.plot(redshift, outflow_inner[3], color='r', ls='-', lw=1, label=r'$\dot{M}_{out}$')
	ax2.plot(redshift, inflow_inner[3], color='g', ls='-', lw=1, label=r'$\dot{M}_{in}$')
	ax2.plot(redshift, sfr_inner[3], color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax2.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, m_gas_inner[3]*1e-9, color='k', ls='-', lw=2, label=r'$M_{g}/1e9$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,outflow_lower[i]+2),np.power(10.0,outflow_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$M_{\odot}/yr$')
	ax2.text(10,1e-2,'1 kpc')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_outflow.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	
	
def z_vs_outflow_diff(output_path, i):
	fig = plt.figure(1)
	fig.suptitle(sim_name[i] + ' cold gas')
	fig.subplots_adjust(wspace=0, hspace=0)

	#mass_loading = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow,sfr)])
	#mass_loading_inner = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow_inner[3],sfr_inner[3])])

	net_inflow = inflow - outflow
	net_inflow_pos = net_inflow[(net_inflow >= 0.0)]
	net_inflow_neg = -net_inflow[(net_inflow < 0.0)]
	redshift_pos = redshift[(net_inflow >= 0.0)]
	redshift_neg = redshift[(net_inflow < 0.0)]

	net_inflow_inner = inflow_inner[3] - outflow_inner[3]
	net_inflow_inner_pos = net_inflow_inner[(net_inflow_inner >= 0.0)]
	net_inflow_inner_neg = -net_inflow_inner[(net_inflow_inner < 0.0)]
	redshift_inner_pos = redshift[(net_inflow_inner >= 0.0)]
	redshift_inner_neg = redshift[(net_inflow_inner < 0.0)]

	ax1 = fig.add_subplot(211)	
	ax1.scatter(redshift_pos, net_inflow_pos, color='r', marker='o', s=4, label=r'$\dot{M}_{in}>\dot{M}_{out}$')
	ax1.scatter(redshift_neg, net_inflow_neg, color='g', marker='s', s=4, label=r'$\dot{M}_{in}<\dot{M}_{out}$')
	ax1.plot(redshift, sfr, color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax1.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, m_gas*1e-9, color='k', ls='-', lw=2, label=r'$M_{g}/1e9$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,outflow_lower[i]+2),np.power(10.0,outflow_upper[i]-1))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$M_{\odot}/yr$')
	ax1.text(10,1e-2,'GAL')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	#ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(212)	
	ax2.scatter(redshift_inner_pos, net_inflow_inner_pos, color='r', marker='o', s=4, label=r'$\dot{M}_{in}>\dot{M}_{out}$')
	ax2.scatter(redshift_inner_neg, net_inflow_inner_neg, color='g', marker='s', s=4, label=r'$\dot{M}_{in}<\dot{M}_{out}$')
	ax2.plot(redshift, sfr_inner[3], color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax2.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, m_gas_inner[3]*1e-9, color='k', ls='-', lw=2, label=r'$M_g/1e9$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,outflow_lower[i]+2),np.power(10.0,outflow_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$M_{\odot}/yr$')
	ax2.text(10,1e-2,'1 kpc')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_outflow_diff.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	

#####################
##### TOTAL GAS #####
#####################

def z_vs_outflow_tot(output_path, i):
	fig = plt.figure(1)
	fig.suptitle(sim_name[i] + ' total gas')
	fig.subplots_adjust(wspace=0, hspace=0)

	mass_loading = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow_tot,sfr)])
	mass_loading_inner = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow_inner_tot[3],sfr_inner[3])])

	ax1 = fig.add_subplot(211)	
	ax1.plot(redshift, mass_loading, color='k', ls='--', lw=1, label=r'$\eta$')
	ax1.plot(redshift, outflow_tot, color='r', ls='-', lw=1, label=r'$\dot{M}_{out}$')
	ax1.plot(redshift, inflow_tot, color='g', ls='-', lw=1, label=r'$\dot{M}_{in}$')
	ax1.plot(redshift, sfr, color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax1.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, m_gas_tot*1e-9, color='k', ls='-', lw=2, label=r'$M_{g}/1e9$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,outflow_lower[i]+3),np.power(10.0,outflow_upper[i]-1))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$M_{\odot}/yr$')
	ax1.text(10,1e-2,'GAL')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	#ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(212)
	ax2.plot(redshift, mass_loading_inner, color='k', ls='--', lw=1, label=r'$\eta$')
	ax2.plot(redshift, outflow_inner_tot[3], color='r', ls='-', lw=1, label=r'$\dot{M}_{out}$')
	ax2.plot(redshift, inflow_inner_tot[3], color='g', ls='-', lw=1, label=r'$\dot{M}_{in}$')
	ax2.plot(redshift, sfr_inner[3], color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax2.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, m_gas_inner_tot[3]*1e-9, color='k', ls='-', lw=2, label=r'$M_{g}/1e9$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,outflow_lower[i]+3),np.power(10.0,outflow_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$M_{\odot}/yr$')
	ax2.text(10,1e-2,'1 kpc')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_outflow_tot.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	
	
def z_vs_outflow_diff_tot(output_path, i):
	fig = plt.figure(1)
	fig.suptitle(sim_name[i] + ' total gas')
	fig.subplots_adjust(wspace=0, hspace=0)

	#mass_loading = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow,sfr)])
	#mass_loading_inner = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow_inner[3],sfr_inner[3])])

	net_inflow = inflow_tot - outflow_tot
	net_inflow_pos = net_inflow[(net_inflow >= 0.0)]
	net_inflow_neg = -net_inflow[(net_inflow < 0.0)]
	redshift_pos = redshift[(net_inflow >= 0.0)]
	redshift_neg = redshift[(net_inflow < 0.0)]

	net_inflow_inner = inflow_inner_tot[3] - outflow_inner_tot[3]
	net_inflow_inner_pos = net_inflow_inner[(net_inflow_inner >= 0.0)]
	net_inflow_inner_neg = -net_inflow_inner[(net_inflow_inner < 0.0)]
	redshift_inner_pos = redshift[(net_inflow_inner >= 0.0)]
	redshift_inner_neg = redshift[(net_inflow_inner < 0.0)]

	ax1 = fig.add_subplot(211)	
	ax1.scatter(redshift_pos, net_inflow_pos, color='r', marker='o', s=4, label=r'$\dot{M}_{in}>\dot{M}_{out}$')
	ax1.scatter(redshift_neg, net_inflow_neg, color='g', marker='s', s=4, label=r'$\dot{M}_{in}<\dot{M}_{out}$')
	ax1.plot(redshift, sfr, color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax1.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, m_gas_tot*1e-9, color='k', ls='-', lw=2, label=r'$M_{g}/1e9$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,outflow_lower[i]+2),np.power(10.0,outflow_upper[i]-1))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$M_{\odot}/yr$')
	ax1.text(10,1e-2,'GAL')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	#ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(212)	
	ax2.scatter(redshift_inner_pos, net_inflow_inner_pos, color='r', marker='o', s=4, label=r'$\dot{M}_{in}>\dot{M}_{out}$')
	ax2.scatter(redshift_inner_neg, net_inflow_inner_neg, color='g', marker='s', s=4, label=r'$\dot{M}_{in}<\dot{M}_{out}$')
	ax2.plot(redshift, sfr_inner[3], color='b', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax2.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, m_gas_inner_tot[3]*1e-9, color='k', ls='-', lw=2, label=r'$M_g/1e9$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,outflow_lower[i]+2),np.power(10.0,outflow_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$M_{\odot}/yr$')
	ax2.text(10,1e-2,'1 kpc')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_outflow_diff_tot.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	

def z_vs_sfr_mgin(output_path, i):
	fig = plt.figure(1)
	fig.suptitle(sim_name[i])
	fig.subplots_adjust(wspace=0, hspace=0)

	mass_loading = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow,sfr)])
	mass_loading_inner = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow_inner[3],sfr_inner[3])])

	ax1 = fig.add_subplot(211)	
	#ax1.plot(redshift, mass_loading, color='k', ls='-', lw=1, label=r'$\eta$')
	ax1.plot(redshift, inflow, color='b', ls='-', lw=1, label=r'$\dot{M}_{in}\;cold$')
	ax1.plot(redshift, inflow_tot, color='r', ls='-', lw=1, label=r'$\dot{M}_{in}\;tot$')

	ax1.plot(redshift, sfr, color='g', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax1.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, m_gas*1e-9, color='k', ls='--', lw=2, label=r'$m_g\;1e-9\;cold$')
	ax1.plot(redshift, m_gas_tot*1e-9, color='k', ls='-', lw=2, label=r'$m_g\;1e-9\;tot$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,outflow_lower[i]+3),np.power(10.0,outflow_upper[i]-1))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$M_{\odot}/yr$')
	ax1.text(10,1e-2,'GAL')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	#ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(212)	
	#ax2.plot(redshift, mass_loading_inner, color='k', ls='-', lw=1, label=r'$\eta$')
	ax2.plot(redshift, inflow_inner[3], color='b', ls='-', lw=1, label=r'$\dot{M}_{in}\;cold$')
	ax2.plot(redshift, inflow_inner_tot[3], color='r', ls='-', lw=1, label=r'$\dot{M}_{in}\;tot$')
	ax2.plot(redshift, sfr_inner[3], color='g', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax2.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, m_gas_inner[3]*1e-9, color='k', ls='--', lw=2, label=r'$m_g\;1e-9\;cold$')
	ax2.plot(redshift, m_gas_inner_tot[3]*1e-9, color='k', ls='-', lw=2, label=r'$m_g\;1e-9\;tot$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,outflow_lower[i]+2),np.power(10.0,outflow_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$M_{\odot}/yr$')
	ax2.text(10,1e-2,'1 kpc')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_sfr_mgin.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	
	
def z_vs_sfr_mgout(output_path, i):
	fig = plt.figure(1)
	fig.suptitle(sim_name[i])
	fig.subplots_adjust(wspace=0, hspace=0)

	mass_loading = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow,sfr)])
	mass_loading_inner = np.array([0.0 if b==0.0 else a/b for a,b in zip(outflow_inner[3],sfr_inner[3])])

	ax1 = fig.add_subplot(211)	
	#ax1.plot(redshift, mass_loading, color='k', ls='-', lw=1, label=r'$\eta$')
	ax1.plot(redshift, outflow, color='b', ls='-', lw=1, label=r'$\dot{M}_{out}\;cold$')
	ax1.plot(redshift, outflow_tot, color='r', ls='-', lw=1, label=r'$\dot{M}_{out}\;tot$')

	ax1.plot(redshift, sfr, color='g', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax1.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax1.plot(redshift, m_gas*1e-9, color='k', ls='--', lw=2, label=r'$m_g\;1e-9\;cold$')
	ax1.plot(redshift, m_gas_tot*1e-9, color='k', ls='-', lw=2, label=r'$m_g\;1e-9\;tot$')
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(lower_limit[i],upper_limit[i])
	ax1.set_ylim(np.power(10.0,outflow_lower[i]+3),np.power(10.0,outflow_upper[i]-1))
	ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax1.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$M_{\odot}/yr$')
	ax1.text(10,1e-2,'GAL')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	#ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(212)	
	#ax2.plot(redshift, mass_loading_inner, color='k', ls='-', lw=1, label=r'$\eta$')
	ax2.plot(redshift, outflow_inner[3], color='b', ls='-', lw=1, label=r'$\dot{M}_{out}\;cold$')
	ax2.plot(redshift, outflow_inner_tot[3], color='r', ls='-', lw=1, label=r'$\dot{M}_{out}\;tot$')
	ax2.plot(redshift, sfr_inner[3], color='g', ls='-', lw=1, label=r'$\dot{M}_{*}$')
	ax2.plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)
	ax2.plot(redshift, m_gas_inner[3]*1e-9, color='k', ls='--', lw=2, label=r'$m_g\;1e-9\;cold$')
	ax2.plot(redshift, m_gas_inner_tot[3]*1e-9, color='k', ls='-', lw=2, label=r'$m_g\;1e-9\;tot$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(lower_limit[i],upper_limit[i])
	ax2.set_ylim(np.power(10.0,outflow_lower[i]+2),np.power(10.0,outflow_upper[i]-1))
	ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax2.yaxis.set_major_locator(plt.FixedLocator(outflow_ticks))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_ylabel(r'$M_{\odot}/yr$')
	ax2.text(10,1e-2,'1 kpc')
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(output_path + 'z_vs_sfr_mgout.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()	
	
def plot(output_path, sim_name, sim_suite, halo_ID, i):

	# upper & lower limits
	global lower_limit 
	global upper_limit 
	global mass_lower 
	global mass_upper 
	global bh_lower 
	global bh_upper 
	global mdot_lower 
	global mdot_upper 
	global f_lower 
	global f_upper 
	global bulge_lower 
	global bulge_upper 
	global vel_lower 
	global vel_upper 
	global sfr_lower 
	global sfr_upper 
	global mdot_over_mbh_lower 
	global mdot_over_mbh_upper 
	global sfr_over_mdot_lower 
	global sfr_over_mdot_upper 
	global r_lower 
	global r_upper 
	global fig6_lower 
	global fig6_upper 
	global fig7_lower 
	global fig7_upper 
	global rhm_lower 
	global rhm_upper 
	global rvir_lower
	global rvir_upper
	global r_diff_lower
	global r_diff_upper
	global mbh_diff_lower
	global mbh_diff_upper
	global slope_lower
	global slope_upper
	global ssfr_lower 
	global ssfr_upper 
	global m_star_comp_lower
	global m_star_comp_upper
	global m_norm_halo_lower
	global m_norm_halo_upper
	global m_norm_bh_lower
	global m_norm_bh_upper
	global fgas_lower
	global fgas_upper
	global final_term_lower
	global final_term_upper
	global outflow_upper
	global outflow_lower

	# ticks & labels
	global label 
	global mdot_over_mbh_label 
	global sfr_over_mdot_label 
	global r_label 
	global z_ticks 
	global mass_ticks 
	global bh_ticks  
	global mdot_ticks  
	global f_ticks 
	global bulge_ticks 
	global vel_ticks 
	global sfr_ticks 
	global mdot_over_mbh_ticks 
	global sfr_over_mdot_ticks 
	global r_ticks 
	global slope_ticks
	global fig6_ticks
	global fig7_ticks
	global rhm_ticks
	global rvir_ticks
	global ssfr_ticks
	global r_diff_ticks
	global mbh_diff_ticks
	global m_star_comp_ticks
	global m_norm_halo_ticks
	global m_norm_bh_ticks
	global fgas_ticks
	global final_term_ticks
	global outflow_ticks

	global data_merger
	global data_nomerger
	global regime_list
	global radius_list
	global radius_name_list
	global mass_retention
	global seed_mass_name_list

	global mass_list 
	global vel_list 
	global c1 #for 1kpc fit
	global c2 #for %10 rvir fit

	global color 
	global marker

	global Ferrerase02_data
	global Haring04_data
	global Kormendy2013_data
	global McConnell2013_data
	global Reines2015_data
	global McConnell_Ma2013_data

	#pdb.set_trace()

	lower_limit = [2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,2,2,2,2,6,6,6,6,6,6,6,6,6,6,6,6,6,6,1,1,1,1]
	upper_limit = [12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12]
	mass_lower = [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
	mass_upper = [13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13.5]
	bh_lower = [4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]
	bh_upper = [9,9,8,9,9,9,9,9,9,9,9,9,10,10,10,9,9,9,9,7,7,7,7,7,7,7,7,7,7,7,7,7,7,11,11,11,11]
	mdot_lower = [-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7,-7]
	mdot_upper = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,-1,-1,-1,-1,-1,-1,-1,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1]
	f_lower = [-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-5,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-5,-5,-5,-5,-5,-5,-5,-5]
	f_upper = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	bulge_lower = [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
	bulge_upper = [11,11,11,11,11,11,11,12,12,12,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,12,12,12,12]
	vel_lower = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
	vel_upper = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
	sfr_lower = [-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2]
	sfr_upper = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
	mdot_over_mbh_lower = [-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3]
	mdot_over_mbh_upper = [3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	sfr_over_mdot_lower = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
	sfr_over_mdot_upper = [7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7]
	r_lower = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2]
	r_upper = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	fig6_lower = [-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6]
	fig6_upper = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	fig7_lower = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2]
	fig7_upper = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
	rhm_lower = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2]
	rhm_upper = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	ssfr_lower = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2]
	ssfr_upper = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
	rvir_lower = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	rvir_upper = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
	r_diff_lower = [-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6,-6]
	r_diff_upper = [5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
	mbh_diff_lower = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	mbh_diff_upper = [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
	slope_lower = [-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2,-2]
	slope_upper = [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
	m_star_comp_lower = [4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]
	m_star_comp_upper = [12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12]
	m_norm_halo_lower = [-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4]
	m_norm_halo_upper = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1]
	m_norm_bh_lower = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	m_norm_bh_upper = [4,4,4,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
	fgas_lower = [-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4]
	fgas_upper = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
	final_term_lower = [-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3,-3]
	final_term_upper = [3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3]
	outflow_upper = [4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]
	outflow_lower = [-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4,-4]
	c1 = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-0.2,-0.2,-0.2,-0.1]
	c2 = [-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-0.65,-0.4,-0.3,-0.1]

	label = [11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,11.5,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
	mdot_over_mbh_label = [0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8]
	sfr_over_mdot_label = [4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75,4.75]
	r_label = [-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6,-0.6]

	color = ['r','g','b','m','c']
	marker = ['^','s','P','o','*']

	#uni = [lower_limit, upper_limit, mass_lower, mass_upper, bh_lower, bh_upper, mdot_lower, mdot_upper, f_lower, f_upper, bulge_lower, bulge_upper, vel_lower, vel_upper, sfr_lower, sfr_upper, mdot_over_mbh_lower, mdot_over_mbh_upper, sfr_over_mdot_lower, sfr_over_mdot_upper, r_lower, r_upper, fig6_lower, fig6_upper, fig7_lower, fig7_upper, rhm_lower, rhm_upper, ssfr_lower, ssfr_upper, rvir_lower, rvir_upper, r_diff_lower, r_diff_upper, mbh_diff_lower, mbh_diff_upper, slope_lower, slope_upper, m_star_comp_lower, m_star_comp_upper, m_norm_halo_lower, m_norm_halo_upper, m_norm_bh_lower, m_norm_bh_upper, fgas_lower, fgas_upper, final_term_lower, final_term_upper, outflow_lower, outflow_upper, c1, c2, label, mdot_over_mbh_label, sfr_over_mdot_label, r_label] 

	#for xxx in range(len(uni)):
	#	print '%02d' %(xxx+1), len(uni[xxx])
	#quit()

	z_ticks = range(lower_limit[i]+1,upper_limit[i])
	mass_ticks = np.logspace(mass_lower[i]+1, int(mass_upper[i])-1, num=mass_upper[i]-mass_lower[i]-1, base=10, dtype='float')
	bh_ticks = np.logspace(bh_lower[i]+1, bh_upper[i]-1, num=bh_upper[i]-bh_lower[i]-1, base=10, dtype='float') 
	mdot_ticks = np.logspace(mdot_lower[i]+1, mdot_upper[i]-1, num=mdot_upper[i]-mdot_lower[i]-1, base=10, dtype='float') 
	f_ticks = np.logspace(f_lower[i]+1, f_upper[i]-1, num=f_upper[i]-f_lower[i]-1, base=10, dtype='float') 
	bulge_ticks = np.logspace(bulge_lower[i]+1, bulge_upper[i]-1, num=bulge_upper[i]-bulge_lower[i]-1, base=10, dtype='float') 
	vel_ticks = np.logspace(vel_lower[i], vel_upper[i]-1, num=vel_upper[i]-vel_lower[i], base=10, dtype='float') 
	sfr_ticks = np.logspace(sfr_lower[i], sfr_upper[i]-1, num=sfr_upper[i]-sfr_lower[i], base=10, dtype='float') 
	mdot_over_mbh_ticks = np.logspace(mdot_over_mbh_lower[i]+1, mdot_over_mbh_upper[i]-1, num=mdot_over_mbh_upper[i]-mdot_over_mbh_lower[i]-1, base=10, dtype='float') 
	sfr_over_mdot_ticks = np.logspace(sfr_over_mdot_lower[i]+1, sfr_over_mdot_upper[i]-1, num=sfr_over_mdot_upper[i]-sfr_over_mdot_lower[i]-1, base=10, dtype='float') 
	r_ticks = np.logspace(r_lower[i]+1, r_upper[i], num=r_upper[i]-r_lower[i], base=10, dtype='float') 
	fig6_ticks = np.logspace(fig6_lower[i]+1, fig6_upper[i]-1, num=fig6_upper[i]-fig6_lower[i]-1, base=10, dtype='float') 
	fig7_ticks = np.logspace(fig7_lower[i]+1, fig7_upper[i]-1, num=fig7_upper[i]-fig7_lower[i]-1, base=10, dtype='float') 
	rhm_ticks = np.logspace(rhm_lower[i], rhm_upper[i]-1, num=rhm_upper[i]-rhm_lower[i], base=10, dtype='float') 
	ssfr_ticks = np.logspace(ssfr_lower[i]+1, ssfr_upper[i]-1, num=ssfr_upper[i]-ssfr_lower[i]-1, base=10, dtype='float') 
	rvir_ticks = np.logspace(rvir_lower[i], rvir_upper[i]-1, num=rvir_upper[i]-rvir_lower[i], base=10, dtype='float') 
	r_diff_ticks = np.arange(r_diff_lower[i], r_diff_upper[i], 1)
	mbh_diff_ticks = np.arange(mbh_diff_lower[i], mbh_diff_upper[i], 2)
	slope_ticks = np.arange(slope_lower[i], slope_upper[i], 2)
	m_star_comp_ticks = np.logspace(m_star_comp_lower[i]+1, m_star_comp_upper[i]-1, num=m_star_comp_upper[i]-m_star_comp_lower[i]-1, base=10, dtype='float')
	m_norm_halo_ticks = np.logspace(m_norm_halo_lower[i]+1, m_norm_halo_upper[i]-1, num=m_norm_halo_upper[i]-m_norm_halo_lower[i]-1, base=10, dtype='float')
	m_norm_bh_ticks = np.logspace(m_norm_bh_lower[i]+1, m_norm_bh_upper[i]-1, num=m_norm_bh_upper[i]-m_norm_bh_lower[i]-1, base=10, dtype='float')
	fgas_ticks = np.logspace(fgas_lower[i]+1, fgas_upper[i]-1, num=fgas_upper[i]-fgas_lower[i]-1, base=10, dtype='float')
	final_term_ticks = np.logspace(final_term_lower[i], final_term_upper[i]-1, num=final_term_upper[i]-final_term_lower[i], base=10, dtype='float')
	outflow_ticks =  np.logspace(outflow_lower[i]+1, outflow_upper[i], num=outflow_upper[i]-outflow_lower[i], base=10, dtype='float')

	print output_path + sim_name + '.hdf5'
	data_merger = h5py.File(output_path + sim_name + '_merger.hdf5','r') 
	data_nomerger = h5py.File(output_path + sim_name + '_nomerger.hdf5','r') 

	regime_list = ['cold', 'total']
	radius_name_list = ['0.1', '0.2', '0.5', '1.0', 'rvir']
	radius_list = [0.1, 0.2, 0.5, 1.0, 10.0]
	mass_retention = [0.05, 0.1, 0.25, 0.5, 1.0]
	seed_mass_name_list = ['1e1', '1e2', '1e3', '1e4', '1e5']

	mass_list = np.array([1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12,1e13,1e14])
	vel_list = np.array([0,20,40,60,80,100,200,400,800])
	Ferrerase02_data = []
	Haring04_data = []
	Kormendy2013_data = []
	McConnell2013_data = []
	Reines2015_data = []
	Savorgnan2016_data = []
	McConnell_Ma2013_data = []

	for mass_dummy in mass_list:
		Ferrerase02_data.append(Ferrerase02(mass_dummy))
		Haring04_data.append(Haring04(mass_dummy))
		Kormendy2013_data.append(Kormendy2013(mass_dummy))
		McConnell2013_data.append(McConnell2013(mass_dummy))
		Reines2015_data.append(Reines2015(mass_dummy))
		#Savorgnan2016_data.append(Savorgnan2016(mass_dummy))
	for vel_dummy in vel_list:
		McConnell_Ma2013_data.append(McConnell_Ma2013(vel_dummy))

	Ferrerase02_data = np.array(Ferrerase02_data)
	Haring04_data = np.array(Haring04_data)
	Kormendy2013_data = np.array(Kormendy2013_data)
	McConnell2013_data = np.array(McConnell2013_data)
	Reines2015_data = np.array(Reines2015_data)
	#Savorgnan2016_data = np.array(Savorgnan2016_data)
	McConnell_Ma2013_data = np.array(McConnell_Ma2013_data)

	####################################
	########## PLOT FUNCTIONS ##########
	####################################

	# COLD GAS
	#scaling_relations(output_path, i)
	#BH_scaling_relations(output_path, i)
	##BH_scaling_relations_supp(output_path, radius, mass_retention, i)
	##m_star_bulge_vs_r_hsm_vir(output_path, i)
	##m_bh_vs_compactness_gas_star_fdisk(output_path, radius, mass_retention, i)
	##m_halo_vs_compactness_gas_star_fdisk(output_path, radius, mass_retention, i)
	##outflow_vs_mstar_mdot_mbh(output_path, i)
	##outflow_vs_mbh(output_path, i)
	##outflow_twozone_mbh(output_path, i)
	#z_vs_mass_gsh_sfr(output_path, i)
	#z_vs_mass_gshdb_f_sfr(output_path, i)
	z_vs_mbh_mdot(output_path, i)
	#z_vs_hmr(output_path, i)
	##z_vs_rvir(output_path, i)
	##z_vs_compactness_gas_star_fdisk(output_path, radius, mass_retention, i)
	##z_vs_fgas_Mtot_tdep(output_path, radius, mass_retention, i)
	##z_vs_sfr(output_path, radius, i)
	##z_vs_outflow(output_path, i)
	##z_vs_outflow_diff(output_path, i)

	# TOTAL GAS
	##z_vs_outflow_tot(output_path, i)
	##z_vs_outflow_diff_tot(output_path, i)
	##z_vs_sfr_mgin(output_path, i)
	##z_vs_sfr_mgout(output_path, i)
#################################################### 
################### MAIN PROGRAM ###################
#################################################### 
global sim_name
global sim_suite

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

global cosmo
global grav_constant
global kpc_cm_conversion
global m_sun
global yr_s_conversion

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)
grav_constant = const.G.cgs.value
kpc_cm_conversion = 3.086e21
m_sun = const.M_sun.cgs.value
yr_s_conversion = 3.15576e7

i_list = [13, 34] #range(37)

for i in i_list:
	output_path = './onuronur/' + sim_suite[i] + '/' + sim_name[i] + '/'
	ID_list = [0]
	for halo_ID in ID_list:
		plot(output_path, sim_name[i], sim_suite[i], halo_ID, i)

