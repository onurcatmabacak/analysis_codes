#!/bin/bash

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

SIM_NAME=(FB15N1024_DM_lev2)
SIM_SUITE=(FIREbox)

START=(0)
FINISH=(1200)

LIST=(0)

for i in ${LIST[@]}
do
	file_path=/bulk1/feldmann/Onur/${SIM_SUITE[$i]}/${SIM_NAME[$i]}
	create_dir /bulk1/feldmann/Onur/${SIM_SUITE[$i]}
	create_dir $file_path

	write_path=$file_path/zred_pos_list
	tmp_path=$file_path/tmp.txt
	echo ${SIM_NAME[$i]}
	rm $write_path

	for ((j=${START[$i]}; j<=${FINISH[$i]}; j++)) 
	do
		SNAPSHOT=`printf "%03d" $j`
		array=/bulk1/feldmann/data/${SIM_SUITE[$i]}/production_runs/${SIM_NAME[$i]}/snapdir_$SNAPSHOT/snapshot_$SNAPSHOT.0.hdf5

		h5dump -a "Header/Redshift" $array > $tmp_path
		name=$(sed -n 6p $tmp_path)
		redshift=`printf "%.5f" ${name:8}`

		dummy=`printf $array | cut -d "/" -f 9`
		echo $redshift   $SNAPSHOT >> $write_path

		rm $tmp_path
	done
done
