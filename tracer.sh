#!/bin/bash
module load hydra
export OMP_NUM_THREADS=32

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

#MassiveFIRE
#0  B100_N512_M3e13_TL00001_baryon_toz2_HR_9915
#1  B100_N512_M3e13_TL00003_baryon_toz2_HR_9915
#2  B100_N512_M3e13_TL00004_baryon_toz2_HR_9915
#3  B100_N512_TL00002_baryon_toz2
#4  B100_N512_TL00007_baryon_toz2_HR
#5  B100_N512_TL00009_baryon_toz2_HR
#6  B100_N512_TL00011_baryon_toz2_HR
#7  B100_N512_TL00013_baryon_toz2_HR
#8  B100_N512_TL00018_baryon_toz2_HR
#9  B100_N512_TL00029_baryon_toz2_HR
#10 B100_N512_TL00031_baryon_toz2_HR
#11 B100_N512_TL00037_baryon_toz0_HR_9915
#12 B100_N512_TL00113_baryon_toz0_HR_9915
#13 B100_N512_TL00206_baryon_toz0_HR_9915
#14 B100_N512_TL00217_baryon_toz2
#15 B100_N512_TL00223_baryon_toz2_HR
#16 B100_N512_TL00228_baryon_toz2
#17 B100_N512_TL00236_baryon_toz2
#18 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915
#19 B400_N512_z6_TL00000_baryon_toz6_HR_9915
#20 B400_N512_z6_TL00001_baryon_toz6_HR_9915
#21 B400_N512_z6_TL00002_baryon_toz6_HR_9915
#22 B400_N512_z6_TL00005_baryon_toz6_HR_9915
#23 B400_N512_z6_TL00006_baryon_toz6_HR_9915
#24 B400_N512_z6_TL00013_baryon_toz6_HR_9915
#25 B400_N512_z6_TL00017_baryon_toz6_HR_9915
#26 B400_N512_z6_TL00021_baryon_toz6_HR_9915
#27 B762_N1024_z6_TL00000_baryon_toz6_HR
#28 B762_N1024_z6_TL00001_baryon_toz6_HR
#29 B762_N1024_z6_TL00002_baryon_toz6_HR

#MassiveFIRE2
#30 B762_N1024_z6_TL00000_baryon_toz6_HR 
#31 B762_N1024_z6_TL00001_baryon_toz6_HR 
#32 B762_N1024_z6_TL00002_baryon_toz6_HR
#33 h113_HR_sn1dy300ro100ss
#34 h206_HR_sn1dy300ro100ss
#35 h29_HR_sn1dy300ro100ss
#36 h2_HR_sn1dy300ro100ss

SIM_NAME=(B100_N512_M3e13_TL00001_baryon_toz2_HR_9915 B100_N512_M3e13_TL00003_baryon_toz2_HR_9915 B100_N512_M3e13_TL00004_baryon_toz2_HR_9915 B100_N512_TL00002_baryon_toz2 B100_N512_TL00007_baryon_toz2_HR B100_N512_TL00009_baryon_toz2_HR B100_N512_TL00011_baryon_toz2_HR B100_N512_TL00013_baryon_toz2_HR B100_N512_TL00018_baryon_toz2_HR B100_N512_TL00029_baryon_toz2_HR B100_N512_TL00031_baryon_toz2_HR B100_N512_TL00037_baryon_toz0_HR_9915 B100_N512_TL00113_baryon_toz0_HR_9915 B100_N512_TL00206_baryon_toz0_HR_9915 B100_N512_TL00217_baryon_toz2 B100_N512_TL00223_baryon_toz2_HR B100_N512_TL00228_baryon_toz2 B100_N512_TL00236_baryon_toz2 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915 B400_N512_z6_TL00000_baryon_toz6_HR_9915 B400_N512_z6_TL00001_baryon_toz6_HR_9915 B400_N512_z6_TL00002_baryon_toz6_HR_9915 B400_N512_z6_TL00005_baryon_toz6_HR_9915 B400_N512_z6_TL00006_baryon_toz6_HR_9915 B400_N512_z6_TL00013_baryon_toz6_HR_9915 B400_N512_z6_TL00017_baryon_toz6_HR_9915 B400_N512_z6_TL00021_baryon_toz6_HR_9915 B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR h113_HR_sn1dy300ro100ss h206_HR_sn1dy300ro100ss h29_HR_sn1dy300ro100ss h2_HR_sn1dy300ro100ss)
SIM_SUITE=(MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2)
DUMMY=(16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 178 178 178 11 11 11 11)

AHF_FILEDIR=AHF_com # AHF or AHF_com
AHF_CENTERING=COM # MDC or COM
HALO=halo
NJOBS=2
MEMORY=64
ANALYSIS=all
LIST=(33 35 36)

for i in ${LIST[@]}
do

	if [ "$DUMMY" == "178" ]
	then
		SNAPSHOT_PATH=/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/binary_files/${SIM_NAME[$i]}
	else
		if [ "${SIM_SUITE[$i]}" == "MassiveFIRE" ] 
		then
			# MassiveFIRE1
			HALO_PATH=/net/cephfs/shares/feldmann/data/${SIM_SUITE[$i]}/analysis/$AHF_FILEDIR/HR/${SIM_NAME[$i]}/$HALO
			SNAPSHOT_PATH=/net/cephfs/shares/feldmann/data/${SIM_SUITE[$i]}/production_runs/HR/${SIM_NAME[$i]}
		else
			# MassiveFIRE2
			HALO_PATH=/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/$AHF_FILEDIR/HR/${SIM_NAME[$i]}/$HALO
			SNAPSHOT_PATH=/bulk1/feldmann/data/${SIM_SUITE[$i]}/production_runs/HR/${SIM_NAME[$i]}
		fi
	fi

	INPUT_PATH=/bulk1/feldmann/Onur/$AHF_CENTERING/${SIM_SUITE[$i]}/${SIM_NAME[$i]}

	if [ "$ANALYSIS" == "all" ]
	then
		filename=$INPUT_PATH/ID_list.txt
		ids=( $(awk '{print $1}' $filename) )
	else
		ids=(0)
	fi 

	for id in ${ids[@]}
	do

		JOBSUB=`printf "jobsub_tracer_%s_%s_id_%s.sh" $AHF_CENTERING ${SIM_NAME[$i]} $id`

		printf "#!/bin/bash -l \n" > $JOBSUB		
		printf "#SBATCH --qos=medium" >> $JOBSUB
		printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
		printf "#SBATCH --time=48:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
		printf "#SBATCH --ntasks=1 --cpus-per-task=$NJOBS \n" >> $JOBSUB
		printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
		printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
		printf "#======START===== \n" >> $JOBSUB
		printf "srun python tracer.py $HALO_PATH $SNAPSHOT_PATH $INPUT_PATH ${DUMMY[$i]} $id \n" >> $JOBSUB 
		printf "#=====END==== \n" >> $JOBSUB
		sbatch $JOBSUB 
		#python tracer.py $HALO_PATH $SNAPSHOT_PATH $INPUT_PATH ${DUMMY[$i]} $id	
	done 

done
