import os
import numpy as np
from onur_tools import *
import onur_tools as ot
import h5py
from joblib import Parallel, delayed
import multiprocessing
import sys

def run(read_path, halo_path, save_path, sim_suite, sim_name, merger, halo_ID, filter_value, filter_flag):

	# starts from z=12 to z=0
	z_path = read_path 

	regime_list = ['cold','total']
	radius_list = [0.1, 0.2, 0.5, 1.0, 10.0]
	radius_name_list = ['0.1', '0.2', '0.5', '1.0', 'rvir']
	mass_retention_list = [0.05, 0.1, 0.25, 0.5, 1.0] 
	seed_mass_list = [1e1, 1e2, 1e3, 1e4, 1e5]
	seed_mass_name_list = ['1e1', '1e2', '1e3', '1e4', '1e5']

	if merger == 1:
		if filter_flag == 0: filename = read_path + '/' + sim_name + '_merger.hdf5'
		if filter_flag == 1: filename = read_path + '/' + sim_name + '_all.hdf5'

	if merger == 0:
		filename = read_path + '/' + sim_name + '_nomerger.hdf5'

	f = h5py.File(filename,'a')

	# Timeline file data
	# starts from z=0 to z=12
	filename_timeline = read_path + '/timeline_' + str(halo_ID) + '.txt'
	z_1_timeline, ID_timeline, mhalo_timeline = np.genfromtxt(filename_timeline, delimiter='', usecols=(0,1,3), skip_header=0, unpack=True)
	ID_timeline = ID_timeline.astype(int)
	# reverse data now it is from 12 to 0, timeline data is REVERSE
	z_1_timeline = z_1_timeline[::-1]
	ID_timeline = ID_timeline[::-1]
	mhalo_timeline = mhalo_timeline[::-1]

	# Bulge data read
	# starts from z=12 to z=0
	bdd_data = h5py.File(save_path + '/BDD_data.hdf5', 'r')
	data = {}

	for regime in regime_list:
		for radius_name in radius_name_list:

			prefix = 'halo_' + str(halo_ID) + '/' + regime + '/' + sim_suite + '/' + sim_name + '/' + radius_name

			if radius_name == 'rvir':
				data[regime + '/' + radius_name + '/radius'] = bdd_data[prefix + '/radius'].value
				data[regime + '/' + radius_name + '/rvir_hgm'] = bdd_data[prefix + '/rvir_hgm'].value
				data[regime + '/' + radius_name + '/rvir_hsm'] = bdd_data[prefix + '/rvir_hsm'].value
				data[regime + '/' + radius_name + '/rvir_hsf'] = bdd_data[prefix + '/rvir_hsf'].value

			data[regime + '/' + radius_name + '/z_1'] = bdd_data[prefix + '/z_1'].value
			data[regime + '/' + radius_name + '/ID'] = bdd_data[prefix + '/ID'].value
			data[regime + '/' + radius_name + '/mgas'] = bdd_data[prefix + '/mgas'].value
			data[regime + '/' + radius_name + '/mstar'] = bdd_data[prefix + '/mstar'].value
			data[regime + '/' + radius_name + '/mhalo'] = bdd_data[prefix + '/mhalo'].value
			data[regime + '/' + radius_name + '/mbulge'] = bdd_data[prefix + '/mbulge'].value
			data[regime + '/' + radius_name + '/mdisk'] = bdd_data[prefix + '/mdisk'].value
			data[regime + '/' + radius_name + '/fbulge'] = bdd_data[prefix + '/fbulge'].value
			data[regime + '/' + radius_name + '/fdisk'] = bdd_data[prefix + '/fdisk'].value
			data[regime + '/' + radius_name + '/inflow'] = bdd_data[prefix + '/inflow'].value
			data[regime + '/' + radius_name + '/outflow'] = bdd_data[prefix + '/outflow'].value
			data[regime + '/' + radius_name + '/bh_vel'] = bdd_data[prefix + '/bh_vel'].value
			data[regime + '/' + radius_name + '/vel_disp'] = bdd_data[prefix + '/vel_disp'].value
			data[regime + '/' + radius_name + '/num_new_stars'] = bdd_data[prefix + '/num_new_stars'].value
			data[regime + '/' + radius_name + '/sfr'] = bdd_data[prefix + '/sfr'].value
			data[regime + '/' + radius_name + '/ssfr'] = bdd_data[prefix + '/ssfr'].value
			data[regime + '/' + radius_name + '/mean_temp'] = bdd_data[prefix + '/mean_temp'].value
			data[regime + '/' + radius_name + '/mean_rho'] = bdd_data[prefix + '/mean_rho'].value
			data[regime + '/' + radius_name + '/mean_gas_surf_density'] = bdd_data[prefix + '/mean_gas_surf_density'].value
			data[regime + '/' + radius_name + '/mean_star_surf_density'] = bdd_data[prefix + '/mean_star_surf_density'].value
			data[regime + '/' + radius_name + '/mean_cs'] = bdd_data[prefix + '/mean_cs'].value
			#data[regime + '/' + radius_name + '/mean_nh'] = bdd_data[prefix + '/mean_nh'].value
			data[regime + '/' + radius_name + '/mean_vel'] = bdd_data[prefix + '/mean_vel'].value
			data[regime + '/' + radius_name + '/mean_vel_phi'] = bdd_data[prefix + '/mean_vel_phi'].value

	redshift, place = ot.z(z_path)
	# WITH MERGERS
	if merger == 1:
		# Halo file data
		# starts from z=0 to z=12
		filename_halo = read_path + '/halo_' + str(halo_ID) + '.txt'

		z_1 = []
		z_2 = []
		position = []
		ID = []
		descendant = []
		mhalo = []

		# but we read it in reverse order, now data starts from z=12 to z=0
		file_halo = open(filename_halo, 'r')
		for line in reversed(file_halo.readlines()):
			data_halo = map(float, np.array(line.split()))
			z_1.append(data_halo[0])
			position.append(redshift.tolist().index(data_halo[0]))
			ID.append(int(data_halo[1]))
			mhalo.append(data_halo[3])
			z_2.append(data_halo[4])
			descendant.append([int(ii) for ii in data_halo[5:]])
		file_halo.close()

	# WITHOUT MERGERS
	if merger == 0:

		z_1, ID, mhalo = np.genfromtxt(filename_timeline, delimiter='', usecols=(0,1,3), skip_header=0, unpack=True)
		ID = ID.astype(int)
		z_1 = z_1[::-1]
		ID = ID[::-1]
		mhalo = mhalo[::-1]
		position = [place[redshift.tolist().index(xxx)] for xxx in z_1]
		z_2 = np.insert(z_1[:-1],0,redshift[redshift.tolist().index(z_1[0]) - 1])
		descendant = np.insert(ID[:-1],0,-1)
		descendant = [[xxx] for xxx in descendant]

		z_1 = z_1.tolist()
		ID = ID.tolist()
		mhalo = mhalo.tolist()
		z_2 = z_2.tolist()

	for regime in regime_list:
		for radius,radius_name in zip(radius_list,radius_name_list):

			xx0 = data[regime + '/' + radius_name + '/z_1']
			xx1 = data[regime + '/' + radius_name + '/ID']
			xx2 = data[regime + '/' + radius_name + '/mgas']
			xx3 = data[regime + '/' + radius_name + '/mstar']
			xx4 = data[regime + '/' + radius_name + '/mhalo']
			xx5 = data[regime + '/' + radius_name + '/mbulge']
			xx6 = data[regime + '/' + radius_name + '/mdisk']
			xx7 = data[regime + '/' + radius_name + '/fbulge']
			xx8 = data[regime + '/' + radius_name + '/fdisk']
			xx9 = data[regime + '/' + radius_name + '/inflow']
			xx10 = data[regime + '/' + radius_name + '/outflow']
			xx11 = data[regime + '/' + radius_name + '/bh_vel']
			xx12 = data[regime + '/' + radius_name + '/vel_disp']
			xx13 = data[regime + '/' + radius_name + '/num_new_stars']
			xx14 = data[regime + '/' + radius_name + '/sfr']
			xx15 = data[regime + '/' + radius_name + '/ssfr']
			xx16 = data[regime + '/' + radius_name + '/mean_temp']
			xx17 = data[regime + '/' + radius_name + '/mean_rho']
			xx18 = data[regime + '/' + radius_name + '/mean_gas_surf_density']
			xx19 = data[regime + '/' + radius_name + '/mean_star_surf_density']
			xx20 = data[regime + '/' + radius_name + '/mean_cs']
			#xx20 = data[regime + '/' + radius_name + '/mean_nh']
			xx21 = data[regime + '/' + radius_name + '/mean_vel']
			xx22 = data[regime + '/' + radius_name + '/mean_vel_phi']
			# Filter timeline from halo data
			if radius_name == 'rvir':
				xx23 = data[regime + '/' + radius_name + '/radius']
				xx24 = data[regime + '/' + radius_name + '/rvir_hgm']
				xx25 = data[regime + '/' + radius_name + '/rvir_hsm']
				xx26 = data[regime + '/' + radius_name + '/rvir_hsf']

			Z_ID = zip(xx0, xx1)
			Z_ID_timeline = zip(z_1_timeline, ID_timeline)

			if radius_name != 'rvir':
				zeytin_store = zip(Z_ID, xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, xx15, xx16, xx17, xx18, xx19, xx20, xx21, xx22)
				zeytin = [[a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21,a22,a23] for a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21,a22,a23 in zeytin_store if a0 in Z_ID_timeline]
			else:
				zeytin_store = zip(Z_ID, xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, xx15, xx16, xx17, xx18, xx19, xx20, xx21, xx22, xx23, xx24, xx25, xx26)
				zeytin = [[a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21,a22,a23,a24,a25,a26,a27] for a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,a21,a22,a23,a24,a25,a26,a27 in zeytin_store if a0 in Z_ID_timeline]

			x_z1 = np.array(zip(*zeytin)[0])
			x_ID = np.array(zip(*zeytin)[1])
			x_mgas = np.array(zip(*zeytin)[2])
			x_mstar = np.array(zip(*zeytin)[3])
			x_mhalo = np.array(zip(*zeytin)[4])
			x_mbulge = np.array(zip(*zeytin)[5])
			x_mdisk = np.array(zip(*zeytin)[6])
			x_fbulge = np.array(zip(*zeytin)[7])
			x_fdisk = np.array(zip(*zeytin)[8])
			x_inflow = np.array(zip(*zeytin)[9])
			x_outflow = np.array(zip(*zeytin)[10])
			x_bh_vel = np.array(zip(*zeytin)[11])
			x_vel_disp = np.array(zip(*zeytin)[12])
			x_numns = np.array(zip(*zeytin)[13])
			x_sfr = np.array(zip(*zeytin)[14])
			x_ssfr = np.array(zip(*zeytin)[15])
			x_temp = np.array(zip(*zeytin)[16])
			x_rho = np.array(zip(*zeytin)[17])
			x_gas_sd = np.array(zip(*zeytin)[18])
			x_star_sd = np.array(zip(*zeytin)[19])
			x_cs = np.array(zip(*zeytin)[20])
			#x_nh = np.array(zip(*zeytin)[20])
			x_vel = np.array(zip(*zeytin)[21])
			x_vel_phi = np.array(zip(*zeytin)[22])

			if radius_name == 'rvir':
				x_r = np.array(zip(*zeytin)[23])
				x_rhgm = np.array(zip(*zeytin)[24])
				x_rhsm = np.array(zip(*zeytin)[25])
				x_rhsf = np.array(zip(*zeytin)[26])

			ext = 'halo_' + str(halo_ID) + '/' + regime + '/' + radius_name

			f[ext + '/z_1'] = x_z1
			f[ext + '/ID'] = x_ID
			f[ext + '/mgas'] = x_mgas
			f[ext + '/mstar'] = x_mstar
			f[ext + '/mhalo'] = x_mhalo
			f[ext + '/mbulge'] = x_mbulge
			f[ext + '/mdisk'] = x_mdisk
			f[ext + '/fbulge'] = x_fbulge
			f[ext + '/fdisk'] = x_fdisk
			f[ext + '/inflow'] = x_inflow
			f[ext + '/outflow'] = x_outflow
			f[ext + '/bh_vel'] = x_bh_vel
			f[ext + '/vel_disp'] = x_vel_disp
			f[ext + '/num_new_stars'] = x_numns
			f[ext + '/sfr'] = x_sfr
			f[ext + '/ssfr'] = x_ssfr
			f[ext + '/mean_temp'] = x_temp
			f[ext + '/mean_rho'] = x_rho
			f[ext + '/mean_gas_surf_density'] = x_gas_sd
			f[ext + '/mean_star_surf_density'] = x_star_sd
			f[ext + '/mean_cs'] = x_cs
			#f[ext + '/mean_nh'] = x_nh
			f[ext + '/mean_vel'] = x_vel
			f[ext + '/mean_vel_phi'] = x_vel_phi

			if radius_name == 'rvir':
				f[ext + '/mhalo_rvir'] = mhalo_timeline
				f[ext + '/radius'] = x_r
				f[ext + '/rvir_hgm'] = x_rhgm
				f[ext + '/rvir_hsm'] = x_rhsm
				f[ext + '/rvir_hsf'] = x_rhsf
				
			if merger == 1:
				fdisk = data[regime + '/' + radius_name + '/fdisk']
				mgas = data[regime + '/' + radius_name + '/mgas']
				mstar = data[regime + '/' + radius_name + '/mstar']
				mhalo = data[regime + '/' + radius_name + '/mhalo']
				mean_cs = data[regime + '/' + radius_name + '/mean_cs']
				#mean_nh = data[regime + '/' + radius_name + '/mean_nh']
				mean_vel = data[regime + '/' + radius_name + '/mean_vel']
				mean_vel_phi = data[regime + '/' + radius_name + '/mean_vel_phi']
				mean_rho = data[regime + '/' + radius_name + '/mean_rho']
				sfr = data[regime + '/' + radius_name + '/sfr']
			
			if merger == 0:
				fdisk = x_fdisk
				mgas = x_mgas
				mstar = x_mstar
				mhalo = x_mhalo
				mean_cs = x_cs
				#mean_nh = x_nh
				mean_vel = x_vel
				mean_vel_phi = x_vel_phi
				mean_rho = x_rho
				sfr = x_sfr 

			for mass_retention in mass_retention_list:
				for seed_mass, seed_mass_name in zip(seed_mass_list,seed_mass_name_list):

					mdot_torque, mdot_bondi, mdot_tremmel, mdot_hobbs, mdot_dyn, mdot_sfr, f_torque_edd, f_bondi_edd, f_tremmel_edd, f_hobbs_edd, f_dyn_edd, f_sfr_edd, m_bh_torque, m_bh_bondi, m_bh_tremmel, m_bh_hobbs, m_bh_dyn, m_bh_sfr, mass_seed = ot.MERGER(z_1, ID, z_2, descendant, halo_path, redshift, place, z_1_timeline, ID_timeline, fdisk, mhalo, mstar, mgas, radius, mean_cs, mean_vel, mean_vel_phi, mean_rho, sfr, mass_retention, seed_mass, merger, supp=0, filter_for_timeline=filter_value)

 					mdot_torque_supp, mdot_bondi_supp, mdot_tremmel_supp, mdot_hobbs_supp, mdot_dyn_supp, mdot_sfr_supp, f_torque_edd_supp, f_bondi_edd_supp, f_tremmel_edd_supp, f_hobbs_edd_supp, f_dyn_edd_supp, f_sfr_edd_supp, m_bh_torque_supp, m_bh_bondi_supp, m_bh_tremmel_supp, m_bh_hobbs_supp, m_bh_dyn_supp, m_bh_sfr_supp, mass_seed_supp = ot.MERGER(z_1, ID, z_2, descendant, halo_path, redshift, place, z_1_timeline, ID_timeline, fdisk, mhalo, mstar, mgas, radius, mean_cs, mean_vel, mean_vel_phi, mean_rho, sfr, mass_retention, seed_mass, merger, supp=1, filter_for_timeline=filter_value)

					prefix = 'halo_' + str(halo_ID) + '/' + regime + '/r_' + radius_name + '/mass_ret_' + str(mass_retention) + '/seed_' + seed_mass_name

					f[prefix + '/mdot_torque'] = mdot_torque
					f[prefix + '/mdot_bondi'] = mdot_bondi
					f[prefix + '/mdot_tremmel'] = mdot_tremmel
					f[prefix + '/mdot_hobbs'] = mdot_hobbs
					f[prefix + '/mdot_dyn'] = mdot_dyn
					f[prefix + '/mdot_sfr'] = mdot_sfr
					f[prefix + '/f_torque_edd'] = f_torque_edd
					f[prefix + '/f_bondi_edd'] = f_bondi_edd
					f[prefix + '/f_tremmel_edd'] = f_tremmel_edd
					f[prefix + '/f_hobbs_edd'] = f_hobbs_edd
					f[prefix + '/f_dyn_edd'] = f_dyn_edd
					f[prefix + '/f_sfr_edd'] = f_sfr_edd
					f[prefix + '/m_bh_torque'] = m_bh_torque
					f[prefix + '/m_bh_bondi'] = m_bh_bondi
					f[prefix + '/m_bh_tremmel'] = m_bh_tremmel
					f[prefix + '/m_bh_hobbs'] = m_bh_hobbs
					f[prefix + '/m_bh_dyn'] = m_bh_dyn
					f[prefix + '/m_bh_sfr'] = m_bh_sfr
					f[prefix + '/mass_seed'] = mass_seed
					f[prefix + '/mdot_torque_supp'] = mdot_torque_supp
					f[prefix + '/mdot_bondi_supp'] = mdot_bondi_supp
					f[prefix + '/mdot_tremmel_supp'] = mdot_tremmel_supp
					f[prefix + '/mdot_hobbs_supp'] = mdot_hobbs_supp
					f[prefix + '/mdot_dyn_supp'] = mdot_dyn_supp
					f[prefix + '/mdot_sfr_supp'] = mdot_sfr_supp
					f[prefix + '/f_torque_edd_supp'] = f_torque_edd_supp
					f[prefix + '/f_bondi_edd_supp'] = f_bondi_edd_supp
					f[prefix + '/f_tremmel_edd_supp'] = f_tremmel_edd_supp
					f[prefix + '/f_hobbs_edd_supp'] = f_hobbs_edd_supp
					f[prefix + '/f_dyn_edd_supp'] = f_dyn_edd_supp
					f[prefix + '/f_sfr_edd_supp'] = f_sfr_edd
					f[prefix + '/m_bh_torque_supp'] = m_bh_torque_supp
					f[prefix + '/m_bh_bondi_supp'] = m_bh_bondi_supp
					f[prefix + '/m_bh_tremmel_supp'] = m_bh_tremmel_supp
					f[prefix + '/m_bh_hobbs_supp'] = m_bh_hobbs_supp
					f[prefix + '/m_bh_dyn_supp'] = m_bh_dyn_supp
					f[prefix + '/m_bh_sfr_supp'] = m_bh_sfr_supp
					f[prefix + '/mass_seed_supp'] = mass_seed_supp

	f.close()

#########################
####### MAIN BODY #######
#########################
ot.global_variables()

read_path = str(sys.argv[1])
halo_path = str(sys.argv[2])
save_path = str(sys.argv[3])
sim_suite = str(sys.argv[4])
sim_name = str(sys.argv[5])
merger = int(sys.argv[6])
filter_value = int(sys.argv[7])
filter_flag = int(sys.argv[8])

check_list = ['B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
	
if sim_name in check_list: 
	remove = len(read_path) + 6
	filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

	halo_ID_list = []
	for f in filelist:
		halo_ID_list.append(f[remove:-4])

	halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
else:
	halo_ID_list = [0]

for halo_ID in halo_ID_list:
	run(read_path, halo_path, save_path, sim_suite, sim_name, merger, halo_ID, filter_value, filter_flag)
