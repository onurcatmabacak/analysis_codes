import glob
import h5py
import sys
import os
import pdb
import numpy as np
import multiprocessing
from joblib import Parallel, delayed
from collections import OrderedDict
#sys.path.append('/bulk1/feldmann/Onur')
import onur_tools as ot

def remove_satellites(data_star_sel, iHalo, halo_i, halo_host, halo_c, halo_rvir, halo_center, rvir_old, rvir_new):

	##### remove satellites #####
	if rvir_old >= 20.0:
		rvir_inner = 2.0      # rvir * 0.01
		rvir_outer = rvir_new # %20 rvir

		# filter satellites according to iHalo and min-max radii
		sat_data = [[a,b] for a,b,c in zip(halo_c, halo_rvir, halo_host) if c == iHalo]

		if len(sat_data) != 0:
			sat_center = np.array(zip(*sat_data)[0])
			sat_rvir = np.array(zip(*sat_data)[1]) 

			# satellites that is between r_in and r_out 
			sat_distance = np.sqrt(np.sum(np.square(sat_center-halo_center), axis=1))
			sat_sel = (sat_distance >= rvir_inner) & (sat_distance <= rvir_outer)
			sat_center_sel = sat_center[sat_sel]
			sat_rvir_sel = sat_rvir[sat_sel]
			sat_distance_sel = sat_distance[sat_sel]

			print "sat data length is ", len(sat_rvir_sel)
			inds = []
			for xxx in range(len(sat_center_sel)):
				radius_sat = np.sqrt(np.sum(np.square(data_star_sel['p'] - sat_center_sel[xxx]), axis=1))
				sel_sat = (radius_sat <= sat_rvir_sel[xxx])
				new_sat_dataset = ot.select_particles(data_star_sel, sel_sat)
				inds.append(new_sat_dataset['id'].tolist())

			index = sum(inds, [])
			print "index is: ", len(index)
			if len(index) != 0:
				index = np.array([int(x) for x in index])
				print "first ten elements of index", index[:10]
				print "last ten elements of index", index[-10:]
				print "indexes of particle ids that will be removed"
				inds = np.where(np.isin(data_star_sel['id'], index))[0]
				print "starts removing"
				data_star_sel = ot.exclude_particles(data_star_sel, inds) 
			
		return data_star_sel
		print "finished, returned"
	else:
		print "rvir is smaller than 20 kpc"
		return data_star_sel
		print "rvir is small, finished, returned"

def filter_Rvir(halodir, snap_dir, savepath, i, comoving, iHalo_list, rvir_per, dummy):

	i_new = i + dummy

	directory = savepath
	if not os.path.exists(directory):
		os.makedirs(directory)
		
	iHalo_list_new = []
	for iHalo in iHalo_list:
		data_filename = directory + 'snapshot_' + str('%03d' %i_new) + '_ID_' + str(iHalo) + '.hdf5'
		if os.path.isfile(data_filename):
			print data_filename
			print "does exist"
			continue
		else:
			iHalo_list_new.append(iHalo)

	if iHalo_list_new:
	
		halodir = halodir + str('%03d' %i_new) + '/'
		halodir = glob.glob(halodir + '*AHF_halos')[0]
		
		data_header = []
		snapshot_dir = snap_dir + str('%03d' %i_new)
		if dummy != 178:
			data_header = ot.readsnap(snapshot_dir, snum=i_new, ptype=0, header_only=1)
		else:
			data_header = ot.readsnap_compact(snapshot_dir, snum=i_new, ptype=0, header_only=1)

		time = data_header['time']
		redshift = data_header['redshift']
		hubble = data_header['hubble']

		if comoving==1:
			ascale = time #1.0 / (1.0 + redshift)
			hinv = 1.0 / hubble
			constant = ascale * hinv
		else:
			ascale = 1.0 
			hinv = 1.0 
			constant = ascale * hinv

		data_gas = []
		data_halo = []
		data_star = []

		if dummy != 178:
			data_gas = ot.readsnap(snapshot_dir, snum=i_new, ptype=0, cosmological=comoving)
			data_halo = ot.readsnap(snapshot_dir, snum=i_new, ptype=1, cosmological=comoving)
			data_star = ot.readsnap(snapshot_dir, snum=i_new, ptype=4, cosmological=comoving)
		else:
			data_gas = ot.readsnap_compact(snapshot_dir, snum=i_new, ptype=0, cosmological=comoving)
			data_halo = ot.readsnap_compact(snapshot_dir, snum=i_new, ptype=1, cosmological=comoving)
			data_star = ot.readsnap_compact(snapshot_dir, snum=i_new, ptype=4, cosmological=comoving)

		halo_i, halo_host, xc, yc, zc, halo_rvir = np.genfromtxt(halodir, delimiter='', usecols=(0,1,5,6,7,11), skip_header=1, unpack=True)
		halo_c = np.array(zip(xc,yc,zc)) * constant
		halo_i = halo_i.astype(int)
		halo_host = halo_host.astype(int)
		halo_rvir *= constant

		print data_filename
		for iHalo in iHalo_list_new:
			print "halo id: ", iHalo
			#pdb.set_trace()
			data_filename = directory + 'snapshot_' + str('%03d' %i_new) + '_ID_' + str(iHalo) + '.hdf5'

			radius_gas = []
			radius_halo = []
			radius_star = []

			halo_center = np.array([b1 for a1,b1 in zip(halo_i,halo_c) if a1==iHalo])[0]
			rvir = np.array([b1 for a1,b1 in zip(halo_i,halo_rvir) if a1==iHalo])[0]
			
			rvir_old = rvir
			rvir_new = rvir * rvir_per #rvir_per %20

			#GAS PARTICLES
			radius_gas = np.sqrt(np.sum(np.square(data_gas['p'] - halo_center), axis=1))
			sel_gas = (radius_gas <= rvir_new) 
			data_gas_sel = ot.select_particles(data_gas, sel_gas)

			#HALO PARTICLES
			radius_halo = np.sqrt(np.sum(np.square(data_halo['p'] - halo_center), axis=1))
			sel_halo = (radius_halo <= rvir_new) 
			data_halo_sel = ot.select_particles(data_halo, sel_halo)

			#STAR PARTICLES
			radius_star = np.sqrt(np.sum(np.square(data_star['p'] - halo_center), axis=1))
			sel_star = (radius_star <= rvir_new) 
			data_star_sel = ot.select_particles(data_star, sel_star)

			print "removing satellites..."
			data_star_sel = remove_satellites(data_star_sel, iHalo, halo_i, halo_host, halo_c, halo_rvir, halo_center, rvir_old, rvir_new)
			print "satellites removed... \n"
			data_gas_sel['p'] -= halo_center
			data_star_sel['p'] -= halo_center
			data_halo_sel['p'] -= halo_center
			center = [list(halo_center - halo_center)]

			npart_gas = len(data_gas_sel['m'])
			npart_halo = len(data_halo_sel['m'])
			npart_disk = 0
			npart_bulge = 0
			npart_star = len(data_star_sel['m'])
			npart_bndry = 1

			f = h5py.File(data_filename,'w')
			f.create_group("Header")

			if dummy == 178:
				f["Header"].attrs["CompactLevel"] = data_header["compactlevel"]

			f["Header"].attrs["NumPart_ThisFile"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
			f["Header"].attrs["NumPart_Total"] = [npart_gas, npart_halo, npart_disk, npart_bulge, npart_star, npart_bndry]
			f["Header"].attrs["NumPart_Total_HighWord"] = data_header["npartTotal_high"]
			f["Header"].attrs["MassTable"] = data_header["massarr"]
			f["Header"].attrs["Time"] = data_header["time"]
			f["Header"].attrs["Redshift"] = data_header["redshift"]
			f["Header"].attrs["BoxSize"] = data_header["boxsize"]
			f["Header"].attrs["NumFilesPerSnapshot"] = 1
			f["Header"].attrs["Omega0"] = data_header["omega_matter"]
			f["Header"].attrs["OmegaLambda"] = data_header["omega_lambda"]
			f["Header"].attrs["HubbleParam"] = data_header["hubble"]
			f["Header"].attrs["Flag_Sfr"] = data_header["flag_sfr"]
			f["Header"].attrs["Flag_Cooling"] = data_header["flag_cooling"]
			f["Header"].attrs["Flag_StellarAge"] = data_header["flag_stellarage"]
			f["Header"].attrs["Flag_Metals"] = data_header["flag_metals"]
			f["Header"].attrs["Flag_Feedback"] = data_header["flag_feedbacktp"]
			f["Header"].attrs["Flag_DoublePrecision"] = data_header["flag_doubleprecision"]
			f["Header"].attrs["Flag_IC_Info"] = data_header["flag_icinfo"]
			f.create_dataset('PartType0/Coordinates', data=data_gas_sel['p'], compression='gzip')
			f.create_dataset('PartType0/Velocities', data=data_gas_sel['v'], compression='gzip')
			f.create_dataset('PartType0/Density', data=data_gas_sel['rho'], compression='gzip')
			f.create_dataset('PartType0/Masses', data=data_gas_sel['m'], compression='gzip')
			f.create_dataset('PartType0/ElectronAbundance', data=data_gas_sel['ne'], compression='gzip')
			f.create_dataset('PartType0/InternalEnergy', data=data_gas_sel['u'], compression='gzip')
			f.create_dataset('PartType0/Metallicity', data=data_gas_sel['z'], compression='gzip')
			f.create_dataset('PartType0/NeutralHydrogenAbundance', data=data_gas_sel['nh'], compression='gzip')
			f.create_dataset('PartType0/StarFormationRate', data=data_gas_sel['sfr'], compression='gzip')
			f.create_dataset('PartType1/Coordinates', data=data_halo_sel['p'], compression='gzip')
			f.create_dataset('PartType1/Velocities', data=data_halo_sel['v'], compression='gzip')
			f.create_dataset('PartType1/Masses', data=data_halo_sel['m'], compression='gzip')
			f.create_dataset('PartType4/Coordinates', data=data_star_sel['p'], compression='gzip')
			f.create_dataset('PartType4/Velocities', data=data_star_sel['v'], compression='gzip')
			f.create_dataset('PartType4/Masses', data=data_star_sel['m'], compression='gzip')
			f.create_dataset('PartType4/Metallicity', data=data_star_sel['z'], compression='gzip')
			f.create_dataset('PartType4/StellarFormationTime', data=data_star_sel['age'], compression='gzip')
			f.create_dataset("PartType5/Coordinates", data=center, compression='gzip') 
			f.close()

def tree(halo_path, output_path, snapshot_path, input_path, dummy, halo_ID, start, finish, ncpu, rvir_per):

	list_order = [halo_ID]
	redshift_list = [] 
	iHalo_list = [] 

	fname = input_path + '/halo_' + str(halo_ID) + '.txt'
	redshift_list, iHalo_list = np.genfromtxt(fname, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	iHalo_list = iHalo_list.astype(int)
	redshift = np.round(list(OrderedDict.fromkeys(redshift_list)), 3)
	z, position = ot.z(input_path)

	i = []
	iHalo_new = []
	
	for j in range(len(redshift)):	
		pos = z.tolist().index(redshift[j])
		i.append(pos)
		sel = (redshift_list == redshift[j])
		iHalo_new.append(iHalo_list[sel])

	new_array = zip(i, iHalo_new)

	comoving = 1
	halodir = halo_path + '/'
	snap_dir = snapshot_path + '/snapdir_'
	savepath = output_path + '/'

	new_input = new_array[start:finish]
	Parallel(n_jobs=ncpu)(delayed(filter_Rvir)(halodir, snap_dir, savepath, i, comoving, iHalo_list, rvir_per, dummy) for i,iHalo_list in new_input)

##########################################################
###################### MAIN BODY ########################
##########################################################

halo_path = str(sys.argv[1])
snapshot_path = str(sys.argv[2]) 
input_path = str(sys.argv[3]) 
output_path = str(sys.argv[4])
dummy = int(sys.argv[5]) 
start = int(sys.argv[6])
finish = int(sys.argv[7])
ncpu = int(sys.argv[8])

halo_ID = 'super'
rvir_per = 0.2

tree(halo_path, output_path, snapshot_path, input_path, dummy, halo_ID, start, finish, ncpu, rvir_per)
