import numpy as np
import onur_tools as ot

comoving = 1 
i_new = 178
snapshot_dir = '../data/MassiveFIRE2/analysis/binary_files/B762_N1024_z6_TL00000_baryon_toz6_HR/snapdir_' + str('%03d' %i_new)
data = ot.readsnap_compact(snapshot_dir, snum=i_new, ptype=0, cosmological=comoving)
print data["z"]

i_new = 192
snapshot_dir = '../data/MassiveFIRE2/analysis/binary_files/B762_N1024_z6_TL00000_baryon_toz6_HR/snapdir_' + str('%03d' %i_new)
data = ot.readsnap_compact(snapshot_dir, snum=i_new, ptype=0, cosmological=comoving)
print data["z"]

i_new = 320
snapshot_dir = '../data/MassiveFIRE2/analysis/binary_files/B762_N1024_z6_TL00000_baryon_toz6_HR/snapdir_' + str('%03d' %i_new)
data = ot.readsnap_compact(snapshot_dir, snum=i_new, ptype=0, cosmological=comoving)
print data["z"]
