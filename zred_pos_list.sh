#!/bin/bash

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

SIM_NAME=(B100_N512_M3e13_TL00001_baryon_toz2_HR_9915 B100_N512_M3e13_TL00003_baryon_toz2_HR_9915 B100_N512_M3e13_TL00004_baryon_toz2_HR_9915 B100_N512_TL00002_baryon_toz2 B100_N512_TL00007_baryon_toz2_HR B100_N512_TL00009_baryon_toz2_HR B100_N512_TL00011_baryon_toz2_HR B100_N512_TL00013_baryon_toz2_HR B100_N512_TL00018_baryon_toz2_HR B100_N512_TL00029_baryon_toz2_HR B100_N512_TL00031_baryon_toz2_HR B100_N512_TL00037_baryon_toz0_HR_9915 B100_N512_TL00113_baryon_toz0_HR_9915 B100_N512_TL00206_baryon_toz0_HR_9915 B100_N512_TL00217_baryon_toz2 B100_N512_TL00223_baryon_toz2_HR B100_N512_TL00228_baryon_toz2 B100_N512_TL00236_baryon_toz2 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915 B400_N512_z6_TL00000_baryon_toz6_HR_9915 B400_N512_z6_TL00001_baryon_toz6_HR_9915 B400_N512_z6_TL00002_baryon_toz6_HR_9915 B400_N512_z6_TL00005_baryon_toz6_HR_9915 B400_N512_z6_TL00006_baryon_toz6_HR_9915 B400_N512_z6_TL00013_baryon_toz6_HR_9915 B400_N512_z6_TL00017_baryon_toz6_HR_9915 B400_N512_z6_TL00021_baryon_toz6_HR_9915 B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR h113_HR_sn1dy300ro100ss h206_HR_sn1dy300ro100ss h29_HR_sn1dy300ro100ss h2_HR_sn1dy300ro100ss)
SIM_SUITE=(MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2)
START=(16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 178 178 178 11 11 11 11)
FINISH=(191 191 191 191 191 191 191 191 191 191 191 191 388 441 191 191 191 191 51 51 51 51 51 51 51 51 51 51 51 51 832 832 832 277 277 277 277)
FILEDIR=Onur/MDC #Onur/COM
LIST=(10)

for i in ${LIST[@]}
do
	create_dir /bulk1/feldmann/$FILEDIR/${SIM_SUITE[$i]}
	create_dir /bulk1/feldmann/$FILEDIR/${SIM_SUITE[$i]}/${SIM_NAME[$i]}

	file_path=/bulk1/feldmann/$FILEDIR/${SIM_SUITE[$i]}/${SIM_NAME[$i]}
	write_path=$file_path/zred_pos_list
	tmp_path=$file_path/tmp.txt
	echo ${SIM_NAME[$i]}
	rm $write_path

	for ((j=${START[$i]}; j<=${FINISH[$i]}; j++)) 
	do
		SNAPSHOT=`printf "%03d" $j`
		array=/bulk1/feldmann/data/${SIM_SUITE[$i]}/production_runs/HR/${SIM_NAME[$i]}/snapdir_$SNAPSHOT/snapshot_$SNAPSHOT.0.hdf5

		h5dump -a "Header/Redshift" $array > $tmp_path
		name=$(sed -n 6p $tmp_path)
		redshift=`printf "%.3f" ${name:8}`

		dummy=`printf $array | cut -d "/" -f 9`
		echo $redshift   $SNAPSHOT >> $write_path

		rm $tmp_path
	done
done
