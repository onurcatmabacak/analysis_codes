#!/bin/bash -l 
#SBATCH --job-name="onur" 
#SBATCH --time=24:0:0 --mem=64G 
#SBATCH --ntasks=2 --cpus-per-task=16 
#SBATCH --output=%j.o 
#SBATCH --error=%j.e 
#======START===== 
/home/cluster/ocatma/rockstar/rockstar -c /bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/FB15N512/halo/100/rockstar.cfg >& /bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/FB15N512/halo/100/server.dat & 
sleep 1 
/home/cluster/ocatma/rockstar/rockstar -c /bulk1/feldmann/data/FIREbox/analysis/ROCKSTAR/FB15N512/halo/100/auto-rockstar.cfg 
 #=====END==== 
