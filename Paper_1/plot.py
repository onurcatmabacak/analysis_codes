import os
import pdb
import numpy as np
import h5py
import sys
from scipy.optimize import curve_fit
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
from astropy import units as u
from astropy.cosmology import Planck15, z_at_value
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy import integrate
from scipy.special import lambertw
from scipy import stats
import operator
from astropy.cosmology import FlatLambdaCDM

def time(i):
	return cosmo.age(i).value * 1e9 # yr

def weighted_average(data, weight, step):
	radius = np.linspace(np.min(weight), np.max(weight), step+1)
	weighted_average = []
	radius_mean = []

	for i in range(len(radius) - 1):
		total_data = np.array([a for a,b in zip(data, weight) if b >= radius[i] if b <= radius[i+1]])
		total_weight = np.array([b for b in weight if b >= radius[i] if b <= radius[i+1]]) 
		weighted_average.append(np.sum(total_data * total_weight) / np.sum(total_weight))
		radius_mean.append((radius[i]+radius[i+1])/2.0)
	return weighted_average, radius_mean

def Muratov15(mstar,a,b):
	return np.log10(a) + b * np.log10(mstar*1e-10)
	#return a * (Mstar**b) # Mstar is normalized with 1e10 M_solar

def HH15(mstar,a,b,c):
	return np.log10(a) + b * np.log10(mstar*1e-10) + np.log10(np.exp(c*mstar*1e-10))

def HH15_eq44(mstar,mgas,a,b,c):
	return np.log10(a) + b* np.log10(mgas*mstar/(mgas+mstar)*1e-10) + np.log10(np.exp(c/mgas*(mgas+mstar)))
	#return np.log10(a) +b* np.log10((mgas*mstar/(mgas+mstar)*1e-10))  +np.log10( np.exp(c/mgas*(mgas+mstar)))

def Ferrerase02(Mass):	
	#return 10.**( 8.0 ) * 0.027 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	return 10.**( 8.0 ) * 0.100 * ( Mass/1e12 )**(1.65) # Ferrerase 2002
	#return 10.**( 8.0 ) * 0.670 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	#return 10.**( 8.18 ) * ( Mass/1e13 )**(1.55) # Bandara et al 2009
	#return 10.**( 7.85 ) * ( Mass/1e12 )**(1.33) # Di Matteo et al 2003

def bin_average(x,y, nbins):
	
	x = np.log10(x[(y != 0.0)])
	y = np.log10(y[(y != 0.0)])
	bins = np.linspace(np.min(x), np.max(x), nbins+1)

	xresult = []
	yresult = []
	for i in range(len(bins)-1):
		sel = (x <= bins[i+1] ) & (x >= bins[i])	
		xdata = np.array(x[sel])
		ydata = np.array(y[sel])
		yresult.append(np.sum(xdata * ydata) / np.sum(xdata))
		xresult.append(np.sum(xdata) / len(xdata))

	xx = 10.0 ** (np.array(xresult))
	yy = 10.0 ** (np.array(yresult))

	return xx, yy

def Haring04(Mbulge):

   return 10.**( 8.20 ) * ( Mbulge/1e11 )**(1.12)


def Kormendy2013(Mbulge):

   return 0.49 * 1e9 * ( Mbulge/1e11 )**(1.16)


def McConnell2013(Mbulge):

   return 10.**( 8.46 ) * ( Mbulge/1e11 )**(1.05)

def Reines2015(Mstar):

   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

def Savorgnan2016( Mstar, sample='all'):
	Mbh_early = 10.**( 8.56 + 1.04*np.log10(Mstar/10.**10.81) )
	Mbh_late = 10.**( 7.24 + 2.28*np.log10(Mstar/10.**10.05) )

	ind = np.where( Mbh_late - Mbh_early >= 0 )[0] 
	Mbh_all =  np.append( Mbh_late[:ind[0]], Mbh_early[ind[0]:] )

	if Mbh_all.size != Mstar.size:
	 print 'ahhhh.... check Savorgnan2016 !!!'

	if sample == 'early':
	 return Mbh_early
	elif sample == 'late':
	 return Mbh_late
	elif sample == 'all':
	 return Mbh_all

def McConnell_Ma2013(vel_disp):
	return 10.**( 8.32 ) * ( vel_disp/200.0 )**(5.64)

def mbh_vs_mstar_mbulge():
	for regime in regime_list:

		fig, axes = plt.subplots(5,8, figsize=(10,20), squeeze=False)
		fig.subplots_adjust(hspace=0.0, wspace=0.0)
		#fig.suptitle(r'$M_{BH} \; vs \; M_{*} \;$' + regime)
		ax = axes.ravel()

		mass_list = np.array([1e5, 1e6, 1e7, 1e8, 1e9, 1e10, 1e11, 1e12, 1e13, 1e14])
		label = ['B100_TL1', 'B100_TL3', 'B100_TL4', 'B100_TL2', 'B100_TL7', 'B100_TL9', 'B100_TL11', 'B100_TL13', 'B100_TL18', 'B100_TL29', 'B100_TL31', 'B100_TL37', 'B100_TL113', 'B100_TL206', 'B100_TL217', 'B100_TL223', 'B100_TL228', 'B100_TL236', 'B400_TL10', 'B400_TL0', 'B400_TL1', 'B400_TL2', 'B400_TL5', 'B400_TL6', 'B400_TL13', 'B400_TL17', 'B400_TL21', 'B762_TL0_MF1', 'B762_TL1_MF1', 'B762_TL2_MF1', 'B762_TL0_MF2', 'B762_TL1_MF2', 'B762_TL2_MF2', 'h113', 'h206', 'h29', 'h2']

		j = 0

		for i in i_list:
			
			if sim_suite[i] == 'MassiveFIRE':
				radius = radius_name_list[3] #MassiveFIRE
			else:
				radius = radius_name_list[0] #MassiveFIRE2

			mass_retention = mass_retention_list[3] # every plot is for 0.1 or 1 kpc and %50 of mass retention rate
			seed_mass = seed_mass_list[3] # seed mass is 1e4	

			redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
			mbh = data[sim_suite[i] + '/' + sim_name[i]][regime + '/r_' + radius + '/mass_ret_' + mass_retention + '/seed_' + seed_mass + '/m_bh_torque'].value
			mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mstar'].value
			mbulge = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mbulge'].value

			z_list = [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0]
			redshift_list = []
			mbh_z = []
			mstar_z = []
			mbulge_z = []
			
			for z in z_list:
			
				if z in redshift:
					sel = (redshift == z)

					redshift_list.append(redshift[sel])
					mbh_z.append(mbh[sel])
					mstar_z.append(mstar[sel])
					mbulge_z.append(mbulge[sel])
			
			redshift_list = np.concatenate(redshift_list)
			mbh_z = np.concatenate(mbh_z)
			mstar_z = np.concatenate(mstar_z)
			mbulge_z = np.concatenate(mbulge_z)

			ax[j].plot(mstar, mbh, color='r', ls='-', lw=2, label=r'$M_{*}$')
			ax[j].plot(mbulge, mbh, color='b', ls='-', lw=2, label=r'$M_{bulge}$')
			ax[j].plot(mass_list, Reines2015(mass_list), color='r', ls='--', lw=1, label='Reines+15')
			ax[j].plot(mass_list, Haring04(mass_list), color='b', ls='--', lw=1, label='Haring+04')
			#ax[j].scatter(mstar_z, mbh_z, color='k', marker='*', s=80)
			ax[j].scatter(mbulge_z, mbh_z, color='k', marker='o', s=80)

			# loop through each x,y pair
			for x,y,z in zip(redshift_list, mbulge_z, mbh_z):
				corr = 0.875 # adds a little correction to put annotation in marker's centrum
				ax[j].annotate(str(int(x)),  xy=(y * corr, z * corr), color='white', size=6, alpha=1.0)

			ax[j].set_xlabel(r'$Mass\;(M_{\odot})$')

			if (j == 0) or (j == 5):
				ax[j].set_ylabel(r'$M_{BH}\;(M_{\odot})$')

			ax[j].set_xscale('log')
			ax[j].set_yscale('log')
			ax[j].set_xlim(0.5*np.min(mstar[np.nonzero(mstar)]), 2.0*np.max(mstar))
			ax[j].set_ylim(0.5*np.min(mbh[np.nonzero(mbh)]), 2.0*np.max(mbh))
			ax[j].tick_params(axis='x', labelsize=8)
			ax[j].tick_params(axis='y', labelsize=8)
			ax[j].xaxis.set_major_locator(plt.FixedLocator([1e7, 1e8, 1e9, 1e10, 1e11, 1e12]))
			ax[j].fill_between(mass_list, Reines2015(mass_list) * np.sqrt(1e1), Reines2015(mass_list) / np.sqrt(1e1), color='red', zorder=0, alpha=0.1 )
			ax[j].fill_between(mass_list, Haring04(mass_list) * np.sqrt(1e1), Haring04(mass_list) / np.sqrt(1e1), color='blue', zorder=0, alpha=0.1 )
			ax[j].text(np.max(mstar),0.5*np.min(mbh),label[j], verticalalignment='bottom', horizontalalignment='right', fontsize=10)
			ax[j].legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

			j += 1

		#fig.delaxes(ax.flatten()[-1])	
		plt.tight_layout(pad=1.5)
		plt.savefig('mbh_vs_mstar_mbulge_' + regime + '.png', dpi=300, format='png')
		plt.close()

def z_vs_sfr_mgin():
	fig, axes = plt.subplots(5,8, figsize=(22,18))
	fig.subplots_adjust(hspace=0.0, wspace=0.0)
	#fig.suptitle(r'$M_{BH} \; vs \; M_{*} \;$' + regime)
	ax = axes.ravel()

	label = ['B100_TL1', 'B100_TL3', 'B100_TL4', 'B100_TL2', 'B100_TL7', 'B100_TL9', 'B100_TL11', 'B100_TL13', 'B100_TL18', 'B100_TL29', 'B100_TL31', 'B100_TL37', 'B100_TL113', 'B100_TL206', 'B100_TL217', 'B100_TL223', 'B100_TL228', 'B100_TL236', 'B400_TL10', 'B400_TL0', 'B400_TL1', 'B400_TL2', 'B400_TL5', 'B400_TL6', 'B400_TL13', 'B400_TL17', 'B400_TL21', 'B762_TL0_MF1', 'B762_TL1_MF1', 'B762_TL2_MF1', 'B762_TL0_MF2', 'B762_TL1_MF2', 'B762_TL2_MF2', 'h113', 'h206', 'h29', 'h2']

	j = 0

	for i in i_list:
			
		radius = radius_name_list[-1] 
		inflow = {}
		mgas = {}

		for regime in regime_list:	
			redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius + '/z_1'].value
			sfr = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius + '/sfr'].value
			inflow[regime] = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius + '/inflow'].value
			mgas[regime] = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius + '/mgas'].value

		ax[j].plot(redshift, sfr, color='g', ls='-', lw=2, label=r'$SFR$')
		ax[j].plot(redshift, inflow['cold'], color='b', ls='-', lw=1, label=r'$M_{in}\;cold$')
		ax[j].plot(redshift, inflow['total'], color='r', ls='-', lw=1, label=r'$M_{in}\;total$')
		ax[j].plot(redshift, mgas['cold']*1e-9, color='k', ls='--', lw=2, label=r'$M_{g}/10^9\;M_{\odot}\;cold$')
		ax[j].plot(redshift, mgas['total']*1e-9, color='k', ls='-', lw=2, label=r'$M_{g}/10^9\;M_{\odot}\;total$')
		ax[j].plot(redshift, np.ones(len(redshift))*0.1, color='k', ls='--', lw=0.5)
		ax[j].plot(redshift, np.ones(len(redshift))*1.0, color='k', ls='--', lw=0.5)
		ax[j].plot(redshift, np.ones(len(redshift))*10.0, color='k', ls='--', lw=0.5)

		if j == 4:
			ax[j].set_ylabel(r'$M_{\odot}/yr$')

		if (j == 8) or (j == 9):
			ax[j].set_xlabel(r'$z$')

		ax[j].set_xscale('linear')
		ax[j].set_yscale('log')
		ax[j].set_xlim(np.min(redshift), np.max(redshift)+1.0)
		ax[j].set_ylim(0.1, 2.0*np.max(inflow['total']))
		ax[j].tick_params(axis='x', labelsize=10)
		ax[j].tick_params(axis='y', labelsize=10)
		#ax[j].xaxis.set_major_locator(plt.FixedLocator([1e7, 1e8, 1e9, 1e10, 1e11, 1e12]))
		ax[j].text(np.min(redshift),1.8*np.max(inflow['total']), label[j], verticalalignment='top', horizontalalignment='left', fontsize=10)
		ax[j].legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

		j += 1
	#fig.delaxes(ax.flatten()[-1])	
	plt.tight_layout(pad=1.5)
	plt.savefig('z_vs_sfr_mgin.png', dpi=300, format='png')
	plt.close()

def plot_1():

	for mass_retention in mass_retention_list:
		for seed_mass in seed_mass_list:
			for regime in regime_list:

				fig = plt.figure(1, figsize=(6,4))
				fig.subplots_adjust(wspace=0, hspace=0)
				ax1 = fig.add_subplot(131)
				ax2 = fig.add_subplot(132)
				ax3 = fig.add_subplot(133)

				for i in i_list:
					
					if sim_suite[i] == 'MassiveFIRE':
						radius = radius_name_list[3] #MassiveFIRE
						shape = 's'
					else:
						radius = radius_name_list[0] #MassiveFIRE2
						shape = '^'	

					#mass_retention = mass_retention_list[3] # every plot is for 0.1 or 1 kpc and %50 of mass retention rate
					#seed_mass = seed_mass_list[3] # seed mass is 1e4	

					redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
					mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mstar'].value
					mbulge = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mbulge'].value
					mbh = data[sim_suite[i] + '/' + sim_name[i]][regime + '/r_' + radius + '/mass_ret_' + mass_retention + '/seed_' + seed_mass + '/m_bh_torque'].value
					vel_disp = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/vel_disp'].value

					#########################################
					### FILTERING FOR DIFFERENT REDSHIFTS ###
					#########################################

					z_list = [6.0, 4.0, 2.0, 1.0, 0.0]
					color = ['magenta','green','blue','red','black']

					#z_list = [0.0, 0.3, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
					#color = ['black','red','blue','green','magenta','yellow','olivedrab','darkcyan','deepskyblue','coral']
					z_data = {}

					if i == 0:
						for z in z_list:
							inds = z_list.index(z)
							ax1.scatter(0.0, 0.0, color=color[inds], marker='.', s=20, label='z='+str(z))
						#ax1.legend(loc=0, prop={'size':10},frameon=False,scatterpoints=1)

					for z in z_list:
						sel = (redshift == z)
						inds = z_list.index(z)

						z_data[str(z) + '/mbh'] = mbh[sel]
						z_data[str(z) + '/mstar'] = mstar[sel]
						z_data[str(z) + '/mbulge'] = mbulge[sel]
						z_data[str(z) + '/vel_disp'] = vel_disp[sel]
						
						ax1.scatter(z_data[str(z) + '/mstar'], z_data[str(z) + '/mbh'], color=color[inds], marker=shape, s=30, alpha=0.3)
						ax2.scatter(z_data[str(z) + '/mbulge'], z_data[str(z) + '/mbh'], color=color[inds], marker=shape, s=30, alpha=0.3)
						ax3.scatter(z_data[str(z) + '/vel_disp'], z_data[str(z) + '/mbh'], color=color[inds], marker=shape, s=30, alpha=0.3)

				ax1.plot(mass_list, Reines2015_data, color='k', ls='--', lw=1, label='Reines15')
				ax2.plot(mass_list, Haring04_data, color='k', ls='--', lw=1, label='Haring04')
				ax3.plot(vel_list, McConnell_Ma2013_data, color='k', ls='--', lw=1, label='McConnell13')

				ax1.fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )
				ax2.fill_between(mass_list, Haring04_data * np.sqrt(1e1), Haring04_data / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )
				ax3.fill_between(vel_list, McConnell_Ma2013_data * np.sqrt(1e1), McConnell_Ma2013_data / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )

				ax1.set_xscale('log')
				ax1.set_yscale('log')
				ax2.set_xscale('log')
				ax2.set_yscale('log')
				ax3.set_xscale('log')
				ax3.set_yscale('log')

				ax1.set_xlim(1e7,3e12)
				ax1.set_ylim(0.7e4,1e10)
				ax2.set_xlim(1e7,3e12)
				ax2.set_ylim(0.7e4,1e10)
				ax3.set_xlim(1e1,1e3)
				ax3.set_ylim(0.7e4,1e10)

				ax1.xaxis.set_major_locator(plt.FixedLocator([1e7,1e8,1e9,1e10,1e11,1e12]))
				ax1.yaxis.set_major_locator(plt.FixedLocator([1e5,1e6,1e7,1e8,1e9,1e10]))
				ax2.xaxis.set_major_locator(plt.FixedLocator([1e8,1e9,1e10,1e11,1e12]))
				ax2.yaxis.set_major_locator(plt.FixedLocator([1e5,1e6,1e7,1e8,1e9,1e10]))
				ax3.xaxis.set_major_locator(plt.FixedLocator([1e2,1e3]))
				ax3.yaxis.set_major_locator(plt.FixedLocator([1e5,1e6,1e7,1e8,1e9,1e10]))

				ax1.set_xlabel(r'$M_{*}\;(M_{\odot})$')
				ax1.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
				ax2.set_xlabel(r'$M_{bulge}\;(M_{\odot})$')
				#ax2.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
				ax3.set_xlabel(r'$\sigma\;(km/s)$')
				#ax3.set_ylabel(r'$M_{BH}\;(M_{\odot})$')

				#ax1.text(1e12, 5e4, r'$M_{star}$', verticalalignment='top', horizontalalignment='left')
				#ax2.text(1e12, 5e4, r'$M_{bulge}$', verticalalignment='top', horizontalalignment='left')

				ax1.legend(loc=2, prop={'size':8},frameon=False,scatterpoints=1)
				ax2.legend(loc=2, prop={'size':8},frameon=False,scatterpoints=1)	
				ax3.legend(loc=2, prop={'size':8},frameon=False,scatterpoints=1)

				ax2.get_yaxis().set_ticks([])
				ax3.get_yaxis().set_ticks([])

				#plt.tight_layout()
				plt.savefig('plot_#1_scaling_relations_' + ahf_centering + '_' + regime + '_seed_' + seed_mass + '_mass_ret_' + mass_retention + '_' + merger + '.png', dpi=300, format='png')
				plt.close()

def plot_5():

	for i in [3,9,12,13,33,34,35,36]:

		for regime in ['cold']:

			if sim_suite[i] == 'MassiveFIRE':
				radius = radius_name_list[3] #MassiveFIRE
			else:
				radius = radius_name_list[0] #MassiveFIRE2

			mass_ret = mass_retention_list[3]
			seed_mass = seed_mass_list[3]
		
			bh_mass = data[sim_suite[i] + '/' + sim_name[i]][regime + '/r_' + radius + '/mass_ret_' + mass_ret + '/seed_' + seed_mass + '/m_bh_torque'].value
			mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mstar'].value

			redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
			sfr_gal = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/sfr'].value
			sfr_inner = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[0] + '/sfr'].value
			t = np.array([time(xxx) for xxx in redshift])

			step = 1e8

			t_list = np.arange(np.min(t), np.max(t), step)
			t_list = np.append(t_list, np.max(t))

			t1 = []
			sfr_gal1 = []
			sfr_inner1 = []	

			for xxx in range(len(t_list)-1):

				sel = (t >= t_list[xxx]) & (t <= t_list[xxx+1])
				t_new = t[sel]
				sfr_gal_new = sfr_gal[sel]
				sfr_inner_new = sfr_inner[sel]
				
				t1.append(np.sum(t_new)/len(t_new))
				sfr_gal1.append(np.sum(sfr_gal_new)/len(sfr_gal_new))
				sfr_inner1.append(np.sum(sfr_inner_new)/len(sfr_inner_new))

			z1 = np.array([z_at_value(Planck15.age, xxx * 1e-9 * u.Gyr) for xxx in t1])
			mstar_gal = integrate.cumtrapz(sfr_gal1, t1, initial=1e7)
			mstar_inner = integrate.cumtrapz(sfr_inner1, t1, initial=1e7)
			#mstar_gal[0] = mstar_gal[1]
			#mstar_inner[0] = mstar_inner[1]
			mdot = mstar_inner * 1e-10
			mbh = integrate.cumtrapz(mdot, t1, initial=1e4)
			mbh_lower = mbh * 1e-1 
			mbh_upper = mbh * 1e1
			#mbh[0] = mbh[1]

			#for xxx in range(len(t1)):
			#	print '%-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e \n' %(t1[xxx], sfr_gal1[xxx], sfr_inner1[xxx], mstar_gal[xxx], mstar_inner[xxx], mbh[xxx])

			z_ticks = np.arange(int(np.min(redshift)), int(np.max(redshift))+1)

			fig = plt.figure(1, figsize=(6,9))
			fig.subplots_adjust(wspace=0.0, hspace=0.3)
			fig.suptitle(sim_name[i])

			ax0 = fig.add_subplot(411)	
			ax0.set_xscale('linear')
			ax0.set_yscale('log')
			ax0.set_xlim(0.9*np.min(z1),1.1*np.max(z1))
			#ax0.set_xlim(0.9*np.min(t),1.1*np.max(t))
			ax0.set_ylim(1e-2,1e3)
			ax0.plot(redshift,sfr_gal,'r-',lw=2,label=r'$GAL$')
			ax0.plot(redshift,sfr_inner,'k-',lw=2,label=r'$INNER$')
			#ax0.plot(t,sfr_gal,'r-',lw=2,label=r'$GAL$')
			#ax0.plot(t,sfr_inner,'k-',lw=2,label=r'$INNER$')
			ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
			ax0.set_xlabel(r'$z$')
			#ax0.set_xlabel(r'$time\;(Gyr)$')
			ax0.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
			ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

			ax1 = fig.add_subplot(412)	
			ax1.set_xscale('linear')
			ax1.set_yscale('log')
			ax1.set_xlim(0.9*np.min(z1),1.1*np.max(z1))
			#ax1.set_xlim(0.9*np.min(t),1.1*np.max(t))
			ax1.set_ylim(1e-2,1e3)
			ax1.plot(z1,sfr_gal1,'r-',lw=2,label=r'$GAL$')
			ax1.plot(z1,sfr_inner1,'k-',lw=2,label=r'$INNER$')
			#ax1.plot(t1,sfr_gal1,'r-',lw=2,label=r'$GAL$')
			#ax1.plot(t1,sfr_inner1,'k-',lw=2,label=r'$INNER$')
			ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
			ax1.set_xlabel(r'$z$')
			#ax1.set_xlabel(r'$time\;(Gyr)$')
			ax1.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
			ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

			ax2 = fig.add_subplot(413)	
			ax2.set_xscale('linear')
			ax2.set_yscale('log')
			ax2.set_xlim(0.9*np.min(z1),1.1*np.max(z1))
			#ax2.set_xlim(0.9*np.min(t),1.1*np.max(t))
			ax2.set_ylim(1e6,1e12)
			ax2.plot(z1,mstar_gal,'r-',lw=2,label=r'$GAL$')
			ax2.plot(z1,mstar_inner,'k-',lw=2,label=r'$INNER$')
			#ax2.plot(t1,mstar_gal,'r-',lw=2,label=r'$GAL$')
			#ax2.plot(t1,mstar_inner,'k-',lw=2,label=r'$INNER$')
			ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
			ax2.set_xlabel(r'$z$')
			#ax2.set_xlabel(r'$time\;(Gyr)$')
			ax2.set_ylabel(r'$M_{*}\;(M_{\odot})$')
			ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

			ax3 = fig.add_subplot(414)	
			ax3.set_xscale('log')
			ax3.set_yscale('log')
			ax3.set_xlim(1e7,1e12)
			ax3.set_ylim(1e3,1e11)
			ax3.plot(mstar, bh_mass, color='k', lw=2, label=r'$REAL$')
			ax3.plot(mstar_gal, mbh, color='r', ls='-', lw=2, label=r'$M_{BH}$')
			ax3.plot(mstar_gal, mbh_lower, color='g', ls='-', lw=2, label=r'$0.1x\;M_{BH}$')
			ax3.plot(mstar_gal, mbh_upper, color='b', ls='-', lw=2, label=r'$10x\;M_{BH}$')
			#ax3.plot(mstar_inner, mbh, color='r', ls='-', lw=2, label=r'$INNER$')
			ax3.plot(mass_list, Reines2015_data, color='k', ls='--', lw=1, label=r'$Reines+15$')
			ax3.fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
			ax3.set_xlabel(r'$M_{*}\;(M_{\odot})$')
			ax3.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
			ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

			#plt.tight_layout()
			plt.savefig('plot_#5_kinematic_' + sim_name[i] + '_redshift.png', dpi=300, format='png')
			#plt.show()
			plt.close()

def plot_7():
	
	for i in [3,9,12,13,33,34,35,36]:

		for regime in ['cold']:

			if sim_suite[i] == 'MassiveFIRE':
				radius = radius_name_list[3] #MassiveFIRE
			else:
				radius = radius_name_list[0] #MassiveFIRE2

			mass_ret = mass_retention_list[3]
			seed_mass = seed_mass_list[3]
		
			bh_mass = data[sim_suite[i] + '/' + sim_name[i]][regime + '/r_' + radius + '/mass_ret_' + mass_ret + '/seed_' + seed_mass + '/m_bh_torque'].value
			mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mstar'].value
			mbulge = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mbulge'].value

			redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
			sfr_gal = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/sfr'].value
			sfr_inner = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[0] + '/sfr'].value
			t = np.array([time(xxx) for xxx in redshift])

			step = 1e8

			t_list = np.arange(np.min(t), np.max(t), step)
			t_list = np.append(t_list, np.max(t))

			t1 = []
			sfr_gal1 = []
			sfr_inner1 = []	
			mstar_gal1 = []
			mbulge_gal1 = []

			for xxx in range(len(t_list)-1):

				sel = (t >= t_list[xxx]) & (t <= t_list[xxx+1])
				t_new = t[sel]
				sfr_gal_new = sfr_gal[sel]
				sfr_inner_new = sfr_inner[sel]
				mstar_gal = mstar[sel]
				mbulge_gal = mbulge[sel]
				
				t1.append(np.sum(t_new)/len(t_new))
				sfr_gal1.append(np.sum(sfr_gal_new)/len(sfr_gal_new))
				sfr_inner1.append(np.sum(sfr_inner_new)/len(sfr_inner_new))
				mstar_gal1.append(np.sum(mstar_gal)/len(mstar_gal))
				mbulge_gal1.append(np.sum(mbulge_gal)/len(mbulge_gal))

			ratio_star_bulge = np.array(mbulge_gal1) / np.array(mstar_gal1)
			mstar_gal = integrate.cumtrapz(sfr_gal1, t1, initial=1e7)
			mstar_inner = integrate.cumtrapz(sfr_inner1, t1, initial=1e7)
			#mstar_gal[0] = mstar_gal[1]
			#mstar_inner[0] = mstar_inner[1]
			mdot = mstar_inner * 1e-10
			mbh = integrate.cumtrapz(mdot, t1, initial=1e4)
			mbh *= C[i]
			#mbh[0] = mbh[1]

			#for xxx in range(len(t1)):
			#	print '%-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e \n' %(t1[xxx], sfr_gal1[xxx], sfr_inner1[xxx], mstar_gal[xxx], mstar_inner[xxx], mbh[xxx])

			fig = plt.figure(1)
			fig.subplots_adjust(wspace=0.0, hspace=0.0)
			fig.suptitle(sim_name[i] + '   C: ' + str(C[i]))

			ax0 = fig.add_subplot(111)	
			ax0.set_xscale('log')
			ax0.set_yscale('log')
			ax0.set_xlim(1e7,1e12)
			ax0.set_ylim(1e3,1e11)
			#ax0.plot(mstar, bh_mass, color='k', ls='-', lw=2, label=r'$PP$')
			#ax0.plot(mstar_gal, mbh, color='r', ls='-', lw=2, label=r'$Kinematic$')
			ax0.plot(mbulge, bh_mass, color='k', ls='-', lw=2, label=r'$PP$')
			ax0.plot(mstar_gal*ratio_star_bulge, mbh, color='r', ls='-', lw=2, label=r'$Kinematic$')
			#ax0.plot(mass_list, Reines2015_data, color='k', ls='--', lw=1, label=r'$Reines+15$')
			ax0.plot(mass_list, Haring04_data, color='k', ls='--', lw=1, label=r'$Haring+04$')
			#ax0.fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
			ax0.fill_between(mass_list, Haring04_data * np.sqrt(1e1), Haring04_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
			#ax0.set_xlabel(r'$M_{*}\;(M_{\odot})$')
			ax0.set_xlabel(r'$M_{bulge}\;(M_{\odot})$')
			ax0.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
			ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

			#plt.tight_layout()
			#plt.savefig('plot_#7_' + sim_name[i] + '.png', dpi=300, format='png')
			plt.savefig('plot_#7_' + sim_name[i] + '_mbh_mbulge.png', dpi=300, format='png')
			#plt.show()
			plt.close()

def plot_8():
	
	for i in [3,9,12,13,33,34,35,36]: #i_list:

		for regime in ['cold']:

			if sim_suite[i] == 'MassiveFIRE':
				radius = radius_name_list[3] #MassiveFIRE
			else:
				radius = radius_name_list[0] #MassiveFIRE2

			mass_ret = mass_retention_list[3]
			seed_mass = seed_mass_list[3]
		
			bh_mass = data[sim_suite[i] + '/' + sim_name[i]][regime + '/r_' + radius + '/mass_ret_' + mass_ret + '/seed_' + seed_mass + '/m_bh_torque'].value
			mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mstar'].value
			mbulge = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mbulge'].value

			redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
			sfr_gal = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/sfr'].value
			sfr_inner = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[0] + '/sfr'].value
			t = np.array([time(xxx) for xxx in redshift])

			step = 1e8

			t_list = np.arange(np.min(t), np.max(t), step)
			t_list = np.append(t_list, np.max(t))

			t1 = []
			sfr_gal1 = []
			sfr_inner1 = []	
			mstar_gal1 = []
			mbulge_gal1 = []

			for xxx in range(len(t_list)-1):

				sel = (t >= t_list[xxx]) & (t <= t_list[xxx+1])
				t_new = t[sel]
				sfr_gal_new = sfr_gal[sel]
				sfr_inner_new = sfr_inner[sel]
				mstar_gal = mstar[sel]
				mbulge_gal = mbulge[sel]
				
				t1.append(np.sum(t_new)/len(t_new))
				sfr_gal1.append(np.sum(sfr_gal_new)/len(sfr_gal_new))
				sfr_inner1.append(np.sum(sfr_inner_new)/len(sfr_inner_new))
				mstar_gal1.append(np.sum(mstar_gal)/len(mstar_gal))
				mbulge_gal1.append(np.sum(mbulge_gal)/len(mbulge_gal))

			ratio_star_bulge = np.array(mbulge_gal1) / np.array(mstar_gal1)
			mstar_gal = integrate.cumtrapz(sfr_gal1, t1, initial=1e7)
			mstar_inner = integrate.cumtrapz(sfr_inner1, t1, initial=1e7)
			#mstar_gal[0] = mstar_gal[1]
			#mstar_inner[0] = mstar_inner[1]
			mdot = mstar_inner * 1e-10
			mbh = integrate.cumtrapz(mdot, t1, initial=1e4)
			mbh *= C[i]
			#mbh[0] = mbh[1]

			#for xxx in range(len(t1)):
			#	print '%-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e \n' %(t1[xxx], sfr_gal1[xxx], sfr_inner1[xxx], mstar_gal[xxx], mstar_inner[xxx], mbh[xxx])

			fig = plt.figure(1)
			fig.subplots_adjust(wspace=0.0, hspace=0.0)
			fig.suptitle(sim_name[i] + '   C: ' + str(C[i]))
			ax0 = fig.add_subplot(111)	

			z_list = [0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0]
			redshift_list = []
			mbh_z = []
			mstar_z = []
			mbulge_z = []
			
			for z in z_list:
			
				if z in redshift:
					sel = (redshift == z)

					redshift_list.append(redshift[sel])
					mbh_z.append(bh_mass[sel])
					mstar_z.append(mstar[sel])
					mbulge_z.append(mbulge[sel])
			
			redshift_list = np.concatenate(redshift_list)
			mbh_z = np.concatenate(mbh_z)
			mstar_z = np.concatenate(mstar_z)
			mbulge_z = np.concatenate(mbulge_z)
			
			ax0.set_xscale('log')
			ax0.set_yscale('log')
			ax0.set_xlim(1e7,1e12)
			ax0.set_ylim(1e3,1e11)
			ax0.plot(mstar, bh_mass, color='black', ls='-', lw=2, label=r'$PP\;M_{*}$')
			ax0.plot(mstar_gal, mbh, color='black', ls='--', lw=2, label=r'$Kinematic\;M_{*}$')
			ax0.plot(mbulge, bh_mass, color='red', ls='-', lw=2, label=r'$PP\;M_{bulge}$')
			ax0.plot(mstar_gal*ratio_star_bulge, mbh, color='red', ls='--', lw=2, label=r'$Kinematic\;M_{bulge}$')
			ax0.plot(mass_list, Reines2015_data, color='black', ls='--', lw=1, label=r'$Reines+15$')
			ax0.plot(mass_list, Haring04_data, color='red', ls='--', lw=1, label=r'$Haring+04$')
			ax0.fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
			ax0.fill_between(mass_list, Haring04_data * np.sqrt(1e1), Haring04_data / np.sqrt(1e1), color='red', zorder=0, alpha=0.1 )

			#ax0.scatter(mbulge_z, mbh_z, color='k', marker='o', s=120)

			# loop through each x,y pair
			for x,y,z in zip(redshift_list, mbulge_z, mbh_z):
				ax0.scatter(y, z, color='k', marker='o', s=80, zorder=10)
				corr_y = 0.91 # adds a little correction to put annotation in marker's centrum
				corr_z = 0.84
				ax0.annotate(str(int(x)),  xy=(y * corr_y, z * corr_z), color='white', size=8, weight='bold', alpha=1.0, zorder=11)

			ax0.set_xlabel(r'$Mass\;(M_{\odot})$')
			ax0.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
			ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

			#plt.tight_layout()
			fig.savefig('plot8_' + sim_name[i] + '_mbh_mstar_mbulge_' + str(C[i]) + '.pdf')
			plt.close()

def plot_10():

	regime = "cold"

	array = [12,13,33,34]
	color = ['black','red','black','red']
	linestyle = ['--','--','-','-']
	onur = np.array([1.0,1.0,1.0])
	z_ticks = [0,1,2,3,4,5,6,7,8,9,10,11]
	zmin = 0
	zmax = 11

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0.0, hspace=0.0)
	ax1 = fig.add_subplot(411)	
	ax12 = ax1.twiny()

	for j in range(len(array)):
		i = array[j]
		redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
		mhalo = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mhalo'].value
		ax12.plot(redshift, mhalo, color=color[j], ls=linestyle[j], lw=1)
	ax12.plot(onur,onur,color='m',ls='--', lw=1, label='MF1')
	ax12.plot(onur,onur,color='m',ls='-', lw=1, label='MF2')
	ax12.plot(onur,onur,color='r',ls='-', lw=1, label='A1')
	#ax12.plot(onur,onur,color='r',ls='-', lw=1, label='A2')
	ax12.plot(onur,onur,color='b',ls='-', lw=1, label='A4')
	#ax12.plot(onur,onur,color='k',ls='-', lw=1, label='A8')

	ax12.set_xscale('linear')
	ax12.set_yscale('log')
	ax12.set_xlim(zmin,zmax)
	ax12.set_ylim(7e9,3e13)
	ax12.set_xlabel(r'$z$')
	ax1.set_ylabel(r'$M_{h}\;(M_{\odot})$')
	ax12.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax12.xaxis.set_minor_locator(ticker.MultipleLocator(0.2))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1e10,1e11,1e12,1e13]))
	ax1.yaxis.set_minor_locator(ticker.LogLocator(base=10.0, subs=(0.2,0.4,0.6,0.8), numticks=4))
	ax1.axes.get_xaxis().set_visible(False)
	#ax1.axes.get_xaxis().set_ticks([])
	ax12.legend(loc=0,prop={'size':6},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(412)	
	for j in range(len(array)):
		i = array[j]
		redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
		mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mstar'].value
		ax2.plot(redshift, mstar, color=color[j], ls=linestyle[j], lw=1)
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(zmin,zmax)
	ax2.set_ylim(1e7,3e12)
	#ax2.set_xlabel(r'$Mass\;(M_{\odot})$')
	ax2.set_ylabel(r'$M_{*}\;(M_{\odot})$')
	ax2.yaxis.set_major_locator(plt.FixedLocator([1e8,1e9,1e10,1e11,1e12]))
	ax2.yaxis.set_minor_locator(ticker.LogLocator(base=10.0, subs=(0.2,0.4,0.6,0.8), numticks=6))
	ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	ax2.axes.get_xaxis().set_visible(False)

	ax3 = fig.add_subplot(413)	
	for j in range(len(array)):
		i = array[j]
		redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
		mgas = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mgas'].value
		ax3.plot(redshift, mgas, color=color[j], ls=linestyle[j], lw=1)
	ax3.set_xscale('linear')
	ax3.set_yscale('log')
	ax3.set_xlim(zmin,zmax)
	ax3.set_ylim(1e6,2e11)
	#ax3.set_xlabel(r'$Mass\;(M_{\odot})$')
	ax3.set_ylabel(r'$M_{g}\;(M_{\odot})$')
	ax3.yaxis.set_major_locator(plt.FixedLocator([1e7,1e8,1e9,1e10,1e11]))
	ax3.yaxis.set_minor_locator(ticker.LogLocator(base=10.0, subs=(0.2,0.4,0.6,0.8), numticks=6))
	ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)
	ax3.axes.get_xaxis().set_visible(False)
	
	ax4 = fig.add_subplot(414)	
	for j in range(len(array)):
		i = array[j]
		redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
		sfr = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/sfr'].value
		ax4.plot(redshift, sfr, color=color[j], ls=linestyle[j], lw=1)
	ax4.set_xscale('linear')
	ax4.set_yscale('log')
	ax4.set_xlim(zmin,zmax)
	ax4.set_ylim(1e-3,2e3)
	ax4.set_xlabel(r'$z$')
	ax4.set_ylabel(r'$SFR\;(M_{\odot}/yr)$')
	ax4.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
	ax4.xaxis.set_minor_locator(ticker.MultipleLocator(0.2))
	ax4.yaxis.set_major_locator(plt.FixedLocator([1e-2,1e-1,1e0,1e1,1e2,1e3]))
	ax4.yaxis.set_minor_locator(ticker.LogLocator(base=10.0, subs=(0.2,0.4,0.6,0.8), numticks=6))
	ax4.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	#plt.tight_layout()
	plt.savefig('plot_MF1_MF2_sample.png', dpi=300, format='png')
	plt.close()

#################################################### 
################### MAIN PROGRAM ###################
#################################################### 
global sim_name
global sim_suite
global dummy
global regime_list
global radius_name_list
global mass_retention_list
global seed_mass_list
global halo_ID
global data
global i_list
global mass_list
global vel_list
global Ferrerase02_data
global Haring04_data
global Kormendy2013_data
global Reines2015_data
global McConnell2013_data
global McConnell_Ma2013_data
global cosmo
global C
global ahf_centering
global merger

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
dummy = [16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 178, 178, 178, 11, 11, 11, 11]
C = [1e0, 1e0, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e0, 1e-1, 1e0, 1e0, 1e-1, 1e-1, 1e0, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1]

regime_list = ['cold', 'total']
radius_name_list = ['0.1', '0.2', '0.5', '1.0', 'rvir']
mass_retention_list = ['0.05', '0.1', '0.25', '0.5', '1.0']
seed_mass_list = ['1e1', '1e2', '1e3', '1e4', '1e5']
halo_ID = 0

mass_list = np.array([1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12,1e13,1e14])
vel_list = np.array([0,20,40,60,80,100,200,400,800])
Ferrerase02_data = []
Haring04_data = []
Kormendy2013_data = []
McConnell2013_data = []
Reines2015_data = []
Savorgnan2016_data = []
McConnell_Ma2013_data = []

for mass_dummy in mass_list:
	Ferrerase02_data.append(Ferrerase02(mass_dummy))
	Haring04_data.append(Haring04(mass_dummy))
	Kormendy2013_data.append(Kormendy2013(mass_dummy))
	McConnell2013_data.append(McConnell2013(mass_dummy))
	Reines2015_data.append(Reines2015(mass_dummy))
	#Savorgnan2016_data.append(Savorgnan2016(mass_dummy))
for vel_dummy in vel_list:
	McConnell_Ma2013_data.append(McConnell_Ma2013(vel_dummy))

Ferrerase02_data = np.array(Ferrerase02_data)
Haring04_data = np.array(Haring04_data)
Kormendy2013_data = np.array(Kormendy2013_data)
McConnell2013_data = np.array(McConnell2013_data)
Reines2015_data = np.array(Reines2015_data)
#Savorgnan2016_data = np.array(Savorgnan2016_data)
McConnell_Ma2013_data = np.array(McConnell_Ma2013_data)

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

ahf_centering = 'COM'
merger = 'nomerger'

i_list = range(37)
data = {}
for regime in regime_list:
	for i in i_list:

		data[sim_suite[i] + '/' + sim_name[i]] = h5py.File('/bulk1/feldmann/Onur/' + ahf_centering + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5','r')

####################################
########## PLOT FUNCTIONS ##########
####################################

#mbh_vs_mstar_mbulge()
#z_vs_sfr_mgin()
#plot_1()
#plot_5()
#plot_7()
plot_8()
#plot_10()
