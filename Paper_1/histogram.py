import numpy as np
from numpy import *
import os
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value
from astropy.cosmology import FlatLambdaCDM
import math

def time(i):
	return cosmo.age(i).value # Gyr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)
sim_name_MF1 = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR']
sim_name_MF2 = ['B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
mass_ret_list = ['mass_ret_0.05', 'mass_ret_0.1', 'mass_ret_0.25', 'mass_ret_0.5', 'mass_ret_1.0']

r_MF1 = 'r_1.0'
r_MF2 = 'r_0.1'
output_path = 'histogram_r1.0'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for mass_ret in mass_ret_list:

	mdot_mc1 = []
	mdot_mm1 = []
	mdot_nc1 = []
	mdot_nm1 = []
	mdot_mc2 = []
	mdot_mm2 = []
	mdot_nc2 = []
	mdot_nm2 = []

	f_mc1 = []
	f_mm1 = []
	f_nc1 = []
	f_nm1 = []
	f_mc2 = []
	f_mm2 = []
	f_nc2 = []
	f_nm2 = []

	z_mc1 = []
	z_mm1 = []
	z_nc1 = []
	z_nm1 = []
	z_mc2 = []
	z_mm2 = []
	z_nc2 = []
	z_nm2 = []

	mbh_mc1 = []
	mbh_mm1 = []
	mbh_nc1 = []
	mbh_nm1 = []
	mbh_mc2 = []
	mbh_mm2 = []
	mbh_nc2 = []
	mbh_nm2 = []

	mhalo_mc1 = []
	mhalo_mm1 = []
	mhalo_nc1 = []
	mhalo_nm1 = []
	mhalo_mc2 = []
	mhalo_mm2 = []
	mhalo_nc2 = []
	mhalo_nm2 = []

	mstar_mc1 = []
	mstar_mm1 = []
	mstar_nc1 = []
	mstar_nm1 = []
	mstar_mc2 = []
	mstar_mm2 = []
	mstar_nc2 = []
	mstar_nm2 = []

	for i in range(len(sim_name_MF1)):
		merger_com_MF1 = glob.glob('../COM/MassiveFIRE/' + sim_name_MF1[i] + '/' + sim_name_MF1[i] + '_merger.hdf5')[0]
		merger_mdc_MF1 = glob.glob('../MDC/MassiveFIRE/' + sim_name_MF1[i] + '/' + sim_name_MF1[i] + '_merger.hdf5')[0]
		nomerger_com_MF1 = glob.glob('../COM/MassiveFIRE/' + sim_name_MF1[i] + '/' + sim_name_MF1[i] + '_nomerger.hdf5')[0]
		nomerger_mdc_MF1 = glob.glob('../MDC/MassiveFIRE/' + sim_name_MF1[i] + '/' + sim_name_MF1[i] + '_nomerger.hdf5')[0]
		data_merger_com_MF1 = h5py.File(merger_com_MF1, 'r')
		data_merger_mdc_MF1 = h5py.File(merger_mdc_MF1, 'r')
		data_nomerger_com_MF1 = h5py.File(nomerger_com_MF1, 'r')
		data_nomerger_mdc_MF1 = h5py.File(nomerger_mdc_MF1, 'r')
		mdot_merger_com_MF1 = data_merger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_merger_mdc_MF1 = data_merger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_com_MF1 = data_nomerger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		f_merger_com_MF1 = data_merger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_mdc_MF1 = data_merger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_com_MF1 = data_nomerger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		z_merger_com_MF1 = data_merger_com_MF1['cold/rvir/z_1'].value
		z_merger_mdc_MF1 = data_merger_mdc_MF1['cold/rvir/z_1'].value
		z_nomerger_com_MF1 = data_nomerger_com_MF1['cold/rvir/z_1'].value
		z_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/rvir/z_1'].value
		mbh_merger_com_MF1 = data_merger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_mdc_MF1 = data_merger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_com_MF1 = data_nomerger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mhalo_merger_com_MF1 = data_merger_com_MF1['cold/rvir/mhalo'].value
		mhalo_merger_mdc_MF1 = data_merger_mdc_MF1['cold/rvir/mhalo'].value
		mhalo_nomerger_com_MF1 = data_nomerger_com_MF1['cold/rvir/mhalo'].value
		mhalo_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/rvir/mhalo'].value
		mstar_merger_com_MF1 = data_merger_com_MF1['cold/rvir/mstar'].value
		mstar_merger_mdc_MF1 = data_merger_mdc_MF1['cold/rvir/mstar'].value
		mstar_nomerger_com_MF1 = data_nomerger_com_MF1['cold/rvir/mstar'].value
		mstar_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/rvir/mstar'].value

		mdot_mc1.append(mdot_merger_com_MF1)
		mdot_mm1.append(mdot_merger_mdc_MF1)
		mdot_nc1.append(mdot_nomerger_com_MF1)
		mdot_nm1.append(mdot_nomerger_mdc_MF1)
		f_mc1.append(f_merger_com_MF1)
		f_mm1.append(f_merger_mdc_MF1)
		f_nc1.append(f_nomerger_com_MF1)
		f_nm1.append(f_nomerger_mdc_MF1)
		z_mc1.append(z_merger_com_MF1)
		z_mm1.append(z_merger_mdc_MF1)
		z_nc1.append(z_nomerger_com_MF1)
		z_nm1.append(z_nomerger_mdc_MF1)
		mbh_mc1.append(mbh_merger_com_MF1)
		mbh_mm1.append(mbh_merger_mdc_MF1)
		mbh_nc1.append(mbh_nomerger_com_MF1)
		mbh_nm1.append(mbh_nomerger_mdc_MF1)
		mhalo_mc1.append(mhalo_merger_com_MF1)
		mhalo_mm1.append(mhalo_merger_mdc_MF1)
		mhalo_nc1.append(mhalo_nomerger_com_MF1)
		mhalo_nm1.append(mhalo_nomerger_mdc_MF1)
		mstar_mc1.append(mstar_merger_com_MF1)
		mstar_mm1.append(mstar_merger_mdc_MF1)
		mstar_nc1.append(mstar_nomerger_com_MF1)
		mstar_nm1.append(mstar_nomerger_mdc_MF1)

	for i in range(len(sim_name_MF2)):
		merger_com_MF2 = glob.glob('../COM/MassiveFIRE2/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_merger.hdf5')[0]
		merger_mdc_MF2 = glob.glob('../MDC/MassiveFIRE2/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_merger.hdf5')[0]
		nomerger_com_MF2 = glob.glob('../COM/MassiveFIRE2/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_nomerger.hdf5')[0]
		nomerger_mdc_MF2 = glob.glob('../MDC/MassiveFIRE2/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_nomerger.hdf5')[0]
		data_merger_com_MF2 = h5py.File(merger_com_MF2, 'r')
		data_merger_mdc_MF2 = h5py.File(merger_mdc_MF2, 'r')
		data_nomerger_com_MF2 = h5py.File(nomerger_com_MF2, 'r')
		data_nomerger_mdc_MF2 = h5py.File(nomerger_mdc_MF2, 'r')
		mdot_merger_com_MF2 = data_merger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_merger_mdc_MF2 = data_merger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_com_MF2 = data_nomerger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		f_merger_com_MF2 = data_merger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_mdc_MF2 = data_merger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_com_MF2 = data_nomerger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		z_merger_com_MF2 = data_merger_com_MF2['cold/rvir/z_1'].value
		z_merger_mdc_MF2 = data_merger_mdc_MF2['cold/rvir/z_1'].value
		z_nomerger_com_MF2 = data_nomerger_com_MF2['cold/rvir/z_1'].value
		z_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/rvir/z_1'].value
		mbh_merger_com_MF2 = data_merger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_mdc_MF2 = data_merger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_com_MF2 = data_nomerger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mhalo_merger_com_MF2 = data_merger_com_MF2['cold/rvir/mhalo'].value
		mhalo_merger_mdc_MF2 = data_merger_mdc_MF2['cold/rvir/mhalo'].value
		mhalo_nomerger_com_MF2 = data_nomerger_com_MF2['cold/rvir/mhalo'].value
		mhalo_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/rvir/mhalo'].value
		mstar_merger_com_MF2 = data_merger_com_MF2['cold/rvir/mstar'].value
		mstar_merger_mdc_MF2 = data_merger_mdc_MF2['cold/rvir/mstar'].value
		mstar_nomerger_com_MF2 = data_nomerger_com_MF2['cold/rvir/mstar'].value
		mstar_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/rvir/mstar'].value

		mdot_mc2.append(mdot_merger_com_MF2)
		mdot_mm2.append(mdot_merger_mdc_MF2)
		mdot_nc2.append(mdot_nomerger_com_MF2)
		mdot_nm2.append(mdot_nomerger_mdc_MF2)
		f_mc2.append(f_merger_com_MF2)
		f_mm2.append(f_merger_mdc_MF2)
		f_nc2.append(f_nomerger_com_MF2)
		f_nm2.append(f_nomerger_mdc_MF2)
		z_mc2.append(z_merger_com_MF2)
		z_mm2.append(z_merger_mdc_MF2)
		z_nc2.append(z_nomerger_com_MF2)
		z_nm2.append(z_nomerger_mdc_MF2)
		mbh_mc2.append(mbh_merger_com_MF2)
		mbh_mm2.append(mbh_merger_mdc_MF2)
		mbh_nc2.append(mbh_nomerger_com_MF2)
		mbh_nm2.append(mbh_nomerger_mdc_MF2)
		mhalo_mc2.append(mhalo_merger_com_MF2)
		mhalo_mm2.append(mhalo_merger_mdc_MF2)
		mhalo_nc2.append(mhalo_nomerger_com_MF2)
		mhalo_nm2.append(mhalo_nomerger_mdc_MF2)
		mstar_mc2.append(mstar_merger_com_MF2)
		mstar_mm2.append(mstar_merger_mdc_MF2)
		mstar_nc2.append(mstar_nomerger_com_MF2)
		mstar_nm2.append(mstar_nomerger_mdc_MF2)

	mdot_mc1 = np.concatenate(mdot_mc1)
	mdot_mm1 = np.concatenate(mdot_mm1)
	mdot_nc1 = np.concatenate(mdot_nc1)
	mdot_nm1 = np.concatenate(mdot_nm1)
	mdot_mc2 = np.concatenate(mdot_mc2)
	mdot_mm2 = np.concatenate(mdot_mm2)
	mdot_nc2 = np.concatenate(mdot_nc2)
	mdot_nm2 = np.concatenate(mdot_nm2)

	f_mc1 = np.concatenate(f_mc1)
	f_mm1 = np.concatenate(f_mm1)
	f_nc1 = np.concatenate(f_nc1)
	f_nm1 = np.concatenate(f_nm1)
	f_mc2 = np.concatenate(f_mc2)
	f_mm2 = np.concatenate(f_mm2)
	f_nc2 = np.concatenate(f_nc2)
	f_nm2 = np.concatenate(f_nm2)

	z_mc1 = np.concatenate(z_mc1)
	z_mm1 = np.concatenate(z_mm1)
	z_nc1 = np.concatenate(z_nc1)
	z_nm1 = np.concatenate(z_nm1)
	z_mc2 = np.concatenate(z_mc2)
	z_mm2 = np.concatenate(z_mm2)
	z_nc2 = np.concatenate(z_nc2)
	z_nm2 = np.concatenate(z_nm2)

	t_mc1 = time(z_mc1)
	t_mm1 = time(z_mm1)
	t_nc1 = time(z_nc1)
	t_nm1 = time(z_nm1)
	t_mc2 = time(z_mc2)
	t_mm2 = time(z_mm2)
	t_nc2 = time(z_nc2)
	t_nm2 = time(z_nm2)

	mbh_mc1 = np.concatenate(mbh_mc1)
	mbh_mm1 = np.concatenate(mbh_mm1)
	mbh_nc1 = np.concatenate(mbh_nc1)
	mbh_nm1 = np.concatenate(mbh_nm1)
	mbh_mc2 = np.concatenate(mbh_mc2)
	mbh_mm2 = np.concatenate(mbh_mm2)
	mbh_nc2 = np.concatenate(mbh_nc2)
	mbh_nm2 = np.concatenate(mbh_nm2)

	logmbh_mc1 = np.log10(mbh_mc1)
	logmbh_mm1 = np.log10(mbh_mm1)
	logmbh_nc1 = np.log10(mbh_nc1)
	logmbh_nm1 = np.log10(mbh_nm1)
	logmbh_mc2 = np.log10(mbh_mc2)
	logmbh_mm2 = np.log10(mbh_mm2)
	logmbh_nc2 = np.log10(mbh_nc2)
	logmbh_nm2 = np.log10(mbh_nm2)

	mhalo_mc1 = np.concatenate(mhalo_mc1)
	mhalo_mm1 = np.concatenate(mhalo_mm1)
	mhalo_nc1 = np.concatenate(mhalo_nc1)
	mhalo_nm1 = np.concatenate(mhalo_nm1)
	mhalo_mc2 = np.concatenate(mhalo_mc2)
	mhalo_mm2 = np.concatenate(mhalo_mm2)
	mhalo_nc2 = np.concatenate(mhalo_nc2)
	mhalo_nm2 = np.concatenate(mhalo_nm2)

	logmhalo_mc1 = np.log10(mhalo_mc1)
	logmhalo_mm1 = np.log10(mhalo_mm1)
	logmhalo_nc1 = np.log10(mhalo_nc1)
	logmhalo_nm1 = np.log10(mhalo_nm1)
	logmhalo_mc2 = np.log10(mhalo_mc2)
	logmhalo_mm2 = np.log10(mhalo_mm2)
	logmhalo_nc2 = np.log10(mhalo_nc2)
	logmhalo_nm2 = np.log10(mhalo_nm2)

	mstar_mc1 = np.concatenate(mstar_mc1)
	mstar_mm1 = np.concatenate(mstar_mm1)
	mstar_nc1 = np.concatenate(mstar_nc1)
	mstar_nm1 = np.concatenate(mstar_nm1)
	mstar_mc2 = np.concatenate(mstar_mc2)
	mstar_mm2 = np.concatenate(mstar_mm2)
	mstar_nc2 = np.concatenate(mstar_nc2)
	mstar_nm2 = np.concatenate(mstar_nm2)

	logmstar_mc1 = np.log10(mstar_mc1)
	logmstar_mm1 = np.log10(mstar_mm1)
	logmstar_nc1 = np.log10(mstar_nc1)
	logmstar_nm1 = np.log10(mstar_nm1)
	logmstar_mc2 = np.log10(mstar_mc2)
	logmstar_mm2 = np.log10(mstar_mm2)
	logmstar_nc2 = np.log10(mstar_nc2)
	logmstar_nm2 = np.log10(mstar_nm2)

	# binning of redshift wrt MF2
	nstep_list = [9,13,17]
	n_xaxis_list = [3,4,4]
	n_yaxis_list = [3,3,4]

	for nstep, n_xaxis, n_yaxis in zip(nstep_list, n_xaxis_list, n_yaxis_list):

		step_z = np.linspace(np.min(t_mm2), np.max(t_mm2), nstep)
		z_step = []
		for xxx in step_z:
			z_step.append(z_at_value(Planck13.age, xxx*u.Gyr))
		
		step_mbh = np.linspace(np.min(logmbh_mm2), np.max(logmbh_mm2), nstep)
		step_mhalo = np.linspace(np.min(logmhalo_mm2), np.max(logmhalo_mm2), nstep)
		step_mstar = np.linspace(np.min(logmstar_mm2), np.max(logmstar_mm2), nstep)

		nbins = 10
		alpha = 0.3
		color_com = 'r'
		color_mdc = 'b'
		density = False
		xmin = -4
		xmax = 1.5

		z_comdata = [z_mc1, z_nc1, z_mc2, z_nc2]
		z_mdcdata = [z_mm1, z_nm1, z_mm2, z_nm2]
		logmbh_comdata = [logmbh_mc1, logmbh_nc1, logmbh_mc2, logmbh_nc2]
		logmbh_mdcdata = [logmbh_mm1, logmbh_nm1, logmbh_mm2, logmbh_nm2]
		logmhalo_comdata = [logmhalo_mc1, logmhalo_nc1, logmhalo_mc2, logmhalo_nc2]
		logmhalo_mdcdata = [logmhalo_mm1, logmhalo_nm1, logmhalo_mm2, logmhalo_nm2]
		logmstar_comdata = [logmstar_mc1, logmstar_nc1, logmstar_mc2, logmstar_nc2]
		logmstar_mdcdata = [logmstar_mm1, logmstar_nm1, logmstar_mm2, logmstar_nm2]
		mdot_comdata = [mdot_mc1, mdot_nc1, mdot_mc2, mdot_nc2]
		mdot_mdcdata = [mdot_mm1, mdot_nm1, mdot_mm2, mdot_nm2]
		f_comdata = [f_mc1, f_nc1, f_mc2, f_nc2]
		f_mdcdata = [f_mm1, f_nm1, f_mm2, f_nm2]

		name = ['MERGER_MF1_', 'NOMERGER_MF1_', 'MERGER_MF2_', 'NOMERGER_MF2_']

		for k in range(4):

			# define dataset here 
			zcom = z_comdata[k]
			zmdc = z_mdcdata[k]
			logmbhcom = logmbh_comdata[k]
			logmbhmdc = logmbh_mdcdata[k]
			logmhalocom = logmhalo_comdata[k]
			logmhalomdc = logmhalo_mdcdata[k]
			logmstarcom = logmstar_comdata[k]
			logmstarmdc = logmstar_mdcdata[k]
			mdotcom = mdot_comdata[k]
			mdotmdc = mdot_mdcdata[k]
			fcom = f_comdata[k]
			fmdc = f_mdcdata[k]

			################
			### REDSHIFT ###
			################

			fig, axes = plt.subplots(n_xaxis, n_yaxis, figsize=(10,5), squeeze=False)
			fig.subplots_adjust(wspace=0.3, hspace=0.3)
			fig.suptitle(mass_ret)
			ax = axes.ravel()

			j = 0

			for i in range(nstep-1):

				selcom = (zcom >= z_step[nstep-i-1]) & (zcom < z_step[nstep-i-2])
				z_com = zcom[selcom]
				mdot_com = ma.log10(mdotcom[selcom]).filled(0)
				f_com = ma.log10(fcom[selcom]).filled(0)
				selmdc = (zmdc >= z_step[nstep-i-1]) & (zmdc < z_step[nstep-i-2])
				z_mdc = zmdc[selmdc]
				mdot_mdc = ma.log10(mdotmdc[selmdc]).filled(0)
				f_mdc = ma.log10(fmdc[selmdc]).filled(0)

				#ax[j] = fig.add_subplot(231)
				ax[j].hist(f_com, nbins, density=density, facecolor=color_com, label=str('%.2f' %(z_step[nstep-i-1])) + '<z<' + str('%.2f' %(z_step[nstep-i-2])), alpha=alpha)
				ax[j].hist(f_mdc, nbins, density=density, facecolor=color_mdc, alpha=alpha)
				ax[j].set_xlim(xmin,xmax)
				#ax[j].set_ylim(0,70)
				ax[j].set_xscale('linear')
				ax[j].set_yscale('linear')
				ax[j].set_xlabel(r'$log(\dot{M}_{t} \; / \; \dot{M}_{edd})$')
				ax[j].set_ylabel('#')
				#ax[j].grid(color='k', ls='--', lw=0.25)
				#ax[j].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
				ax[j].legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

				j += 1

			#plt.show()
			plt.savefig(output_path + '/histogram_z_' + name[k] + mass_ret + '_' + str(nstep-1) + '_bins.png', dpi=300, format='png')
			plt.close()

			###########
			### MBH ###
			###########
			fig, axes = plt.subplots(n_xaxis,n_yaxis, figsize=(10,5), squeeze=False)
			fig.subplots_adjust(wspace=0.3, hspace=0.3)
			fig.suptitle(mass_ret)
			ax = axes.ravel()

			j = 0

			for i in range(nstep-1):

				selcom = (logmbhcom <= step_mbh[nstep-i-1]) & (logmbhcom > step_mbh[nstep-i-2])
				logmbh_com = logmbhcom[selcom]
				mdot_com = ma.log10(mdotcom[selcom]).filled(0)
				f_com = ma.log10(fcom[selcom]).filled(0)
				selmdc = (logmbhmdc <= step_mbh[nstep-i-1]) & (logmbhmdc > step_mbh[nstep-i-2])
				logmbh_mdc = logmbhmdc[selmdc]
				mdot_mdc = ma.log10(mdotmdc[selmdc]).filled(0)
				f_mdc = ma.log10(fmdc[selmdc]).filled(0)

				#ax[j] = fig.add_subplot(231)
				ax[j].hist(f_com, nbins, density=density, facecolor=color_com, label=str('%.2f' %(step_mbh[nstep-i-1])) + '<Mbh<' + str('%.2f' %(step_mbh[nstep-i-2])), alpha=alpha)
				ax[j].hist(f_mdc, nbins, density=density, facecolor=color_mdc, alpha=alpha)
				ax[j].set_xlim(xmin,xmax)
				#ax[j].set_ylim(0,70)
				ax[j].set_xscale('linear')
				ax[j].set_yscale('linear')
				ax[j].set_xlabel(r'$log(\dot{M}_{t}\; / \; \dot{M}_{edd})$')
				ax[j].set_ylabel('#')
				#ax[j].grid(color='k', ls='--', lw=0.25)
				#ax[j].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
				ax[j].legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

				j += 1

			#plt.show()
			plt.savefig(output_path + '/histogram_mbh_' + name[k] + mass_ret + '_' + str(nstep-1) + '_bins.png', dpi=300, format='png')
			plt.close()

			#############
			### MHALO ###
			#############

			fig, axes = plt.subplots(n_xaxis,n_yaxis, figsize=(10,5), squeeze=False)
			fig.subplots_adjust(wspace=0.3, hspace=0.3)
			fig.suptitle(mass_ret)
			ax = axes.ravel()

			j = 0

			for i in range(nstep-1):

				selcom = (logmhalocom <= step_mhalo[nstep-i-1]) & (logmhalocom > step_mhalo[nstep-i-2])
				logmhalo_com = logmhalocom[selcom]
				mdot_com = ma.log10(mdotcom[selcom]).filled(0)
				f_com = ma.log10(fcom[selcom]).filled(0)
				selmdc = (logmhalomdc <= step_mhalo[nstep-i-1]) & (logmhalomdc > step_mhalo[nstep-i-2])
				logmhalo_mdc = logmhalomdc[selmdc]
				mdot_mdc = ma.log10(mdotmdc[selmdc]).filled(0)
				f_mdc = ma.log10(fmdc[selmdc]).filled(0)

				#ax[j] = fig.add_subplot(231)
				ax[j].hist(f_com, nbins, density=density, facecolor=color_com, label=str('%.2f' %(step_mhalo[nstep-i-1])) + '<Mhalo<' + str('%.2f' %(step_mhalo[nstep-i-2])), alpha=alpha)
				ax[j].hist(f_mdc, nbins, density=density, facecolor=color_mdc, alpha=alpha)
				ax[j].set_xlim(xmin,xmax)
				#ax[j].set_ylim(0,70)
				ax[j].set_xscale('linear')
				ax[j].set_yscale('linear')
				ax[j].set_xlabel(r'$log(\dot{M}_{t}\; / \; \dot{M}_{edd})$')
				ax[j].set_ylabel('#')
				#ax[j].grid(color='k', ls='--', lw=0.25)
				#ax[j].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
				ax[j].legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

				j += 1

			#plt.show()
			plt.savefig(output_path + '/histogram_mhalo_' + name[k] + mass_ret + '_' + str(nstep-1) + '_bins.png', dpi=300, format='png')
			plt.close()

			#############
			### MSTAR ###
			#############

			fig, axes = plt.subplots(n_xaxis,n_yaxis, figsize=(10,5), squeeze=False)
			fig.subplots_adjust(wspace=0.3, hspace=0.3)
			fig.suptitle(mass_ret)
			ax = axes.ravel()

			j = 0

			for i in range(nstep-1):

				selcom = (logmstarcom <= step_mstar[nstep-i-1]) & (logmstarcom > step_mstar[nstep-i-2])
				logmstar_com = logmstarcom[selcom]
				mdot_com = ma.log10(mdotcom[selcom]).filled(0)
				f_com = ma.log10(fcom[selcom]).filled(0)
				selmdc = (logmstarmdc <= step_mstar[nstep-i-1]) & (logmstarmdc > step_mstar[nstep-i-2])
				logmstar_mdc = logmstarmdc[selmdc]
				mdot_mdc = ma.log10(mdotmdc[selmdc]).filled(0)
				f_mdc = ma.log10(fmdc[selmdc]).filled(0)

				#ax[j] = fig.add_subplot(231)
				ax[j].hist(f_com, nbins, density=density, facecolor=color_com, label=str('%.2f' %(step_mstar[nstep-i-1])) + '<Mstar<' + str('%.2f' %(step_mstar[nstep-i-2])), alpha=alpha)
				ax[j].hist(f_mdc, nbins, density=density, facecolor=color_mdc, alpha=alpha)
				ax[j].set_xlim(xmin,xmax)
				#ax[j].set_ylim(0,70)
				ax[j].set_xscale('linear')
				ax[j].set_yscale('linear')
				ax[j].set_xlabel(r'$log(\dot{M}_{t}\; / \; \dot{M}_{edd})$')
				ax[j].set_ylabel('#')
				#ax[j].grid(color='k', ls='--', lw=0.25)
				#ax[j].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
				ax[j].legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

				j += 1

			#plt.show()
			plt.savefig(output_path + '/histogram_mstar_' + name[k] + mass_ret + '_' + str(nstep-1) + '_bins.png', dpi=300, format='png')
			plt.close()
