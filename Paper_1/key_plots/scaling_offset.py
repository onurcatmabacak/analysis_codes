import numpy as np
import glob
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.colors as mcolors
import matplotlib as mlp
from scipy.optimize import curve_fit
from scipy import stats
from scipy.stats import chisquare as chi
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import WMAP9, z_at_value

def McConnell_Ma2013(vel_disp):
	return 10.**( 8.32 ) * ( vel_disp/200.0 )**(5.64)

def Reines2015(Mstar):
   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

def fitfunc_mass(mass, a, b):
	return  10.**(a) * (mass/1e11)**(b)

def fitfunc_vel(vel, a, b):
	return 10.**(a) * (vel/200.0)**(b)

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

def func(z, x, y):
	return x * np.log10(1.0+z) + y

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'
extra_list = [12, 13, 33, 34, 35, 36]
time_list = [30, 31, 32]

down_sample = [12.004, 11.6, 11.205, 10.807, 10.396, 10.0, 9.798, 9.602, 9.404, 9.203, 9.0, 8.796, 8.6, 8.403, 8.197, 8.0, 7.904, 7.802, 7.703, 7.597, 7.401, 7.303, 7.199, 7.0, 6.897, 6.797, 6.7, 6.599, 6.501, 6.399, 6.301, 6.199, 6.01]

data = {}

for merger in merger_list:

	for center in center_list:
		for mass_ret in mass_ret_list:

			props = ['redshift', 'mbh', 'mstar', 'mbulge', 'vel_disp']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for i in range(37):

				filename = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5'

				if i in extra_list:

					read_path = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i]
					remove = len(read_path) + 6
					filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

					halo_ID_list = []
					for f in filelist:
						halo_ID_list.append(f[remove:-4])

					halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
				else:
					halo_ID_list = [0]

				f = h5py.File(filename, 'r')

				for halo_ID in halo_ID_list:

					dummy_mbh = f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
					dummy_redshift = f['halo_' + str(halo_ID) + '/total/rvir/z_1'].value
					dummy_mstar = f['halo_' + str(halo_ID) + '/total/rvir/mstar'].value
					dummy_mbulge = f['halo_' + str(halo_ID) + '/total/rvir/mbulge'].value
					dummy_vel_disp = 	f['halo_' + str(halo_ID) + '/total/rvir/vel_disp'].value				

					# down sample B762s so that we do not have selection bias beyond reshift 6
					if i in time_list:
						zip_sel = [[x,y,z,w,q] for x,y,z,w,q in zip(dummy_redshift, dummy_mbh, dummy_mstar, dummy_vel_disp, dummy_mbulge) if x in down_sample]	
						dummy_redshift = np.array(zip(*zip_sel)[0])
						dummy_mbh = np.array(zip(*zip_sel)[1])
						dummy_mstar = np.array(zip(*zip_sel)[2])
						dummy_vel_disp = np.array(zip(*zip_sel)[3])
						dummy_mbulge = np.array(zip(*zip_sel)[4])

					onur_sel = (dummy_mstar >= 1e6)
					dummy_redshift = dummy_redshift[onur_sel]
					dummy_mbh = dummy_mbh[onur_sel]
					dummy_mstar = dummy_mstar[onur_sel]
					dummy_mbulge = dummy_mbulge[onur_sel]
					dummy_vel_disp = dummy_vel_disp[onur_sel]

					data[center + '/' + merger + '/redshift'].append(dummy_redshift)
					data[center + '/' + merger + '/mbh'].append(dummy_mbh)
					data[center + '/' + merger + '/mstar'].append(dummy_mstar)
					data[center + '/' + merger + '/mbulge'].append(dummy_mbulge)
					data[center + '/' + merger + '/vel_disp'].append(dummy_vel_disp)

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

fig, axes = plt.subplots(2,2, figsize=(20,10))
fig.subplots_adjust(wspace=0.0, hspace=0.0)

cm = mcolors.ListedColormap(['red', 'orange', 'yellow', 'green', 'blue', 'magenta', 'brown', 'black'])
c_array = np.array([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 9.0, 12.0])
norm = mlp.colors.BoundaryNorm(c_array, cm.N)

for ax, merger, center, flag in zip(axes.flat, ['merger', 'nomerger', 'merger', 'nomerger'], ['MDC', 'MDC', 'COM', 'COM'], [1, 2, 3, 4]):

	z = data[center + '/' + merger + '/redshift']
	mbh = data[center + '/' + merger + '/mbh']
	mstar = data[center + '/' + merger + '/mstar']
	mbulge = data[center + '/' + merger + '/mbulge']
	vel_disp = data[center + '/' + merger + '/vel_disp']

	x = np.log10(mbh)
	y = np.log10(vel_disp)
	z = np.log10(mstar)
	scale1 = np.log10(1e11)
	scale2 = np.log10(200.0)

	slope1, intercept1, r_value1, p_value1, std_err1 = stats.linregress(z-scale1,x)
	slope2, intercept2, r_value2, p_value2, std_err2 = stats.linregress(z-scale2,y)

	mbh_fit = x - (slope1 * (z-scale1) + intercept1)
	vel_disp_fit = y - (slope2 * (z-scale2) + intercept2)

	slope, intercept, r_value, p_value, std_err = stats.linregress(vel_disp_fit, mbh_fit)
	
	mass_list = np.array([4,5,6,7,8,9,10,11,12,13,14])
	vel_list = np.array([0.1, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0])

	ax.scatter(vel_disp_fit, mbh_fit, color='k', marker='.', s=20, alpha=0.3)
	ax.plot(vel_disp_fit, slope*vel_disp_fit+intercept, color='r', lw=2, ls='-', label=r'$\propto \sigma^{%5.3f}$' %slope)
	ax.set_xscale('linear')
	ax.set_yscale('linear')
	ax.set_xlim(-1.5, 1.5)
	ax.set_ylim(-1.5, 1.5)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)

	ax.xaxis.set_major_locator(ticker.FixedLocator([0.5, 1.0, 1.5, 2.0, 2.5]))	
	ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))	
	ax.yaxis.set_major_locator(ticker.FixedLocator([4, 5, 6, 7, 8]))	
	ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.5))	

	ax.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)

	#if (flag == 2) or (flag == 4):
	#	ax.yaxis.set_tick_params(labelsize=1e-100)

	#ax.text(8.0, -2.7, '%s' %(merger + ' ' + center), fontsize=20)
	ax.legend(loc=2, fontsize=12, frameon=False, scatterpoints=1)

plt.show()
#fig.subplots_adjust(left=0.8)
#cbar = fig.colorbar(sc, pad=0.05, ticks=c_array, ax=axes.ravel().tolist())
#cbar.set_label('    z', rotation=0, fontsize=30)
#cbar.ax.tick_params(labelsize=30)
#cbar.solids.set(alpha=1)
#plt.tight_layout()
#plt.show()
#quit()

#fig.savefig('scaling_offset.pdf')
#plt.close()
