import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def merger_statistics(xxx, pos, descendant_list, regime, sim_suite, sim_name):

	if regime == 'COM':
		AHF_path = 'AHF_com'
	else:
		AHF_path = 'AHF'

	filepath = '/bulk1'


sim_name = ['h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite=['MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
label = ['A4', 'A1', 'A2', 'A8']
color = ['b', 'k', 'r', 'g']
linestyle = ['-', '-', '-', '-']
locator = [1, 1, 1, 2]

regime_list = ['MDC']
i_list = [0, 1, 2, 3]

for i in i_list:

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0.0, hspace=0.0)
	fig.suptitle(label[i])
	ax = fig.add_subplot(111)	

	x = {}

	for regime in regime_list:

		filename = '/bulk1/feldmann/Onur/' + regime + '/' + sim_suite[i] + '/' + sim_name[i] + '/'

		redshift, position = np.genfromtxt(filename+'zred_pos_list', delimiter='', usecols=(0,1), skip_header=0, unpack=True)
		position = position.astype(int)

		z_1 = []
		ID = []
		position = []
		mstar = []
		mhalo = []
		z_2 = []
		descendant = []

		file_halo = open(filename+'halo_0.txt', 'r')
		for line in file_halo.readlines():
			data = map(float, np.array(line.split()))
			z_1.append(data[0])
			position.append(redshift.tolist().index(data[0]))
			ID.append(int(data[1]))
			mstar.append(data[2])
			mhalo.append(data[3])
			z_2.append(data[4])
			descendant.append([int(xxx) for xxx in data[5:]])
		file_halo.close()

		z_1 = np.array(z_1)
		position = np.array(position)
		ID = np.array(ID)
		mstar = np.array(mstar)
		mhalo = np.array(mhalo)
		z_2 = np.array(z_2)
		descendant = np.array(descendant)

		if sim_suite[i] == 'MassiveFIRE2':
			radius_name = '0.1'
		else:
			radius_name = '0.1'

		data = h5py.File(filename + sim_name[i] + '_merger.hdf5','r')
		mbh = data['total/r_' + radius_name + '/mass_ret_0.1/seed_1e4/m_bh_torque'].value
		mbh = mbh[::-1]

		#major_merger = []
		#minor_merger = []
		tot_merger = []

		for xxx in range(len(z_1)):
			
			#major, minor = merger_statistics(xxx, position[xxx], descendant[xxx], regime, sim_suite[i], sim_name[i])			
			#major_merger.append(major)
			#minor_merger.append(minor)

			inds = len(descendant[xxx])-1
			tot_merger.append(inds)
		
		x[regime] = tot_merger * z_1

	#ax.hist(x['COM'][x['COM'] != 0], 20, density=False, facecolor='r', alpha=0.3, label='COM')
	ax.hist(x['MDC'][x['MDC'] != 0], 20, density=False, facecolor='b', alpha=0.3, label='MDC')

	ax.set_xscale('linear')
	ax.set_yscale('linear')
	ax.set_xlim(0,10)
	#ax.set_ylim(0,10)
	ax.xaxis.set_major_locator(ticker.MultipleLocator(1))
	ax.yaxis.set_major_locator(ticker.MultipleLocator(locator[i]))
	ax.set_xlabel(r'$z$')
	ax.set_ylabel(r'$\# \; of \; mergers$')
	ax.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.show()
	#plt.tight_layout()
	#plt.savefig('plot_#9_cumulative_#_of_merger_' + plotname[j] + '.png', dpi=300, format='png')
	#plt.close()
