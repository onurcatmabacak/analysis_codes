import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from astropy.cosmology import Planck15, z_at_value
from astropy import constants as const
from astropy import units as u
from astropy.cosmology import FlatLambdaCDM

def time(i):
	return cosmo.age(i).value * 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

MF2_sim_list = ['h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR']

MF1_sim_list = ['B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00002_baryon_toz2', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR']

label = ['113', '206', '29', '2', 'B762_0', 'B762_1', 'B762_2']
fontsize = 30

for i in range(len(MF2_sim_list)):

	fig = plt.figure(1, figsize=(15,10))
	fig.suptitle(label[i], fontsize=fontsize)

	ax0 = fig.add_subplot(111)	
	ax0.set_xscale('linear')
	ax0.set_yscale('log')

	z_min = []
	z_max = []
	ssfr_min = []
	ssfr_max = []

	for j in range(2):

		if j == 0:
			suite = 'MassiveFIRE'
			sim = MF1_sim_list[i]
			color = 'k'
		if j == 1:
			suite = 'MassiveFIRE2'
			sim = MF2_sim_list[i]	
			color = 'r'

		filepath = '/bulk1/feldmann/Onur/MDC/' + suite + '/' + sim + '/' + sim + '_merger.hdf5'
		data = h5py.File(filepath,'r')
		
		redshift = data['total/rvir/z_1'].value
		ssfr_gal = data['total/rvir/ssfr'].value
		t = time(redshift)

		step = 1e8

		t_list = np.arange(np.min(t), np.max(t), step)
		t_list = np.append(t_list, np.max(t))

		t1 = []
		ssfr_gal1 = []
		ssfr_inner1 = []	

		for xxx in range(len(t_list)-1):

			sel = (t >= t_list[xxx]) & (t <= t_list[xxx+1])
			t_new = t[sel]
			ssfr_gal_new = ssfr_gal[sel]
			
			t1.append(np.median(t_new))
			ssfr_gal1.append(np.median(ssfr_gal_new))

		t1 = np.array(t1)
		ssfr_gal1 = np.array(ssfr_gal1)

		z1 = np.array([z_at_value(Planck15.age, xxx * 1e-9 * u.Gyr) for xxx in t1])

		#ax0.plot(z1, ssfr_gal1, color=color, ls='-', lw=2, label=suite)
		ax0.plot(redshift, ssfr_gal, color=color, ls='-', lw=2, label=suite)

		sel = (ssfr_gal1 > 0.0)
		z_min.append(np.min(z1))
		z_max.append(np.max(z1))
		ssfr_min.append(np.min(ssfr_gal1[sel]))
		ssfr_max.append(np.max(ssfr_gal1[sel]))

	z_min = np.array(z_min)
	z_max = np.array(z_max)
	ssfr_min = np.array(ssfr_min)
	ssfr_max = np.array(ssfr_max)
	
	ax0.set_xlim(0.9*np.min(z_min), 1.1*np.max(z_max))
	ax0.set_ylim(0.1*np.min(ssfr_min), 10.0*np.max(ssfr_max))
	ax0.xaxis.set_major_locator(plt.MultipleLocator(1))
	ax0.set_xlabel(r'$z$', fontsize=fontsize)
	#ax0.set_xlabel(r'$time\;(Gyr)$')
	ax0.set_ylabel(r'$sSFR\;(Gyr^{-1})$', fontsize=fontsize)
	ax0.xaxis.set_tick_params(labelsize=fontsize)
	ax0.yaxis.set_tick_params(labelsize=fontsize)
	ax0.legend(loc=0,prop={'size':fontsize},frameon=False,scatterpoints=1)

	#plt.show()
	#plt.tight_layout()
	fig.savefig('ssfr_vs_z_' + label[i] + '.pdf')
	plt.close()
