import numpy as np
import h5py
import matplotlib.pyplot as plt
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value


def time(i):
	return cosmo.age(i).value #* 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

sim_name_MF2 = ['h206_HR_sn1dy300ro100ss']
sim_suite_MF2 = ['MassiveFIRE2']
label = ['M_{seed}=10^1\;M_{\odot}', 'M_{seed}=10^2\;M_{\odot}', 'M_{seed}=10^3\;M_{\odot}', 'M_{seed}=10^4\;M_{\odot}', 'M_{seed}=10^5\;M_{\odot}']
seed_list = ['seed_1e1', 'seed_1e2', 'seed_1e3', 'seed_1e4', 'seed_1e5']
color = ['r', 'g', 'b', 'k', 'm']
xmax = [12]

for i in range(len(sim_name_MF2)):
	data = h5py.File('/bulk1/feldmann/Onur/MDC/' + sim_suite_MF2[i] + '/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_merger.hdf5','r')

	redshift = data['total/rvir/z_1'].value
	time = cosmo.age(redshift).value
	mbh_gtda = []
	mbh_sfr = []
	mbh_dyn = []
	for j in range(len(seed_list)):
		mbh_gtda.append(data['total/r_0.1/mass_ret_0.1/' + seed_list[j] + '/m_bh_torque'].value)
		mbh_sfr.append(data['total/r_0.1/mass_ret_0.1/' + seed_list[j] + '/m_bh_sfr'].value)
		mbh_dyn.append(data['total/r_0.1/mass_ret_0.1/' + seed_list[j] + '/m_bh_dyn_3'].value)
		
	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)

	ax1 = fig.add_subplot(311)	

	for j in range(len(seed_list)):
		ax1.plot(time, mbh_gtda[j], color=color[j], ls='-', lw=2, label=r'$%s$' %label[j])   

	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(0,6.2)
	ax1.set_ylim(1e0,1e9)
	ax1.xaxis.set_major_locator(plt.FixedLocator([0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0]))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1e2,1e4,1e6,1e8]))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_xlabel(r'$time\;(Gyr)$')
	ax1.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		#dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		dummy_age = z_at_value(Planck13.age, X * u.Gyr)
		ax12Xs.append("%.1f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	ax12.set_xlabel('z')
	ax1.text(0.05, 1e8, 'gtda')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax2 = fig.add_subplot(312)	

	for j in range(len(seed_list)):
		ax2.plot(time, mbh_sfr[j], color=color[j], ls='-', lw=2)   

	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(0,6.2)
	ax2.set_ylim(1e0,1e9)
	ax2.xaxis.set_major_locator(plt.FixedLocator([0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0]))
	ax2.yaxis.set_major_locator(plt.FixedLocator([1e2,1e4,1e6,1e8]))
	ax2.tick_params(axis='y', labelsize=8)
	ax2.set_xlabel(r'$time\;(Gyr)$')
	ax2.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
	ax2.text(0.05, 1e8, 'sfr')
	#ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	ax3 = fig.add_subplot(313)	

	for j in range(len(seed_list)):
		ax3.plot(time, mbh_dyn[j], color=color[j], ls='-', lw=2)   

	ax3.set_xscale('linear')
	ax3.set_yscale('log')
	ax3.set_xlim(0,6.2)
	ax3.set_ylim(1e0,1e9)
	ax3.xaxis.set_major_locator(plt.FixedLocator([0.5,1.0,1.5,2.0,2.5,3.0,3.5,4.0,4.5,5.0,5.5,6.0]))
	ax3.yaxis.set_major_locator(plt.FixedLocator([1e2,1e4,1e6,1e8]))
	ax3.tick_params(axis='y', labelsize=8)
	ax3.set_xlabel(r'$time\;(Gyr)$')
	ax3.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
	ax3.text(0.05, 1e8, 'dyn')
	#ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	plt.show()
	#plt.savefig(label[i] + '.png', dpi=300, format='png')
	#plt.close()

