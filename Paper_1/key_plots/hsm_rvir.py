import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

sim_name_list = ['B100_N512_TL00206_baryon_toz0_HR_9915','h206_HR_sn1dy300ro100ss']
sim_suite_list = ['MassiveFIRE', 'MassiveFIRE2']
center_list = ['MDC', 'COM']
regime = 'total'
radius = 'rvir'

x_min = [0]
x_max = [9.2]
y1_min = [0,0]
y1_max = [8,3]
y2_min = [1e-3,1e-3]
y2_max = [2e-1,2e-1]

for center in center_list:

	title= 'A1 ' + center

	redshift = []
	r_hsm = []
	r_hsm_vir = []

	for sim_name, sim_suite in zip(sim_name_list,sim_suite_list):

		filename = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite + '/' + sim_name + '/' + sim_name + '_merger.hdf5'
		data = h5py.File(filename,'r')

		redshift.append(data[regime + '/' + radius + '/z_1'].value)
		rvir = data[regime + '/' + radius + '/radius'].value * 10.0
		rhsm = data[regime + '/' + radius + '/rvir_hsm'].value
		r_hsm.append(rhsm)
		r_hsm_vir.append(rhsm / rvir)

	fig = plt.figure(1)
	fig.suptitle(title)
	fig.subplots_adjust(wspace=0, hspace=0)

	ax1 = fig.add_subplot(211)	
	ax2 = ax1.twinx()
	ax1.plot(redshift[0], r_hsm[0], color='k', ls='-', lw=2, label=r'$R_{HSM}$')
	ax2.plot(redshift[0], r_hsm_vir[0], color='r', ls='-', lw=2, label=r'$R_{HSM}/R_{VIR}$')
	ax1.set_xscale('linear')
	ax1.set_yscale('linear')
	ax2.set_yscale('log')
	ax1.set_xlim(x_min[0], x_max[0])
	ax1.set_ylim(y1_min[0], y1_max[0])
	ax2.set_ylim(y2_min[0], y2_max[0])
	ax1.xaxis.set_major_locator(plt.FixedLocator(np.arange(x_min[0]+1, x_max[0])))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1,2,3,4,5,6,7]))
	ax2.yaxis.set_major_locator(plt.FixedLocator([1e-3,1e-2,1e-1]))
	#ax2.spines['right'].set_color('red')
	ax1.tick_params(axis='y', labelsize=8)
	ax2.tick_params(axis='y', labelsize=8, colors='red')
	ax1.set_xlabel(r'$z$')
	ax1.set_ylabel(r'$R_{HSM}\;(kpc)$')
	ax1.text(0.5,0.5,'MF1')
	ax1.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
	ax2.legend(loc=1,prop={'size':8},frameon=False,scatterpoints=1)

	ax3 = fig.add_subplot(212)	
	ax4 = ax3.twinx()
	ax3.plot(redshift[1], r_hsm[1], color='k', ls='-', lw=2)
	ax4.plot(redshift[1], r_hsm_vir[1], color='r', ls='-', lw=2)
	ax3.set_xscale('linear')
	ax3.set_yscale('linear')
	ax4.set_yscale('log')
	ax3.set_xlim(x_min[0],x_max[0])
	ax3.set_ylim(y1_min[1], y1_max[1])
	ax4.set_ylim(y2_min[1], y2_max[1])
	ax3.xaxis.set_major_locator(plt.FixedLocator(np.arange(x_min[0]+1, x_max[0])))
	ax3.yaxis.set_major_locator(plt.FixedLocator([0.5,1.0,1.5,2.0,2.5]))
	ax4.yaxis.set_major_locator(plt.FixedLocator([1e-3,1e-2,1e-1]))
	ax3.tick_params(axis='y', labelsize=8)
	ax4.tick_params(axis='y', labelsize=8, colors='red')
	ax3.set_xlabel(r'$z$')
	ax3.set_ylabel(r'$R_{HSM}\;(kpc)$')
	ax3.text(0.5,0.25,'MF2')
	ax3.legend(loc=2,prop={'size':8},frameon=False,scatterpoints=1)
	ax4.legend(loc=1,prop={'size':8},frameon=False,scatterpoints=1)

	#plt.show()
	fig.savefig('z_vs_rhsmvir_' + center + '.pdf')
	plt.close()

