import numpy as np
import h5py
import matplotlib.pyplot as plt
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

sim_name_MF2 = ['h206_HR_sn1dy300ro100ss']
sim_suite_MF2 = ['MassiveFIRE2']
label = ['5\%', '10\%', '25\%', '50\%', '100\%']
eps = ['0.05', '0.1', '0.25', '0.5', '1.0']
color = ['r', 'g', 'b', 'k', 'm']
regime_list = ['COM', 'MDC']
fontsize = 30
halo_ID = 0

for i in range(len(sim_name_MF2)):

	fig = plt.figure(1, figsize=(12,8))
	fig.subplots_adjust(wspace=0, hspace=0)

	mbh = {}

	redshift = {}
	time = {}

	for regime in regime_list:

		data = h5py.File('/bulk1/feldmann/Onur/' + regime + '/' + sim_suite_MF2[i] + '/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_merger.hdf5','r')

		redshift[regime] = data['halo_' + str(halo_ID) + '/total/rvir/z_1'].value
		time[regime] = cosmo.age(redshift[regime]).value

		for j in range(len(eps)):
			mbh[regime + '/' + eps[j]] = data['halo_' + str(halo_ID) + '/total/r_0.1/mass_ret_' + eps[j] + '/seed_1e4/m_bh_torque'].value

	ax1 = fig.add_subplot(111)	
	#ax1.set_title('COM')

	#ax1.plot(0.0, 0.0, color='k', ls='-', lw=2, label='total gas')
	
	for j in range(len(eps)):
		ax1.plot(redshift['MDC'], mbh['MDC/' + eps[j]], color=color[j], ls='-', lw=2, label=r'$%s$' %label[j])   
		
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(0.8,8.9)
	ax1.set_ylim(8e3, 2e9)
	ax1.xaxis.set_major_locator(plt.MultipleLocator(1))
	ax1.xaxis.set_minor_locator(plt.MultipleLocator(0.2))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1e4,1e5,1e6,1e7,1e8,1e9]))
	ax1.xaxis.set_tick_params(labelsize=fontsize)
	ax1.yaxis.set_tick_params(labelsize=fontsize)
	ax1.set_xlabel('z', fontsize=fontsize)
	#ax1.tick_params(axis='y', labelsize=fontsize)
	ax1.set_ylabel(r'$M_{BH} \; (M_{\odot})$', fontsize=fontsize)
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		#dummy_age = z_at_value(Planck13.age, X * u.Gyr)
		ax12Xs.append("%.1f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	ax12.set_xlabel(r'$time\;(Gyr)$', fontsize=fontsize)
	ax12.tick_params(axis='x', labelsize=fontsize)

	ax1.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax12.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax1.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)
	ax12.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)

	ax1.legend(loc=0,prop={'size':fontsize},frameon=False,scatterpoints=1)
	#ax1.grid(b=None, which='major', axis='both')

	plt.show()
	#plt.tight_layout()
	fig.savefig('bh_growth_eps.pdf')
	plt.close()

