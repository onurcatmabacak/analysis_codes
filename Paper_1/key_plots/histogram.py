import numpy as np
from numpy import *
import os
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value
from astropy.cosmology import FlatLambdaCDM
import math

def time(i):
	return cosmo.age(i).value # Gyr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
file_list = {}

mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'
output_path = 'histogram'

if not os.path.exists(output_path):
    os.makedirs(output_path)

data = {}

for merger in merger_list:
	for center in center_list:
		file_list[center + '/' + merger] = sorted(glob.glob('/bulk1/feldmann/Onur/' + center + '/MassiveFIRE*/*/*_' + merger + '.hdf5'))
		for mass_ret in mass_ret_list:

			props = ['mdot', 'f', 'z', 't', 'mhalo', 'mstar', 'mbh']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for filename in file_list[center + '/' + merger]:
				f = h5py.File(filename, 'r')

				data[center + '/' + merger + '/mdot'].append(f['total/' + radius + '/' + mass_ret + '/seed_1e4/mdot_torque'].value)
				data[center + '/' + merger + '/f'].append(f['total/' + radius + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value)
				data[center + '/' + merger + '/z'].append(f['total/rvir/z_1'].value)
				data[center + '/' + merger + '/t'].append(time(f['total/rvir/z_1'].value))
				data[center + '/' + merger + '/mhalo'].append(np.log10(f['total/rvir/mhalo_rvir'].value))
				data[center + '/' + merger + '/mstar'].append(np.log10(f['total/rvir/mstar'].value))
				data[center + '/' + merger + '/mbh'].append(np.log10(f['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value))

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

	name_list = ['z', 'mhalo', 'mstar', 'mbh']
	label_list = ['<z<', '>log(M_{H})>', '>log(M_{*})>', '>log(M_{BH})>']

	for name, label in zip(name_list, label_list):

		# binning of redshift wrt MF2
		nstep_list = [5]
		n_xaxis_list = [2]
		n_yaxis_list = [2]

		for nstep, n_xaxis, n_yaxis in zip(nstep_list, n_xaxis_list, n_yaxis_list):

			if name == 'z':

				#step = np.linspace(np.min(data[center + '/' + merger + '/t']), time(1.0), nstep)
				step = np.linspace(time(7.0), time(1.0), nstep)

				z_step = []
				for xxx in step:
					z_step.append(z_at_value(Planck13.age, xxx*u.Gyr))

				z_step = [7.0, 4.0, 2.1, 1.4, 1.0]

			else:
				z_step = np.linspace(np.min(data[center + '/' + merger + '/' + name]), np.max(data[center + '/' + merger + '/' + name]), nstep)

			nbins = 30
			alpha = 0.3
			color_com = 'r'
			color_mdc = 'b'
			density = False
			xmin = -3
			xmax = 1.0

			# define dataset here 
			zcom = data['COM/' + merger + '/' + name]
			zmdc = data['MDC/' + merger + '/' + name]
			mdotcom = data['COM/' + merger + '/mdot']
			mdotmdc = data['MDC/' + merger + '/mdot']
			fcom = data['COM/' + merger + '/f']
			fmdc = data['MDC/' + merger + '/f']

			fig, axes = plt.subplots(n_xaxis, n_yaxis, figsize=(12,8), squeeze=False)
			fig.subplots_adjust(wspace=0.3, hspace=0.3)
			#fig.suptitle(mass_ret)
			ax = axes.ravel()

			fontsize= 20

			j = 0
			for i in range(nstep-1):

				if name == 'z':
					selcom = (zcom >= z_step[nstep-i-1]) & (zcom < z_step[nstep-i-2])
					f_com = ma.log10(fcom[selcom]).filled(0)
					selmdc = (zmdc >= z_step[nstep-i-1]) & (zmdc < z_step[nstep-i-2])
					f_mdc = ma.log10(fmdc[selmdc]).filled(0)
				else:
					selcom = (zcom < z_step[nstep-i-1]) & (zcom >= z_step[nstep-i-2])
					f_com = ma.log10(fcom[selcom]).filled(0)
					selmdc = (zmdc < z_step[nstep-i-1]) & (zmdc >= z_step[nstep-i-2])
					f_mdc = ma.log10(fmdc[selmdc]).filled(0)

				#ax[j] = fig.add_subplot(231)
				ax[j].plot(1e-10,1e-10, color='k', ls='-', lw=1e-100, label=str('%.1f' %(z_step[nstep-i-1])) + r'$' + label + '$' + str('%.1f' %(z_step[nstep-i-2])), alpha=alpha) 
				n1,bins1,patches1 = ax[j].hist(f_com, nbins, density=density, facecolor=color_com, alpha=alpha, histtype='stepfilled', lw=2)
				n2,bins2,patches2 = ax[j].hist(f_mdc, nbins, density=density, facecolor=color_mdc, alpha=alpha, histtype='stepfilled', lw=2)
				ax[j].set_xlim(xmin,xmax)
				ax[j].set_ylim(0,max(np.max(n1),np.max(n2))*1.5)
				ax[j].set_xscale('linear')
				ax[j].set_yscale('linear')
				ax[j].xaxis.set_tick_params(labelsize=fontsize)
				ax[j].yaxis.set_tick_params(labelsize=fontsize)
				#ax[j].set_xlabel(r'$log(\dot{M}_{t} \; / \; \dot{M}_{edd})$', fontsize=fontsize)
				ax[j].set_xlabel(r'$log(\lambda_{t})$', fontsize=fontsize)
				ax[j].set_ylabel('#', fontsize=fontsize)
				#ax[j].grid(color='k', ls='--', lw=0.25)
				#ax[j].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
				ax[j].legend(loc=0, prop={'size':fontsize}, frameon=False, scatterpoints=1)

				j += 1

			#plt.show()
			plt.tight_layout()
			fig.savefig(output_path + '/histogram_' + merger + '_' + name + '_' + str(nstep-1) + '_bins.pdf')
			plt.close()

