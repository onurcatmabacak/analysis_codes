import numpy as np
import glob
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.colors as mcolors
import matplotlib as mlp
from scipy.optimize import curve_fit
from scipy import stats
from scipy.stats import chisquare as chi
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import WMAP9, z_at_value

def Reines2015(Mstar):
   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

def func(z, x, y):
	return x * np.log10(1.0+z) + y

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'
extra_list = [12, 13, 33, 34, 35, 36]
time_list = [30, 31, 32]

down_sample = [12.004, 11.6, 11.205, 10.807, 10.396, 10.0, 9.798, 9.602, 9.404, 9.203, 9.0, 8.796, 8.6, 8.403, 8.197, 8.0, 7.904, 7.802, 7.703, 7.597, 7.401, 7.303, 7.199, 7.0, 6.897, 6.797, 6.7, 6.599, 6.501, 6.399, 6.301, 6.199, 6.01]

data = {}

for merger in merger_list:

	for center in center_list:
		for mass_ret in mass_ret_list:

			props = ['ratio', 'redshift']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for i in range(37):

				filename = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5'

				if i in extra_list:

					read_path = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i]
					remove = len(read_path) + 6
					filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

					halo_ID_list = []
					for f in filelist:
						halo_ID_list.append(f[remove:-4])

					halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
				else:
					halo_ID_list = [0]

				f = h5py.File(filename, 'r')

				for halo_ID in halo_ID_list:

					dummy_mbh = f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
					dummy_redshift = f['halo_' + str(halo_ID) + '/total/rvir/z_1'].value
					dummy_mstar = f['halo_' + str(halo_ID) + '/total/rvir/mstar'].value
					
					# down sample B762s so that we do not have selection bias beyond reshift 6
					if i in time_list:
						zip_sel = [[x,y,z] for x,y,z in zip(dummy_redshift, dummy_mbh, dummy_mstar) if x in down_sample]	
						dummy_redshift = np.array(zip(*zip_sel)[0])
						dummy_mbh = np.array(zip(*zip_sel)[1])
						dummy_mstar = np.array(zip(*zip_sel)[2])

					onur_sel = (dummy_mstar >= 1e9)
					dummy_redshift = dummy_redshift[onur_sel]
					dummy_mbh = dummy_mbh[onur_sel]
					dummy_mstar = dummy_mstar[onur_sel]

					ratio_mbh_mstar = np.log10(dummy_mbh / dummy_mstar)
					ratio_reines15 = np.log10(Reines2015(dummy_mstar) / dummy_mstar)
					diff = ratio_mbh_mstar - ratio_reines15
					
					data[center + '/' + merger + '/ratio'].append(diff)
					data[center + '/' + merger + '/redshift'].append(dummy_redshift)

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

fig, axes = plt.subplots(2,2, figsize=(20,10))
fig.subplots_adjust(wspace=0.0, hspace=0.0)

cm = mcolors.ListedColormap(['red', 'orange', 'yellow', 'green', 'blue', 'magenta', 'brown', 'black'])
c_array = np.array([0.0, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 9.0, 12.0])
norm = mlp.colors.BoundaryNorm(c_array, cm.N)

for ax, merger, center, flag in zip(axes.flat, ['merger', 'nomerger', 'merger', 'nomerger'], ['MDC', 'MDC', 'COM', 'COM'], [1, 2, 3, 4]):

	z = data[center + '/' + merger + '/redshift']
	ratio = data[center + '/' + merger + '/ratio']

	# PLOT
	#ax.set_title('%s' %(merger + ' ' + center), fontsize=20)
	#ax.scatter(z, ratio, color='k', alpha=0.1, marker='.', s=40)

	alpha = 0.5

	ratio_min = []	
	ratio_max = []
	z_mean = []
	t_arr = np.linspace(time(12), time(0), 200, endpoint=True)
	for xx in range(len(t_arr)-1):

		z1_arr = z_at_value(WMAP9.age, t_arr[xx] * u.Gyr)
		z2_arr = z_at_value(WMAP9.age, t_arr[xx+1] * u.Gyr)

		sel = (z < z1_arr) & (z > z2_arr)
		ratio_min.append(np.min(ratio[sel]))
		ratio_max.append(np.max(ratio[sel]))
		z_mean.append((z1_arr + z2_arr) / 2.0)

	ax.fill_between(z_mean, ratio_min, ratio_max, color='k', zorder=0, alpha=0.2)

	nbins = 20
	n, _ = np.histogram(z, bins=nbins)
	sy, _ = np.histogram(z, bins=nbins, weights=ratio)
	sy2, _ = np.histogram(z, bins=nbins, weights=ratio*ratio)
	mean = sy / n
	std = np.sqrt(sy2/n - mean*mean)

	#ax.scatter(z, ratio, color='b', alpha=0.5)
	z_arr = (_[1:] + _[:-1])/2
	ax.errorbar(z_arr, mean, yerr=std, fmt='bo', capsize=10, elinewidth=2)	

	z_dummy = np.linspace(0,15,200, endpoint=True)
	slope, intercept, r_value, p_value, std_err = stats.linregress(np.log10(1.0+z_arr), mean)
	popt, pcov = curve_fit(func, z_arr, mean)
	perr = np.sqrt(np.diag(pcov))
	ax.plot(z_dummy, intercept + slope * np.log10(z_dummy+1.0), color='k', ls='-', lw=2, label=r'$\Delta log(M_{BH}/M_*) = %.3f \pm %.3f \; log(1+z) %+.3f \pm %.3f$' %(slope, perr[0], intercept, perr[1]))

	if (flag == 3) or (flag == 4):
		ax.set_xlabel(r'$z$', fontsize=30)
	if (flag == 1) or (flag == 3):
		ax.set_ylabel(r'$\Delta log(M_{BH} / M_*)$', fontsize=30)

	ax.set_xscale('linear')
	ax.set_yscale('linear')
	ax.set_xlim(-0.2, 13.0)
	ax.set_ylim(-3.0, 1.0)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)

	ax.xaxis.set_major_locator(ticker.FixedLocator(range(1,13)))	
	ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.5))	
	ax.yaxis.set_major_locator(ticker.FixedLocator([-2.5, -2.0, -1.5, -1.0, -0.5, 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5]))	
	ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))	

	ax.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)

	if (flag == 2) or (flag == 4):
		ax.yaxis.set_tick_params(labelsize=1e-100)

	ax.text(8.0, -2.7, '%s' %(merger + ' ' + center), fontsize=20)
	ax.legend(loc=2, fontsize=12, frameon=False, scatterpoints=1)

#fig.subplots_adjust(left=0.8)
#cbar = fig.colorbar(sc, pad=0.05, ticks=c_array, ax=axes.ravel().tolist())
#cbar.set_label('    z', rotation=0, fontsize=30)
#cbar.ax.tick_params(labelsize=30)
#cbar.solids.set(alpha=1)
#plt.tight_layout()
#plt.show()
#quit()

fig.savefig('scaling_rel_z_evolution_avrg_mstar_1e9_bh_1e4.pdf')
plt.close()
