import numpy as np
import h5py
import matplotlib.pyplot as plt
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

sim_name_MF2 = ['h206_HR_sn1dy300ro100ss']
sim_suite_MF2 = ['MassiveFIRE2']
label = ['M_{seed}=10^1\;M_{\odot}', 'M_{seed}=10^2\;M_{\odot}', 'M_{seed}=10^3\;M_{\odot}', 'M_{seed}=10^4\;M_{\odot}', 'M_{seed}=10^5\;M_{\odot}']
seed_list = ['seed_1e1', 'seed_1e2', 'seed_1e3', 'seed_1e4', 'seed_1e5']
color = ['r', 'g', 'b', 'k', 'm']
xmax = [12]
halo_ID = 0

for i in range(len(sim_name_MF2)):
	data = h5py.File('/bulk1/feldmann/Onur/MDC/' + sim_suite_MF2[i] + '/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_merger.hdf5','r')

	z = data['halo_' + str(halo_ID) + '/total/rvir/z_1'].value
	sel = (z >= 2.0)
	redshift = data['halo_' + str(halo_ID) + '/total/rvir/z_1'].value[sel]
	t = cosmo.age(redshift).value
	mbh_gtda = []
	for j in range(len(seed_list)):
		mbh_gtda.append(data['halo_' + str(halo_ID) + '/total/r_0.1/mass_ret_0.1/' + seed_list[j] + '/m_bh_torque'].value[sel])
		
	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)

	ax1 = fig.add_subplot(111)	

	for j in range(len(seed_list)):
		ax1.plot(t, mbh_gtda[j], color=color[j], ls='-', lw=2, label=r'$%s$' %label[j])   

	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(0.5,time(2.0))
	ax1.set_ylim(1e1,1e8)
	ax1.xaxis.set_major_locator(plt.FixedLocator([0.5,1.0,1.5,2.0,2.5,3.0]))
	ax1.xaxis.set_minor_locator(plt.MultipleLocator(0.1))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1e1,1e2,1e3,1e4,1e5,1e6,1e7,1e8]))
	ax1.xaxis.set_tick_params(labelsize=14)
	ax1.yaxis.set_tick_params(labelsize=14)
	ax1.set_xlabel(r'$time\;(Gyr)$', fontsize=16)
	#ax1.tick_params(axis='y', labelsize=30)
	ax1.set_ylabel(r'$M_{BH}\;(M_{\odot})$', fontsize=20)
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		#dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		dummy_age = z_at_value(Planck13.age, X * u.Gyr)
		ax12Xs.append("%.1f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	ax12.set_xlabel('z', fontsize=16)
	ax12.tick_params(axis='x', labelsize=14)
	ax1.text(0.6, 2e7, 'GTDA')

	ax1.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax12.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax1.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)
	ax12.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)

	ax1.legend(loc=0,prop={'size':12},frameon=False,scatterpoints=1)

	plt.show()
	fig.savefig('figure0.pdf')
	plt.close()

