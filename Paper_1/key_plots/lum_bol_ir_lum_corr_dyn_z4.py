import numpy as np
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib as mlp
import matplotlib.ticker as ticker
import matplotlib.tri as tri
from matplotlib.colors import LogNorm
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

def x_tick_function(lum):
	sfr = lum * 1.49e-10
	result = []
	for x in sfr:
		result.append(str('%.1e' %x))

	return sfr

def y_tick_function(lum):

	eps = 0.1
	solar_lum = 3.839e33
	light_speed = 3e10
	solar_lum = 3.839e33
	solar_mass = 2.0e33
	kpc_to_cm = 3.086e21
	year_to_s = 3.15e7

	mdot = lum * (1.0 - eps) / eps / solar_mass / (light_speed**2.0) * year_to_s * solar_lum

	result = []
	for x in mdot:

		result.append(str('%.1e' %x))

	return mdot

def calculate_luminosity(mdot, sfr, f, mbh):
	
	eps = 0.1
	eta = 4.0
	delta = 5e-4
	alpha_adaf = 0.1
	beta = 1.0 - (alpha_adaf/0.55)
	Grav_constant = 6.67e-8
	light_speed = 3e10
	solar_lum = 3.839e33
	solar_mass = 2.0e33
	kpc_to_cm = 3.086e21
	year_to_s = 3.15e7

	mdot_crit = 1e-3 * (delta / 5e-4) * ((1.0 - beta) / beta) * (alpha_adaf ** 2.0)
	r_isco = 3.0 # R_ISCO / R_sch
	L_bol_TD = (eps / (1.0-eps)) * (mdot * solar_mass / year_to_s) * (light_speed**2.0) / solar_lum # in solar luminosity
	L_edd = 1.26e46 * (mbh * 1e-8) / solar_lum # in solar luminosity

	lum_bol = []

	for i in range(len(mdot)):

		if f[i] < mdot_crit:
			lum = 2e-4 * L_bol_TD[i] * (delta / 5e-4) * ((1.0 - beta) / 0.5) * (6.0/r_isco)

		if (f[i] >= mdot_crit) and (f[i] < 1e-2):
			lum = 0.2 * L_bol_TD[i] * (f[i] / (alpha_adaf**2.0)) * (beta / 0.5) * (6.0 / r_isco)

		if (f[i] >= 1e-2) and (f[i] < eta):
			lum = L_bol_TD[i]

		if f[i] >= eta:
			lum = eta * (1.0 + np.log(f[i] / eta)) * L_edd[i]

		lum_bol.append(lum)

	lum_bol = np.array(lum_bol)

	lum_ir = sfr / 1.49e-10

	return lum_bol, lum_ir

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'
extra_list = [12, 13, 33, 34, 35, 36]

data = {}

for merger in merger_list:

	for center in center_list:
		for mass_ret in mass_ret_list:

			props = ['mbh', 'mdot', 'f', 'sfr', 'redshift']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for i in range(37):

				filename = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5'

				if i in extra_list:

					read_path = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i]
					remove = len(read_path) + 6
					filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

					halo_ID_list = []
					for f in filelist:
						halo_ID_list.append(f[remove:-4])

					halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
				else:
					halo_ID_list = [0]

				f = h5py.File(filename, 'r')

				for halo_ID in halo_ID_list:

					data[center + '/' + merger + '/mdot'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/mdot_dyn_3'].value)
					data[center + '/' + merger + '/mbh'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_dyn_3'].value)
					data[center + '/' + merger + '/f'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/f_dyn_edd_3'].value)
					data[center + '/' + merger + '/redshift'].append(f['halo_' + str(halo_ID) + '/total/rvir/z_1'].value)
					data[center + '/' + merger + '/sfr'].append(f['halo_' + str(halo_ID) + '/total/rvir/sfr'].value)

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

x = np.array([1e8, 1e9, 1e10, 1e11, 1e12, 1e13, 1e14, 1e15])

fig, axes = plt.subplots(1,1, figsize=(20,15))
fig.subplots_adjust(wspace=0.0, hspace=0.0)

light_speed = 3e10
solar_lum = 3.839e33
eps = 0.1
solar_mass = 2.0e33
year_to_s = 3.15e7

for ax, merger, center, flag in zip([axes], ['merger'], ['MDC'], [1]):

	z = data[center + '/' + merger + '/redshift']
	sel = (z > 4.0)
	z = data[center + '/' + merger + '/redshift'][sel]
	mdot = data[center + '/' + merger + '/mdot'][sel]
	mbh = data[center + '/' + merger + '/mbh'][sel]
	f = data[center + '/' + merger + '/f'][sel]
	sfr = data[center + '/' + merger + '/sfr'][sel]

	lum_bol, lum_ir = calculate_luminosity(mdot, sfr, f, mbh)

	# READ OBSERVATIONAL DATA

	# Lei+15 0.3 < z < 2.5
	lum_agn_lei15_03_08_type1, lum_sfr_lei15_03_08_type1 = np.genfromtxt('AGN_data/Lei+15_03_08_type1.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	lum_agn_lei15_03_08_type2, lum_sfr_lei15_03_08_type2 = np.genfromtxt('AGN_data/Lei+15_03_08_type2.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	lum_agn_lei15_08_15_type1, lum_sfr_lei15_08_15_type1 = np.genfromtxt('AGN_data/Lei+15_08_15_type1.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	lum_agn_lei15_15_25_type1, lum_sfr_lei15_15_25_type1 = np.genfromtxt('AGN_data/Lei+15_15_25_type1.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	
	lum_agn_lei15_03_08_type1 = np.log10((10.0**lum_agn_lei15_03_08_type1) * 1e46 / solar_lum)
	lum_sfr_lei15_03_08_type1 = np.log10((10.0**lum_sfr_lei15_03_08_type1) * 1e46 / solar_lum)
	lum_agn_lei15_03_08_type2 = np.log10((10.0**lum_agn_lei15_03_08_type2) * 1e46 / solar_lum)
	lum_sfr_lei15_03_08_type2 = np.log10((10.0**lum_sfr_lei15_03_08_type2) * 1e46 / solar_lum)
	lum_agn_lei15_08_15_type1 = np.log10((10.0**lum_agn_lei15_08_15_type1) * 1e46 / solar_lum)
	lum_sfr_lei15_08_15_type1 = np.log10((10.0**lum_sfr_lei15_08_15_type1) * 1e46 / solar_lum)
	lum_agn_lei15_15_25_type1 = np.log10((10.0**lum_agn_lei15_15_25_type1) * 1e46 / solar_lum)
	lum_sfr_lei15_15_25_type1 = np.log10((10.0**lum_sfr_lei15_15_25_type1) * 1e46 / solar_lum)

	# Duras+17 1.8 < z < 4.5
	z_WISSH, lum_agn_duras17_WISSH, lum_sfr_duras17_WISSH = np.genfromtxt('AGN_data/WISSH+18_Lum_WISSH.txt', delimiter='', usecols=(0,1,2), skip_header=1, unpack=True)
	lum_agn_duras17_WISSH_50per, lum_sfr_duras17_WISSH_50per = np.genfromtxt('AGN_data/WISSH+18_Lum_WISSH_50per.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True) 
	lum_agn_duras17_WISSH_Gruppioni16, lum_sfr_duras17_WISSH_Gruppioni16 = np.genfromtxt('AGN_data/WISSH+18_Lum_WISSH_Gruppioni+16.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True) 
	z_WISSH_HotDogs, lum_agn_duras17_WISSH_HotDogs, lum_sfr_duras17_WISSH_HotDogs = np.genfromtxt('AGN_data/WISSH+18_Lum_WISSH_HotDogs.txt', delimiter='', usecols=(0,1,2), skip_header=1, unpack=True) 
	lum_agn_duras17_WISSH_Netzer14, lum_sfr_duras17_WISSH_Netzer14 = np.genfromtxt('AGN_data/WISSH+18_Lum_WISSH_Netzer+14.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True) 
	lum_agn_duras17_WISSH_Netzer16, lum_sfr_duras17_WISSH_Netzer16 = np.genfromtxt('AGN_data/WISSH+18_Lum_WISSH_Netzer+16.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True) 

	lum_agn_duras17_WISSH = np.log10(lum_agn_duras17_WISSH / solar_lum)
	lum_sfr_duras17_WISSH = np.log10(lum_sfr_duras17_WISSH / solar_lum)
	lum_agn_duras17_WISSH_50per = np.log10(lum_agn_duras17_WISSH_50per / solar_lum)
	lum_sfr_duras17_WISSH_50per = np.log10(lum_sfr_duras17_WISSH_50per / solar_lum)
	lum_agn_duras17_WISSH_Gruppioni16 = np.log10(lum_agn_duras17_WISSH_Gruppioni16 / solar_lum)
	lum_sfr_duras17_WISSH_Gruppioni16 = np.log10(lum_sfr_duras17_WISSH_Gruppioni16 / solar_lum)
	lum_agn_duras17_WISSH_HotDogs = np.log10(lum_agn_duras17_WISSH_HotDogs / solar_lum)
	lum_sfr_duras17_WISSH_HotDogs = np.log10(lum_sfr_duras17_WISSH_HotDogs / solar_lum)
	lum_agn_duras17_WISSH_Netzer14 = np.log10(lum_agn_duras17_WISSH_Netzer14 / solar_lum)
	lum_sfr_duras17_WISSH_Netzer14 = np.log10(lum_sfr_duras17_WISSH_Netzer14 / solar_lum)
	lum_agn_duras17_WISSH_Netzer16 = np.log10(lum_agn_duras17_WISSH_Netzer16 / solar_lum)
	lum_sfr_duras17_WISSH_Netzer16 = np.log10(lum_sfr_duras17_WISSH_Netzer16 / solar_lum)

	# Netzer+14
	z_Netzer14, lum_agn_Netzer14, lum_sfr_Netzer14 = np.genfromtxt('AGN_data/Netzer+14.txt', delimiter='', usecols=(0,1,2), skip_header=1, unpack=True)
	lum_agn_Netzer14 = np.log10((10.0**lum_agn_Netzer14) / solar_lum)
	lum_sfr_Netzer14 = np.log10((10.0**lum_sfr_Netzer14) / solar_lum)

	# Netzer+16
	z_Netzer16, lum_agn_Netzer16, lum_sfr_Netzer16 = np.genfromtxt('AGN_data/Netzer+16.txt', delimiter='', usecols=(1,2,3), skip_header=1, unpack=True)
	lum_agn_Netzer16 = lum_agn_Netzer16 - np.log10(solar_lum)

	# Fan+16
	#z_fan16, lum_sfr_fan16 = np.genfromtxt('Fan+16.txt', delimiter='', usecols=(3,4), skip_header=1, unpack=True)
	#lum_sfr_fan16 = 10.0**lum_sfr_fan16
	#lum_agn_fan16 = lum_sfr_fan16 / 1.4

	# Bischetti 3.0 < z
	sfr_bischetti18_J1015, mdot_bischetti18_J1015 = np.genfromtxt('AGN_data/WISSH+18_sfr_mdot_J1015+0020.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	sfr_bischetti18_z3, mdot_bischetti18_z3 = np.genfromtxt('AGN_data/WISSH+18_sfr_mdot_z3.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	sfr_bischetti18_z45, mdot_bischetti18_z45 = np.genfromtxt('AGN_data/WISSH+18_sfr_mdot_z45.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	sfr_bischetti18_z6, mdot_bischetti18_z6 = np.genfromtxt('AGN_data/WISSH+18_sfr_mdot_z6.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)

	lum_agn_bischetti18_J1015 = (eps / (1.0-eps)) * ((10.0**mdot_bischetti18_J1015) * solar_mass / year_to_s) * (light_speed**2.0) / solar_lum # in solar luminosity
	lum_agn_bischetti18_J1015 = np.log10(lum_agn_bischetti18_J1015)
	lum_sfr_bischetti18_J1015 = sfr_bischetti18_J1015 - np.log10(1.49e-10)
	lum_agn_bischetti18_z3 = (eps / (1.0-eps)) * ((10.0**mdot_bischetti18_z3) * solar_mass / year_to_s) * (light_speed**2.0) / solar_lum # in solar luminosity
	lum_agn_bischetti18_z3 = np.log10(lum_agn_bischetti18_z3)
	lum_sfr_bischetti18_z3 = sfr_bischetti18_z3 - np.log10(1.49e-10)
	lum_agn_bischetti18_z45 = (eps / (1.0-eps)) * ((10.0**mdot_bischetti18_z45) * solar_mass / year_to_s) * (light_speed**2.0) / solar_lum # in solar luminosity
	lum_agn_bischetti18_z45 = np.log10(lum_agn_bischetti18_z45)
	lum_sfr_bischetti18_z45 = sfr_bischetti18_z45 - np.log10(1.49e-10)
	lum_agn_bischetti18_z6 = (eps / (1.0-eps)) * ((10.0**mdot_bischetti18_z6) * solar_mass / year_to_s) * (light_speed**2.0) / solar_lum # in solar luminosity
	lum_agn_bischetti18_z6 = np.log10(lum_agn_bischetti18_z6)
	lum_sfr_bischetti18_z6 = sfr_bischetti18_z6 - np.log10(1.49e-10)

	# Wang+11 z=2.3, 4.2, ~6, luminosities are already in units of solar luminosity
	lum_agn_Omont03_z23, lum_sfr_Omont03_z23 = np.genfromtxt('AGN_data/Omont+03_z23.txt', delimiter='',usecols=(0,1), skip_header=1, unpack=True)
	lum_agn_Omont03_z42, lum_sfr_Omont03_z42 = np.genfromtxt('AGN_data/Omont+03_z42.txt', delimiter='',usecols=(0,1), skip_header=1, unpack=True)
	lum_agn_Priddey03_z23, lum_sfr_Priddey03_z23 = np.genfromtxt('AGN_data/Priddey+03_z23.txt', delimiter='',usecols=(0,1), skip_header=1, unpack=True)
	lum_agn_Priddey03_z42, lum_sfr_Priddey03_z42 = np.genfromtxt('AGN_data/Priddey+03_z42.txt', delimiter='',usecols=(0,1), skip_header=1, unpack=True)
	lum_agn_Wang11_z6, lum_sfr_Wang11_z6 = np.genfromtxt('AGN_data/Wang+11_z6.txt', delimiter='',usecols=(0,1), skip_header=1, unpack=True)

	# Izumi+18 z~6
	mag_agn_Izumi18_z6_prev, lum_sfr_Izumi18_z6_prev = np.genfromtxt('AGN_data/Izumi+18_previous.txt', delimiter='',usecols=(0,1), skip_header=1, unpack=True)
	mag_agn_Izumi18_z6_HSC, lum_sfr_Izumi18_z6_HSC = np.genfromtxt('AGN_data/Izumi+18_HSC.txt', delimiter='',usecols=(0,1), skip_header=1, unpack=True)
	L_0 = 3.0128e35
	lum_agn_Izumi18_z6_prev = 4.4 * L_0 * (10.0**(-0.4*mag_agn_Izumi18_z6_prev)) / solar_lum
	lum_agn_Izumi18_z6_HSC = 4.4 * L_0 * (10.0**(-0.4*mag_agn_Izumi18_z6_HSC)) / solar_lum

	lum_agn_Izumi18_z6_prev = np.log10(lum_agn_Izumi18_z6_prev)
	lum_sfr_Izumi18_z6_prev = np.log10(lum_sfr_Izumi18_z6_prev)
	lum_agn_Izumi18_z6_HSC = np.log10(lum_agn_Izumi18_z6_HSC)
	lum_sfr_Izumi18_z6_HSC = np.log10(lum_sfr_Izumi18_z6_HSC)

	#ax.set_title('%s' %(merger + ' ' + center), fontsize=20)
	
	#cm = plt.cm.get_cmap('gist_rainbow')
	cm = mcolors.ListedColormap(['magenta', 'cyan', 'greenyellow', 'brown']) #  Magenta (or purple), cyan, greenish/gold, brown.
	cm1 = mcolors.ListedColormap(['magenta', 'cyan', 'greenyellow', 'brown'])
	c_array = np.array([4.0, 4.5, 5.0, 6.0, 8.0])
	norm = mlp.colors.BoundaryNorm(c_array, cm.N)
	norm1 = mlp.colors.BoundaryNorm(c_array, cm1.N)
	sel1 = (lum_ir > 0.0) & (lum_bol > 0.0)
	z = z[sel1]
	lum_bol = lum_bol[sel1]
	lum_ir = lum_ir[sel1]
	sc = ax.scatter(np.log10(lum_ir), np.log10(lum_bol), c=z, alpha=0.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm)

	sel_z1 = (z > c_array[0]) & (z < c_array[1])
	sel_z2 = (z > c_array[1]) & (z < c_array[2])
	sel_z3 = (z > c_array[2]) & (z < c_array[3])
	sel_z4 = (z > c_array[3]) & (z < c_array[4])

	x1 = np.log10(lum_ir[sel_z1])
	y1 = np.log10(lum_bol[sel_z1])
	x2 = np.log10(lum_ir[sel_z2])
	y2 = np.log10(lum_bol[sel_z2])
	x3 = np.log10(lum_ir[sel_z3])
	y3 = np.log10(lum_bol[sel_z3])
	x4 = np.log10(lum_ir[sel_z4])
	y4 = np.log10(lum_bol[sel_z4])

	ax.scatter(x4, y4, c=z[sel_z4], s=120, alpha=1.0, vmin=np.min(z[sel_z4]), vmax=np.max(z[sel_z4]), cmap=cm, norm=norm)
	ax.scatter(x3, y3, c=z[sel_z3], s=120, alpha=1.0, vmin=np.min(z[sel_z3]), vmax=np.max(z[sel_z3]), cmap=cm, norm=norm)
	ax.scatter(x2, y2, c=z[sel_z2], s=120, alpha=1.0, vmin=np.min(z[sel_z2]), vmax=np.max(z[sel_z2]), cmap=cm, norm=norm)
	ax.scatter(x1, y1, c=z[sel_z1], s=120, alpha=1.0, vmin=np.min(z[sel_z1]), vmax=np.max(z[sel_z1]), cmap=cm, norm=norm)

	mf = ax.scatter(1e-10, 1e-10, color='k', marker='.', s=400)

	'''
	# z=<4 observational sample
	ax.scatter(lum_sfr_lei15_03_08_type1, lum_agn_lei15_03_08_type1, c=np.ones(len(lum_sfr_lei15_03_08_type1))*0.3, marker='s', s=80, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm1, norm=norm1, label='0.3<z<0.8 Type 1 Lei+15')
	ax.scatter(lum_sfr_lei15_03_08_type2, lum_agn_lei15_03_08_type2, c=np.ones(len(lum_sfr_lei15_03_08_type2))*0.3, marker='D', facecolors='None', s=80, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm1, norm=norm1, label='0.3<z<0.8 Type 2 Lei+15')
	ax.scatter(lum_sfr_lei15_08_15_type1, lum_agn_lei15_08_15_type1, c=np.ones(len(lum_sfr_lei15_08_15_type1))*1.0, marker='^', s=80, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='0.8<z<1.5 Type 1 Lei+15')
	ax.scatter(lum_sfr_lei15_15_25_type1, lum_agn_lei15_15_25_type1, c=np.ones(len(lum_sfr_lei15_15_25_type1))*2.0, marker='X', s=80, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='1.5<z<2.5 Type 1 Lei+15')
	ax.scatter(lum_sfr_duras17_WISSH, lum_agn_duras17_WISSH, c=z_WISSH, marker='*', facecolors='None', s=120, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='1.8<z<4.5 WISSH Duras+17')
	#ax.scatter(lum_sfr_duras17_WISSH_50per, lum_agn_duras17_WISSH_50per, color='r', marker=(8,2,0), s=80, alpha=1.0, label='WISSH 50% reduced Duras+17')
	ax.scatter(lum_sfr_duras17_WISSH_Gruppioni16, lum_agn_duras17_WISSH_Gruppioni16, c=np.ones(len(lum_sfr_duras17_WISSH_Gruppioni16))*0.2, marker='P', s=120, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm1, norm=norm1, label='z<0.2 Gruppioni+16')
	ax.scatter(lum_sfr_duras17_WISSH_HotDogs, lum_agn_duras17_WISSH_HotDogs, c=z_WISSH_HotDogs, marker='<', s=60, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='1.7<z<4.6 Hot DOGs Fan+16')
	#ax.scatter(lum_sfr_fan16, lum_agn_fan16, c=z_fan16, marker='.', s=120, alpha=0.3, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='1.7<z<4.6 Hot DOGs Fan+16')
	ax.scatter(lum_sfr_Netzer16, lum_agn_Netzer16, c=z_Netzer16, marker='v', s=80, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='2.0<z<3.5 Netzer+16')
	ax.scatter(lum_sfr_bischetti18_z3, lum_agn_bischetti18_z3, c=np.ones(len(lum_sfr_bischetti18_z3))*3.0, marker='+', s=80, alpha=1.0, facecolors='None', vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='z~3 WISSH QSOs Bischetti+18')
	ax.scatter(lum_sfr_Omont03_z23, lum_agn_Omont03_z23, c=np.ones(len(lum_sfr_Omont03_z23))*2.3, marker='h', s=120, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='z~2.3 Omont+03')
	ax.scatter(lum_sfr_Priddey03_z23, lum_agn_Priddey03_z23, c=np.ones(len(lum_sfr_Priddey03_z23))*2.3, marker='x', s=120, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='z~2.3 Priddey+03')
	'''

	# z>4 observartional sample
	ax.scatter(np.array([1e-10, np.log10(3.76e12)]), np.array([1e-10, np.log10(3.5e14)]), c=np.ones(2)*4.6, marker='s', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='most lum gal at z=4.6')
	ax.scatter(lum_sfr_bischetti18_z45, lum_agn_bischetti18_z45, c=np.ones(len(lum_sfr_bischetti18_z45))*4.5, marker='d', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='z~4.5 QSOs Bischetti+18')
	ax.scatter(lum_sfr_bischetti18_z6, lum_agn_bischetti18_z6, c=np.ones(len(lum_sfr_bischetti18_z6))*6.0, marker=(8,2,0), s=200, alpha=0.8, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label=r'$z \geq 6,\; QSOs \; Bischetti+18$')
	ax.scatter(np.append(lum_sfr_bischetti18_J1015,1e-10), np.append(lum_agn_bischetti18_J1015,1e-10), c=np.ones(2)*4.4, marker='X', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='z=4.4 J1015+0020 Bischetti+18')
	ax.scatter(lum_sfr_Netzer14, lum_agn_Netzer14, c=z_Netzer14, marker='*', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label='Netzer+14')
	ax.scatter(lum_sfr_Omont03_z42, lum_agn_Omont03_z42, c=np.ones(len(lum_sfr_Omont03_z42))*4.2, marker='^', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label=r'$z \sim 4.2 \; Omont+03$')
	ax.scatter(lum_sfr_Priddey03_z42, lum_agn_Priddey03_z42, c=np.ones(len(lum_sfr_Priddey03_z42))*4.2, marker='P', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label=r'$z \sim 4.2\;Priddey+03$')
	ax.scatter(lum_sfr_Wang11_z6, lum_agn_Wang11_z6, c=np.ones(len(lum_sfr_Wang11_z6))*6.0, marker='v', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label=r'$z \sim 6 \; Wang+11$')
	ax.scatter(lum_sfr_Izumi18_z6_prev, lum_agn_Izumi18_z6_prev, c=np.ones(len(lum_sfr_Izumi18_z6_prev))*6.0, marker='<', s=200, alpha=0.6, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label=r'$z>6 \; Izumi+18$')
	ax.scatter(lum_sfr_Izumi18_z6_HSC, lum_agn_Izumi18_z6_HSC, c=np.ones(len(lum_sfr_Izumi18_z6_HSC))*6.0, marker='D', s=200, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm, label=r'$z>6 \; Izumi+18 \; HSC$')

	ax.set_xlabel(r'$log(L_{IR} / L_{\odot})$', fontsize=30)
	ax.set_ylabel(r'$log(L_{bol} / L_{\odot})$', fontsize=30)

	ax.set_xscale('linear')
	ax.set_yscale('linear')
	ax.set_xlim(9,14)
	ax.set_ylim(8,15)
	ax.xaxis.set_tick_params(labelsize=30)
	ax.yaxis.set_tick_params(labelsize=30)

	#fig.subplots_adjust(left=0.8)
	cbar = fig.colorbar(sc, pad=0.15, ticks=c_array)#, ax=axes.ravel().tolist())
	cbar.set_label('    z', rotation=0, fontsize=30)
	cbar.ax.tick_params(labelsize=30)
	cbar.solids.set(alpha=1)

	ax1 = ax.twiny()
	ax2 = ax1.twinx()
	ax1.set_xscale('linear')
	ax2.set_yscale('linear')
	ax2.set_xlim(9.0+np.log10(1.49e-10),14.0+np.log10(1.49e-10))

	ax1.xaxis.set_tick_params(labelsize=30)
	ax2.yaxis.set_tick_params(labelsize=30)

	sfr_array = np.array([-1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0])
	line1, = ax2.plot(sfr_array, sfr_array - 2.0, color='k', lw=3, ls='-')
	line2, = ax2.plot(sfr_array, sfr_array - 3.0, color='k', lw=3, ls='--')
	line3, = ax2.plot(sfr_array, sfr_array - 4.0, color='k', lw=3, ls='-.')

	ax.xaxis.set_major_locator(ticker.MultipleLocator(1))	
	ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))	
	ax.yaxis.set_major_locator(ticker.MultipleLocator(1))	
	ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))	
	ax2.xaxis.set_major_locator(ticker.MultipleLocator(1))	
	ax2.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))	
	ax2.yaxis.set_major_locator(ticker.MultipleLocator(1))	
	ax2.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))

	ax.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax1.tick_params(axis='x', which='minor', direction='in', length=5.0, width=1.0)
	ax2.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	#ax2.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)

	ax.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)
	ax1.tick_params(axis='x', which='major', direction='in', length=10.0, width=2.0)
	ax2.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)
	#ax2.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)

	ax1.set_xlabel(r'$log(SFR)\;(M_{\odot}/yr)$', fontsize=30)
	ax2.set_ylabel(r'$log(\dotM_{acc})\;(M_{\odot}/yr)$', fontsize=30)

	ax.legend(loc=0,fontsize=20,frameon=False,scatterpoints=1)
	plt.legend([mf,line1,line2,line3], ['MassiveFIRE',r'$\dot{M}_{acc}/SFR=10^{-2}$',r'$\dot{M}_{acc}/SFR=10^{-3}$',r'$\dot{M}_{acc}/SFR=10^{-4}$'], loc=4, fontsize=20, frameon=False, scatterpoints=1)
#plt.tight_layout()
#plt.show()
#quit()

fig.savefig('lum_bol_ir_lum_corr_dyn_z4.pdf')
plt.close()
