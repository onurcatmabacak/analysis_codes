import numpy as np
import h5py
import matplotlib.pyplot as plt
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

sim_name_MF2 = ['h206_HR_sn1dy300ro100ss']
sim_suite_MF2 = ['MassiveFIRE2']
label = ['0.1\;kpc', '0.2\;kpc', '0.5\;kpc', '1.0\;kpc', '\%10\;R_{vir}']
radius = ['0.1', '0.2', '0.5', '1.0', 'rvir']
color = ['r', 'g', 'b', 'k', 'm']
regime_list = ['COM', 'MDC']
fontsize = 24
xmax = [12]

fig = plt.figure(1, figsize=(12,8))
fig.subplots_adjust(wspace=0, hspace=0)

ax1 = fig.add_subplot(111)	
#ax1.set_title('COM')

#ax1.plot(0.0, 0.0, color='k', ls='-', lw=2, label='total gas')

ax1.plot(redshift['MDC'], mdc_sfr_ratio, color='k', ls='-', lw=4, label='MDC')   
ax1.plot(redshift['COM'], com_sfr_ratio, color='r', ls='--', lw=2, label='COM')   
	
ax1.set_xscale('linear')
ax1.set_yscale('linear')
ax1.set_xlim(0.8,9)
ax1.set_ylim(0,1.05)
ax1.xaxis.set_major_locator(plt.MultipleLocator(1))
ax1.yaxis.set_major_locator(plt.MultipleLocator(0.1))
ax1.xaxis.set_tick_params(labelsize=fontsize)
ax1.yaxis.set_tick_params(labelsize=fontsize)
ax1.set_xlabel('z', fontsize=fontsize)
#ax1.tick_params(axis='y', labelsize=30)
ax1.set_ylabel(r'$SFR_{1kpc} \; / \; SFR_{gal}$', fontsize=fontsize)
ax12 = ax1.twiny()
ax1Xs = ax1.get_xticks()
ax12Xs = []
for X in ax1Xs:
	dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
	#dummy_age = z_at_value(Planck13.age, X * u.Gyr)
	ax12Xs.append("%.1f" %dummy_age)
ax12.set_xticks(ax1Xs)
ax12.set_xbound(ax1.get_xbound())
ax12.set_xticklabels(ax12Xs)
ax12.set_xlabel(r'$time\;(Gyr)$', fontsize=fontsize)
ax12.tick_params(axis='x', labelsize=fontsize)
ax1.legend(loc=0,prop={'size':fontsize},frameon=False,scatterpoints=1)
ax1.grid(b=None, which='major', axis='both')

plt.show()
#plt.tight_layout()
#plt.savefig('sfr_mdot.png', dpi=300, format='png')
#plt.close()

