import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

sim_name = ['h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite=['MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
label = ['A4', 'A1', 'A2', 'A8']
color = ['r', 'g', 'b', 'k']
linestyle = ['-', '-', '-', '-']
i_list = [1]
regime_list = ['MDC']

for i in range(len(i_list)):

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0.0, hspace=0.0)
	fig.suptitle(label[i])
	ax0 = fig.add_subplot(111)	

	for regime in regime_list:
		filename = '/bulk1/feldmann/Onur/' + regime + '/' + sim_suite[i] + '/' + sim_name[i] + '/'

		redshift, position = np.genfromtxt(filename + 'zred_pos_list', delimiter='', usecols=(0,1), skip_header=0, unpack=True)
		position = position.astype(int)

		z_1 = []
		ID = []
		position = []
		mstar = []
		mhalo = []
		z_2 = []
		descendant = []

		file_halo = open(filename + 'halo_0.txt', 'r')
		for line in file_halo.readlines():
			data = map(float, np.array(line.split()))
			z_1.append(data[0])
			position.append(redshift.tolist().index(data[0]))
			ID.append(int(data[1]))
			mstar.append(data[2])
			mhalo.append(data[3])
			z_2.append(data[4])
			descendant.append([int(xxx) for xxx in data[5:]])
		file_halo.close()

		z_1 = np.array(z_1)
		position = np.array(position)
		ID = np.array(ID)
		mstar = np.array(mstar)
		mhalo = np.array(mhalo)
		z_2 = np.array(z_2)
		descendant = np.array(descendant)

		if sim_suite[i] == 'MassiveFIRE2':
			radius_name = '0.1'
		else:
			radius_name = '0.1'

		data = h5py.File(filename + sim_name[i] + '_all.hdf5','r')
		mbh = data['cold/r_' + radius_name + '/mass_ret_0.5/seed_1e4/m_bh_torque'].value
		mbh = mbh[::-1]

		tot_merger = []

		for xxx in range(len(z_1)):

			inds = len(descendant[xxx])-1
			tot_merger.append(inds)
		
		merger_final = []

		for xxx in range(len(redshift)):

			sel = (z_1 == redshift[xxx])
			new = int(np.sum(np.array(tot_merger)[sel]))

			if new:
				merger_final.append(new)
			else:
				merger_final.append(0)

		merger = np.cumsum(merger_final)

		ax0.plot(redshift, merger, color=color[i], ls=linestyle[i], lw=2, label=label[i])

	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(z_min[j],12)
	ax0.set_ylim(min_lim[j], max_lim[j])
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$cumulative \; \# \; of \; mergers$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	#plt.tight_layout()
	plt.savefig('plot_#9_cumulative_#_of_merger_' + plotname[j] + '.png', dpi=300, format='png')
	plt.close()
