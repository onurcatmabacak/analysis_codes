import numpy as np
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy.optimize import curve_fit
from scipy import stats
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

def func(x, mbh, a, b, c):
	return a*mbh + b*x + c

def func1(x, a, b):
	return a*x + b

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
file_list = {}

mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'
extra_list = [12, 13, 33, 34, 35, 36]

data = {}

for merger in merger_list:
	for center in center_list:
		for mass_ret in mass_ret_list:

			props = ['mbh', 'mdot', 'f', 'sfr', 'redshift']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for i in range(37):
				filename = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5' 

				if i in extra_list:

					read_path = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i]
					remove = len(read_path) + 6
					filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

					halo_ID_list = []
					for f in filelist:
						halo_ID_list.append(f[remove:-4])

					halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
				else:
					halo_ID_list = [0]

				f = h5py.File(filename, 'r')

				for halo_ID in halo_ID_list:

					data[center + '/' + merger + '/mdot'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/mdot_torque'].value)
					data[center + '/' + merger + '/mbh'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value)
					data[center + '/' + merger + '/f'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value)
					data[center + '/' + merger + '/redshift'].append(f['halo_' + str(halo_ID) + '/total/rvir/z_1'].value)
					data[center + '/' + merger + '/sfr'].append(f['halo_' + str(halo_ID) + '/total/rvir/sfr'].value)

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

x = np.array([1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4])
z_list = np.array(range(13))

fig, axes = plt.subplots(1,1, figsize=(12,6))
fig.subplots_adjust(wspace=0.2, hspace=0.2)
cm = plt.cm.get_cmap('gist_rainbow')

#for ax, merger, center, flag in zip(axes.flat, ['merger', 'nomerger', 'merger', 'nomerger'], ['MDC', 'MDC', 'COM', 'COM'], [1,2,3,4]):
for ax, merger, center in zip([axes], ['merger'], ['MDC']):

	z = data[center + '/' + merger + '/redshift']
	f = data[center + '/' + merger + '/f']
	mdot = data[center + '/' + merger + '/mdot']
	mbh = data[center + '/' + merger + '/mbh']
	sfr = data[center + '/' + merger + '/sfr']

	sel1 = (f != 0.0) & (sfr != 0.0) 
	z = z[sel1]
	f = f[sel1]
	mbh = mbh[sel1]

	z = np.log10(1.0+z)
	f = np.log10(f)
	mbh = np.log10(mbh)

	bins = np.linspace(np.min(z), np.max(z), 30)
	average_f = []
	average_z = []
	average_mbh = []

	for xxx in range(len(bins)-1):

		sel = (z > bins[xxx]) & (z <= bins[xxx+1])
		
		average_f.append(np.median(f[sel]))
		average_mbh.append(np.median(mbh[sel]))
		average_z.append((bins[xxx] + bins[xxx+1]) / 2.0)

	average_z = np.array(average_z)
	average_f = np.array(average_f)
	average_mbh = np.array(average_mbh)

	real_z = (10.0 ** z) -1
	ind = np.argsort(z)

	popt, pcov = curve_fit(lambda x, a,b,c: func(x, average_mbh, a,b,c), average_z, average_f)	
	#popt1, pcov1 = curve_fit(func1, average_z, average_f)
	slope, intercept, r_value, p_value, std_err = stats.linregress(average_z,average_f)

	# PLOT
	#ax.set_title('%s' %(merger + ' ' + center), fontsize=20)
	sc = ax.scatter(z, f, c=real_z, alpha=1.0, vmin=np.min(real_z), vmax=np.max(real_z), cmap=cm)
	ax.scatter(average_z, average_f, color='k', marker='s', s=80)
	ax.plot(average_z, func(average_z, average_mbh, *popt), 'k-', label=r'$log \lambda = %.2f \; log(M_{BH}) + %.2f \; log(1+z) + %.2f$' % tuple(popt))
	#ax.plot(average_z, func1(average_z, *popt1), 'k--', label=r'$log \lambda = %.2f \; log(1+z) %.2f$' % tuple(popt1))
	ax.plot(average_z, slope * average_z + intercept, 'k--', label=r'$log \lambda = %.2f \; log(1+z) %.2f$' %(slope, intercept))

	#if (flag == 3) or (flag == 4):
	ax.set_xlabel('log(1+z)', fontsize=20)
	#if (flag == 1) or (flag == 3):
	ax.set_ylabel(r'$log(\lambda)$', fontsize=20)
	ax.set_xscale('linear')
	ax.set_yscale('linear')
	ax.set_xlim(0,1.15)
	ax.set_ylim(-4,1)
	ax.xaxis.set_tick_params(labelsize=20)
	ax.yaxis.set_tick_params(labelsize=20)
	ax.xaxis.set_major_locator(ticker.MultipleLocator(0.2))	
	ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))	
	ax.yaxis.set_major_locator(ticker.MultipleLocator(1))	
	ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.2))	
	ax.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)
	ax.legend(loc=0,prop={'size':14},frameon=False,scatterpoints=1)

#fig.subplots_adjust(left=0.8)
cbar = fig.colorbar(sc) #, ax=axes.ravel().tolist())
cbar.set_label('z', rotation=0, fontsize=20)
#plt.tight_layout()
plt.show()
#quit()

fig.savefig('z_f_fit.pdf')
plt.close()

