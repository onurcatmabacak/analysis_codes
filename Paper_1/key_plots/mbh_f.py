import numpy as np
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy.optimize import curve_fit
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

def func(x, mbh, a, b, c):
	return a*np.log10(mbh) + b*np.log10(1.0+x) + c

def func1(x, a, b):
	return a*np.log10(1.0+x) + b

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
file_list = {}

mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'

data = {}

for merger in merger_list:
	for center in center_list:
		file_list[center + '/' + merger] = sorted(glob.glob('/bulk1/feldmann/Onur/' + center + '/MassiveFIRE*/*/*_' + merger + '.hdf5'))
		for mass_ret in mass_ret_list:

			props = ['mbh', 'mdot', 'f', 'sfr', 'redshift']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for filename in file_list[center + '/' + merger]:
				f = h5py.File(filename, 'r')

				data[center + '/' + merger + '/mbh'].append(f['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value)
				data[center + '/' + merger + '/mdot'].append(f['total/' + radius + '/' + mass_ret + '/seed_1e4/mdot_torque'].value)
				data[center + '/' + merger + '/f'].append(f['total/' + radius + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value)
				data[center + '/' + merger + '/redshift'].append(f['total/rvir/z_1'].value)
				data[center + '/' + merger + '/sfr'].append(f['total/rvir/sfr'].value)

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

x = np.array([1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4])
z_list = np.array(range(13))

fig, axes = plt.subplots(2,2, figsize=(20,10))
fig.subplots_adjust(wspace=0.2, hspace=0.2)
cm = plt.cm.get_cmap('RdYlBu')

for ax, merger, center, flag in zip(axes.flat, ['merger', 'nomerger', 'merger', 'nomerger'], ['MDC', 'MDC', 'COM', 'COM'], [1,2,3,4]):

	z = data[center + '/' + merger + '/redshift']
	f = data[center + '/' + merger + '/f']
	mdot = data[center + '/' + merger + '/mdot']
	mbh = data[center + '/' + merger + '/mbh']
	sfr = data[center + '/' + merger + '/sfr']

	#bins = np.linspace(0, 12, 20)
	#average_f = []
	#average_z = []
	#average_mbh = []

	#for xxx in range(len(bins)-1):

	#	sel = (z > bins[xxx]) & (z <= bins[xxx+1])
		
	#	average_f.append(np.sum(f[sel]) / len(f[sel]))
	#	average_mbh.append(np.sum(mbh[sel]) / len(mbh[sel]))
	#	average_z.append((bins[xxx] + bins[xxx+1]) / 2.0)

	#average_z = np.array(average_z)
	#average_f = np.array(average_f)
	#average_mbh = np.array(average_mbh)

	#popt, pcov = curve_fit(lambda x, a,b,c: func(x, average_mbh, a,b,c), average_z, np.log10(average_f))	
	#popt1, pcov1 = curve_fit(func1, average_z, np.log10(average_f))	
	# PLOT
	ax.set_title('%s' %(merger + ' ' + center), fontsize=10)
	sc = ax.scatter(np.log10(mbh), f, c=z, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm)
	#ax.plot(np.log10(z_list), np.zeros(len(z_list)), color='k', lw=1, ls='-')
	#ax.plot(np.ones(len(x))*np.log10(1.0+2.0), np.log10(x), color='k', lw=1, ls='-')
	#ax.scatter(average_z, average_f, color='k', marker='s', s=80)
	#ax.plot(average_z, 10.0**(func(average_z, average_mbh, *popt)), 'k-', label=r'$log \lambda = %.2f \; log(M_{BH}) + %.2f \; log(1+z) + %.2f$' % tuple(popt))
	#ax.plot(bins, 10.0**(func1(bins, *popt1)), 'k--', label=r'$log \lambda = %.2f \; log(1+z) %.2f$' % tuple(popt1))

	if (flag == 3) or (flag == 4):
		ax.set_xlabel(r'$log(M_{BH})$', fontsize=20)
	if (flag == 1) or (flag == 3):
		ax.set_ylabel(r'$\lambda \; \left( \frac{\dot{M}_{BH}}{\dot{M}_{Edd}} \right)$', fontsize=20)
	ax.set_xscale('linear')
	ax.set_yscale('log')
	ax.set_xlim(3.8,9.2)
	ax.set_ylim(1e-4,1e2)
	ax.xaxis.set_tick_params(labelsize=12)
	ax.yaxis.set_tick_params(labelsize=12)
	ax.xaxis.set_major_locator(ticker.MultipleLocator(1))	
	ax.legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

	#if flag == 1:
	#	ax.legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

#fig.subplots_adjust(left=0.8)
fig.colorbar(sc, ax=axes.ravel().tolist())
#plt.tight_layout()
plt.show()
#quit()

fig.savefig('mbh_f.pdf')
plt.close()

