import os
import pdb
import numpy as np
import h5py
import sys
from scipy.optimize import curve_fit
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
from astropy import units as u
from astropy.cosmology import Planck15, z_at_value
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from scipy import integrate
from scipy.special import lambertw
from scipy import stats
import operator
from astropy.cosmology import FlatLambdaCDM

def time(i):
	return cosmo.age(i).value * 1e9 # yr

def weighted_average(data, weight, step):
	radius = np.linspace(np.min(weight), np.max(weight), step+1)
	weighted_average = []
	radius_mean = []

	for i in range(len(radius) - 1):
		total_data = np.array([a for a,b in zip(data, weight) if b >= radius[i] if b <= radius[i+1]])
		total_weight = np.array([b for b in weight if b >= radius[i] if b <= radius[i+1]]) 
		weighted_average.append(np.sum(total_data * total_weight) / np.sum(total_weight))
		radius_mean.append((radius[i]+radius[i+1])/2.0)
	return weighted_average, radius_mean

def Muratov15(mstar,a,b):
	return np.log10(a) + b * np.log10(mstar*1e-10)
	#return a * (Mstar**b) # Mstar is normalized with 1e10 M_solar

def HH15(mstar,a,b,c):
	return np.log10(a) + b * np.log10(mstar*1e-10) + np.log10(np.exp(c*mstar*1e-10))

def HH15_eq44(mstar,mgas,a,b,c):
	return np.log10(a) + b* np.log10(mgas*mstar/(mgas+mstar)*1e-10) + np.log10(np.exp(c/mgas*(mgas+mstar)))
	#return np.log10(a) +b* np.log10((mgas*mstar/(mgas+mstar)*1e-10))  +np.log10( np.exp(c/mgas*(mgas+mstar)))

def Ferrerase02(Mass):	
	#return 10.**( 8.0 ) * 0.027 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	return 10.**( 8.0 ) * 0.100 * ( Mass/1e12 )**(1.65) # Ferrerase 2002
	#return 10.**( 8.0 ) * 0.670 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	#return 10.**( 8.18 ) * ( Mass/1e13 )**(1.55) # Bandara et al 2009
	#return 10.**( 7.85 ) * ( Mass/1e12 )**(1.33) # Di Matteo et al 2003

def bin_average(x,y, nbins):
	
	x = np.log10(x[(y != 0.0)])
	y = np.log10(y[(y != 0.0)])
	bins = np.linspace(np.min(x), np.max(x), nbins+1)

	xresult = []
	yresult = []
	for i in range(len(bins)-1):
		sel = (x <= bins[i+1] ) & (x >= bins[i])	
		xdata = np.array(x[sel])
		ydata = np.array(y[sel])
		yresult.append(np.sum(xdata * ydata) / np.sum(xdata))
		xresult.append(np.sum(xdata) / len(xdata))

	xx = 10.0 ** (np.array(xresult))
	yy = 10.0 ** (np.array(yresult))

	return xx, yy

def Haring04(Mbulge):

   return 10.**( 8.20 ) * ( Mbulge/1e11 )**(1.12)


def Kormendy2013(Mbulge):

   return 0.49 * 1e9 * ( Mbulge/1e11 )**(1.16)


def McConnell2013(Mbulge):

   return 10.**( 8.46 ) * ( Mbulge/1e11 )**(1.05)

def Reines2015(Mstar):

   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

def Savorgnan2016( Mstar, sample='all'):
	Mbh_early = 10.**( 8.56 + 1.04*np.log10(Mstar/10.**10.81) )
	Mbh_late = 10.**( 7.24 + 2.28*np.log10(Mstar/10.**10.05) )

	ind = np.where( Mbh_late - Mbh_early >= 0 )[0] 
	Mbh_all =  np.append( Mbh_late[:ind[0]], Mbh_early[ind[0]:] )

	if Mbh_all.size != Mstar.size:
	 print 'ahhhh.... check Savorgnan2016 !!!'

	if sample == 'early':
	 return Mbh_early
	elif sample == 'late':
	 return Mbh_late
	elif sample == 'all':
	 return Mbh_all

def McConnell_Ma2013(vel_disp):
	return 10.**( 8.32 ) * ( vel_disp/200.0 )**(5.64)

#################################################### 
################### MAIN PROGRAM ###################
#################################################### 
global sim_name
global sim_suite
global dummy
global regime_list
global radius_name_list
global mass_retention_list
global seed_mass_list
global halo_ID
global data
global i_list
global mass_list
global vel_list
global Ferrerase02_data
global Haring04_data
global Kormendy2013_data
global Reines2015_data
global McConnell2013_data
global McConnell_Ma2013_data
global cosmo
global C
global ahf_centering
global merger

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
dummy = [16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 178, 178, 178, 11, 11, 11, 11]
C = [1e0, 1e0, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e0, 1e-1, 1e0, 1e0, 1e-1, 1e-1, 1e0, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1, 1e-1]

regime_list = ['cold', 'total']
radius_name_list = ['0.1', '0.2', '0.5', '1.0', 'rvir']
mass_retention_list = ['0.05', '0.1', '0.25', '0.5', '1.0']
seed_mass_list = ['1e1', '1e2', '1e3', '1e4', '1e5']
halo_ID = 0

mass_list = np.array([1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12,1e13,1e14])
vel_list = np.array([0,20,40,60,80,100,200,400,800])
Ferrerase02_data = []
Haring04_data = []
Kormendy2013_data = []
McConnell2013_data = []
Reines2015_data = []
Savorgnan2016_data = []
McConnell_Ma2013_data = []

for mass_dummy in mass_list:
	Ferrerase02_data.append(Ferrerase02(mass_dummy))
	Haring04_data.append(Haring04(mass_dummy))
	Kormendy2013_data.append(Kormendy2013(mass_dummy))
	McConnell2013_data.append(McConnell2013(mass_dummy))
	Reines2015_data.append(Reines2015(mass_dummy))
	#Savorgnan2016_data.append(Savorgnan2016(mass_dummy))
for vel_dummy in vel_list:
	McConnell_Ma2013_data.append(McConnell_Ma2013(vel_dummy))

Ferrerase02_data = np.array(Ferrerase02_data)
Haring04_data = np.array(Haring04_data)
Kormendy2013_data = np.array(Kormendy2013_data)
McConnell2013_data = np.array(McConnell2013_data)
Reines2015_data = np.array(Reines2015_data)
#Savorgnan2016_data = np.array(Savorgnan2016_data)
McConnell_Ma2013_data = np.array(McConnell_Ma2013_data)

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

ahf_centering = 'MDC'
merger = 'merger'

i_list = range(37)
data = {}
for regime in regime_list:
	for i in i_list:

		data[sim_suite[i] + '/' + sim_name[i]] = h5py.File('/bulk1/feldmann/Onur/' + ahf_centering + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5','r')

####################################
########## PLOT FUNCTIONS ##########
####################################
for i in [34]:		# 3 9 12 13 33 34 35 36

	for regime in ['total']:

		if sim_suite[i] == 'MassiveFIRE':
			radius = radius_name_list[0] #MassiveFIRE 100 pc
		else:
			radius = radius_name_list[0] #MassiveFIRE2 100 pc

		mass_ret = mass_retention_list[1] 	# %10
		seed_mass = seed_mass_list[3] 		# 1e4 solar mass
	
		bh_mass = data[sim_suite[i] + '/' + sim_name[i]][regime + '/r_' + radius + '/mass_ret_' + mass_ret + '/seed_' + seed_mass + '/m_bh_torque'].value
		mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/rvir/mstar'].value

		redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/rvir/z_1'].value
		sfr_gal = data[sim_suite[i] + '/' + sim_name[i]][regime + '/rvir/sfr'].value
		sfr_inner = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius + '/sfr'].value
		t = np.array([time(xxx) for xxx in redshift])

		step = 1e8

		t_list = np.arange(np.min(t), np.max(t), step)
		t_list = np.append(t_list, np.max(t))

		t1 = []
		sfr_gal1 = []
		sfr_inner1 = []	

		for xxx in range(len(t_list)-1):

			sel = (t >= t_list[xxx]) & (t <= t_list[xxx+1])
			t_new = t[sel]
			sfr_gal_new = sfr_gal[sel]
			sfr_inner_new = sfr_inner[sel]
			
			t1.append(np.sum(t_new)/len(t_new))
			sfr_gal1.append(np.sum(sfr_gal_new)/len(sfr_gal_new))
			sfr_inner1.append(np.sum(sfr_inner_new)/len(sfr_inner_new))

		z1 = np.array([z_at_value(Planck15.age, xxx * 1e-9 * u.Gyr) for xxx in t1])
		mstar_gal = integrate.cumtrapz(sfr_gal1, t1, initial=1e7)
		mstar_inner = integrate.cumtrapz(sfr_inner1, t1, initial=1e7)
		#mstar_gal[0] = mstar_gal[1]
		#mstar_inner[0] = mstar_inner[1]
		mdot = mstar_inner * 1e-10
		mbh = integrate.cumtrapz(mdot, t1, initial=1e4)
		mbh_lower = mbh * 1e-1 
		mbh_upper = mbh * 1e1
		#mbh[0] = mbh[1]

		#for xxx in range(len(t1)):
		#	print '%-10.3e %-10.3e %-10.3e %-10.3e %-10.3e %-10.3e \n' %(t1[xxx], sfr_gal1[xxx], sfr_inner1[xxx], mstar_gal[xxx], mstar_inner[xxx], mbh[xxx])

		z_ticks = np.arange(int(np.min(redshift)), int(np.max(redshift))+1)

		fig = plt.figure(1, figsize=(8,12))
		fig.subplots_adjust(wspace=0.0, hspace=0.3)
		fig.suptitle(sim_name[i])

		ax0 = fig.add_subplot(411)	
		ax0.set_xscale('linear')
		ax0.set_yscale('log')
		ax0.set_xlim(0.9*np.min(z1),1.1*np.max(z1))
		#ax0.set_xlim(0.9*np.min(t),1.1*np.max(t))
		ax0.set_ylim(1e-2,1e3)
		ax0.plot(redshift,sfr_gal,'r-',lw=2,label=r'$GAL$')
		ax0.plot(redshift,sfr_inner,'k-',lw=2,label=r'$INNER$')
		#ax0.plot(t,sfr_gal,'r-',lw=2,label=r'$GAL$')
		#ax0.plot(t,sfr_inner,'k-',lw=2,label=r'$INNER$')
		ax0.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax0.set_xlabel(r'$z$')
		#ax0.set_xlabel(r'$time\;(Gyr)$')
		ax0.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
		ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

		ax1 = fig.add_subplot(412)	
		ax1.set_xscale('linear')
		ax1.set_yscale('log')
		ax1.set_xlim(0.9*np.min(z1),1.1*np.max(z1))
		#ax1.set_xlim(0.9*np.min(t),1.1*np.max(t))
		ax1.set_ylim(1e-2,1e3)
		ax1.plot(z1,sfr_gal1,'r-',lw=2,label=r'$GAL$')
		ax1.plot(z1,sfr_inner1,'k-',lw=2,label=r'$INNER$')
		#ax1.plot(t1,sfr_gal1,'r-',lw=2,label=r'$GAL$')
		#ax1.plot(t1,sfr_inner1,'k-',lw=2,label=r'$INNER$')
		ax1.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax1.set_xlabel(r'$z$')
		#ax1.set_xlabel(r'$time\;(Gyr)$')
		ax1.set_ylabel(r'$<SFR>\;(M_{\odot}\;yr^{-1})$')
		ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

		ax2 = fig.add_subplot(413)	
		ax2.set_xscale('linear')
		ax2.set_yscale('log')
		ax2.set_xlim(0.9*np.min(z1),1.1*np.max(z1))
		#ax2.set_xlim(0.9*np.min(t),1.1*np.max(t))
		ax2.set_ylim(1e6,1e12)
		ax2.plot(z1,mstar_gal,'r-',lw=2,label=r'$GAL$')
		ax2.plot(z1,mstar_inner,'k-',lw=2,label=r'$INNER$')
		#ax2.plot(t1,mstar_gal,'r-',lw=2,label=r'$GAL$')
		#ax2.plot(t1,mstar_inner,'k-',lw=2,label=r'$INNER$')
		ax2.xaxis.set_major_locator(plt.FixedLocator(z_ticks))
		ax2.set_xlabel(r'$z$')
		#ax2.set_xlabel(r'$time\;(Gyr)$')
		ax2.set_ylabel(r'$M_{*}\;(M_{\odot})$')
		ax2.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

		ax3 = fig.add_subplot(414)	
		ax3.set_xscale('log')
		ax3.set_yscale('log')
		ax3.set_xlim(1e7,1e12)
		ax3.set_ylim(1e3,1e11)
		ax3.plot(mstar, bh_mass, color='k', lw=2, label=r'$REAL$')
		ax3.plot(mstar_gal, mbh, color='r', ls='-', lw=2, label=r'$M_{BH}$')
		ax3.plot(mstar_gal, mbh_lower, color='g', ls='-', lw=2, label=r'$0.1x\;M_{BH}$')
		ax3.plot(mstar_gal, mbh_upper, color='b', ls='-', lw=2, label=r'$10x\;M_{BH}$')
		#ax3.plot(mstar_inner, mbh, color='r', ls='-', lw=2, label=r'$INNER$')
		ax3.plot(mass_list, Reines2015_data, color='k', ls='--', lw=1, label=r'$Reines+15$')
		ax3.fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='black', zorder=0, alpha=0.1 )
		ax3.set_xlabel(r'$M_{*}\;(M_{\odot})$')
		ax3.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
		ax3.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

		#plt.show()
		#plt.tight_layout()
		fig.savefig('plot_5_toy_model_' + sim_name[i] + '.pdf')
		plt.close()

