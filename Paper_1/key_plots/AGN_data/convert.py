import numpy as np
import h5py
import glob

def calc_sfr(lum_sfr):
	return lum_sfr * 1.49e-10 / 3.84e33

def calc_mdot(lum_agn):
	eps = 0.1
	return lum_agn * (1.0 - eps) / eps / 2.0e33 / 9e20 * 3.15e7

def calc_lum_agn(mdot):
	eps = 0.1
	return (eps / (1.0 - eps)) * mdot * 2.0e33 * 9.0e20 / 3.15e7

def calc_lum_sfr(sfr):
	return sfr / 1.49e-10 * 3.84e33


filelist1 = sorted(glob.glob('Lei+15*.txt'))
filelist2 = sorted(glob.glob('WISSH+18_Lum*.txt'))
filelist3 = sorted(glob.glob('WISSH+18_sfr*.txt'))

data = h5py.File('lum_data.hdf5', 'w')

for filename in filelist1:
	
	lum_agn, lum_sfr = np.genfromtxt(filename, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	lum_agn = (10.0**lum_agn) * 1e46
	lum_sfr = (10.0**lum_sfr) * 1e46
	mdot = calc_mdot(lum_agn)
	sfr = calc_sfr(lum_sfr)

	data.create_dataset(filename[:-4] + '/lum_agn', data=lum_agn, compression='gzip')
	data.create_dataset(filename[:-4] + '/lum_sfr', data=lum_sfr, compression='gzip')
	data.create_dataset(filename[:-4] + '/mdot', data=mdot, compression='gzip')
	data.create_dataset(filename[:-4] + '/sfr', data=sfr, compression='gzip')

for filename in filelist2:

	lum_agn, lum_sfr = np.genfromtxt(filename, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	mdot = calc_mdot(lum_agn)
	sfr = calc_sfr(lum_sfr)
	print filename
	print lum_sfr / lum_agn
	data.create_dataset(filename[:-4] + '/lum_agn', data=lum_agn, compression='gzip')
	data.create_dataset(filename[:-4] + '/lum_sfr', data=lum_sfr, compression='gzip')
	data.create_dataset(filename[:-4] + '/mdot', data=mdot, compression='gzip')
	data.create_dataset(filename[:-4] + '/sfr', data=sfr, compression='gzip')

for filename in filelist3:
	
	sfr, mdot = np.genfromtxt(filename, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	sfr = 10.0**sfr
	mdot = 10.0**mdot
	lum_agn = calc_lum_agn(mdot)
	lum_sfr = calc_lum_sfr(sfr)

	if isinstance(lum_agn, np.ndarray) == True:
		data.create_dataset(filename[:-4] + '/lum_agn', data=lum_agn, compression='gzip')
		data.create_dataset(filename[:-4] + '/lum_sfr', data=lum_sfr, compression='gzip')
		data.create_dataset(filename[:-4] + '/mdot', data=mdot, compression='gzip')
		data.create_dataset(filename[:-4] + '/sfr', data=sfr, compression='gzip')
	else:
		data.create_dataset(filename[:-4] + '/lum_agn', data=np.array([lum_agn]), compression='gzip')
		data.create_dataset(filename[:-4] + '/lum_sfr', data=np.array([lum_sfr]), compression='gzip')
		data.create_dataset(filename[:-4] + '/mdot', data=np.array([mdot]), compression='gzip')
		data.create_dataset(filename[:-4] + '/sfr', data=np.array([sfr]), compression='gzip')

data.close()
