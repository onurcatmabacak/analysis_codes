import numpy as np
import glob
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.colors as mcolors
import matplotlib as mlp
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'
extra_list = [12, 13, 33, 34, 35, 36]

data = {}

for merger in merger_list:

	for center in center_list:
		for mass_ret in mass_ret_list:

			props = ['mbh', 'mdot', 'f', 'mstar', 'sfr', 'redshift']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for i in range(37):

				filename = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5'

				if i in extra_list:

					read_path = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i]
					remove = len(read_path) + 6
					filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

					halo_ID_list = []
					for f in filelist:
						halo_ID_list.append(f[remove:-4])

					halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
				else:
					halo_ID_list = [0]

				f = h5py.File(filename, 'r')

				for halo_ID in halo_ID_list:

					data[center + '/' + merger + '/mdot'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/mdot_torque'].value)
					data[center + '/' + merger + '/mbh'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value)
					data[center + '/' + merger + '/f'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value)
					data[center + '/' + merger + '/redshift'].append(f['halo_' + str(halo_ID) + '/total/rvir/z_1'].value)
					data[center + '/' + merger + '/sfr'].append(f['halo_' + str(halo_ID) + '/total/rvir/sfr'].value)
					data[center + '/' + merger + '/mstar'].append(f['halo_' + str(halo_ID) + '/total/rvir/mstar'].value)

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

fig, axes = plt.subplots(2,2, figsize=(20,15))
fig.subplots_adjust(wspace=0.0, hspace=0.0)

cm = mcolors.ListedColormap(['magenta', 'cyan', 'greenyellow', 'brown'])   # Magenta (or purple), cyan, greenish/gold, brown.
cm1 = mcolors.ListedColormap(['magenta', 'cyan', 'greenyellow', 'brown'])
c_array = np.array([4.0, 4.5, 5.0, 6.0, 8.0])
norm = mlp.colors.BoundaryNorm(c_array, cm.N)
norm1 = mlp.colors.BoundaryNorm(c_array, cm1.N)

light_speed = 3e10
solar_lum = 3.839e33
eps = 0.1
solar_mass = 2.0e33
year_to_s = 3.15e7
mbhseed = 1e3
mstarseed = 1e6

for ax, merger, center, flag in zip(axes.flat, ['merger','nomerger','merger','nomerger'], ['MDC','MDC','COM','COM'], [1,2,3,4]):

	z = data[center + '/' + merger + '/redshift']
	mdot = data[center + '/' + merger + '/mdot']
	sfr = data[center + '/' + merger + '/sfr']

	sel = (z > 4.0) & (mdot > 0.0) & (sfr > 0.0)
	z = data[center + '/' + merger + '/redshift'][sel]
	mdot = data[center + '/' + merger + '/mdot'][sel]
	mbh = data[center + '/' + merger + '/mbh'][sel]
	f = data[center + '/' + merger + '/f'][sel]
	sfr = data[center + '/' + merger + '/sfr'][sel]
	mstar = data[center + '/' + merger + '/mstar'][sel]

	# READ OBSERVATIONAL DATA IF THERE IS ANY
	#########################################
	
	t_sfr_z45, t_acc_z45 = np.genfromtxt('AGN_data/WISSH+18_t_sfr_acc_z45.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	t_sfr_z6, t_acc_z6 = np.genfromtxt('AGN_data/WISSH+18_t_sfr_acc_z6.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	t_sfr_J1015, t_acc_J1015 = np.genfromtxt('AGN_data/WISSH+18_t_sfr_acc_J1015+0200.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	t_tau_z45, t_edd_z45 = np.genfromtxt('AGN_data/WISSH+18_t_tau_edd_z45.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	t_tau_z6, t_edd_z6 = np.genfromtxt('AGN_data/WISSH+18_t_tau_edd_z6.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)
	t_tau_J1015, t_edd_J1015 = np.genfromtxt('AGN_data/WISSH+18_t_tau_edd_J1015+0200.txt', delimiter='', usecols=(0,1), skip_header=1, unpack=True)

	#########################################

	# TIMESCALES

	t_acc = mbh / mdot
	t_sfr = mstar / sfr
	tau_acc = 0.45e9
	t_edd = tau_acc * (eps / (1.0 - eps)) * f * np.log(mbh / mbhseed)
	t_tau = t_sfr * np.log(mstar / mstarseed)
	
	t_sfr = np.log10(t_sfr * 1e-9) 
	t_acc = np.log10(t_acc * 1e-9)
	t_tau = np.log10(t_tau * 1e-9)
	t_edd = np.log10(t_edd * 1e-9)

	# PLOT
	#ax.set_title('%s' %(merger + ' ' + center), fontsize=20)
	sc = ax.scatter(t_sfr, t_acc, c=z, alpha=0.0, vmin=np.min(z), vmax=np.max(z), cmap=cm, norm=norm)
	
	if flag == 1:
		mf = ax.scatter(-10, -10, color='k', marker='.', s=200)

	sel_z1 = (z > c_array[0]) & (z < c_array[1])
	sel_z2 = (z > c_array[1]) & (z < c_array[2])
	sel_z3 = (z > c_array[2]) & (z < c_array[3])
	sel_z4 = (z > c_array[3]) & (z < c_array[4])

	#ax.scatter(t_sfr[sel_z4], t_acc[sel_z4], c=z[sel_z4], alpha=0.5, marker='.', s=200, cmap=cm, norm=norm)
	#ax.scatter(t_sfr[sel_z3], t_acc[sel_z3], c=z[sel_z3], alpha=0.5, marker='.', s=200, cmap=cm, norm=norm)
	#ax.scatter(t_sfr[sel_z2], t_acc[sel_z2], c=z[sel_z2], alpha=1.0, marker='.', s=200, cmap=cm, norm=norm)
	#ax.scatter(t_sfr[sel_z1], t_acc[sel_z1], c=z[sel_z1], alpha=0.5, marker='.', s=200, cmap=cm, norm=norm)

	ax.scatter(t_tau[sel_z1], t_edd[sel_z1], c=z[sel_z1], alpha=0.75, marker='.', s=80, cmap=cm, norm=norm)
	ax.scatter(t_tau[sel_z2], t_edd[sel_z2], c=z[sel_z2], alpha=0.75, marker='.', s=80, cmap=cm, norm=norm)
	ax.scatter(t_tau[sel_z3], t_edd[sel_z3], c=z[sel_z3], alpha=0.75, marker='.', s=80, cmap=cm, norm=norm)
	ax.scatter(t_tau[sel_z4], t_edd[sel_z4], c=z[sel_z4], alpha=0.75, marker='.', s=80, cmap=cm, norm=norm)

	#ax.scatter(t_sfr_z6, t_acc_z6, c=np.ones(len(t_sfr_z6))*6.0, alpha=0.5, marker='s', s=400, cmap=cm, norm=norm)
	#ax.scatter(np.array([-10, t_sfr_J1015]), np.array([-10, t_acc_J1015]), c=np.ones(2)*4.4, alpha=1.0, marker='^', s=400, cmap=cm, norm=norm)
	#ax.scatter(t_sfr_z45, t_acc_z45, c=np.ones(len(t_sfr_z45))*4.5, alpha=0.5, marker='*', s=400, cmap=cm, norm=norm)
	ax.scatter(t_tau_z45, t_edd_z45, c=np.ones(len(t_tau_z45))*4.5, alpha=0.5, marker='*', s=200, cmap=cm, norm=norm)
	ax.scatter(t_tau_z6, t_edd_z6, c=np.ones(len(t_tau_z6))*6.0, alpha=0.5, marker='s', s=200, cmap=cm, norm=norm)
	ax.scatter(np.array([-10, t_tau_J1015]), np.array([-10, t_edd_J1015]), c=np.ones(2)*4.4, alpha=0.5, marker='^', s=200, cmap=cm, norm=norm)

	array = np.array([-7.0, -6.0, -5.0, -4.0, -3.0, -2.0, -1.0, 0.0, 1.0, 2.0, 3.0, 4.0, 5.0])
	line1, = ax.plot(array, array + 1.0, color='k', lw=2, ls='--')
	line2, = ax.plot(array, array + 0.0, color='k', lw=2, ls='--')
	line3, = ax.plot(array, array - 1.0, color='k', lw=2, ls='--')
	#line4, = ax.plot(array, array - 2.0, color='k', lw=2, ls='--')
	#line5, = ax.plot(array, array - 3.0, color='k', lw=2, ls='--')

	if (flag == 3) or (flag == 4):
		ax.set_xlabel(r'$log(t_{\tau} / Gyr)$', fontsize=40)
	if (flag == 1) or (flag == 3):
		ax.set_ylabel(r'$log(t_{edd} / Gyr)$', fontsize=40)

	ax.set_xscale('linear')
	ax.set_yscale('linear')
	ax.set_xlim(-3.0, 5.0)
	ax.set_ylim(-6.0, 2.0)
	ax.xaxis.set_tick_params(labelsize=30)
	ax.yaxis.set_tick_params(labelsize=30)

	ax.xaxis.set_major_locator(ticker.FixedLocator([-2,-1,0,1,2,3,4]))	
	ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))	
	ax.yaxis.set_major_locator(ticker.FixedLocator([-5,-4,-3,-2,-1,0,1]))	
	ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))	

	if (flag == 2) or (flag == 4):
		ax.yaxis.set_tick_params(labelsize=1e-100)

	ax.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
	ax.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)

	ax.text(-2.8, 1.5, '%s' %(merger + ' ' + center), fontsize=20)

	if flag == 1:
		ax.text(-0.3, 1.5, '10:1', fontsize=20, rotation=45)
		ax.text(0.8, 1.5, '1:1', fontsize=20, rotation=45)
		ax.text(1.7, 1.5, '0.1:1', fontsize=20, rotation=45)
		ax.legend([mf], ['MassiveFIRE'], loc=4, fontsize=20, frameon=False, scatterpoints=1)

	ax.legend(loc=0,fontsize=20,frameon=False,scatterpoints=1)

#fig.subplots_adjust(left=0.8)
cbar = fig.colorbar(sc, pad=0.05, ticks=c_array, ax=axes.ravel().tolist())
cbar.set_label('    z', rotation=0, fontsize=30)
cbar.ax.tick_params(labelsize=30)
cbar.solids.set(alpha=1)
#plt.tight_layout()
#plt.show()
#quit()

fig.savefig('growth_timescales_tau_edd_z4.pdf')
plt.close()
