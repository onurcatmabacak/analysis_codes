import numpy as np
import os
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def Haring04(Mbulge):
   return 10.**( 8.20 ) * ( Mbulge/1e11 )**(1.12)

def Kormendy2013(Mbulge):
   return 0.49 * 1e9 * ( Mbulge/1e11 )**(1.16)

def McConnell2013(Mbulge):
   return 10.**( 8.46 ) * ( Mbulge/1e11 )**(1.05)

def Reines2015(Mstar):
   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

def Ferrerase02(Mass):	
	#return 10.**( 8.0 ) * 0.027 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	return 10.**( 8.0 ) * 0.100 * ( Mass/1e12 )**(1.65) # Ferrerase 2002
	#return 10.**( 8.0 ) * 0.670 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	#return 10.**( 8.18 ) * ( Mass/1e13 )**(1.55) # Bandara et al 2009
	#return 10.**( 7.85 ) * ( Mass/1e12 )**(1.33) # Di Matteo et al 2003

def McConnell_Ma2013(vel_disp):
	return 10.**( 8.32 ) * ( vel_disp/200.0 )**(5.64)

mass_list = np.array([1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12,1e13,1e14])
vel_list = np.array([0,20,40,60,80,100,200,400,800])
Ferrerase02_data = []
Haring04_data = []
Kormendy2013_data = []
McConnell2013_data = []
Reines2015_data = []
Savorgnan2016_data = []
McConnell_Ma2013_data = []

for mass_dummy in mass_list:
	Ferrerase02_data.append(Ferrerase02(mass_dummy))
	Haring04_data.append(Haring04(mass_dummy))
	Kormendy2013_data.append(Kormendy2013(mass_dummy))
	McConnell2013_data.append(McConnell2013(mass_dummy))
	Reines2015_data.append(Reines2015(mass_dummy))
	#Savorgnan2016_data.append(Savorgnan2016(mass_dummy))

for vel_dummy in vel_list:
	McConnell_Ma2013_data.append(McConnell_Ma2013(vel_dummy))

Ferrerase02_data = np.array(Ferrerase02_data)
Haring04_data = np.array(Haring04_data)
Kormendy2013_data = np.array(Kormendy2013_data)
McConnell2013_data = np.array(McConnell2013_data)
Reines2015_data = np.array(Reines2015_data)
#Savorgnan2016_data = np.array(Savorgnan2016_data)
McConnell_Ma2013_data = np.array(McConnell_Ma2013_data)

sim_name = ['B100_N512_TL00002', 'B100_N512_TL00029', 'B100_N512_TL00113', 'B100_N512_TL00206', 'h2', 'h29', 'h113', 'h206']
label = ['A8', 'A2', 'A4', 'A1', 'A8', 'A2', 'A4', 'A1']
h_sim_name = ['h2', 'h29', 'h113', 'h206', 'h2', 'h29', 'h113', 'h206']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
mass_ret_list = ['mass_ret_0.1']
merger_list = ['merger', 'nomerger']
output_path = 'bh_mass_acc_models_r0.1_GTDA'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for mass_ret in mass_ret_list:
	for merger in merger_list:

		for i in range(len(sim_name)):

			filename_daa = '../galdata/' + h_sim_name[i] + '_HR_galdata_00000.hdf5'
			filename_daa_bh = '../galdata/' + h_sim_name[i] + '_HR_galdata_fbh_00000.hdf5'
			filename_oc_com = glob.glob('../../COM/' + sim_suite[i] + '/' + sim_name[i] + '_*/' + sim_name[i] + '*_' + merger + '.hdf5')[0]
			filename_oc_mdc = glob.glob('../../MDC/' + sim_suite[i] + '/' + sim_name[i] + '_*/' + sim_name[i] + '*_' + merger + '.hdf5')[0]

			data_daa = h5py.File(filename_daa, 'r')
			data_daa_bh = h5py.File(filename_daa_bh, 'r')
			data_oc_com = h5py.File(filename_oc_com, 'r')
			data_oc_mdc = h5py.File(filename_oc_mdc, 'r')
			
			mbh_daa = data_daa['100pc/Mbh'].value
			mbh_daa_bh = data_daa_bh['100pc/Mbh'].value

			if sim_suite[i] == 'MassiveFIRE':
				radius = 'r_0.1'
			else:
				radius = 'r_0.1'

			mbh_com_torque = data_oc_com['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
			mbh_mdc_torque = data_oc_mdc['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
			mbh_com_dyn100 = data_oc_com['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_dyn_1'].value
			mbh_com_dyn10 = data_oc_com['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_dyn_2'].value
			mbh_com_dyn1 = data_oc_com['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_dyn_3'].value
			mbh_com_bondi = data_oc_com['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_bondi'].value
			mbh_com_sfr = data_oc_com['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_sfr'].value
			mbh_mdc_dyn100 = data_oc_mdc['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_dyn_1'].value
			mbh_mdc_dyn10 = data_oc_mdc['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_dyn_2'].value
			mbh_mdc_dyn1 = data_oc_mdc['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_dyn_3'].value
			mbh_mdc_bondi = data_oc_mdc['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_bondi'].value
			mbh_mdc_sfr = data_oc_mdc['total/' + radius + '/' + mass_ret + '/seed_1e4/m_bh_sfr'].value

			mbulge_daa = data_daa['0.1Rvir/MbulgeStar'].value
			mbulge_daa_bh = data_daa_bh['0.1Rvir/MbulgeStar'].value
			mbulge_com = data_oc_com['total/rvir/mbulge'].value
			mbulge_mdc = data_oc_mdc['total/rvir/mbulge'].value

			#sigma_daa = np.linalg.norm(data_daa['0.1Rvir/SigmaStar'].value, axis=1)
			#sigma_daa_bh = np.linalg.norm(data_daa_bh['0.1Rvir/SigmaStar'].value, axis=1)
			#sigma_oc_nomerger = data_oc_nomerger['cold/rvir/vel_disp'].value
			#sigma_oc_nomerger = data_oc_nomerger['cold/rvir/vel_disp'].value

			daa_otf_mbulge, daa_otf_mbh = np.genfromtxt('../Daniel_Data/' + h_sim_name[i] + '_otf.txt', delimiter='',usecols=(0,1), skip_header=0, unpack=True)
			daa_pp_mbulge, daa_pp_mbh = np.genfromtxt('../Daniel_Data/' + h_sim_name[i] + '_pp.txt', delimiter='',usecols=(0,1), skip_header=0, unpack=True)

			daa_otf_mbulge = 10.0 ** daa_otf_mbulge
			daa_pp_mbulge = 10.0 ** daa_pp_mbulge
			daa_otf_mbh = 10.0 ** daa_otf_mbh
			daa_pp_mbh = 10.0 ** daa_pp_mbh

			z_ticks = np.arange(1,12)
			mass_ticks = np.array([1e7,1e8,1e9,1e10,1e11])

			fig = plt.figure(3)
			fig.suptitle(label[i])
			fig.subplots_adjust(wspace=0.3, hspace=0)

			ax1 = fig.add_subplot(121)
			ax1.set_title('COM')
			ax1.plot(daa_otf_mbulge, daa_otf_mbh, c='b', ls='-', lw=1, label='daa otf')
			#ax1.plot(daa_pp_mbulge, daa_pp_mbh, c='b', ls='--', lw=1, label='daa pp')
			ax1.plot(mbulge_daa_bh, mbh_daa_bh, c='k', ls='-', lw=1, label='daa bh')
			#ax1.plot(mbulge_daa, mbh_daa, c='r', ls='--', lw=1, label='daa')
			ax1.plot(mbulge_com, mbh_com_torque, c='r', ls='-', lw=1, label='GTDA')
			#ax1.plot(mbulge_com, mbh_com_dyn100, c='r', ls='--', lw=1, label='dyn 1e-3')
			#ax1.plot(mbulge_com, mbh_com_dyn10, c='g', ls='--', lw=1, label='dyn 2e-4')
			ax1.plot(mbulge_com, mbh_com_dyn1, c='b', ls='--', lw=1, label=r'$dyn\; \alpha = 10^{-4}$')
			#ax1.plot(mbulge_com, mbh_com_bondi, c='m', ls='-', lw=1, label='bondi')
			#ax1.plot(mbulge_com, mbh_com_sfr, c='g', ls='-', lw=1, label='sfr')
			ax1.plot(mass_list, Reines2015_data, color='k', ls='--', lw=2, label='Reines15')
			ax1.set_xlim(1e7,1e12)
			ax1.set_ylim(1e4,5e9)
			ax1.set_xscale('log')
			ax1.set_yscale('log')
			ax1.set_xlabel(r'$Bulge\;Mass\;(M_{\odot})$')
			ax1.set_ylabel(r'$BH\;Mass\;(M_{\odot})$')
			ax1.grid(color='k', ls='--', lw=0.25)
			ax1.fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )
			ax1.xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
			ax1.legend(loc=2,prop={'size':6},frameon=False,scatterpoints=1)

			ax2 = fig.add_subplot(122)
			ax2.set_title('MDC')
			ax2.plot(daa_otf_mbulge, daa_otf_mbh, c='b', ls='-', lw=1, label='daa otf')
			#ax2.plot(daa_pp_mbulge, daa_pp_mbh, c='b', ls='--', lw=1, label='daa pp')
			ax2.plot(mbulge_daa_bh, mbh_daa_bh, c='k', ls='-', lw=1, label='daa bh')
			#ax2.plot(mbulge_daa, mbh_daa, c='r', ls='--', lw=2, label='daa')
			ax2.plot(mbulge_mdc, mbh_mdc_torque, c='r', ls='-', lw=1, label='GTDA')
			#ax2.plot(mbulge_mdc, mbh_mdc_dyn100, c='r', ls='-.', lw=1, label='dyn 1e-3')
			#ax2.plot(mbulge_mdc, mbh_mdc_dyn10, c='g', ls='-.', lw=1, label='dyn 2e-4')
			ax2.plot(mbulge_mdc, mbh_mdc_dyn1, c='b', ls='-.', lw=1, label=r'$dyn\; \alpha = 10^{-4}$')
			#ax2.plot(mbulge_mdc, mbh_mdc_bondi, c='m', ls='-', lw=1, label='bondi')
			#ax2.plot(mbulge_mdc, mbh_mdc_sfr, c='g', ls='-', lw=1, label='sfr')
			ax2.plot(mass_list, Reines2015_data, color='k', ls='--', lw=2, label='Reines15')
			ax2.set_xlim(1e7,1e12)
			ax2.set_ylim(1e4,5e9)
			ax2.set_xscale('log')
			ax2.set_yscale('log')
			ax2.set_xlabel(r'$Bulge\;Mass\;(M_{\odot})$')
			ax2.set_ylabel(r'$BH\;Mass\;(M_{\odot})$')
			ax2.grid(color='k', ls='--', lw=0.25)
			ax2.fill_between(mass_list, Reines2015_data * np.sqrt(1e1), Reines2015_data / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )
			ax2.xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
			ax2.legend(loc=2,prop={'size':6},frameon=False,scatterpoints=1)
			#plt.show()
			fig.savefig(output_path + '/bh_mass_acc_models_' + sim_name[i] + '_' +  merger + '_' + mass_ret + '.pdf')
			plt.close()
