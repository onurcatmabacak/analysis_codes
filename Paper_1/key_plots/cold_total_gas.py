import numpy as np
import h5py
import matplotlib.pyplot as plt
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

sim_name_MF2 = ['h206_HR_sn1dy300ro100ss']
sim_suite_MF2 = ['MassiveFIRE2']
label = ['0.1\;kpc', '0.2\;kpc', '0.5\;kpc', '1.0\;kpc', '\%10\;R_{vir}']
radius = ['0.1', '0.2', '0.5', '1.0', 'rvir']
color = ['r', 'g', 'b', 'k', 'm']
regime_list = ['COM', 'MDC']
fontsize = 24
xmax = [12]

for i in range(len(sim_name_MF2)):

	fig = plt.figure(1, figsize=(25,10))
	fig.subplots_adjust(wspace=0, hspace=0)

	total_mgas = {}
	cold_mgas = {}

	redshift = {}
	time = {}

	for regime in regime_list:

		data = h5py.File('/bulk1/feldmann/Onur/' + regime + '/' + sim_suite_MF2[i] + '/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_merger.hdf5','r')

		redshift[regime] = data['total/rvir/z_1'].value
		time[regime] = cosmo.age(redshift[regime]).value

		for j in range(len(radius)):
			total_mgas[regime + '/' + radius[j]] = data['total/' + radius[j] + '/mgas'].value
			cold_mgas[regime + '/' + radius[j]] = data['cold/' + radius[j] + '/mgas'].value
			
	ax1 = fig.add_subplot(221)	
	#ax1.set_title('COM')

	#ax1.plot(0.0, 0.0, color='k', ls='-', lw=2, label='total gas')

	for j in range(len(radius)):
		k = len(radius) - (j+1)
		#ax1.plot(redshift['COM'], total_mgas['COM/' + radius[k]], color=color[k], ls='-', lw=2, label=r'$%s$' %label[k])   
		ax1.plot(redshift['COM'], total_mgas['COM/' + radius[k]], color=color[k], ls='-', lw=2)   
		
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(0.8,9)
	ax1.set_ylim(1e4,1e11)
	ax1.xaxis.set_major_locator(plt.FixedLocator([1,2,3,4,5,6,7,8]))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1e5,1e6,1e7,1e8,1e9,1e10]))
	ax1.xaxis.set_tick_params(labelsize=fontsize)
	ax1.yaxis.set_tick_params(labelsize=fontsize)
	ax1.set_xlabel('z', fontsize=fontsize)
	#ax1.tick_params(axis='y', labelsize=30)
	ax1.set_ylabel(r'$M_{gas,\; tot}\;(M_{\odot})$', fontsize=fontsize)
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		#dummy_age = z_at_value(Planck13.age, X * u.Gyr)
		ax12Xs.append("%.1f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	ax12.set_xlabel(r'$time\;(Gyr)$', fontsize=fontsize)
	ax12.tick_params(axis='x', labelsize=fontsize)
	ax1.text(1.0, 2e4, 'COM', fontsize=fontsize)
	#ax1.legend(loc=0,prop={'size':fontsize},frameon=False,scatterpoints=1)
	ax1.grid(b=None, which='major', axis='both')

	ax2 = fig.add_subplot(223)	

	for j in range(len(radius)):
		k = len(radius) - (j+1)
		#cold_total_ratio = np.array([float('nan') if b==0.0 else a/b for a,b in zip(cold_mgas['COM/' + radius[k]],total_mgas['COM/' + radius[k]])])
		cold_total_ratio = np.array([float('nan') if b==0.0 else float('nan') if a/b <= 0.01 else a/b for a,b in zip(cold_mgas['COM/' + radius[k]],total_mgas['COM/' + radius[k]])])
		ax2.plot(redshift['COM'], cold_total_ratio, color=color[k], ls='-', lw=2)   
	
	ax2.set_xscale('linear')
	ax2.set_yscale('linear')
	ax2.set_xlim(0.8,9)
	ax2.set_ylim(0,1.1)
	ax2.xaxis.set_major_locator(plt.FixedLocator([1,2,3,4,5,6,7,8]))
	ax2.yaxis.set_major_locator(plt.MultipleLocator(0.2))
	ax2.xaxis.set_tick_params(labelsize=fontsize)
	ax2.yaxis.set_tick_params(labelsize=fontsize)
	ax2.set_xlabel('z', fontsize=fontsize)
	#ax2.tick_params(axis='y', labelsize=fontsize)
	#ax2.set_ylabel(r'$\frac{M_{gas,\;cold}}{M_{gas,\; tot}}$', fontsize=fontsize)
	ax2.set_ylabel(r'$M_{gas,\;cold} \; / \; M_{gas,\; tot}$', fontsize=fontsize)
	ax2.grid(b=None, which='major', axis='both')

	ax3 = fig.add_subplot(222)	
	#ax3.set_title('MDC')

	#ax3.plot(0.0, 0.0, color='k', ls='-', lw=2, label='total gas')

	for j in range(len(radius)):
		k = len(radius) - (j+1)
		ax3.plot(redshift['MDC'], total_mgas['MDC/' + radius[k]], color=color[k], ls='-', lw=2, label=r'$%s$' %label[k])   
		
	ax3.set_xscale('linear')
	ax3.set_yscale('log')
	ax3.set_xlim(0.8,9)
	ax3.set_ylim(1e4,1e11)
	ax3.xaxis.set_major_locator(plt.FixedLocator([1,2,3,4,5,6,7,8]))
	ax3.yaxis.set_major_locator(plt.FixedLocator([1e5,1e6,1e7,1e8,1e9,1e10]))
	ax3.xaxis.set_tick_params(labelsize=fontsize)
	ax3.yaxis.set_tick_params(labelsize=1e-100)
	ax3.set_xlabel('z', fontsize=fontsize)
	#ax3.tick_params(axis='y', labelsize=fontsize)
	#ax3.set_ylabel(r'$M_{gas,\; tot}\;(M_{\odot})$', fontsize=fontsize)
	ax32 = ax3.twiny()
	ax3Xs = ax3.get_xticks()
	ax32Xs = []
	for X in ax3Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		#dummy_age = z_at_value(Planck13.age, X * u.Gyr)
		ax32Xs.append("%.1f" %dummy_age)
	ax32.set_xticks(ax3Xs)
	ax32.set_xbound(ax3.get_xbound())
	ax32.set_xticklabels(ax32Xs)
	ax32.set_xlabel(r'$time\;(Gyr)$', fontsize=fontsize)
	ax32.tick_params(axis='x', labelsize=fontsize)
	ax3.text(1.0, 2e4, 'MDC', fontsize=fontsize)
	#ax3.legend(loc=0,prop={'size':12},frameon=False,scatterpoints=1)
	ax3.grid(b=None, which='major', axis='both')
	#ax3.get_yaxis().set_ticks([])

	ax4 = fig.add_subplot(224)	

	for j in range(len(radius)):
		k = len(radius) - (j+1)
		#cold_total_ratio = np.array([float('nan') if b==0.0 else a/b for a,b in zip(cold_mgas['MDC/' + radius[k]],total_mgas['MDC/' + radius[k]])])
		cold_total_ratio = np.array([float('nan') if b==0.0 else float('nan') if a/b <= 0.01 else a/b for a,b in zip(cold_mgas['MDC/' + radius[k]],total_mgas['MDC/' + radius[k]])])
		ax4.plot(redshift['MDC'], cold_total_ratio, color=color[k], ls='-', lw=2, label=r'$%s$' %label[k])   
	
	ax4.set_xscale('linear')
	ax4.set_yscale('linear')
	ax4.set_xlim(0.8,9)
	ax4.set_ylim(0,1.1)
	ax4.xaxis.set_major_locator(plt.FixedLocator([1,2,3,4,5,6,7,8]))
	ax4.yaxis.set_major_locator(plt.MultipleLocator(0.2))
	ax4.xaxis.set_tick_params(labelsize=fontsize)
	ax4.yaxis.set_tick_params(labelsize=1e-100)
	ax4.set_xlabel('z', fontsize=fontsize)
	#ax4.tick_params(axis='y', labelsize=fontsize)
	#ax4.set_ylabel(r'$\frac{M_{gas,\;cold}}{M_{gas,\; tot}}$', fontsize=fontsize)
	#ax4.set_ylabel(r'$M_{gas,\;cold} \; / \; M_{gas,\; tot}$', fontsize=fontsize)
	ax4.grid(b=None, which='major', axis='both')
	#ax4.get_yaxis().set_ticks([])
	ax4.legend(loc=0,prop={'size':fontsize},frameon=False,scatterpoints=1)

	#plt.show()
	#plt.tight_layout()
	fig.savefig('cold_total_gas.pdf')
	plt.close()

