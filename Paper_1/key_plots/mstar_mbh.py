import numpy as np
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.colors as mcolors
from scipy.optimize import curve_fit
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

def func(x, mbh, a, b, c):
	return a*np.log10(mbh) + b*np.log10(1.0+x) + c

def func1(x, a, b):
	return a*np.log10(1.0+x) + b

def Reines2015(Mstar):
   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
file_list = {}

mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'
extra_list = [12, 13, 33, 34, 35, 36]
mdot_list = ['mdot_torque', 'mdot_bondi', 'mdot_dyn_3', 'mdot_hobbs', 'mdot_sfr']
mbh_list = ['m_bh_torque', 'm_bh_bondi', 'm_bh_dyn_3', 'm_bh_hobbs', 'm_bh_sfr']
f_list = ['f_torque_edd', 'f_bondi_edd', 'f_dyn_edd_3', 'f_hobbs_edd', 'f_sfr_edd']
acc_model_list = ['GTDA', 'Bondi', 'Dyn', 'Hobbs', 'SFR']

for j in range(5):

	data = {}

	for merger in merger_list:
		for center in center_list:
			for mass_ret in mass_ret_list:

				props = ['mbh', 'mdot', 'f', 'mstar', 'redshift']
				for prop in props:
					data[center + '/' + merger + '/' + prop] = []

				for i in range(37):
					filename = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5' 

					if i in extra_list:

						read_path = '/bulk1/feldmann/Onur/' + center + '/' + sim_suite[i] + '/' + sim_name[i]
						remove = len(read_path) + 6
						filelist = sorted(glob.glob(read_path + '/halo_*.txt'))

						halo_ID_list = []
						for f in filelist:
							halo_ID_list.append(f[remove:-4])

						halo_ID_list = np.sort(np.array(halo_ID_list[:-1]).astype(int))
					else:
						halo_ID_list = [0]

					f = h5py.File(filename, 'r')

					for halo_ID in halo_ID_list:

						data[center + '/' + merger + '/mdot'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/' + mdot_list[j]].value)
						data[center + '/' + merger + '/mbh'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/' + mbh_list[j]].value)
						data[center + '/' + merger + '/f'].append(f['halo_' + str(halo_ID) + '/total/' + radius + '/' + mass_ret + '/seed_1e4/' + f_list[j]].value)
						data[center + '/' + merger + '/redshift'].append(f['halo_' + str(halo_ID) + '/total/rvir/z_1'].value)
						data[center + '/' + merger + '/mstar'].append(f['halo_' + str(halo_ID) + '/total/rvir/mstar'].value)

				for prop in props:
					data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

	x = np.array([1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4])
	z_list = np.array(range(13))

	fig, axes = plt.subplots(2,2, figsize=(20,10))
	fig.subplots_adjust(wspace=0, hspace=0)
	#cm = plt.cm.get_cmap('RdYlBu')
	# snow white w whitesmoke gainsboro lightgrey lightgray silver darkgrey darkgray grey gray dimgrey dimgray k black
	cm = mcolors.ListedColormap(['white', 'darkgray', 'gray', 'dimgray', 'black'])
	c_array = np.array([0.0, 1e0, 1e1, 1e2, 1e3, 1e4])
	norm = mcolors.BoundaryNorm(c_array, cm.N)

	for ax, merger, center, flag in zip(axes.flat, ['merger', 'nomerger', 'merger', 'nomerger'], ['MDC', 'MDC', 'COM', 'COM'], [1,2,3,4]):
		
		z = data[center + '/' + merger + '/redshift']
		f = data[center + '/' + merger + '/f']
		mdot = data[center + '/' + merger + '/mdot']
		mbh = data[center + '/' + merger + '/mbh']
		mstar = data[center + '/' + merger + '/mstar']

		# PLOT
		#ax.set_title('%s' %(merger + ' ' + center), fontsize=20)
		#sc = ax.scatter(np.log10(mstar), np.log10(mbh), c=z, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm)
		im = ax.hist2d(np.log10(mstar), np.log10(mbh), bins=(40,40), cmap=cm, norm=norm)
		array = np.array([5,6,7,8,9,10,11,12,13,14])
		ax.plot(array, array-3, color='k', lw=1, ls='-', label=r'$log \; M_{BH}/M_* \propto -3$')
		ax.plot(array, array-4, color='k', lw=1, ls='--', label=r'$log \; M_{BH}/M_* \propto -4$')
		ax.plot(array, np.log10(Reines2015(10**array)), color='k', lw=2, ls='-', label='RV15')	

		if (flag == 3) or (flag == 4):
			ax.set_xlabel(r'$log \; M_{*} \; (M_{\odot})$', fontsize=30)
		if (flag == 1) or (flag == 3):
			ax.set_ylabel(r'$log \; M_{BH} \; (M_{\odot})$', fontsize=30)

		ax.set_xscale('linear')
		ax.set_yscale('linear')
		ax.set_xlim(6.8,12.0)
		ax.set_ylim(3.8, 9.2)
		ax.xaxis.set_tick_params(labelsize=20)
		ax.yaxis.set_tick_params(labelsize=20)
		ax.xaxis.set_major_locator(ticker.FixedLocator([8,9,10,11,12]))	
		ax.xaxis.set_minor_locator(ticker.MultipleLocator(0.1))	
		ax.yaxis.set_major_locator(ticker.FixedLocator([4,5,6,7,8]))	
		ax.yaxis.set_minor_locator(ticker.MultipleLocator(0.1))	

		ax.tick_params(axis='both', which='minor', direction='in', length=5.0, width=1.0)
		ax.tick_params(axis='both', which='major', direction='in', length=10.0, width=2.0)

		if (flag == 1) or (flag == 2):
			ax.xaxis.set_tick_params(labelsize=1e-100)
		if (flag == 2) or (flag == 4):
			ax.yaxis.set_tick_params(labelsize=1e-100)
		
		if flag == 1:
			ax.text(7.0, 6.7, '%s' %(merger + ' ' + center), fontsize=20)
		else:
			ax.text(7.0, 8.5, '%s' %(merger + ' ' + center), fontsize=20)

		if flag == 1:
			ax.legend(loc=0,prop={'size':20},frameon=False,scatterpoints=1)

		#if flag == 1:
		#	ax.legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

	#fig.subplots_adjust(left=0.8)
	#fig.colorbar(sc, ax=axes.ravel().tolist())
	cbar = fig.colorbar(im[3], ticks=c_array, ax=axes.ravel().tolist())
	cbar.solids.set(alpha=1)
	#plt.tight_layout()
	#plt.show()
	#quit()

	fig.savefig('mstar_mbh_' + acc_model_list[j] + '.pdf')
	plt.close()

