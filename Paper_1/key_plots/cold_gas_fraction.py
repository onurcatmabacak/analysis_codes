import numpy as np
import h5py
import sys
sys.path.append('/bulk1/feldmann/Onur')
import onur_tools as ot

ot.global_variables()

sim_name = ['B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'h113_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
regime = ['COM', 'MDC']
AHF = ['AHF_com', 'AHF']
temp_limit = 1e5

output = h5py.File('cold_gas_ratio.hdf5', 'w')

for k in range(len(regime)):
	for i in range(len(sim_name)):
		
		zred_pos = '/bulk1/feldmann/Onur/' + regime[k] + '/' + sim_suite[i] + '/' + sim_name[i] + '/zred_pos_list'
		timeline = '/bulk1/feldmann/Onur/' + regime[k] + '/' + sim_suite[i] + '/' + sim_name[i] + '/timeline_0.txt'
		redshift, pos = np.genfromtxt(zred_pos, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
		zred, ID = np.genfromtxt(timeline, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	
		z_min = float(int(np.min(zred)))
		z_max = float(int(np.max(zred)))+1.0
			
		if z_min < np.min(zred):
			z_min += 1.0

		z_list = np.arange(z_min, z_max)

		print regime[k]
		print sim_name[i]
		print np.min(zred), np.max(zred)
		print z_list

		new_pos = []
		new_ID = []

		for j in range(len(z_list)):
			new_ID.append(int(ID[(zred == z_list[j])][0]))
			new_pos.append(int(pos[(redshift == z_list[j])][0]))

		for j in range(len(z_list)):
		
			filename = '/bulk1/feldmann/data/' + sim_suite[i] + '/analysis/' + AHF[k] + '/HR/' + sim_name[i] + '/output/halo_0/snapshot_' + str('%03d' %new_pos[j]) + '_ID_' + str(new_ID[j]) + '.hdf5'

			data = h5py.File(filename, 'r')
			dm = ot.dict_converter(data["PartType1"])
			# %10 of rvir since extracted data contains all particles within %20 of rvir
			radius_gal = np.max(np.sqrt(np.sum(np.square(dm["Coordinates"]), axis=1))) / 2.0 

			dm = {}

			gas = ot.dict_converter(data["PartType0"])

			if len(gas["Masses"]) > 0:

				gas["Temperature"] = ot.calculate_temperature(gas["InternalEnergy"],gas["ElectronAbundance"])
				gas["Distance"] = np.sqrt(np.sum(np.square(gas["Coordinates"]), axis=1))

				if len(gas["Distance"]) >= 10:
					x = 10
				elif len(gas["Distance"]) >= 5: 
					x = 5
				else: 
					x = 1

				radius_min = sorted(gas["Distance"])[x]

				radii = np.logspace(np.log10(radius_min), np.log10(radius_gal), num=100, endpoint=True, base=10.0, dtype=float)

				data_radius = []
				data_total_mgas = []
				data_cold_mgas = []
				data_ratio = []

				for radius in radii:
					
					sel_total = (gas["Distance"] < radius)
					sel_cold = (gas["Distance"] < radius) & (gas["Temperature"] < temp_limit)

					total_gas = ot.select_particles(gas, sel_total)
					cold_gas = ot.select_particles(gas, sel_cold)

					total_mgas = np.sum(total_gas["Masses"]) * 1e10 
					cold_mgas = np.sum(cold_gas["Masses"]) * 1e10

					#print '%-10.3e %-10.3e %-10.5f' %(cold_mgas, total_mgas, cold_mgas / total_mgas)

					data_radius.append(radius)
					data_total_mgas.append(total_mgas)
					data_cold_mgas.append(cold_mgas)
					data_ratio.append(cold_mgas/total_mgas)

				output.create_dataset(regime[k] + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + str(z_list[j]) + '/radius', data = np.array(data_radius))
				output.create_dataset(regime[k] + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + str(z_list[j]) + '/total_mgas', data = np.array(data_total_mgas))
				output.create_dataset(regime[k] + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + str(z_list[j]) + '/cold_mgas', data = np.array(data_cold_mgas))
				output.create_dataset(regime[k] + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + str(z_list[j]) + '/ratio', data = np.array(data_ratio))

output.close()
