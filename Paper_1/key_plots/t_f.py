import numpy as np
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.ticker as ticker
from matplotlib.colors import LogNorm
from astropy.cosmology import FlatLambdaCDM
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value

def time(i):
	return cosmo.age(i).value #* 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

merger_list = ['merger', 'nomerger']
center_list = ['MDC', 'COM']
file_list = {}

mass_ret_list = ['mass_ret_0.1']
radius = 'r_0.1'

data = {}

for merger in merger_list:
	for center in center_list:
		file_list[center + '/' + merger] = sorted(glob.glob('/bulk1/feldmann/Onur/' + center + '/MassiveFIRE*/*/*_' + merger + '.hdf5'))
		for mass_ret in mass_ret_list:

			props = ['mdot', 'f', 'sfr', 'redshift']
			for prop in props:
				data[center + '/' + merger + '/' + prop] = []

			for filename in file_list[center + '/' + merger]:
				f = h5py.File(filename, 'r')

				data[center + '/' + merger + '/mdot'].append(f['total/' + radius + '/' + mass_ret + '/seed_1e4/mdot_torque'].value)
				data[center + '/' + merger + '/f'].append(f['total/' + radius + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value)
				data[center + '/' + merger + '/redshift'].append(f['total/rvir/z_1'].value)
				data[center + '/' + merger + '/sfr'].append(f['total/rvir/sfr'].value)

			for prop in props:
				data[center + '/' + merger + '/' + prop] = np.concatenate(data[center + '/' + merger + '/' + prop])

x = np.array([1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1, 1e2, 1e3, 1e4])
z_list = np.array(range(13))

fig, axes = plt.subplots(2,2, figsize=(10,10))
fig.subplots_adjust(wspace=0.2, hspace=0.2)
cm = plt.cm.get_cmap('RdYlBu')

for ax, merger, center, flag in zip(axes.flat, ['merger', 'nomerger', 'merger', 'nomerger'], ['MDC', 'MDC', 'COM', 'COM'], [1,2,3,4]):

	z = data[center + '/' + merger + '/redshift']
	ax.set_title('%s' %(merger + ' ' + center), fontsize=10)
	sc = ax.scatter(time(data[center + '/' + merger + '/redshift']), data[center + '/' + merger + '/f'], c=z, alpha=1.0, vmin=np.min(z), vmax=np.max(z), cmap=cm)
	#ax.plot(x, x*1e-3, color='k', lw=1, ls='-', label=r'$10^{-3}$')
	#ax.plot(x, x*1e-2, color='k', lw=1, ls='--', label=r'$10^{-2}$')
	#ax.plot(x, x*1e-1, color='b', lw=1, ls='-', label=r'$10^{-1}$')
	#ax.plot(x, x*1e0, color='b', lw=1, ls='--', label=r'$10^{0}$')
	ax.plot(time(z_list), np.ones(len(z_list)), color='k', lw=1, ls='-')
	ax.plot(time(np.ones(len(x))*2.0), x, color='k', lw=1, ls='-')

	if (flag == 3) or (flag == 4):
		ax.set_xlabel('Gyr', fontsize=20)
	if (flag == 1) or (flag == 3):
		ax.set_ylabel(r'$\lambda \; \left( \frac{\dot{M}_{BH}}{\dot{M}_{Edd}} \right)$', fontsize=20)
	ax.set_xscale('linear')
	ax.set_yscale('log')
	ax.set_xlim(time(0),time(12))
	ax.set_ylim(1e-4,2e1)
	ax.xaxis.set_tick_params(labelsize=12)
	ax.yaxis.set_tick_params(labelsize=12)
	ax.xaxis.set_major_locator(ticker.MultipleLocator(1))	

	#if flag == 1:
	#	ax.legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

#fig.subplots_adjust(left=0.8)
fig.colorbar(sc, ax=axes.ravel().tolist())
#plt.tight_layout()
#plt.show()
#quit()

fig.savefig('t_f.pdf')
plt.close()

