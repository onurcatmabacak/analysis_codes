import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

sim_name = ['B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'h113_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
regime = ['COM', 'MDC']
AHF = ['AHF_com', 'AHF']

color = ['r', 'g', 'b', 'k', 'm', 'c', 'r', 'g', 'b', 'k', 'm', 'c']
linestyle = ['-', '-', '-', '-', '-', '-', '--', '--', '--', '--', '--', '--']
fontsize=18

filename = 'cold_gas_ratio.hdf5'

data = h5py.File(filename, 'r')

for regime in data:
	for sim_suite in data[regime]:
		for sim_name in data[regime][sim_suite]:	

			fig = plt.figure(1, figsize=(12,8))
			fig.subplots_adjust(wspace=0, hspace=0)
			fig.suptitle(regime + '  ' + sim_suite + '  ' + sim_name)
			ax1 = fig.add_subplot(211)
			ax2 = fig.add_subplot(212)

			for z in data[regime][sim_suite][sim_name]:	

				total_mgas = data[regime][sim_suite][sim_name][z]['total_mgas'].value
				cold_mgas = data[regime][sim_suite][sim_name][z]['cold_mgas'].value
				radius = data[regime][sim_suite][sim_name][z]['radius'].value
				ratio = data[regime][sim_suite][sim_name][z]['ratio'].value
				inds = int(float(z))-1	
				ax1.plot(radius, total_mgas, color=color[inds], ls=linestyle[inds], label='z = ' + z)
				ax2.plot(radius, ratio, color=color[inds], ls=linestyle[inds])

			ax1.set_xscale('linear')
			ax1.set_yscale('log')
			ax1.set_ylabel(r'$M_{gas, \;tot}$ \; (M_{})')
			ax1.legend(loc=0,prop={'size':fontsize},frameon=False,scatterpoints=1)
			ax1.grid(b=None, which='major', axis='both')

			ax2.set_xscale('linear')
			ax2.set_yscale('linear')
			ax2.legend(loc=0,prop={'size':fontsize},frameon=False,scatterpoints=1)
			ax2.grid(b=None, which='major', axis='both')

			plt.show()
			#quit()	
