import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def merger_counter(z_1, merger_ratio_list):

	tot_merger_major = []
	tot_merger_minor = []
	tot_merger_minorminor = []

	for xxx in range(len(z_1)):
		if merger_ratio_list[xxx] == [-1]:
			tot_merger_major.append(0)
			tot_merger_minor.append(0)
			tot_merger_minorminor.append(0)
		else:
			major = 0
			minor = 0
			minorminor = 0

			for merging_bh in merger_ratio_list[xxx]:
				if (merging_bh >= 0.25): # major merger
					major += 1
				elif (merging_bh >= 0.1 and merging_bh < 0.25): # minor merger
					minor += 1
				else: # minorminor merger
					minorminor += 1
			
			tot_merger_major.append(major)
			tot_merger_minor.append(minor)
			tot_merger_minorminor.append(minorminor)

	return tot_merger_major, tot_merger_minor, tot_merger_minorminor

def count(z_1, redshift, tot_merger):
	merger_final = []

	for xxx in range(len(redshift)):

		sel = (z_1 == redshift[xxx])
		new = int(np.sum(np.array(tot_merger)[sel]))

		if new:
			merger_final.append(new)
		else:
			merger_final.append(0)

	return merger_final

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite=['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
label = ['B100_TL4', 'B100_TL2', 'B100_TL9', 'B100_TL18', 'B100_TL29', 'B100_TL37', 'B100_TL113', 'B100_TL206', 'B400_TL10', 'B400_TL0', 'B400_TL1', 'B400_TL2', 'B400_TL5', 'B400_TL6', 'B400_TL13', 'B400_TL17', 'B400_TL21', 'B762_TL0_MF1', 'B762_TL1_MF1', 'B762_TL2_MF1', 'B762_TL0_MF2', 'B762_TL1_MF2', 'B762_TL2_MF2', 'h113', 'h206', 'h29', 'h2']
color = ['m','m','k', 'k', 'r', 'r', 'r', 'g', 'g', 'b', 'b', 'k', 'k', 'r', 'r', 'g', 'g', 'b', 'b', 'b', 'k', 'r', 'g', 'k', 'r', 'g', 'black', 'red', 'green', 'blue']
linestyle = ['-','--','-', '--', '-', '--', '-.', '-', '--', '-', '--', '-', '--', '-', '--', '-', '--', '-', '--', '-.', '-', '-', '-', '--', '--', '--', '-', '-', '-', '-']
x_min = [1.8,1.8,1.8,1.8,1.8,1.8,1.8,1.8,1.8,0.0,0.0,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,0.8,0.8,0.8,0.8,0.8,0.8]
x_max = [12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0,12.0]
y_min = [1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3]
y_max = [5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9,5e9]

i_list = [0,1,5]

for i in i_list:

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0.0, hspace=0.0)
	fig.suptitle(sim_name[i])
	ax0 = fig.add_subplot(111)	

	filename = '/bulk1/feldmann/Onur/' + sim_suite[i] + '/' + sim_name[i] + '/'

	z_timeline, ID_timeline = np.genfromtxt(filename + 'timeline_0.txt', delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	ID_timeline = ID_timeline.astype(int)

	z_timeline = z_timeline[::-1]
	ID_timeline = ID_timeline[::-1]

	z_1 = []
	ID = []
	position = []
	mstar = []
	mhalo = []
	z_2 = []
	descendant = []

	file_halo = open(filename+'halo_0.txt', 'r')
	for line in file_halo.readlines():
		data = map(float, np.array(line.split()))
		z_1.append(data[0])
		ID.append(int(data[1]))
		mstar.append(data[2])
		mhalo.append(data[3])
		z_2.append(data[4])
		descendant.append([int(xxx) for xxx in data[5:]])
	file_halo.close()

	z_1 = np.array(z_1)
	position = np.array(position)
	ID = np.array(ID)
	mstar = np.array(mstar)
	mhalo = np.array(mhalo)
	z_2 = np.array(z_2)
	descendant = np.array(descendant)

	if sim_suite[i] == 'MassiveFIRE2':
		radius_name = '0.1'
	else:
		radius_name = '1.0'

	data = h5py.File(filename + sim_name[i] + '_all.hdf5','r')
	mbh = data['cold/r_' + radius_name + '/mass_ret_0.5/seed_1e4/m_bh_torque'].value
	mbh = mbh[::-1]

	seed_list = []
	for xxx in range(len(z_1)):
		if int(descendant[xxx][0]) == -1:
			seed_list.append(1)
		else:
			seed_list.append(0)

	seed_list = np.array(seed_list)

	seed_mass = []
	for xxx in range(len(z_timeline)):
		sel = (z_1 == z_timeline[xxx])
		seed_mass.append(np.sum(seed_list[sel]))

	seed_cum = np.cumsum(seed_mass) * 1e4
	zipped_timeline = zip(z_timeline, ID_timeline)
	zipped_halo = zip(z_1, ID)

	zeytin_store = zip(zipped_halo, mbh)
	mbh_timeline = [b for a,b in zeytin_store if a in zipped_timeline][::-1]

	# mbh_timeline and cumulative seed mass is ready

	tot_merger = []
	merger_list_bh = []

	for xxx in range(len(z_1)):

		inds = len(descendant[xxx])-1
		tot_merger.append(inds)

		if inds > 0:
	
			merger_bh = []
			for des in descendant[xxx][1:]:
			
				sel = (z_1 == z_2[xxx]) & (ID == des)
				merger_bh.append(mbh[sel][0])

			merger_list_bh.append(merger_bh[0])
		
		else:
			
			merger_list_bh.append(0)

	merger_mass = []
	for xxx in range(len(z_timeline)):

		sel = (z_1 == z_timeline[xxx]) & (ID == ID_timeline[xxx])
		merger_mass.append(np.sum(np.array(merger_list_bh)[sel]))

	# total merged bh masses at every redshifts: merger_mass
	# stayed here, we have all merged bh masses, total bh mass and cumulative seed masses

	#for xxx in range(len(redshift)):
	#	print redshift[xxx], mbh_timeline[xxx], seed_cum[xxx], merger_mass[xxx]

	mbh_naked = mbh_timeline - np.cumsum(merger_mass)
	#for xxx in range(len(z_timeline)):
	#	print '%-6.3f %-10.3e %-10.3e %-10.3e %-10.3e ' %(z_timeline[xxx], mbh_timeline[xxx], np.cumsum(merger_mass)[xxx], mbh_naked[xxx], seed_cum[xxx])

	#quit()
	ax0.plot(z_timeline, mbh_timeline, color='black', ls='-', lw=4, label='BH')
	ax0.plot(z_timeline, mbh_naked, color='darkgreen', ls='--', lw=2, label='BH in-situ')
	ax0.plot(z_timeline, np.cumsum(merger_mass), color='red', ls='-', lw=1, label='cum merging bh masses')
	ax0.plot(z_timeline, seed_cum, color='blue', ls='-', lw=2, label='cum seed mass')

	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(x_min[i],x_max[i])
	ax0.set_ylim(y_min[i], y_max[i])
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$cumulative \; \# \; of \; mergers$')
	ax0.xaxis.set_major_locator(plt.FixedLocator(np.arange(int(x_min[i]), int(x_max[i])+1 )))

	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	#plt.tight_layout()
	plt.savefig('plot_#9_bh_merger_' + sim_name[i] + '.png', dpi=300, format='png')
	plt.close()
