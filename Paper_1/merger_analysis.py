import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def merger_counter(z_1, merger_ratio_list):

	tot_merger_major = []
	tot_merger_minor = []
	tot_merger_minorminor = []

	for xxx in range(len(z_1)):
		if merger_ratio_list[xxx] == [-1]:
			tot_merger_major.append(0)
			tot_merger_minor.append(0)
			tot_merger_minorminor.append(0)
		else:
			major = 0
			minor = 0
			minorminor = 0

			for merging_bh in merger_ratio_list[xxx]:
				if (merging_bh >= 0.25): # major merger
					major += 1
				elif (merging_bh >= 0.1 and merging_bh < 0.25): # minor merger
					minor += 1
				else: # minorminor merger
					minorminor += 1
			
			tot_merger_major.append(major)
			tot_merger_minor.append(minor)
			tot_merger_minorminor.append(minorminor)

	return tot_merger_major, tot_merger_minor, tot_merger_minorminor

def count(z_1, redshift, tot_merger):
	merger_final = []

	for xxx in range(len(redshift)):

		sel = (z_1 == redshift[xxx])
		new = int(np.sum(np.array(tot_merger)[sel]))

		if new:
			merger_final.append(new)
		else:
			merger_final.append(0)

	return merger_final

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite=['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
label = ['B100_TL4', 'B100_TL2', 'B100_TL9', 'B100_TL18', 'B100_TL29', 'B100_TL37', 'B100_TL113', 'B100_TL206', 'B400_TL10', 'B400_TL0', 'B400_TL1', 'B400_TL2', 'B400_TL5', 'B400_TL6', 'B400_TL13', 'B400_TL17', 'B400_TL21', 'B762_TL0_MF1', 'B762_TL1_MF1', 'B762_TL2_MF1', 'B762_TL0_MF2', 'B762_TL1_MF2', 'B762_TL2_MF2', 'h113', 'h206', 'h29', 'h2']
color = ['m','m','k', 'k', 'r', 'r', 'r', 'g', 'g', 'b', 'b', 'k', 'k', 'r', 'r', 'g', 'g', 'b', 'b', 'b', 'k', 'r', 'g', 'k', 'r', 'g', 'black', 'red', 'green', 'blue']
linestyle = ['-','--','-', '--', '-', '--', '-.', '-', '--', '-', '--', '-', '--', '-', '--', '-', '--', '-', '--', '-.', '-', '-', '-', '--', '--', '--', '-', '-', '-', '-']
x_min = [1.8,1.8,1.8,1.8,1.8,1.8,1.8,1.8,1.8,0.0,0.0,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,5.8,0.8,0.8,0.8,0.8,0.8,0.8]
x_max = [9.0,9.0,9.0,6.0,9.0,9.0,8.0,7.0,9.0,10.0,8.0,9.0,11.0,11.0,11.0,12.0,11.0,11.0,11.0,11.0,12.0,12.0,12.0,12.0,12.0,12.0,6.0,8.0,6.0,5.0]
y_min = [1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1,1e-1]
y_max = [1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e3,1e2,1e2,1e2,1e2,1e2,1e2,1e2,1e2,1e2,1e2,1e2,1e2,1e3,1e3,1e3,1e2,1e2,1e2,1e2,1e2,1e2]
i_list = [0,1,5]

for i in i_list:

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0.0, hspace=0.0)
	fig.suptitle(sim_name[i])
	ax0 = fig.add_subplot(111)	

	filename = '/bulk1/feldmann/Onur/' + sim_suite[i] + '/' + sim_name[i] + '/'

	redshift, position = np.genfromtxt(filename+'zred_pos_list', delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	position = position.astype(int)

	z_1 = []
	ID = []
	position = []
	mstar = []
	mhalo = []
	z_2 = []
	descendant = []

	file_halo = open(filename+'halo_0.txt', 'r')
	for line in file_halo.readlines():
		data = map(float, np.array(line.split()))
		z_1.append(data[0])
		position.append(redshift.tolist().index(data[0]))
		ID.append(int(data[1]))
		mstar.append(data[2])
		mhalo.append(data[3])
		z_2.append(data[4])
		descendant.append([int(xxx) for xxx in data[5:]])
	file_halo.close()

	z_1 = np.array(z_1)
	position = np.array(position)
	ID = np.array(ID)
	mstar = np.array(mstar)
	mhalo = np.array(mhalo)
	z_2 = np.array(z_2)
	descendant = np.array(descendant)

	if sim_suite[i] == 'MassiveFIRE2':
		radius_name = '0.1'
	else:
		radius_name = '1.0'

	data = h5py.File(filename + sim_name[i] + '_all.hdf5','r')
	mbh = data['cold/r_' + radius_name + '/mass_ret_0.5/seed_1e4/m_bh_torque'].value
	mbh = mbh[::-1]

	tot_merger = []
	merger_ratio_list_star = []
	merger_ratio_list_halo = []
	merger_ratio_list_bh = []

	for xxx in range(len(z_1)):

		inds = len(descendant[xxx])-1
		tot_merger.append(inds)
	
		if inds > 0:

			merger_ratio_star = []
			merger_ratio_halo = []
			merger_ratio_bh = []
			for des in descendant[xxx][1:]:

				parent_mstar = mstar[(z_1 == z_2[xxx]) & (ID == descendant[xxx][0])][0]
				parent_mhalo = mhalo[(z_1 == z_2[xxx]) & (ID == descendant[xxx][0])][0]
				parent_mbh = mbh[(z_1 == z_2[xxx]) & (ID == descendant[xxx][0])][0]

				sel = (z_1 == z_2[xxx]) & (ID == des)
				merger_ratio_star.append(mstar[sel][0] / parent_mstar)
				merger_ratio_halo.append(mhalo[sel][0] / parent_mhalo)
				merger_ratio_bh.append(mbh[sel][0] / parent_mbh)

			merger_ratio_list_star.append(merger_ratio_star)
			merger_ratio_list_halo.append(merger_ratio_halo)
			merger_ratio_list_bh.append(merger_ratio_bh)

		else:

			merger_ratio_list_star.append([-1])
			merger_ratio_list_halo.append([-1])		
			merger_ratio_list_bh.append([-1])

	# tot_merger and zipped_z_ID has same lenghts
	# now we have merger ratios of mstar, mhalo and mbh
	# stayed here now we have merger ratios, filter them with sel_merger for major minor and minorminor mergers
	# just like merger_final

	merger_bh_major, merger_bh_minor, merger_bh_minorminor = merger_counter(z_1, merger_ratio_list_bh)
	merger_star_major, merger_star_minor, merger_star_minorminor = merger_counter(z_1, merger_ratio_list_star)
	merger_halo_major, merger_halo_minor, merger_halo_minorminor = merger_counter(z_1, merger_ratio_list_halo)

	merger_final = count(z_1, redshift, tot_merger)
	merger_bh_major_final = count(z_1, redshift, merger_bh_major)
	merger_bh_minor_final = count(z_1, redshift, merger_bh_minor)
	merger_bh_minorminor_final = count(z_1, redshift, merger_bh_minorminor)
	merger_star_major_final = count(z_1, redshift, merger_star_major)
	merger_star_minor_final = count(z_1, redshift, merger_star_minor)
	merger_star_minorminor_final = count(z_1, redshift, merger_star_minorminor)
	merger_halo_major_final = count(z_1, redshift, merger_halo_major)
	merger_halo_minor_final = count(z_1, redshift, merger_halo_minor)
	merger_halo_minorminor_final = count(z_1, redshift, merger_halo_minorminor)

	merger = np.cumsum(merger_final)

	bh_major = np.cumsum(merger_bh_major_final)
	bh_minor = np.cumsum(merger_bh_minor_final)
	bh_minorminor = np.cumsum(merger_bh_minorminor_final)
	star_major = np.cumsum(merger_star_major_final)
	star_minor = np.cumsum(merger_star_minor_final)
	star_minorminor = np.cumsum(merger_star_minorminor_final)
	halo_major = np.cumsum(merger_halo_major_final)
	halo_minor = np.cumsum(merger_halo_minor_final)
	halo_minorminor = np.cumsum(merger_halo_minorminor_final)

	ax0.plot(redshift, merger, color='k', ls='-', lw=2, label='tot merger')
	ax0.plot(redshift, bh_major, color='r', ls='-', lw=1, label='bh major')
	ax0.plot(redshift, bh_minor, color='r', ls='--', lw=1, label='bh minor')
	ax0.plot(redshift, bh_minorminor, color='r', ls='-.', lw=1, label='bh minorminor')
	ax0.plot(redshift, star_major, color='b', ls='-', lw=1, label='star major')
	ax0.plot(redshift, star_minor, color='b', ls='--', lw=1, label='star minor')
	ax0.plot(redshift, star_minorminor, color='b', ls='-.', lw=1, label='star minorminor')
	ax0.plot(redshift, halo_major, color='g', ls='-', lw=1, label='halo major')
	ax0.plot(redshift, halo_minor, color='g', ls='--', lw=1, label='halo minor')
	ax0.plot(redshift, halo_minorminor, color='g', ls='-.', lw=1, label='halo minorminor')

	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(x_min[i],x_max[i])
	ax0.set_ylim(y_min[i], y_max[i])
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$cumulative \; \# \; of \; mergers$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	#plt.tight_layout()
	plt.savefig('plot_#9_merger_analysis_' + sim_name[i] + '.png', dpi=300, format='png')
	plt.close()
