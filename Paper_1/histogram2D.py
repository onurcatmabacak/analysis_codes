import numpy as np
from numpy import *
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.colors as mcolors
from matplotlib.colors import LogNorm
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value
from astropy.cosmology import FlatLambdaCDM
import math

def time(i):
	return cosmo.age(i).value # Gyr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)
sim_name_MF1 = ['B100_N512_TL00002', 'B100_N512_TL00029', 'B100_N512_TL00113', 'B100_N512_TL00206']
sim_name_MF2 = ['h2', 'h29', 'h113', 'h206']

mass_ret_list = ['mass_ret_0.05', 'mass_ret_0.1', 'mass_ret_0.25', 'mass_ret_0.5', 'mass_ret_1.0']

for mass_ret in mass_ret_list:

	mbh_mc1 = []
	mbh_mm1 = []
	mbh_nc1 = []
	mbh_nm1 = []
	mbh_mc2 = []
	mbh_mm2 = []
	mbh_nc2 = []
	mbh_nm2 = []

	mstar_mc1 = []
	mstar_mm1 = []
	mstar_nc1 = []
	mstar_nm1 = []
	mstar_mc2 = []
	mstar_mm2 = []
	mstar_nc2 = []
	mstar_nm2 = []

	mhalo_mc1 = []
	mhalo_mm1 = []
	mhalo_nc1 = []
	mhalo_nm1 = []
	mhalo_mc2 = []
	mhalo_mm2 = []
	mhalo_nc2 = []
	mhalo_nm2 = []

	f_mc1 = []
	f_mm1 = []
	f_nc1 = []
	f_nm1 = []
	f_mc2 = []
	f_mm2 = []
	f_nc2 = []
	f_nm2 = []

	z_mc1 = []
	z_mm1 = []
	z_nc1 = []
	z_nm1 = []
	z_mc2 = []
	z_mm2 = []
	z_nc2 = []
	z_nm2 = []


	for i in range(4):
		merger_com_MF1 = glob.glob('../COM/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_merger.hdf5')[0]
		merger_mdc_MF1 = glob.glob('../MDC/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_merger.hdf5')[0]
		nomerger_com_MF1 = glob.glob('../COM/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_nomerger.hdf5')[0]
		nomerger_mdc_MF1 = glob.glob('../MDC/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_nomerger.hdf5')[0]
		merger_com_MF2 = glob.glob('../COM/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_merger.hdf5')[0]
		merger_mdc_MF2 = glob.glob('../MDC/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_merger.hdf5')[0]
		nomerger_com_MF2 = glob.glob('../COM/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_nomerger.hdf5')[0]
		nomerger_mdc_MF2 = glob.glob('../MDC/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_nomerger.hdf5')[0]

		data_merger_com_MF1 = h5py.File(merger_com_MF1, 'r')
		data_merger_mdc_MF1 = h5py.File(merger_mdc_MF1, 'r')
		data_nomerger_com_MF1 = h5py.File(nomerger_com_MF1, 'r')
		data_nomerger_mdc_MF1 = h5py.File(nomerger_mdc_MF1, 'r')
		data_merger_com_MF2 = h5py.File(merger_com_MF2, 'r')
		data_merger_mdc_MF2 = h5py.File(merger_mdc_MF2, 'r')
		data_nomerger_com_MF2 = h5py.File(nomerger_com_MF2, 'r')
		data_nomerger_mdc_MF2 = h5py.File(nomerger_mdc_MF2, 'r')

		# z mbh mhalo mstar

		mbh_merger_com_MF1 = data_merger_com_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_mdc_MF1 = data_merger_mdc_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_com_MF1 = data_nomerger_com_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_com_MF2 = data_merger_com_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_mdc_MF2 = data_merger_mdc_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_com_MF2 = data_nomerger_com_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/m_bh_torque'].value

		mstar_merger_com_MF1 = data_merger_com_MF1['cold/rvir/mstar'].value
		mstar_merger_mdc_MF1 = data_merger_mdc_MF1['cold/rvir/mstar'].value
		mstar_nomerger_com_MF1 = data_nomerger_com_MF1['cold/rvir/mstar'].value
		mstar_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/rvir/mstar'].value
		mstar_merger_com_MF2 = data_merger_com_MF2['cold/rvir/mstar'].value
		mstar_merger_mdc_MF2 = data_merger_mdc_MF2['cold/rvir/mstar'].value
		mstar_nomerger_com_MF2 = data_nomerger_com_MF2['cold/rvir/mstar'].value
		mstar_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/rvir/mstar'].value

		mhalo_merger_com_MF1 = data_merger_com_MF1['cold/rvir/mhalo'].value
		mhalo_merger_mdc_MF1 = data_merger_mdc_MF1['cold/rvir/mhalo'].value
		mhalo_nomerger_com_MF1 = data_nomerger_com_MF1['cold/rvir/mhalo'].value
		mhalo_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/rvir/mhalo'].value
		mhalo_merger_com_MF2 = data_merger_com_MF2['cold/rvir/mhalo'].value
		mhalo_merger_mdc_MF2 = data_merger_mdc_MF2['cold/rvir/mhalo'].value
		mhalo_nomerger_com_MF2 = data_nomerger_com_MF2['cold/rvir/mhalo'].value
		mhalo_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/rvir/mhalo'].value

		f_merger_com_MF1 = data_merger_com_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_mdc_MF1 = data_merger_mdc_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_com_MF1 = data_nomerger_com_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/r_1.0/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_com_MF2 = data_merger_com_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_mdc_MF2 = data_merger_mdc_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_com_MF2 = data_nomerger_com_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/r_0.1/' + mass_ret + '/seed_1e4/f_torque_edd'].value

		z_merger_com_MF1 = data_merger_com_MF1['cold/rvir/z_1'].value
		z_merger_mdc_MF1 = data_merger_mdc_MF1['cold/rvir/z_1'].value
		z_nomerger_com_MF1 = data_nomerger_com_MF1['cold/rvir/z_1'].value
		z_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/rvir/z_1'].value
		z_merger_com_MF2 = data_merger_com_MF2['cold/rvir/z_1'].value
		z_merger_mdc_MF2 = data_merger_mdc_MF2['cold/rvir/z_1'].value
		z_nomerger_com_MF2 = data_nomerger_com_MF2['cold/rvir/z_1'].value
		z_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/rvir/z_1'].value

		mbh_mc1.append(mbh_merger_com_MF1)
		mbh_mm1.append(mbh_merger_mdc_MF1)
		mbh_nc1.append(mbh_nomerger_com_MF1)
		mbh_nm1.append(mbh_nomerger_mdc_MF1)
		mbh_mc2.append(mbh_merger_com_MF2)
		mbh_mm2.append(mbh_merger_mdc_MF2)
		mbh_nc2.append(mbh_nomerger_com_MF2)
		mbh_nm2.append(mbh_nomerger_mdc_MF2)

		mstar_mc1.append(mstar_merger_com_MF1)
		mstar_mm1.append(mstar_merger_mdc_MF1)
		mstar_nc1.append(mstar_nomerger_com_MF1)
		mstar_nm1.append(mstar_nomerger_mdc_MF1)
		mstar_mc2.append(mstar_merger_com_MF2)
		mstar_mm2.append(mstar_merger_mdc_MF2)
		mstar_nc2.append(mstar_nomerger_com_MF2)
		mstar_nm2.append(mstar_nomerger_mdc_MF2)

		mhalo_mc1.append(mhalo_merger_com_MF1)
		mhalo_mm1.append(mhalo_merger_mdc_MF1)
		mhalo_nc1.append(mhalo_nomerger_com_MF1)
		mhalo_nm1.append(mhalo_nomerger_mdc_MF1)
		mhalo_mc2.append(mhalo_merger_com_MF2)
		mhalo_mm2.append(mhalo_merger_mdc_MF2)
		mhalo_nc2.append(mhalo_nomerger_com_MF2)
		mhalo_nm2.append(mhalo_nomerger_mdc_MF2)

		f_mc1.append(f_merger_com_MF1)
		f_mm1.append(f_merger_mdc_MF1)
		f_nc1.append(f_nomerger_com_MF1)
		f_nm1.append(f_nomerger_mdc_MF1)
		f_mc2.append(f_merger_com_MF2)
		f_mm2.append(f_merger_mdc_MF2)
		f_nc2.append(f_nomerger_com_MF2)
		f_nm2.append(f_nomerger_mdc_MF2)

		z_mc1.append(z_merger_com_MF1)
		z_mm1.append(z_merger_mdc_MF1)
		z_nc1.append(z_nomerger_com_MF1)
		z_nm1.append(z_nomerger_mdc_MF1)
		z_mc2.append(z_merger_com_MF2)
		z_mm2.append(z_merger_mdc_MF2)
		z_nc2.append(z_nomerger_com_MF2)
		z_nm2.append(z_nomerger_mdc_MF2)

	mbh_mc1 = np.concatenate(mbh_mc1)
	mbh_mm1 = np.concatenate(mbh_mm1)
	mbh_nc1 = np.concatenate(mbh_nc1)
	mbh_nm1 = np.concatenate(mbh_nm1)
	mbh_mc2 = np.concatenate(mbh_mc2)
	mbh_mm2 = np.concatenate(mbh_mm2)
	mbh_nc2 = np.concatenate(mbh_nc2)
	mbh_nm2 = np.concatenate(mbh_nm2)

	mstar_mc1 = np.concatenate(mstar_mc1)
	mstar_mm1 = np.concatenate(mstar_mm1)
	mstar_nc1 = np.concatenate(mstar_nc1)
	mstar_nm1 = np.concatenate(mstar_nm1)
	mstar_mc2 = np.concatenate(mstar_mc2)
	mstar_mm2 = np.concatenate(mstar_mm2)
	mstar_nc2 = np.concatenate(mstar_nc2)
	mstar_nm2 = np.concatenate(mstar_nm2)

	mhalo_mc1 = np.concatenate(mhalo_mc1)
	mhalo_mm1 = np.concatenate(mhalo_mm1)
	mhalo_nc1 = np.concatenate(mhalo_nc1)
	mhalo_nm1 = np.concatenate(mhalo_nm1)
	mhalo_mc2 = np.concatenate(mhalo_mc2)
	mhalo_mm2 = np.concatenate(mhalo_mm2)
	mhalo_nc2 = np.concatenate(mhalo_nc2)
	mhalo_nm2 = np.concatenate(mhalo_nm2)

	f_mc1 = np.concatenate(f_mc1)
	f_mm1 = np.concatenate(f_mm1)
	f_nc1 = np.concatenate(f_nc1)
	f_nm1 = np.concatenate(f_nm1)
	f_mc2 = np.concatenate(f_mc2)
	f_mm2 = np.concatenate(f_mm2)
	f_nc2 = np.concatenate(f_nc2)
	f_nm2 = np.concatenate(f_nm2)

	z_mc1 = np.concatenate(z_mc1)
	z_mm1 = np.concatenate(z_mm1)
	z_nc1 = np.concatenate(z_nc1)
	z_nm1 = np.concatenate(z_nm1)
	z_mc2 = np.concatenate(z_mc2)
	z_mm2 = np.concatenate(z_mm2)
	z_nc2 = np.concatenate(z_nc2)
	z_nm2 = np.concatenate(z_nm2)

	t_mc1 = time(z_mc1)
	t_mm1 = time(z_mm1)
	t_nc1 = time(z_nc1)
	t_nm1 = time(z_nm1)
	t_mc2 = time(z_mc2)
	t_mm2 = time(z_mm2)
	t_nc2 = time(z_nc2)
	t_nm2 = time(z_nm2)

	mbh_mc1 = ma.log10(mbh_mc1).filled(0)
	mbh_mm1 = ma.log10(mbh_mm1).filled(0)
	mbh_nc1 = ma.log10(mbh_nc1).filled(0)
	mbh_nm1 = ma.log10(mbh_nm1).filled(0)
	mbh_mc2 = ma.log10(mbh_mc2).filled(0)
	mbh_mm2 = ma.log10(mbh_mm2).filled(0)
	mbh_nc2 = ma.log10(mbh_nc2).filled(0)
	mbh_nm2 = ma.log10(mbh_nm2).filled(0)

	mstar_mc1 = ma.log10(mstar_mc1).filled(0)
	mstar_mm1 = ma.log10(mstar_mm1).filled(0)
	mstar_nc1 = ma.log10(mstar_nc1).filled(0)
	mstar_nm1 = ma.log10(mstar_nm1).filled(0)
	mstar_mc2 = ma.log10(mstar_mc2).filled(0)
	mstar_mm2 = ma.log10(mstar_mm2).filled(0)
	mstar_nc2 = ma.log10(mstar_nc2).filled(0)
	mstar_nm2 = ma.log10(mstar_nm2).filled(0)

	mhalo_mc1 = ma.log10(mhalo_mc1).filled(0)
	mhalo_mm1 = ma.log10(mhalo_mm1).filled(0)
	mhalo_nc1 = ma.log10(mhalo_nc1).filled(0)
	mhalo_nm1 = ma.log10(mhalo_nm1).filled(0)
	mhalo_mc2 = ma.log10(mhalo_mc2).filled(0)
	mhalo_mm2 = ma.log10(mhalo_mm2).filled(0)
	mhalo_nc2 = ma.log10(mhalo_nc2).filled(0)
	mhalo_nm2 = ma.log10(mhalo_nm2).filled(0)

	f_mc1 = ma.log10(f_mc1).filled(0)
	f_mm1 = ma.log10(f_mm1).filled(0)
	f_nc1 = ma.log10(f_nc1).filled(0)
	f_nm1 = ma.log10(f_nm1).filled(0)
	f_mc2 = ma.log10(f_mc2).filled(0)
	f_mm2 = ma.log10(f_mm2).filled(0)
	f_nc2 = ma.log10(f_nc2).filled(0)
	f_nm2 = ma.log10(f_nm2).filled(0)

	name = ['MERGER_COM', 'NOMERGER_COM', 'MERGER_MDC', 'NOMERGER_MDC']
	mbh_MF1data = [mbh_mc1, mbh_nc1, mbh_mm1, mbh_nm1]
	mbh_MF2data = [mbh_mc2, mbh_nc2, mbh_mm2, mbh_nm2]
	mstar_MF1data = [mstar_mc1, mstar_nc1, mstar_mm1, mstar_nm1]
	mstar_MF2data = [mstar_mc2, mstar_nc2, mstar_mm2, mstar_nm2]
	mhalo_MF1data = [mhalo_mc1, mhalo_nc1, mhalo_mm1, mhalo_nm1]
	mhalo_MF2data = [mhalo_mc2, mhalo_nc2, mhalo_mm2, mhalo_nm2]
	f_MF1data = [f_mc1, f_nc1, f_mm1, f_nm1]
	f_MF2data = [f_mc2, f_nc2, f_mm2, f_nm2]
	z_MF1data = [z_mc1, z_nc1, z_mm1, z_nm1]
	z_MF2data = [z_mc2, z_nc2, z_mm2, z_nm2]

	xdata_list = mhalo_MF2data
	ydata_list = mbh_MF2data
	sim_suite = 'mhalo_mbh_MF2_'

	fig, axes = plt.subplots(2,2, figsize=(10,10))
	fig.subplots_adjust(wspace=0.2, hspace=0.2)
	for ax, xdata, ydata, label in zip(axes.flat, xdata_list, ydata_list, name):
		ax.set_title('%s' %label, fontsize=10)
		im = ax.hist2d(xdata, ydata, bins=(10,10), norm=LogNorm())
		ax.set_xlabel(r'$M_{h}$')
		ax.set_ylabel(r'$M_{bh}$')

	cax = fig.add_axes([0.915, 0.1, 0.02, 0.78])
	fig.colorbar(im[3], cax=cax)
	#plt.tight_layout()
	#plt.show()
	#quit()

	plt.savefig('histogram2D_' + sim_suite + mass_ret + '.png', dpi=300, format='png')
	plt.close()

