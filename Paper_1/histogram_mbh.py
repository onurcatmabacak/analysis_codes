import numpy as np
from numpy import *
import os
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import astropy.units as u
from astropy.cosmology import Planck13, z_at_value
from astropy.cosmology import FlatLambdaCDM
import math

def time(i):
	return cosmo.age(i).value # Gyr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)
sim_name_MF1 = ['B100_N512_TL00002', 'B100_N512_TL00029', 'B100_N512_TL00113', 'B100_N512_TL00206']
sim_name_MF2 = ['h2', 'h29', 'h113', 'h206']

mass_ret_list = ['mass_ret_0.05', 'mass_ret_0.1', 'mass_ret_0.25', 'mass_ret_0.5', 'mass_ret_1.0']
r_MF1 = 'r_0.1'
r_MF2 = 'r_0.1'
output_path = 'histogram_mbh_r0.1'

if not os.path.exists(output_path):
    os.makedirs(output_path)

for mass_ret in mass_ret_list:

	mdot_mc1 = []
	mdot_mm1 = []
	mdot_nc1 = []
	mdot_nm1 = []
	mdot_mc2 = []
	mdot_mm2 = []
	mdot_nc2 = []
	mdot_nm2 = []

	f_mc1 = []
	f_mm1 = []
	f_nc1 = []
	f_nm1 = []
	f_mc2 = []
	f_mm2 = []
	f_nc2 = []
	f_nm2 = []

	mbh_mc1 = []
	mbh_mm1 = []
	mbh_nc1 = []
	mbh_nm1 = []
	mbh_mc2 = []
	mbh_mm2 = []
	mbh_nc2 = []
	mbh_nm2 = []

	for i in range(4):

		merger_com_MF1 = glob.glob('../COM/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_merger.hdf5')[0]
		merger_mdc_MF1 = glob.glob('../MDC/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_merger.hdf5')[0]
		nomerger_com_MF1 = glob.glob('../COM/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_nomerger.hdf5')[0]
		nomerger_mdc_MF1 = glob.glob('../MDC/MassiveFIRE/' + sim_name_MF1[i] + '_*/' + sim_name_MF1[i] + '*_nomerger.hdf5')[0]
		merger_com_MF2 = glob.glob('../COM/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_merger.hdf5')[0]
		merger_mdc_MF2 = glob.glob('../MDC/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_merger.hdf5')[0]
		nomerger_com_MF2 = glob.glob('../COM/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_nomerger.hdf5')[0]
		nomerger_mdc_MF2 = glob.glob('../MDC/MassiveFIRE2/' + sim_name_MF2[i] + '_*/' + sim_name_MF2[i] + '*_nomerger.hdf5')[0]

		data_merger_com_MF1 = h5py.File(merger_com_MF1, 'r')
		data_merger_mdc_MF1 = h5py.File(merger_mdc_MF1, 'r')
		data_nomerger_com_MF1 = h5py.File(nomerger_com_MF1, 'r')
		data_nomerger_mdc_MF1 = h5py.File(nomerger_mdc_MF1, 'r')
		data_merger_com_MF2 = h5py.File(merger_com_MF2, 'r')
		data_merger_mdc_MF2 = h5py.File(merger_mdc_MF2, 'r')
		data_nomerger_com_MF2 = h5py.File(nomerger_com_MF2, 'r')
		data_nomerger_mdc_MF2 = h5py.File(nomerger_mdc_MF2, 'r')

		mdot_merger_com_MF1 = data_merger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_merger_mdc_MF1 = data_merger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_com_MF1 = data_nomerger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_merger_com_MF2 = data_merger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_merger_mdc_MF2 = data_merger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_com_MF2 = data_nomerger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value
		mdot_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/mdot_torque'].value

		f_merger_com_MF1 = data_merger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_mdc_MF1 = data_merger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_com_MF1 = data_nomerger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_com_MF2 = data_merger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_merger_mdc_MF2 = data_merger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_com_MF2 = data_nomerger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value
		f_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/f_torque_edd'].value

		mbh_merger_com_MF1 = data_merger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_mdc_MF1 = data_merger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_com_MF1 = data_nomerger_com_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_mdc_MF1 = data_nomerger_mdc_MF1['cold/' + r_MF1 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_com_MF2 = data_merger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_merger_mdc_MF2 = data_merger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_com_MF2 = data_nomerger_com_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value
		mbh_nomerger_mdc_MF2 = data_nomerger_mdc_MF2['cold/' + r_MF2 + '/' + mass_ret + '/seed_1e4/m_bh_torque'].value

		mdot_mc1.append(mdot_merger_com_MF1)
		mdot_mm1.append(mdot_merger_mdc_MF1)
		mdot_nc1.append(mdot_nomerger_com_MF1)
		mdot_nm1.append(mdot_nomerger_mdc_MF1)
		mdot_mc2.append(mdot_merger_com_MF2)
		mdot_mm2.append(mdot_merger_mdc_MF2)
		mdot_nc2.append(mdot_nomerger_com_MF2)
		mdot_nm2.append(mdot_nomerger_mdc_MF2)

		f_mc1.append(f_merger_com_MF1)
		f_mm1.append(f_merger_mdc_MF1)
		f_nc1.append(f_nomerger_com_MF1)
		f_nm1.append(f_nomerger_mdc_MF1)
		f_mc2.append(f_merger_com_MF2)
		f_mm2.append(f_merger_mdc_MF2)
		f_nc2.append(f_nomerger_com_MF2)
		f_nm2.append(f_nomerger_mdc_MF2)

		mbh_mc1.append(mbh_merger_com_MF1)
		mbh_mm1.append(mbh_merger_mdc_MF1)
		mbh_nc1.append(mbh_nomerger_com_MF1)
		mbh_nm1.append(mbh_nomerger_mdc_MF1)
		mbh_mc2.append(mbh_merger_com_MF2)
		mbh_mm2.append(mbh_merger_mdc_MF2)
		mbh_nc2.append(mbh_nomerger_com_MF2)
		mbh_nm2.append(mbh_nomerger_mdc_MF2)

	mdot_mc1 = np.concatenate(mdot_mc1)
	mdot_mm1 = np.concatenate(mdot_mm1)
	mdot_nc1 = np.concatenate(mdot_nc1)
	mdot_nm1 = np.concatenate(mdot_nm1)
	mdot_mc2 = np.concatenate(mdot_mc2)
	mdot_mm2 = np.concatenate(mdot_mm2)
	mdot_nc2 = np.concatenate(mdot_nc2)
	mdot_nm2 = np.concatenate(mdot_nm2)

	f_mc1 = np.concatenate(f_mc1)
	f_mm1 = np.concatenate(f_mm1)
	f_nc1 = np.concatenate(f_nc1)
	f_nm1 = np.concatenate(f_nm1)
	f_mc2 = np.concatenate(f_mc2)
	f_mm2 = np.concatenate(f_mm2)
	f_nc2 = np.concatenate(f_nc2)
	f_nm2 = np.concatenate(f_nm2)

	mbh_mc1 = np.concatenate(mbh_mc1)
	mbh_mm1 = np.concatenate(mbh_mm1)
	mbh_nc1 = np.concatenate(mbh_nc1)
	mbh_nm1 = np.concatenate(mbh_nm1)
	mbh_mc2 = np.concatenate(mbh_mc2)
	mbh_mm2 = np.concatenate(mbh_mm2)
	mbh_nc2 = np.concatenate(mbh_nc2)
	mbh_nm2 = np.concatenate(mbh_nm2)

	logmbh_mc1 = np.log10(mbh_mc1)
	logmbh_mm1 = np.log10(mbh_mm1)
	logmbh_nc1 = np.log10(mbh_nc1)
	logmbh_nm1 = np.log10(mbh_nm1)
	logmbh_mc2 = np.log10(mbh_mc2)
	logmbh_mm2 = np.log10(mbh_mm2)
	logmbh_nc2 = np.log10(mbh_nc2)
	logmbh_nm2 = np.log10(mbh_nm2)

	# binning of redshift wrt MF2
	nstep_list = [9,13,17]
	n_xaxis_list = [3,4,4]
	n_yaxis_list = [3,3,4]

	for nstep, n_xaxis, n_yaxis in zip(nstep_list, n_xaxis_list, n_yaxis_list):

		step = np.linspace(np.min(logmbh_mm2), np.max(logmbh_mm2), nstep)
		nbins = 10
		alpha = 0.3
		color_com = 'r'
		color_mdc = 'b'
		density = False
		xmin = -4
		xmax = 1.5

		logmbh_comdata = [logmbh_mc1, logmbh_nc1, logmbh_mc2, logmbh_nc2]
		logmbh_mdcdata = [logmbh_mm1, logmbh_nm1, logmbh_mm2, logmbh_nm2]
		mdot_comdata = [mdot_mc1, mdot_nc1, mdot_mc2, mdot_nc2]
		mdot_mdcdata = [mdot_mm1, mdot_nm1, mdot_mm2, mdot_nm2]
		f_comdata = [f_mc1, f_nc1, f_mc2, f_nc2]
		f_mdcdata = [f_mm1, f_nm1, f_mm2, f_nm2]

		name = ['MERGER_MF1_', 'NOMERGER_MF1_', 'MERGER_MF2_', 'NOMERGER_MF2_']

		for k in range(4):

			# define dataset here 
			logmbhcom = logmbh_comdata[k]
			logmbhmdc = logmbh_mdcdata[k]
			mdotcom = mdot_comdata[k]
			mdotmdc = mdot_mdcdata[k]
			fcom = f_comdata[k]
			fmdc = f_mdcdata[k]

			fig, axes = plt.subplots(n_xaxis,n_yaxis, figsize=(10,5), squeeze=False)
			fig.subplots_adjust(wspace=0.3, hspace=0.3)
			fig.suptitle(mass_ret)
			ax = axes.ravel()

			j = 0

			for i in range(nstep-1):

				selcom = (logmbhcom <= step[nstep-i-1]) & (logmbhcom > step[nstep-i-2])
				logmbh_com = logmbhcom[selcom]
				mdot_com = ma.log10(mdotcom[selcom]).filled(0)
				f_com = ma.log10(fcom[selcom]).filled(0)
				selmdc = (logmbhmdc <= step[nstep-i-1]) & (logmbhmdc > step[nstep-i-2])
				logmbh_mdc = logmbhmdc[selmdc]
				mdot_mdc = ma.log10(mdotmdc[selmdc]).filled(0)
				f_mdc = ma.log10(fmdc[selmdc]).filled(0)

				#ax[j] = fig.add_subplot(231)
				ax[j].hist(f_com, nbins, density=density, facecolor=color_com, label=str('%.2f' %(step[nstep-i-1])) + '<Mbh<' + str('%.2f' %(step[nstep-i-2])), alpha=alpha)
				ax[j].hist(f_mdc, nbins, density=density, facecolor=color_mdc, alpha=alpha)
				ax[j].set_xlim(xmin,xmax)
				#ax[j].set_ylim(0,70)
				ax[j].set_xscale('linear')
				ax[j].set_yscale('linear')
				ax[j].set_xlabel(r'$log(\dot{M}_{t}\; / \; \dot{M}_{edd})$')
				ax[j].set_ylabel('#')
				#ax[j].grid(color='k', ls='--', lw=0.25)
				#ax[j].xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
				ax[j].legend(loc=0,prop={'size':10},frameon=False,scatterpoints=1)

				j += 1

			#plt.show()
			plt.savefig(output_path + '/histogram_mbh_' + name[k] + mass_ret + '_' + str(nstep-1) + '_bins.png', dpi=300, format='png')
			plt.close()
