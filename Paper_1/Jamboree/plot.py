import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def Reines2015(Mstar):
   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)


filecom = '../../COM/MassiveFIRE2/h206_HR_sn1dy300ro100ss/h206_HR_sn1dy300ro100ss_merger.hdf5'
filemdc = '../../MDC/MassiveFIRE2/h206_HR_sn1dy300ro100ss/h206_HR_sn1dy300ro100ss_merger.hdf5'
filenomerger = '../../MDC/MassiveFIRE2/h206_HR_sn1dy300ro100ss/h206_HR_sn1dy300ro100ss_nomerger.hdf5'

datacom = h5py.File(filecom,'r')
datamdc = h5py.File(filemdc,'r')

bh_com = datacom['cold/r_0.1/mass_ret_0.1/seed_1e4/m_bh_torque'].value
bh_mdc = datamdc['cold/r_0.1/mass_ret_0.1/seed_1e4/m_bh_torque'].value

mstar_com = datacom['cold/rvir/mstar'].value
mstar_mdc = datamdc['cold/rvir/mstar'].value

mass_list_obs = np.array([1e9,1e10,1e11,1e12])
mass_list_ext = np.array([1e4,1e5,1e6,1e7,1e8,1e9])

fig = plt.figure(1)
ax1 = fig.add_subplot(111)

ax1.plot(mstar_com, bh_com, color='b', lw=2, label='Gas poor')
ax1.plot(mstar_mdc, bh_mdc, color='r', lw=2, label='Gas rich')
ax1.plot(mass_list_obs, Reines2015(mass_list_obs), color='k', ls='-', lw=1, label='Reines15')
ax1.plot(mass_list_ext, Reines2015(mass_list_ext), color='k', ls='--', lw=1)

ax1.set_xlabel(r'$STELLAR\;MASS\;(M_{\odot})$', fontsize=14)
ax1.set_ylabel(r'$SMBH\;MASS\;(M_{\odot})$', fontsize=14)
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlim(1e7,1e12)
ax1.set_ylim(8e3,1e9)
ax1.legend(loc=2, prop={'size':12},frameon=False,scatterpoints=1)
plt.savefig('BH_growth.png', dpi=300, format='png')
plt.close()


