import numpy as np
import h5py
import matplotlib.pyplot as plt
from astropy.cosmology import FlatLambdaCDM

def time(i):
	return cosmo.age(i).value * 1e9 # yr

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

sim_name_MF1 = ['B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915']
sim_name_MF2 = ['h2_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss']
sim_suite_MF1 = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE']
sim_suite_MF2 = ['MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
label = ['fig1_2', 'fig1_29', 'fig1_113', 'fig1_206']

for i in range(len(sim_name_MF1)):

	data_MF1 = h5py.File('/bulk1/feldmann/Onur/MDC/' + sim_suite_MF1[i] + '/' + sim_name_MF1[i] + '/' + sim_name_MF1[i] + '_merger.hdf5','r')
	data_MF2 = h5py.File('/bulk1/feldmann/Onur/MDC/' + sim_suite_MF2[i] + '/' + sim_name_MF2[i] + '/' + sim_name_MF2[i] + '_merger.hdf5','r')

	redshift_MF1 = data_MF1['total/rvir/z_1'].value
	m_star_MF1 = data_MF1['total/rvir/mstar'].value
	m_gas_MF1 = data_MF1['total/rvir/mgas'].value
	m_halo_MF1 = data_MF1['total/rvir/mhalo'].value
	sfrgal_MF1 = data_MF1['total/rvir/sfr'].value
	sfr1kpc_MF1 = data_MF1['total/1.0/sfr'].value

	redshift_MF2 = data_MF2['total/rvir/z_1'].value
	m_star_MF2 = data_MF2['total/rvir/mstar'].value
	m_gas_MF2 = data_MF2['total/rvir/mgas'].value
	m_halo_MF2 = data_MF2['total/rvir/mhalo'].value
	sfrgal_MF2 = data_MF2['total/rvir/sfr'].value
	sfr1kpc_MF2 = data_MF2['total/1.0/sfr'].value

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)

	ax1 = fig.add_subplot(211)	
	#ax1.plot(redshift_MF1, m_gas_MF1, color='r', ls='--', lw=1, label=r'$gas MF1$')
	ax1.plot(redshift_MF1, m_star_MF1, color='r', ls='--', lw=1, label=r'$star MF1$')
	ax1.plot(redshift_MF1, m_halo_MF1, color='k', ls='--', lw=1, label=r'$halo MF1$')   
	#ax1.plot(redshift_MF2, m_gas_MF2, color='r', ls='-', lw=1, label=r'$gas MF2$')
	ax1.plot(redshift_MF2, m_star_MF2, color='r', ls='-', lw=1, label=r'$star MF2$')
	ax1.plot(redshift_MF2, m_halo_MF2, color='k', ls='-', lw=1, label=r'$halo MF2$')   
	ax1.set_xscale('linear')
	ax1.set_yscale('log')
	ax1.set_xlim(0,12)
	ax1.set_ylim(1e7,2e12)
	ax1.xaxis.set_major_locator(plt.FixedLocator([1,2,3,4,5,6,7,8,9,10,11]))
	ax1.yaxis.set_major_locator(plt.FixedLocator([1e8,1e9,1e10,1e11,1e12]))
	ax1.tick_params(axis='y', labelsize=8)
	ax1.set_ylabel(r'$Mass\;(M_{\odot})$')
	ax12 = ax1.twiny()
	ax1Xs = ax1.get_xticks()
	ax12Xs = []
	for X in ax1Xs:
		dummy_age = cosmo.age(X).value #cosmo.age(0).value - cosmo.age(X).value
		ax12Xs.append("%.2f" %dummy_age)
	ax12.set_xticks(ax1Xs)
	ax12.set_xbound(ax1.get_xbound())
	ax12.set_xticklabels(ax12Xs)
	#ax12.set_xlabel(r'$time \quad (Gyr)$')
	ax1.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	#ax2 = ax1.twinx()
	ax2 = fig.add_subplot(212)
	ax2.plot(redshift_MF1, sfrgal_MF1, color='r', ls='-', lw=1, label=r'$SFR\;gal\;MF1$')
	#ax2.plot(redshift_MF1, sfr1kpc_MF1, color='g', ls='--', lw=1, label=r'$SFR\;inner\;MF1$')
	ax2.plot(redshift_MF2, sfrgal_MF2, color='k', ls='-', lw=1, label=r'$SFR\;gal\;MF2$')
	#ax2.plot(redshift_MF2, sfr1kpc_MF2, color='g', ls='-', lw=1, label=r'$SFR\;inner\;MF2$')
	ax2.set_xscale('linear')
	ax2.set_yscale('log')
	ax2.set_xlim(0,12)
	ax2.set_ylim(1e-2,1e3)
	ax2.xaxis.set_major_locator(plt.FixedLocator([1,2,3,4,5,6,7,8,9,10,11]))
	ax2.yaxis.set_major_locator(plt.FixedLocator([1e-1,1e0,1e1,1e2]))
	ax2.set_xlabel(r'$z$')
	ax2.set_ylabel(r'$SFR\;(M_{\odot}\;yr^{-1})$')
	ax2.tick_params(axis='y', labelsize=8)
	ax2.legend(loc=4,prop={'size':8},frameon=False,scatterpoints=1)

	plt.savefig(label[i] + '.png', dpi=300, format='png')
	#plt.tight_layout()
	#plt.subplots_adjust(top=0.88)
	plt.close()

