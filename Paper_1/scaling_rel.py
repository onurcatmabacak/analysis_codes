import numpy as np
import h5py
import os
import sys
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
from astropy import units as u
from astropy.cosmology import Planck15, z_at_value
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def time(i):
	return cosmo.age(i).value * 1e9 # yr

def Ferrerase02(Mass):	
	#return 10.**( 8.0 ) * 0.027 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	return 10.**( 8.0 ) * 0.100 * ( Mass/1e12 )**(1.65) # Ferrerase 2002
	#return 10.**( 8.0 ) * 0.670 * ( Mass/1e12 )**(1.82) # Ferrerase 2002
	#return 10.**( 8.18 ) * ( Mass/1e13 )**(1.55) # Bandara et al 2009
	#return 10.**( 7.85 ) * ( Mass/1e12 )**(1.33) # Di Matteo et al 2003

def Haring04(Mbulge):
   return 10.**( 8.20 ) * ( Mbulge/1e11 )**(1.12)

def Kormendy2013(Mbulge):
   return 0.49 * 1e9 * ( Mbulge/1e11 )**(1.16)

def McConnell2013(Mbulge):
   return 10.**( 8.46 ) * ( Mbulge/1e11 )**(1.05)

def Reines2015(Mstar):
   return 10.**( 7.45 ) * ( Mstar/1e11 )**(1.05)

def Savorgnan2016( Mstar, sample='all'):
	Mbh_early = 10.**( 8.56 + 1.04*np.log10(Mstar/10.**10.81) )
	Mbh_late = 10.**( 7.24 + 2.28*np.log10(Mstar/10.**10.05) )

	ind = np.where( Mbh_late - Mbh_early >= 0 )[0] 
	Mbh_all =  np.append( Mbh_late[:ind[0]], Mbh_early[ind[0]:] )

	if Mbh_all.size != Mstar.size:
	 print 'ahhhh.... check Savorgnan2016 !!!'

	if sample == 'early':
	 return Mbh_early
	elif sample == 'late':
	 return Mbh_late
	elif sample == 'all':
	 return Mbh_all

def McConnell_Ma2013(vel_disp):
	return 10.**( 8.32 ) * ( vel_disp/200.0 )**(5.64)

#################################################### 
################### MAIN PROGRAM ###################
#################################################### 

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

regime_list = ['cold', 'total']
radius_name_list = ['0.1', '0.2', '0.5', '1.0', 'rvir']
mass_retention_list = ['0.05', '0.1', '0.25', '0.5', '1.0']
seed_mass_list = ['1e1', '1e2', '1e3', '1e4', '1e5']
halo_ID = 0

mass_list = np.array([1e4,1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12])
mass_list_obs = np.array([1e9,1e10,1e11,1e12])
mass_list_ext = np.array([1e4,1e5,1e6,1e7,1e8,1e9])
vel_list = np.array([1e0,1e1,2.5e1,5e1,1e2,2.5e2,5e2])
vel_list_obs = np.array([8e1,1e2,2e2,3e2,4e2])
vel_list_ext = np.array([1e0,1e1,2e1,3e1,4e1,5e1,6e1,7e1,8e1])

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)

output_path_list = ['scaling_rel_r0.5', 'scaling_rel_r1.0']#, 'scaling_rel_r0.5', 'scaling_rel_r1.0']

for output_path in output_path_list:

	if not os.path.exists(output_path):
		os.makedirs(output_path) 

	ahf_centering_list = ['COM','MDC']
	merger_list = ['merger','nomerger']

	for ahf_centering in ahf_centering_list:
		for merger in merger_list:

			i_list = range(37)
			data = {}
			for regime in regime_list:
				for i in i_list:

					data[sim_suite[i] + '/' + sim_name[i]] = h5py.File('/bulk1/feldmann/Onur/' + ahf_centering + '/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_' + merger + '.hdf5','r')

			for mass_retention in mass_retention_list:
				for seed_mass in seed_mass_list:
					for regime in regime_list:

						fig = plt.figure(1, figsize=(8,4))
						fig.subplots_adjust(wspace=0.2, hspace=0.2)
						ax1 = fig.add_subplot(131)
						ax2 = fig.add_subplot(132)
						ax3 = fig.add_subplot(133)

						for i in i_list:
							
							if sim_suite[i] == 'MassiveFIRE':
								if output_path == 'scaling_rel_r0.1':
									radius = radius_name_list[0] #MassiveFIRE
								if output_path == 'scaling_rel_r0.2':
									radius = radius_name_list[1] #MassiveFIRE
								if output_path == 'scaling_rel_r0.5':
									radius = radius_name_list[2] #MassiveFIRE
								if output_path == 'scaling_rel_r1.0':
									radius = radius_name_list[3] #MassiveFIRE
								shape = 's'
							else:
								radius = radius_name_list[0] #MassiveFIRE2
								shape = '^'	

							#mass_retention = mass_retention_list[3] # every plot is for 0.1 or 1 kpc and %50 of mass retention rate
							#seed_mass = seed_mass_list[3] # seed mass is 1e4	

							redshift = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/z_1'].value
							mstar = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mstar'].value
							mbulge = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/mbulge'].value
							mbh = data[sim_suite[i] + '/' + sim_name[i]][regime + '/r_' + radius + '/mass_ret_' + mass_retention + '/seed_' + seed_mass + '/m_bh_torque'].value
							vel_disp = data[sim_suite[i] + '/' + sim_name[i]][regime + '/' + radius_name_list[-1] + '/vel_disp'].value

							#########################################
							### FILTERING FOR DIFFERENT REDSHIFTS ###
							#########################################

							z_list = [6.0, 4.0, 2.0, 1.0, 0.0]
							color = ['magenta','green','blue','red','black']

							#z_list = [0.0, 0.3, 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0]
							#color = ['black','red','blue','green','magenta','yellow','olivedrab','darkcyan','deepskyblue','coral']
							z_data = {}

							if i == 0:
								for z in z_list:
									inds = z_list.index(z)
									ax1.scatter(0.0, 0.0, color=color[inds], marker='.', s=20, label='z='+str(z))
								#ax1.legend(loc=0, prop={'size':10},frameon=False,scatterpoints=1)

							for z in z_list:
								sel = (redshift == z)
								inds = z_list.index(z)

								z_data[str(z) + '/mbh'] = mbh[sel]
								z_data[str(z) + '/mstar'] = mstar[sel]
								z_data[str(z) + '/mbulge'] = mbulge[sel]
								z_data[str(z) + '/vel_disp'] = vel_disp[sel]
								
								ax1.scatter(z_data[str(z) + '/mstar'], z_data[str(z) + '/mbh'], color=color[inds], marker=shape, s=30, alpha=0.3)
								ax2.scatter(z_data[str(z) + '/mbulge'], z_data[str(z) + '/mbh'], color=color[inds], marker=shape, s=30, alpha=0.3)
								ax3.scatter(z_data[str(z) + '/vel_disp'], z_data[str(z) + '/mbh'], color=color[inds], marker=shape, s=30, alpha=0.3)

						ax1.plot(mass_list_obs, Reines2015(mass_list_obs), color='k', ls='-', lw=1, label='Reines15')
						ax1.plot(mass_list_ext, Reines2015(mass_list_ext), color='k', ls='--', lw=1)
						ax2.plot(mass_list_obs, Haring04(mass_list_obs), color='k', ls='-', lw=1, label='Haring04')
						ax2.plot(mass_list_ext, Haring04(mass_list_ext), color='k', ls='--', lw=1)
						ax3.plot(vel_list_obs, McConnell_Ma2013(vel_list_obs), color='k', ls='-', lw=1, label='McConnell13')
						ax3.plot(vel_list_ext, McConnell_Ma2013(vel_list_ext), color='k', ls='--', lw=1)

						ax1.grid(which='major', axis='both', color='k', ls='--', lw=0.25)
						ax2.grid(which='major', axis='both', color='k', ls='--', lw=0.25)
						ax3.grid(which='major', axis='both', color='k', ls='--', lw=0.25)

						ax1.fill_between(mass_list, Reines2015(mass_list) * np.sqrt(1e1), Reines2015(mass_list) / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )
						ax2.fill_between(mass_list, Haring04(mass_list) * np.sqrt(1e1), Haring04(mass_list) / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )
						ax3.fill_between(vel_list, McConnell_Ma2013(vel_list) * np.sqrt(1e1), McConnell_Ma2013(vel_list) / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )

						ax1.set_xscale('log')
						ax1.set_yscale('log')
						ax2.set_xscale('log')
						ax2.set_yscale('log')
						ax3.set_xscale('log')
						ax3.set_yscale('log')

						ax1.set_xlim(1e7,3e12)
						ax1.set_ylim(0.7e4,1e10)
						ax2.set_xlim(1e7,3e12)
						ax2.set_ylim(0.7e4,1e10)
						ax3.set_xlim(1e1,1e3)
						ax3.set_ylim(0.7e4,1e10)

						ax1.xaxis.set_major_locator(plt.FixedLocator([1e7,1e8,1e9,1e10,1e11,1e12]))
						ax1.yaxis.set_major_locator(plt.FixedLocator([1e4,1e5,1e6,1e7,1e8,1e9,1e10]))
						ax2.xaxis.set_major_locator(plt.FixedLocator([1e7,1e8,1e9,1e10,1e11,1e12]))
						ax2.yaxis.set_major_locator(plt.FixedLocator([1e4,1e5,1e6,1e7,1e8,1e9,1e10]))
						ax3.xaxis.set_major_locator(plt.FixedLocator([1e1,1e2,1e3]))
						ax3.yaxis.set_major_locator(plt.FixedLocator([1e4,1e5,1e6,1e7,1e8,1e9,1e10]))

						ax1.xaxis.set_tick_params(labelsize=8)
						ax1.yaxis.set_tick_params(labelsize=8)
						ax2.xaxis.set_tick_params(labelsize=8)
						ax2.yaxis.set_tick_params(labelsize=8)
						ax3.xaxis.set_tick_params(labelsize=8)
						ax3.yaxis.set_tick_params(labelsize=8)

						ax1.set_xlabel(r'$M_{*}\;(M_{\odot})$')
						ax1.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
						ax2.set_xlabel(r'$M_{bulge}\;(M_{\odot})$')
						#ax2.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
						ax3.set_xlabel(r'$\sigma\;(km/s)$')
						#ax3.set_ylabel(r'$M_{BH}\;(M_{\odot})$')

						#ax1.text(1e12, 5e4, r'$M_{star}$', verticalalignment='top', horizontalalignment='left')
						#ax2.text(1e12, 5e4, r'$M_{bulge}$', verticalalignment='top', horizontalalignment='left')

						ax1.legend(loc=2, prop={'size':8},frameon=False,scatterpoints=1)
						ax2.legend(loc=2, prop={'size':8},frameon=False,scatterpoints=1)	
						ax3.legend(loc=2, prop={'size':8},frameon=False,scatterpoints=1)

						#ax2.get_yaxis().set_visible(False)
						#ax3.get_yaxis().set_visible(False)

						#plt.tight_layout()
						plt.savefig(output_path + '/plot_#1_scaling_relations_' + ahf_centering + '_' + regime + '_seed_' + seed_mass + '_mass_ret_' + mass_retention + '_' + merger + '.png', dpi=300, format='png')
						plt.close()


