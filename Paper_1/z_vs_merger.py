import numpy as np
import h5py
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite=['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']
label = ['B100_TL1', 'B100_TL3', 'B100_TL4', 'B100_TL2', 'B100_TL9', 'B100_TL13', 'B100_TL18', 'B100_TL29', 'B100_TL37', 'B100_TL113', 'B100_TL206', 'B400_TL10', 'B400_TL0', 'B400_TL1', 'B400_TL2', 'B400_TL5', 'B400_TL6', 'B400_TL13', 'B400_TL17', 'B400_TL21', 'B762_TL0_MF1', 'B762_TL1_MF1', 'B762_TL2_MF1', 'B762_TL0_MF2', 'B762_TL1_MF2', 'B762_TL2_MF2', 'h113', 'h206', 'h29', 'h2']
color = ['m', 'm', 'k', 'k', 'r', 'r', 'r', 'g', 'g', 'b', 'b', 'k', 'k', 'r', 'r', 'g', 'g', 'b', 'b', 'b', 'k', 'r', 'g', 'k', 'r', 'g', 'black', 'red', 'green', 'blue']
linestyle = ['-', '--', '-', '--', '-', '--', '-.', '-', '--', '-', '--', '-', '--', '-', '--', '-', '--', '-', '--', '-.', '-', '-', '-', '--', '--', '--', '-', '-', '-', '-']

i_list_B100s = [0,1,2,3,4,5,6,7,8,9,10]
i_list_B400s = [11,12,13,14,15,16,17,18,19]
i_list_B762s = [20,21,22,23,24,25]
i_list_MF2 = [26,27,28,29]
i_list = [i_list_B100s, i_list_B400s, i_list_B762s, i_list_MF2]

plotname = ['MF1_B100s', 'MF1_B400s', 'B762s', 'MF2 h series']
min_lim = [1e-1, 1e-1, 1e-1, 1e-1]
max_lim = [1e3, 1e2, 1e3, 1e2]
z_min = [0.0, 5.8, 5.8, 0.8]

for j in range(len(i_list)):

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0.0, hspace=0.0)
	fig.suptitle(plotname[j])
	ax0 = fig.add_subplot(111)	

	for i in i_list[j]:
		filename = '/bulk1/feldmann/Onur/' + sim_suite[i] + '/' + sim_name[i] + '/'

		redshift, position = np.genfromtxt(filename+'zred_pos_list', delimiter='', usecols=(0,1), skip_header=0, unpack=True)
		position = position.astype(int)

		z_1 = []
		ID = []
		position = []
		mstar = []
		mhalo = []
		z_2 = []
		descendant = []

		file_halo = open(filename+'halo_0.txt', 'r')
		for line in file_halo.readlines():
			data = map(float, np.array(line.split()))
			z_1.append(data[0])
			position.append(redshift.tolist().index(data[0]))
			ID.append(int(data[1]))
			mstar.append(data[2])
			mhalo.append(data[3])
			z_2.append(data[4])
			descendant.append([int(xxx) for xxx in data[5:]])
		file_halo.close()

		z_1 = np.array(z_1)
		position = np.array(position)
		ID = np.array(ID)
		mstar = np.array(mstar)
		mhalo = np.array(mhalo)
		z_2 = np.array(z_2)
		descendant = np.array(descendant)

		if sim_suite[i] == 'MassiveFIRE2':
			radius_name = '0.1'
		else:
			radius_name = '1.0'

		data = h5py.File(filename + sim_name[i] + '_all.hdf5','r')
		mbh = data['cold/r_' + radius_name + '/mass_ret_0.5/seed_1e4/m_bh_torque'].value
		mbh = mbh[::-1]

		tot_merger = []

		for xxx in range(len(z_1)):

			inds = len(descendant[xxx])-1
			tot_merger.append(inds)
		
		merger_final = []

		for xxx in range(len(redshift)):

			sel = (z_1 == redshift[xxx])
			new = int(np.sum(np.array(tot_merger)[sel]))

			if new:
				merger_final.append(new)
			else:
				merger_final.append(0)

		merger = np.cumsum(merger_final)

		ax0.plot(redshift, merger, color=color[i], ls=linestyle[i], lw=2, label=label[i])

	ax0.set_xscale('linear')
	ax0.set_yscale('log')
	ax0.set_xlim(z_min[j],12)
	ax0.set_ylim(min_lim[j], max_lim[j])
	ax0.set_xlabel(r'$z$')
	ax0.set_ylabel(r'$cumulative \; \# \; of \; mergers$')
	ax0.legend(loc=0,prop={'size':8},frameon=False,scatterpoints=1)

	#plt.tight_layout()
	plt.savefig('plot_#9_cumulative_#_of_merger_' + plotname[j] + '.png', dpi=300, format='png')
	plt.close()
