import numpy as np
import h5py
import glob
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

def mdot(sfr):
   return sfr / 500.0

sim_name = ['h2', 'h29', 'h113', 'h206']
com_marker = ['*', 'v', 's', 'o']
mdc_marker = ['P', 'x', 'd', '>']

mass_ret = 'mass_ret_0.1'

sfr = np.logspace(-6,4,num=100,endpoint=True)
for i in range(4):

	fig = plt.figure(1)
	fig.subplots_adjust(wspace=0, hspace=0)

	ax1 = fig.add_subplot(111)

	filename_oc_nomerger_com = glob.glob('../COM/MassiveFIRE2/' + sim_name[i] + '_*/' + sim_name[i] + '*_nomerger.hdf5')[0]
	filename_oc_nomerger_mdc = glob.glob('../MDC/MassiveFIRE2/' + sim_name[i] + '_*/' + sim_name[i] + '*_nomerger.hdf5')[0]

	data_oc_nomerger_com = h5py.File(filename_oc_nomerger_com, 'r')
	data_oc_nomerger_mdc = h5py.File(filename_oc_nomerger_mdc, 'r')
	
	mdot_com = data_oc_nomerger_com['cold/r_0.1/' + mass_ret + '/seed_1e4/mdot_torque'].value
	mdot_mdc = data_oc_nomerger_mdc['cold/r_0.1/' + mass_ret + '/seed_1e4/mdot_torque'].value
	
	sfr_com = data_oc_nomerger_com['cold/rvir/sfr'].value
	sfr_mdc = data_oc_nomerger_mdc['cold/rvir/sfr'].value

	ax1.scatter(mdot_com, sfr_com, c='g', marker=com_marker[i], s=20, label='COM', alpha=0.3)
	ax1.scatter(mdot_mdc, sfr_mdc, c='r', marker=mdc_marker[i], s=20, label='MDC', alpha=0.3)
	ax1.plot(mdot(sfr), sfr, color='k', ls='--', lw=2, label=r'$\dot{M}=SFR/500$')

	ax1.set_xlim(1e-7,1e0)
	ax1.set_ylim(1e-5,1e4)
	ax1.set_xscale('log')
	ax1.set_yscale('log')
	ax1.set_xlabel(r'$\dot{M}\;(M_{\odot}/yr)$')
	ax1.set_ylabel(r'$SFR\;(M_{\odot}/yr)$')
	ax1.grid(color='k', ls='--', lw=0.25)
	ax1.fill_between(mdot(sfr), sfr * np.sqrt(1e1), sfr / np.sqrt(1e1), color='k', zorder=0, alpha=0.1 )
	#ax1.xaxis.set_major_locator(plt.FixedLocator(mass_ticks))
	ax1.legend(loc=2,prop={'size':6},frameon=False,scatterpoints=1)

	#plt.show()
	plt.savefig('sfr_mdot_' +  sim_name[i] + '_' + mass_ret + '.png', dpi=300, format='png')
	plt.close()
