import os
import glob
import numpy as np
from astropy.cosmology import FlatLambdaCDM
from astropy import constants as const
from astropy import units as u
from matplotlib import pyplot as plt
from matplotlib import animation
import multiprocessing
from joblib import Parallel, delayed
from pynverse import inversefunc
import h5py 

def Kormendy2013(Mbulge):

   return 0.49 * 1e9 * ( Mbulge/1e11 )**(1.16)

##############################################
################## MAIN BODY #################
##############################################
global cosmo
global grav_constant
global kpc_cm_conversion
global m_sun
global yr_s_conversion
global h
global data
global r_hsm

cosmo = FlatLambdaCDM(H0 = 69.7, Om0 = 0.284)
h = 0.697
grav_constant = const.G.cgs.value
kpc_cm_conversion = 3.086e21
m_sun = const.M_sun.cgs.value
yr_s_conversion = 3.15576e7

sim_name = ['B100_N512_M3e13_TL00001_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00003_baryon_toz2_HR_9915', 'B100_N512_M3e13_TL00004_baryon_toz2_HR_9915', 'B100_N512_TL00002_baryon_toz2', 'B100_N512_TL00007_baryon_toz2_HR', 'B100_N512_TL00009_baryon_toz2_HR', 'B100_N512_TL00011_baryon_toz2_HR', 'B100_N512_TL00013_baryon_toz2_HR', 'B100_N512_TL00018_baryon_toz2_HR', 'B100_N512_TL00029_baryon_toz2_HR', 'B100_N512_TL00031_baryon_toz2_HR', 'B100_N512_TL00037_baryon_toz0_HR_9915', 'B100_N512_TL00113_baryon_toz0_HR_9915', 'B100_N512_TL00206_baryon_toz0_HR_9915', 'B100_N512_TL00217_baryon_toz2', 'B100_N512_TL00223_baryon_toz2_HR', 'B100_N512_TL00228_baryon_toz2', 'B100_N512_TL00236_baryon_toz2','B400_N512_M1e14_TL00010_baryon_toz6_HR_9915', 'B400_N512_z6_TL00000_baryon_toz6_HR_9915', 'B400_N512_z6_TL00001_baryon_toz6_HR_9915', 'B400_N512_z6_TL00002_baryon_toz6_HR_9915', 'B400_N512_z6_TL00005_baryon_toz6_HR_9915', 'B400_N512_z6_TL00006_baryon_toz6_HR_9915', 'B400_N512_z6_TL00013_baryon_toz6_HR_9915', 'B400_N512_z6_TL00017_baryon_toz6_HR_9915', 'B400_N512_z6_TL00021_baryon_toz6_HR_9915', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'B762_N1024_z6_TL00000_baryon_toz6_HR', 'B762_N1024_z6_TL00001_baryon_toz6_HR', 'B762_N1024_z6_TL00002_baryon_toz6_HR', 'h113_HR_sn1dy300ro100ss', 'h206_HR_sn1dy300ro100ss', 'h29_HR_sn1dy300ro100ss', 'h2_HR_sn1dy300ro100ss']
sim_suite = ['MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2', 'MassiveFIRE2']

dummy = [16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 16, 178, 178, 178, 11, 11, 11, 11]
regime_list = ['cold', 'total']
radius_name_list = ['0.1', '0.2', '0.5', '1.0', 'rvir']
mass_retention_list = ['0.05', '0.1', '0.25', '0.5', '1.0']
seed_mass_list = ['1e1', '1e2', '1e3', '1e4', '1e5']
mass_list = np.array([1e5,1e6,1e7,1e8,1e9,1e10,1e11,1e12,1e13,1e14])

##### ADDITIONAL DATA #####

hsc_mass, hsc_quasars, hsc_uperror, hsc_lowerror = np.genfromtxt('HSC_quasars.txt', delimiter='', usecols=(0,1,2,3), skip_header=1, unpack=True)
hsc_mass_edd, hsc_quasars_edd, hsc_uperror_edd, hsc_lowerror_edd = np.genfromtxt('HSC_quasars_edd.txt', delimiter='', usecols=(0,1,2,3), skip_header=1, unpack=True)

lowlum_mass, lowlum_quasars, lowlum_uperror, lowlum_lowerror = np.genfromtxt('Low_lum_quasars.txt', delimiter='', usecols=(0,1,2,3), skip_header=1, unpack=True)
lowlum_mass_edd, lowlum_quasars_edd, lowlum_uperror_edd, lowlum_lowerror_edd = np.genfromtxt('Low_lum_quasars_edd.txt', delimiter='', usecols=(0,1,2,3), skip_header=1, unpack=True)

lum_mass, lum_quasars, lum_uperror, lum_lowerror = np.genfromtxt('Lum_quasars.txt', delimiter='', usecols=(0,1,2,3), skip_header=1, unpack=True)
lum_mass_edd, lum_quasars_edd, lum_uperror_edd, lum_lowerror_edd = np.genfromtxt('Lum_quasars_edd.txt', delimiter='', usecols=(0,1,2,3), skip_header=1, unpack=True)

hsc_yerr = np.array([hsc_quasars-hsc_lowerror, hsc_uperror-hsc_quasars])
hsc_yerr_edd = np.array([hsc_quasars_edd-hsc_lowerror_edd, hsc_uperror_edd-hsc_quasars_edd])
lowlum_yerr = np.array([lowlum_quasars-lowlum_lowerror, lowlum_uperror-lowlum_quasars])
lowlum_yerr_edd = np.array([lowlum_quasars_edd-lowlum_lowerror_edd, lowlum_uperror_edd-lowlum_quasars_edd])
lum_yerr = np.array([lum_quasars-lum_lowerror, lum_uperror-lum_quasars])
lum_yerr_edd = np.array([lum_quasars_edd-lum_lowerror_edd, lum_uperror_edd-lum_quasars_edd])

i_list = range(37)

###############################################
#################### FIG 1 ####################
###############################################
fig = plt.figure(1)
fig.suptitle(r'$M_{tot}\;(<R_{hsm})\;vs\;M_{BH}$')
fig.subplots_adjust(wspace=0, hspace=0)

ax1 = fig.add_subplot(111)	

for i in i_list:

	if sim_suite[i] == 'MassiveFIRE':
		radius = radius_name_list[0] #MassiveFIRE
		shape = 's'
	else:
		radius = radius_name_list[0] #MassiveFIRE2
		shape = '^'	

	regime = regime_list[0]
	mass_retention = mass_retention_list[3] # every plot is for 0.1 or 1 kpc and %50 of mass retention rate
	seed_mass = seed_mass_list[3] # seed mass is 1e4	

	output = '/bulk1/feldmann/Onur/Paper_1/'
	redshift_path = '/bulk1/feldmann/Onur/MDC/' + sim_suite[i] + '/' + sim_name[i] + '/zred_pos_list'
	timeline_path = '/bulk1/feldmann/Onur/MDC/' + sim_suite[i] + '/' + sim_name[i] + '/timeline_0.txt'
	data_path = '/bulk1/feldmann/Onur/MDC/' + sim_suite[i] + '/' + sim_name[i] + '/' + sim_name[i] + '_merger.hdf5'
	data = h5py.File(data_path,'r')

	r_hsm = data['cold/rvir/r_hsm'].value
	mbh = data[regime + '/r_' + radius + '/mass_ret_' + mass_retention + '/seed_' + seed_mass + '/m_bh_torque'].value
	redshift, position = np.genfromtxt(redshift_path, delimiter='', usecols=(0,1), unpack=True)

	redshift_list, halo_ID = np.genfromtxt(timeline_path, delimiter='', usecols=(0,1), skip_header=0, unpack=True)
	redshift_list = redshift_list[::-1]

	halo_ID = halo_ID.astype(int)
	halo_ID = halo_ID[::-1]
	scale = 1.0 / (redshift_list + 1.0)

	filename = []
	for xxx in redshift_list:
		filename.append(glob.glob('/bulk1/feldmann/data/' + sim_suite[i] + '/analysis/AHF/HR/' + sim_name[i] + '/halo/' + str('%03d' %(redshift.tolist().index(xxx)+dummy[i])) + '/*disks')[0])

	filename = np.array(filename)

	z_list = [8.0, 6.0, 4.0]
	color = ['blue','red','black']

	if i == 35:
		for z in z_list:
			inds = z_list.index(z)
			ax1.scatter(0.0, 0.0, color=color[inds], marker='.', s=20, label='z='+str(z))

	for z in z_list:
		sel = (redshift_list == z)
		inds = z_list.index(z)

		if len(redshift_list[sel]) > 0: 
			
			fname = filename[sel][0]
			ID = halo_ID[sel][0]
			radius_hsm = r_hsm[sel][0]
			ascale = scale[sel][0]
			mass_bh = mbh[sel][0]

			radius, m_tot = np.genfromtxt(fname, delimiter='', usecols=(0,1), skip_header=1, unpack=True)
			radius = np.array([np.abs(a) for a in radius])

			flag = 0
			init = [0]
			for xxx in range(10000):
				if radius[xxx+1] < radius[xxx]:
					init.append(xxx)
					if flag == ID*2+1:
						break
					else:
						flag += 1

			start = init[2 * ID] + 1
			finish = init[2 * ID + 1] + 1

			hinv = 1.0 / 0.697
			constant = ascale * hinv

			radius = radius[start:finish] * constant
			m_tot = m_tot[start:finish] * hinv

			fit = np.poly1d(np.polyfit(radius, m_tot, 5))
			inverse_fit = inversefunc(fit)

			#new_data.append(inverse_fit(r_hsm[x]))
			new_data = fit(radius_hsm)

			ax1.scatter(new_data, mass_bh, color=color[inds], marker=shape, s=60, alpha=0.5)


ax1.errorbar(hsc_mass, hsc_quasars, yerr=hsc_yerr, c='black', fmt='*', mfc='turquoise', mec='black', ms=8, capsize=5, mew=1, label='HSC quasars')
ax1.errorbar(hsc_mass_edd, hsc_quasars_edd, yerr=hsc_yerr_edd, c='turquoise', fmt='*', mfc='turquoise', mec='black', ms=16, capsize=5, mew=1)
ax1.errorbar(lowlum_mass, lowlum_quasars, yerr=lowlum_yerr, c='black', fmt='v', mfc='blue', mec='black', ms=8, capsize=5, mew=1, label='Low-luminosity quasars')
ax1.errorbar(lowlum_mass_edd, lowlum_quasars_edd, yerr=lowlum_yerr_edd, c='blue', fmt='v', mfc='blue', mec='black', ms=16, capsize=5, mew=1)
ax1.errorbar(lum_mass, lum_quasars, yerr=lum_yerr, c='black', fmt='d', mfc='red', mec='black', ms=8, capsize=5, mew=1, label='Luminous quasars')
ax1.errorbar(lum_mass_edd, lum_quasars_edd, yerr=lum_yerr_edd, c='red', fmt='d', mfc='red', mec='black', ms=16, capsize=5, mew=1)

ax1.plot(mass_list, Kormendy2013(mass_list), color='black', ls='--', lw=1, label='Kormendy & Ho 2013')
ax1.fill_between(mass_list, Kormendy2013(mass_list) * np.sqrt(1e1), Kormendy2013(mass_list) / np.sqrt(1e1), color='black', zorder=0, alpha=0.1)
ax1.set_xscale('log')
ax1.set_yscale('log')
ax1.set_xlim(0.5e10,3e11)
ax1.set_ylim(1e6,2e10)
ax1.xaxis.set_major_locator(plt.FixedLocator([1e7,1e8,1e9,1e10,1e11,1e12,1e13]))
ax1.yaxis.set_major_locator(plt.FixedLocator([1e3,1e4,1e5,1e6,1e7,1e8,1e9,1e10,1e11]))
ax1.tick_params(axis='y', labelsize=8)
ax1.set_xlabel(r'$M_{dyn}\;(M_{\odot})$')
ax1.set_ylabel(r'$M_{BH}\;(M_{\odot})$')
ax1.legend(loc=2,prop={'size':6},frameon=False,scatterpoints=1)

plt.savefig('plot_#6c_mbh_mdyn.png', dpi=300, format='png')
plt.close()

#plt.show()
