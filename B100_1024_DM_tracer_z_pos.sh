#!/bin/bash

SIM_NAME=(B100_1024_DM)
SIM_SUITE=(MassiveFIRE2)
START=(0)
FINISH=(600)

LIST=(0)

for i in ${LIST[@]}
do
	file_path=/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}
	write_path=$file_path/zred_pos_list
	tmp_path=$file_path/tmp.txt
	echo ${SIM_NAME[$i]}
	rm $write_path

	for ((j=${START[$i]}; j<=${FINISH[$i]}; j++)) 
	do
		SNAPSHOT=`printf "%03d" $j`
		array=/bulk1/feldmann/data/${SIM_SUITE[$i]}/production_runs/${SIM_NAME[$i]}/snapdir_$SNAPSHOT/snapshot_$SNAPSHOT.0.hdf5

		h5dump -a "Header/Redshift" $array > $tmp_path
		name=$(sed -n 6p $tmp_path)
		redshift=`printf "%.4f" ${name:8}`

		dummy=`printf $array | cut -d "/" -f 8`
		position=${dummy:8}
		echo $redshift   $position >> $write_path

		rm $tmp_path
	done
done
