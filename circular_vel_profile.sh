#!/bin/bash -l
#
#SBATCH --job-name="onur"
#SBATCH --time=24:0:0 --mem=200G
#SBATCH --ntasks=1 --cpus-per-task=32
#SBATCH --output=%j.o
#SBATCH --error=%j.e

#======START=====
#export OMP_NUM_THREADS=32
srun python circular_vel_profile.py
#=====END====
