#!/bin/bash
module load hydra
export OMP_NUM_THREADS=32

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

#MassiveFIRE
#0  B100_N512_M3e13_TL00001_baryon_toz2_HR_9915
#1  B100_N512_M3e13_TL00003_baryon_toz2_HR_9915
#2  B100_N512_M3e13_TL00004_baryon_toz2_HR_9915
#3  B100_N512_TL00002_baryon_toz2
#4  B100_N512_TL00007_baryon_toz2_HR
#5  B100_N512_TL00009_baryon_toz2_HR
#6  B100_N512_TL00011_baryon_toz2_HR
#7  B100_N512_TL00013_baryon_toz2_HR
#8  B100_N512_TL00018_baryon_toz2_HR
#9  B100_N512_TL00029_baryon_toz2_HR
#10 B100_N512_TL00031_baryon_toz2_HR
#11 B100_N512_TL00037_baryon_toz0_HR_9915
#12 B100_N512_TL00113_baryon_toz0_HR_9915
#13 B100_N512_TL00206_baryon_toz0_HR_9915
#14 B100_N512_TL00217_baryon_toz2
#15 B100_N512_TL00223_baryon_toz2_HR
#16 B100_N512_TL00228_baryon_toz2
#17 B100_N512_TL00236_baryon_toz2
#18 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915
#19 B400_N512_z6_TL00000_baryon_toz6_HR_9915
#20 B400_N512_z6_TL00001_baryon_toz6_HR_9915
#21 B400_N512_z6_TL00002_baryon_toz6_HR_9915
#22 B400_N512_z6_TL00005_baryon_toz6_HR_9915
#23 B400_N512_z6_TL00006_baryon_toz6_HR_9915
#24 B400_N512_z6_TL00013_baryon_toz6_HR_9915
#25 B400_N512_z6_TL00017_baryon_toz6_HR_9915
#26 B400_N512_z6_TL00021_baryon_toz6_HR_9915
#27 B762_N1024_z6_TL00000_baryon_toz6_HR
#28 B762_N1024_z6_TL00001_baryon_toz6_HR
#29 B762_N1024_z6_TL00002_baryon_toz6_HR

#MassiveFIRE2
#30 B762_N1024_z6_TL00000_baryon_toz6_HR 
#31 B762_N1024_z6_TL00001_baryon_toz6_HR 
#32 B762_N1024_z6_TL00002_baryon_toz6_HR
#33 h113_HR_sn1dy300ro100ss
#34 h206_HR_sn1dy300ro100ss
#35 h29_HR_sn1dy300ro100ss
#36 h2_HR_sn1dy300ro100ss
	
SIM_NAME=(B100_N512_M3e13_TL00001_baryon_toz2_HR_9915 B100_N512_M3e13_TL00003_baryon_toz2_HR_9915 B100_N512_M3e13_TL00004_baryon_toz2_HR_9915 B100_N512_TL00002_baryon_toz2 B100_N512_TL00007_baryon_toz2_HR B100_N512_TL00009_baryon_toz2_HR B100_N512_TL00011_baryon_toz2_HR B100_N512_TL00013_baryon_toz2_HR B100_N512_TL00018_baryon_toz2_HR B100_N512_TL00029_baryon_toz2_HR B100_N512_TL00031_baryon_toz2_HR B100_N512_TL00037_baryon_toz0_HR_9915 B100_N512_TL00113_baryon_toz0_HR_9915 B100_N512_TL00206_baryon_toz0_HR_9915 B100_N512_TL00217_baryon_toz2 B100_N512_TL00223_baryon_toz2_HR B100_N512_TL00228_baryon_toz2 B100_N512_TL00236_baryon_toz2 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915 B400_N512_z6_TL00000_baryon_toz6_HR_9915 B400_N512_z6_TL00001_baryon_toz6_HR_9915 B400_N512_z6_TL00002_baryon_toz6_HR_9915 B400_N512_z6_TL00005_baryon_toz6_HR_9915 B400_N512_z6_TL00006_baryon_toz6_HR_9915 B400_N512_z6_TL00013_baryon_toz6_HR_9915 B400_N512_z6_TL00017_baryon_toz6_HR_9915 B400_N512_z6_TL00021_baryon_toz6_HR_9915 B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR h113_HR_sn1dy300ro100ss h206_HR_sn1dy300ro100ss h29_HR_sn1dy300ro100ss h2_HR_sn1dy300ro100ss)
SIM_SUITE=(MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2)

AHF_CENTERING=$1 # MDC or COM
AHF_FILEDIR=$2 # AHF or AHF_com
MERGER=(1)
FILTER_VALUE=0
MEMORY_PER_JOB=4
LIST=$(seq 0 36)

for i in ${LIST[@]}
do

	if [ "${SIM_SUITE[$i]}" == "MassiveFIRE" ] 
	then
		ROOT=/net/cephfs/shares
	else
		ROOT=/bulk1
	fi

	INPUT_PATH=/bulk1/feldmann/Onur/$AHF_CENTERING/${SIM_SUITE[$i]}/${SIM_NAME[$i]}
	HALO_PATH=$ROOT/feldmann/data/${SIM_SUITE[$i]}/analysis/$AHF_FILEDIR/HR/${SIM_NAME[$i]}/halo
	SAVE_PATH=/bulk1/feldmann/Onur/$AHF_CENTERING

	for j in ${MERGER[@]}
	do
		NJOBS=2
		let "MEMORY=$NJOBS*$MEMORY_PER_JOB"
		JOBSUB=`printf "jobsub_bhgrowth_%s_merger_%s.sh" ${SIM_NAME[$i]} $j`

		#python bh_growth.py $INPUT_PATH $HALO_PATH $SAVE_PATH ${SIM_SUITE[$i]} ${SIM_NAME[$i]} $j $FILTER_VALUE

		printf "#!/bin/bash -l \n" > $JOBSUB
		printf "#SBATCH --qos=medium" >> $JOBSUB
		printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
		printf "#SBATCH --time=24:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
		printf "#SBATCH --ntasks=1 --cpus-per-task=$NJOBS \n" >> $JOBSUB
		printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
		printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
		printf "#======START===== \n" >> $JOBSUB
		printf "srun python bh_growth_all.py $INPUT_PATH $HALO_PATH $SAVE_PATH ${SIM_SUITE[$i]} ${SIM_NAME[$i]} $j $FILTER_VALUE \n" >> $JOBSUB 
		printf "#=====END==== \n" >> $JOBSUB
		sbatch $JOBSUB 
	done
done
