#!/bin/bash
module load hydra
export OMP_NUM_THREADS=32

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

#MassiveFIRE
#0  B100_N512_M3e13_TL00001_baryon_toz2_HR_9915
#1  B100_N512_M3e13_TL00003_baryon_toz2_HR_9915
#2  B100_N512_M3e13_TL00004_baryon_toz2_HR_9915
#3  B100_N512_TL00002_baryon_toz2
#4  B100_N512_TL00009_baryon_toz2_HR
#5  B100_N512_TL00013_baryon_toz2_HR
#6  B100_N512_TL00018_baryon_toz2_HR
#7  B100_N512_TL00029_baryon_toz2_HR
#8  B100_N512_TL00037_baryon_toz0_HR_9915
#9  B100_N512_TL00113_baryon_toz0_HR_9915
#10 B100_N512_TL00206_baryon_toz0_HR_9915
#11 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915
#12 B400_N512_z6_TL00000_baryon_toz6_HR_9915
#13 B400_N512_z6_TL00001_baryon_toz6_HR_9915
#14 B400_N512_z6_TL00002_baryon_toz6_HR_9915
#15 B400_N512_z6_TL00005_baryon_toz6_HR_9915
#16 B400_N512_z6_TL00006_baryon_toz6_HR_9915
#17 B400_N512_z6_TL00013_baryon_toz6_HR_9915
#18 B400_N512_z6_TL00017_baryon_toz6_HR_9915
#19 B400_N512_z6_TL00021_baryon_toz6_HR_9915
#20 B762_N1024_z6_TL00000_baryon_toz6_HR
#21 B762_N1024_z6_TL00001_baryon_toz6_HR
#22 B762_N1024_z6_TL00002_baryon_toz6_HR

#MassiveFIRE2
#23 B762_N1024_z6_TL00000_baryon_toz6_HR 
#24 B762_N1024_z6_TL00001_baryon_toz6_HR 
#25 B762_N1024_z6_TL00002_baryon_toz6_HR
#26 h113_HR_sn1dy300ro100ss
#27 h206_HR_sn1dy300ro100ss
#28 h29_HR_sn1dy300ro100ss
#29 h2_HR_sn1dy300ro100ss

SIM_NAME=(B100_N512_M3e13_TL00001_baryon_toz2_HR_9915 B100_N512_M3e13_TL00003_baryon_toz2_HR_9915 B100_N512_M3e13_TL00004_baryon_toz2_HR_9915 B100_N512_TL00002_baryon_toz2 B100_N512_TL00009_baryon_toz2_HR B100_N512_TL00013_baryon_toz2_HR B100_N512_TL00018_baryon_toz2_HR B100_N512_TL00029_baryon_toz2_HR B100_N512_TL00037_baryon_toz0_HR_9915 B100_N512_TL00113_baryon_toz0_HR_9915 B100_N512_TL00206_baryon_toz0_HR_9915 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915 B400_N512_z6_TL00000_baryon_toz6_HR_9915 B400_N512_z6_TL00001_baryon_toz6_HR_9915 B400_N512_z6_TL00002_baryon_toz6_HR_9915 B400_N512_z6_TL00005_baryon_toz6_HR_9915 B400_N512_z6_TL00006_baryon_toz6_HR_9915 B400_N512_z6_TL00013_baryon_toz6_HR_9915 B400_N512_z6_TL00017_baryon_toz6_HR_9915 B400_N512_z6_TL00021_baryon_toz6_HR_9915 B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR h113_HR_sn1dy300ro100ss h206_HR_sn1dy300ro100ss h29_HR_sn1dy300ro100ss h2_HR_sn1dy300ro100ss)
SIM_SUITE=(MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2)
DUMMY=(16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 178 178 178 11 11 11 11)
AHF=/bulk1/feldmann/data/MassiveFIRE2/analysis/ahf
AHF_VERSION=AHF-v1.0-094
HALO=halo
MEMORY_PER_JOB=50
LIST=(3 7 9 10)

for i in ${LIST[@]}
do
	FILE_LIST=(/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}/halo/*/*particles)
	NUMOFLINES=${#FILE_LIST[@]}	
	NJOBS=20
	NCPU=2
	ZERO=0

	let "MEMORY=$NCPU*$MEMORY_PER_JOB"

	let "NUM_START=$NUMOFLINES-1"

	for ((j=$NUM_START; j>=0; j=j-NJOBS))
	do
		let "k=$j-$NJOBS"

		if [ "$k" -le "$ZERO" ];
		then
			let "k=$ZERO"
			let "NJOBS=$j-$k+1"
		fi

		START=$j
		let "FINISH=$k"
		let "DIFF=$NJOBS+1"
		
		if [ "$k" -eq "$ZERO" ];
		then
			let "DIFF=$NJOBS"
		fi

		###### PREPARING MERGERTREE FILE
		MERGERTREE=`printf "mergertree_%s_%03d_%03d" ${SIM_NAME[$i]} $START $FINISH`
		rm $MERGERTREE
		echo $DIFF > $MERGERTREE
		echo " " >> $MERGERTREE 

		for ((k=$START; k>=$FINISH; k--))	
		do
			echo ${FILE_LIST[$k]} >> $MERGERTREE
		done
		
		echo " " >> $MERGERTREE 

		for ((k=$START; k>=$FINISH; k--))	
		do
			PREFIX=${FILE_LIST[$k]::(-10)}
			echo $PREFIX"_backward" >> $MERGERTREE
		done
		################################

		JOBSUB=`printf "jobsub_mergertree_%s_%03d_%03d.sh" ${SIM_NAME[$i]} $START $FINISH`

		printf "#!/bin/bash -l \n" > $JOBSUB
		printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
		printf "#SBATCH --time=24:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
		printf "#SBATCH --ntasks=1 --cpus-per-task=$NCPU \n" >> $JOBSUB
		printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
		printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
		printf "#======START===== \n" >> $JOBSUB
		printf "srun $AHF/$AHF_VERSION/bin/MergerTree < $MERGERTREE \n" >> $JOBSUB 
		printf "#=====END==== \n" >> $JOBSUB
		sbatch $JOBSUB 
	done
done
