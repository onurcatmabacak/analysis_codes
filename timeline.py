import numpy as np
import onur_tools as ot
import sys
import glob

def timeline(input_path, halo_ID):

	redshift, place = ot.z(input_path)
	redshift = redshift[::-1]		

	z_1 = []
	position = []
	ID = []
	mstar = []
	mhalo = []
	z_2 = []
	descendant = []	

	filename = input_path + '/halo_' + str(halo_ID) + '.txt'
	file_halo = open(filename, 'r')
	for line in file_halo.readlines():
		data = map(float, np.array(line.split()))
		z_1.append(data[0])
		position.append(redshift.tolist().index(data[0]))
		ID.append(int(data[1]))
		mstar.append(data[2])
		mhalo.append(data[3])
		z_2.append(data[4])
		descendant.append(int(data[5]))
	file_halo.close()

	timeline_file = input_path + '/timeline_' + str(halo_ID) + '.txt'
	write_data = open(timeline_file, 'w')	

	ID_filter = halo_ID
	for i in range(len(redshift)):

		data = [[a,b,c,d,e] for a,b,c,d,e in zip(z_1,ID,mstar,mhalo,descendant) if a == redshift[i] if b == ID_filter]

		if data:
			if data[0] != -1:
				write_data.write('%-6.3f %-5i %-10.3e %-10.3e \n' %(data[0][0],data[0][1],data[0][2],data[0][3]))
				#print '%-6.3f %-5i %-10.3e %-10.3e' %(data[0][0],data[0][1],data[0][2],data[0][3])
				ID_filter = data[0][4]
			else:
				break

	write_data.close()

##############################
######### MAIN BODY ##########
##############################

input_path = str(sys.argv[1])

remove = len(input_path) + 6
filelist = sorted(glob.glob(input_path + '/halo_*.txt'))
halo_ID_list = []

for filename in filelist:
	halo_ID_list.append(filename[remove:-4])

halo_ID_list = [162] #np.sort(np.array(halo_ID_list).astype(int))

for halo_ID in halo_ID_list:
	timeline(input_path, halo_ID)
