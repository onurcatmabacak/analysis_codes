#!/bin/bash
module load hydra
export OMP_NUM_THREADS=32

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

#MassiveFIRE2
#1 B100_1024_DM 

SIM_NAME=(B100_1024_DM)
SIM_SUITE=(MassiveFIRE2)
AHF_VERSION=AHF-v1.0-094_DM
HALO=halo
MEMORY_PER_JOB=50
LIST=(0)

for i in ${LIST[@]}
do

	AHF=/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/ahf
	FILE_LIST=(/bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}/halo/*/*particles)
	NUMOFLINES=568 #${#FILE_LIST[@]}	
	NJOBS=10
	NCPU=2
	let "MEMORY=$NCPU*$MEMORY_PER_JOB"

	INITIAL=567 #0

	for ((j=$INITIAL; j<$NUMOFLINES; j=j+NJOBS))
	do
		let "k=$j+$NJOBS"

		if [ "$k" -ge "$NUMOFLINES" ];
		then
			let "k=$NUMOFLINES"
			let "NJOBS=$k-$j+1"
		fi

		START=$j
		let "FINISH=$k"
		let "DIFF=$NJOBS+1"
		
		if [ "$k" -eq "$NUMOFLINES" ];
		then
			let "DIFF=$NJOBS-1"
		fi

		###### PREPARING MERGERTREE FILE
		MERGERTREE=`printf "mergertree_%s_%03d_%03d" ${SIM_NAME[$i]} $START $FINISH`
		rm $MERGERTREE
		echo $DIFF > $MERGERTREE
		echo " " >> $MERGERTREE 

		for ((k=$START; k<=$FINISH; k++))	
		do
			echo ${FILE_LIST[$k]} >> $MERGERTREE
		done
		
		echo " " >> $MERGERTREE 

		for ((k=$START; k<=$FINISH; k++))	
		do
			PREFIX=${FILE_LIST[$k]::(-10)}
			echo $PREFIX >> $MERGERTREE
		done
		################################

		JOBSUB=`printf "jobsub_mergertree_%s_%03d_%03d.sh" ${SIM_NAME[$i]} $START $FINISH`

		printf "#!/bin/bash -l \n" > $JOBSUB
		printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
		printf "#SBATCH --time=24:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
		printf "#SBATCH --ntasks=1 --cpus-per-task=$NCPU \n" >> $JOBSUB
		printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
		printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
		printf "#======START===== \n" >> $JOBSUB
		printf "srun $AHF/$AHF_VERSION/bin/MergerTree < $MERGERTREE \n" >> $JOBSUB 
		printf "#=====END==== \n" >> $JOBSUB
		sbatch $JOBSUB 
	done
	
done

