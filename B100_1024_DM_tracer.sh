#!/bin/bash
module load hydra
halo_path=/bulk1/feldmann/data/MassiveFIRE2/analysis/AHF/HR/B100_1024_DM/halo
sim_dir=/bulk1/feldmann/data/MassiveFIRE2/analysis/AHF/HR/B100_1024_DM
redshift=(8.0) #(0.0 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0)
#start=0
#finish=1

MEMORY_PER_JOB=50

for i in ${redshift[@]}
do
	NJOBS=2
	let "MEMORY=$NJOBS*$MEMORY_PER_JOB"
	JOBSUB=`printf "jobsub_B100_1024_DM_z_%s.sh" $i`

	printf "#!/bin/bash -l \n" > $JOBSUB
	printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
	printf "#SBATCH --time=24:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
	printf "#SBATCH --ntasks=1 --cpus-per-task=$NJOBS \n" >> $JOBSUB
	printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
	printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
	printf "#======START===== \n" >> $JOBSUB
	printf "srun python B100_1024_DM_tracer.py -H $halo_path -P $sim_dir -z $i \n" >> $JOBSUB 
	printf "#=====END==== \n" >> $JOBSUB
	sbatch $JOBSUB 
done


