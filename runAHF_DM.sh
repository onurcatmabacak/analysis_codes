#!/bin/bash
# FB15N128_DM
# FB15N128_DM_EH 
# FB15N256_DM     
# FB15N256_DM_EH  
# FB15N512_DM
# FB15N512_DM_A 
# FB15N1024_DM  
# FB15N2048_DM 

SIM_NAME=(B100_1024_DM)
SIM_SUITE=(MassiveFIRE2)
NUMBER_LIST=(567)
AHF_VERSION=AHF-v1.0-094
AHF_FOLDER=AHF-v1.0-094_DM

LIST=(0)

for i in ${LIST[@]}
do

	if [ "${SIM_SUITE[$i]}" -eq "MassiveFIRE" ] 
	then
		ROOT=/net/cephfs/shares
	else
		ROOT=/bulk1
	fi

	AHF=/bulk1/feldmann/Onur/ahf/$AHF_FOLDER/bin/$AHF_VERSION
	SOURCE_DIR=$ROOT/feldmann/data/${SIM_SUITE[$i]}/production_runs/${SIM_NAME[$i]}
	SIMULATION=$ROOT/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}/halo
	CODE_DIR=/bulk1/feldmann/Onur/manipulate_hdf5_original

    for j in ${NUMBER_LIST[@]}
    do    
        SNAPSHOT=`printf "%03d" $j`
		INDIR=$SOURCE_DIR/snapdir_$SNAPSHOT
		OUTDIR=$SIMULATION/$SNAPSHOT

        PARAMETERS=$OUTDIR/AHF.input
        DVIR=-1 # .. virial parameter, >0 delta_crit or delta_mean. 
        RHOVIR=0 # 0 or 1

        printf "[AHF]\n" > $PARAMETERS
	    printf "ic_filename = $OUTDIR/snapshot_converted_$SNAPSHOT.\n" >> $PARAMETERS #
    	printf "ic_filetype = 61\n" >> $PARAMETERS
        printf "outfile_prefix = $OUTDIR/${SIM_NAME[$i]}\n" >> $PARAMETERS #
        printf "LgridDomain = 512\n"  >> $PARAMETERS
        printf "LgridMax = 134217728\n" >> $PARAMETERS # 2^30=1073741824
        printf "NperDomCell = 50.0\n" >> $PARAMETERS
        printf "NperRefCell = 50.0\n" >> $PARAMETERS
        printf "VescTune = 1.5\n" >> $PARAMETERS
        printf "NminPerHalo = 100\n" >> $PARAMETERS
        printf "RhoVir = $RHOVIR\n"  >> $PARAMETERS # 0.. Delta relative to crit, 1.. Delta relative to mean
        printf "Dvir = $DVIR\n" >> $PARAMETERS
        printf "MaxGatherRad = 3.0\n" >> $PARAMETERS # normally it is 3.0
        printf "LevelDomainDecomp = 6\n" >> $PARAMETERS
        printf "NcpuReading = 1\n" >> $PARAMETERS
        printf "\n[GADGET]\n" >> $PARAMETERS
        printf "GADGET_LUNIT = 0.001\n" >> $PARAMETERS # in Mpc/h
        printf "GADGET_MUNIT = 1e10" >>  $PARAMETERS # in Msun/h

		python $CODE_DIR/HDF5converter2.py $INDIR $OUTDIR
        $AHF $PARAMETERS &> $OUTDIR/ahf.out
		rm $OUTDIR/*converted*
    done 
done
