#!/bin/bash
module load hydra
export OMP_NUM_THREADS=32

function create_dir {
	if [ ! -d "$1" ];
	then
		mkdir $1
	fi
}

#MassiveFIRE
#0  B100_N512_M3e13_TL00001_baryon_toz2_HR_9915
#1  B100_N512_M3e13_TL00003_baryon_toz2_HR_9915
#2  B100_N512_M3e13_TL00004_baryon_toz2_HR_9915
#3  B100_N512_TL00002_baryon_toz2
#4  B100_N512_TL00009_baryon_toz2_HR
#5  B100_N512_TL00013_baryon_toz2_HR
#6  B100_N512_TL00018_baryon_toz2_HR
#7  B100_N512_TL00029_baryon_toz2_HR
#8  B100_N512_TL00037_baryon_toz0_HR_9915
#9  B100_N512_TL00113_baryon_toz0_HR_9915
#10 B100_N512_TL00206_baryon_toz0_HR_9915
#11 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915
#12 B400_N512_z6_TL00000_baryon_toz6_HR_9915
#13 B400_N512_z6_TL00001_baryon_toz6_HR_9915
#14 B400_N512_z6_TL00002_baryon_toz6_HR_9915
#15 B400_N512_z6_TL00005_baryon_toz6_HR_9915
#16 B400_N512_z6_TL00006_baryon_toz6_HR_9915
#17 B400_N512_z6_TL00013_baryon_toz6_HR_9915
#18 B400_N512_z6_TL00017_baryon_toz6_HR_9915
#19 B400_N512_z6_TL00021_baryon_toz6_HR_9915
#20 B762_N1024_z6_TL00000_baryon_toz6_HR
#21 B762_N1024_z6_TL00001_baryon_toz6_HR
#22 B762_N1024_z6_TL00002_baryon_toz6_HR

#MassiveFIRE2
#23 B762_N1024_z6_TL00000_baryon_toz6_HR 
#24 B762_N1024_z6_TL00001_baryon_toz6_HR 
#25 B762_N1024_z6_TL00002_baryon_toz6_HR
#26 h113_HR_sn1dy300ro100ss
#27 h206_HR_sn1dy300ro100ss
#28 h29_HR_sn1dy300ro100ss
#29 h2_HR_sn1dy300ro100ss

SIM_NAME=(B100_N512_M3e13_TL00001_baryon_toz2_HR_9915 B100_N512_M3e13_TL00003_baryon_toz2_HR_9915 B100_N512_M3e13_TL00004_baryon_toz2_HR_9915 B100_N512_TL00002_baryon_toz2 B100_N512_TL00009_baryon_toz2_HR B100_N512_TL00013_baryon_toz2_HR B100_N512_TL00018_baryon_toz2_HR B100_N512_TL00029_baryon_toz2_HR B100_N512_TL00037_baryon_toz0_HR_9915 B100_N512_TL00113_baryon_toz0_HR_9915 B100_N512_TL00206_baryon_toz0_HR_9915 B400_N512_M1e14_TL00010_baryon_toz6_HR_9915 B400_N512_z6_TL00000_baryon_toz6_HR_9915 B400_N512_z6_TL00001_baryon_toz6_HR_9915 B400_N512_z6_TL00002_baryon_toz6_HR_9915 B400_N512_z6_TL00005_baryon_toz6_HR_9915 B400_N512_z6_TL00006_baryon_toz6_HR_9915 B400_N512_z6_TL00013_baryon_toz6_HR_9915 B400_N512_z6_TL00017_baryon_toz6_HR_9915 B400_N512_z6_TL00021_baryon_toz6_HR_9915 B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR B762_N1024_z6_TL00000_baryon_toz6_HR B762_N1024_z6_TL00001_baryon_toz6_HR B762_N1024_z6_TL00002_baryon_toz6_HR h113_HR_sn1dy300ro100ss h206_HR_sn1dy300ro100ss h29_HR_sn1dy300ro100ss h2_HR_sn1dy300ro100ss)
SIM_SUITE=(MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2 MassiveFIRE2)
DUMMY=(16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 16 178 178 178 11 11 11 11)

HALO=halo
OUTPUT=output
MEMORY_PER_JOB=128
LIST=(0 1 5)

for i in ${LIST[@]}
do
	FILE_PATH=./${SIM_SUITE[$i]}/${SIM_NAME[$i]}/halo_0.txt
	NUMOFLINES=$(wc -l < $FILE_PATH)

	create_dir /bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}
	create_dir /bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}/$OUTPUT
	create_dir /bulk1/feldmann/data/${SIM_SUITE[$i]}/analysis/AHF/HR/${SIM_NAME[$i]}/$OUTPUT/halo_0

	NJOBS=12
	let "MEMORY=$NJOBS*$MEMORY_PER_JOB"

	START=0
	let "FINISH=$NUMOFLINES"
	JOBSUB=`printf "jobsub_extract_%s_%03d_%03d.sh" ${SIM_NAME[$i]} $START $FINISH`

	printf "#!/bin/bash -l \n" > $JOBSUB
	printf "#SBATCH --job-name=\"onur\" \n" >> $JOBSUB
	printf "#SBATCH --time=24:0:0 --mem=%sG \n" $MEMORY >> $JOBSUB
	printf "#SBATCH --ntasks=1 --cpus-per-task=$NJOBS \n" >> $JOBSUB
	printf "#SBATCH --output=%%j.o \n" >> $JOBSUB
	printf "#SBATCH --error=%%j.e \n" >> $JOBSUB
	printf "#======START===== \n" >> $JOBSUB
	printf "srun python extract_data.py ${SIM_NAME[$i]} ${SIM_SUITE[$i]} ${DUMMY[$i]} $HALO $OUTPUT $START $FINISH $NJOBS \n" >> $JOBSUB 
	printf "#=====END==== \n" >> $JOBSUB
	sbatch $JOBSUB 
done
